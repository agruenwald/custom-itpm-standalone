# CDL Flex :: Custom ITPM :: Standalone 
Provides two modules: The EngSb-independent Web User Interface and the
synchronization component as a leightweight-alternative to the 
Automated EngSb.

## Execution

### All in One: 2 Minutes to go
There are several ways to run the application. The most convenient one is to
download the pre-built [ITPM Standalone(https://bitbucket.org/agruenwald/custom-itpm-builds)
jar-file and run it on any file system. When executing the jar-file a Web-Server will
start up and the synchronization job is available as a background thread.

### Unix Shell Scripts (deprecated)
An alternate way is to start the Web-UI and the synchronization process as standalone versions.
On UNIX you may want to use the startup script in the $SEMBASE/standalone_scripts.
Execute

     ./itpm.sh
     
from the command line:
  + ./itpm start will start the two components as background processes.
  + ./itpm reset-data will load default data
  + use ./itpm stop to kill the background processes again  (use ./itpm show or ps -f if that fails).

### Engineering Service Bus including Workflows
To use the full power of the OpenEngSb Workflow system you need to unpack the created zip-file in the target directory
and start the Engineering Service Bus. The Web application needs to be started as a separate process.  

## Further Information:

More documentation can be found at Confluence under [Custom ITPM](http://cdl-ind.ifs.tuwien.ac.at/confluence/display/UC/Custom+ITPM 
"Custom ITPM").