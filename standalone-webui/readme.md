# CDL Flex :: Custom ITPM :: Standalone :: WebUI
The standalone version of the Web interface for the ITPM project. The standalone version is
provided because it allows more freedom for testing etc. than a version integrated into the
EngSb. It is based on Spring annotations hence it can be transformed into the EngSb
environment very easily. Also, the structure is designed to make a transformation into
the OpenEngSb environment as painless as possible.

## Execution
Start with either mvn jetty:run or mvn jetty:run -Djetty.port={portNumber}
You can pass the path to the configuration files (sembase directory) with 
'''mvn jetty:run -Dsembase.home=/absolute/path/to/sembase/''' 
(e.g. mvn jetty:run -Dsembase.home=/Users/andreas_gruenwald/git/custom-itpm-builds/builts). If no such
parameter is passed the directory in which the executable resides is assumed to be
the place where the configuration data resides.

## Further Information:

More documentation can be found at Confluence under [Custom ITPM](http://cdl-ind.ifs.tuwien.ac.at/confluence/display/UC/Custom+ITPM 
"Custom ITPM").

Get the [ITPM Builts](https://bitbucket.org/agruenwald/custom-itpm-builds "ITPM Builts"), download them
and start the app with just one comment!