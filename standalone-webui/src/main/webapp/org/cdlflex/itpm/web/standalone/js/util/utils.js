// Array Remove - By John Resig (MIT Licensed)
Array.prototype.remove = function(from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
};

/* check if event is already bound to an element 
$.fn.isBound = function(type, fn) {
    var data = null;
    if (this.data('events') != null) {
    	data = this.data('events')[type];
    } else {
    	data = jQuery._data(this[0], 'events')[type]; //jQuery 1.8
    }
    
    if (data === undefined || data.length === 0) {
        return false;
    }

    return (-1 !== $.inArray(fn, data));
};
*/


$('body').delegate('[contenteditable=true]','click',function(){
    $(this).parents('[draggable=true]')
        .attr('data-draggableDisabled',1)
        .removeAttr('draggable');
    $(this).blur(function(){
        $(this).parents('[data-draggableDisabled="1"]')
            .attr('draggable','true')
            .removeAttr('data-draggableDisabled');
    });
});

function mark (x,y,color, diameter) {
    if(color==null) {
        color='red';
    }
    if(diameter == null) {
    	diameter=10;
    }
    $('body').append('<span style="position:absolute;z-index:10000;width:'+diameter+'px;height:'+diameter+'px;background-color:'+color+';top:'+x+';left:'+y+'"></span>');

}

/* draw a static line from (x,y) to (x1,y2) */
function drawHelperLine (originTop, originLeft, toTop, toLeft, optColor) {
	if(optColor == null) {
		optColor='red';
	}
	
	var randId = Math.floor((Math.random()*1000)+1);
	$('#draw').append('<div id="'+randId+'" class="association_line helper_line line_fresh"></div>');
	var lineObj = $('#'+randId);
	var length = Math.sqrt((toLeft - originLeft) * (toLeft - originLeft) 
		+ (toTop - originTop) * (toTop - originTop));

	var angle = 180 / 3.1415 * Math.acos((toTop - originTop) / length);
	if(toLeft > originLeft) {
		angle *= -1;
	}

	$(lineObj)
		.css('height', length)
		.css('width', '2px')
		.css('z-index', 10000)
		.css('-webkit-transform', 'rotate(' + angle + 'deg)')
		.css('-moz-transform', 'rotate(' + angle + 'deg)')
		.css('-o-transform', 'rotate(' + angle + 'deg)')
		.css('-ms-transform', 'rotate(' + angle + 'deg)')
		.css('transform', 'rotate(' + angle + 'deg)')
		.css('background-color', optColor);
	
	$(lineObj).css('top', originTop);
	$(lineObj).css('left', originLeft);
	
	return $(lineObj);

}
    
function generateId(prefix) {
    return prefix + Math.ceil(Math.random() * 100000); // + Math.round(new Date().getTime() / 1000);
}

function twoDigits(str) {
	if(str.toString().length < 2) {
		str = '0' + str.toString();
	}
	return str;
}

function printableDate(date) {
	if(date == null) {
		return '';
	}
	
	date = new Date(date); //parse Strings
	
	
	var d = date.getFullYear() + "-" 
		+ twoDigits(date.getMonth()) 
		+ "-" + twoDigits(date.getDate())
		+ ' ' 
		+ twoDigits(date.getHours()) 
		+ ':' 
		+ twoDigits(date.getMinutes());
	return d;
}

function stripSpecialChars(text) {
	text = text.replace(/\s/g, ''); //whitespaces such as single spaces cause problems too, e.g. avoid id = '234234324-Library Update'
    text = text.replace(/\./g, '');  //points in ids cause problems
    text = text.replace(/\_/g, ''); //remove underscore and try to improve results
    return text;
}