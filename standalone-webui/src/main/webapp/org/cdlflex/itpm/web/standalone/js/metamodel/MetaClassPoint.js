/**
* @author Andreas Gruenwald <a.gruenw@gmail.com>
* An abstract class that contains default properties for classes, attributes, associations
    * and other meta model objects.
    */
var MetaClassPoint = Class.extend({
    init:function(metaClass) {
    	this.class="org.cdlflex.mdd.umltuowl.metamodel.MetaClassPoint"; //necessary for serializing the meta model;

        //this.metaClass=metaClass; do not save to avoid circularities
        if(metaClass==null) {
            this.metaClassId="";
        } else {
            this.metaClassId=metaClass.id; //for JSON serialization according to umlTUowl
        }
        this.range=new Array();
    },

    /**
     * Sets the range of an association.
     * @param min an single value or blank or "*" or a vector of elements separated by colon, e.g. 1,2,3,4.
     * @param max same as parameter min or end of the range, e.g. 10 or * or even blank.
     * Usually higher than min.
     */
    setRangeSmart:function(min,max) {
        this.range = new MetaUtil().normalizeRange(min,max);
    },

    setRange:function(range) {
        this.range=range;
    },

    /**
    * @return a sorted list of all range values where blank is lowest and "*" is highest.
    * All other values (in between) are numeric. List contains at least one element if filled
    * using {@code setRange()}.
    */
    getRange:function() {
        return this.range;
    },

    /* a displayable string instead of a sorted array */
    getRangeNormalized:function() {
        var res = this.range.join(',');
        if(res=='*') {
            res='';
        }
        return res;
    },

    /**
    * @return name of the metaclass (=range) the class point belongs to.
    */
    getMetaClass:function() {
        return this.metaClass;
    }
});
