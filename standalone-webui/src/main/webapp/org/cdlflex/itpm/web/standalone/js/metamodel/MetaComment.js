/**
 * This class handles commments, respectively annotations, which
 * are either created manually, or automated to store information
 * during the transformation processes. In the simplest case a 
 * comment is just a String. In more advanced cases, this String
 * contains specific additional information about the type of the
 * comment. However, every annotation/comment can be transformed
 * into a single String easily.
 * @author andreas_gruenwald
 *
 */

/** default, plain comment **/
var MetaComment_TYPE_COMMENT          = "";
/** used to store internal transformation and positioning info, which is mainly useful for machines, not for human */
var MetaComment_TYPE_INTERNAL		  = "@internal";
/** might be used to save additional model constraints, e.g. a regex for an attribute or a more complicated constraint for
 * a class, or a package.
*/
var MetaComment_TYPE_CONSTRAINT  = "@constraint";

var MetaComment = Class.extend({
    init:function(text, initDate) {
        this.class="org.cdlflex.mdd.umltuowl.metamodel.MetaComment"; //necessary for serializing the meta model;

        this.text=text;
        this.type=MetaComment_TYPE_COMMENT;
        this.creatorName ='';
        this.createdAt = null;
        if(initDate) {
        	this.createdAt = new Date();
        } else {
        	this.createdAt = null;	
        }
        
    }
});