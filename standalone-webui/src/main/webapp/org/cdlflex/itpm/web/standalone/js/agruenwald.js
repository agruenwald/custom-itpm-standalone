/**
 * Some default functionality which is handy for each page, such as table
   highlighting on click, a fancy deletion button with auto-disabled state,
   the handling of search fields including icons, etc. 
**/
var selectedEntries = new Array();

$.tableManager = function() {
	var selectedEntries = new Array();
	
	this.defaultOptions = {
		keyFieldClassName: '.deleteId',
		tableClassName: '.table',
		serializedField: '#serializedIds',
		disabled: false,
		toggleAllPlaceholder: '[Checkbox-Placeholder]'
	}
	
	/** 
	 * Serializes the selected entries (ids) and creates
	 * a String with entries separated by "," 
	 */
	
	this.serialize = function() {
		var result="";
		for(var i=0; i < selectedEntries.length; i++) {
			if(i>0) {
				result+=",";
			}
			result+=selectedEntries[i].val() == '' ? selectedEntries[i].text() : selectedEntries[i].val();
		}
		return result;
	},
	
	/** returns the number of selected rows in the table **/
	
	this.getSelectedItems = function() {
		return selectedEntries.length;
	},
	
	/**
	 * select/deselect a row (jquery tag), based on 
	 * whether selectEntry is true or false.
	 */
	this.selectItem = function(row,selectEntry) {
		var cb = $(row).find(':checkbox');
		if(!selectEntry || this.defaultOptions.disabled) {
			this.defaultOptions.disabled=false; //only once (?)
			$(row).find('td').removeClass('highlighted');
			cb.attr('checked', false);
			//click deactivates the checkbox and triggers event which is recognized by Wicket (AjaxCheckBox)
			//currently recursive cb.trigger('click');
			
			for(var i=0; i<selectedEntries.length; i++) {
		        if (selectedEntries[i].text() == $(row).find(this.defaultOptions.keyFieldClassName).text()) {
		        	selectedEntries.splice(i,1);
		        	break;
		        }
		    }
		} else {
			cb.attr('checked', true);
			//click selects the checkbox and triggers event which is recognized by Wicket (AjaxCheckBox)
			if(!$(row).find('td.highlighted').length>0) {
				$(row).find('td').addClass('highlighted');
				var recIdElem = $(row).find(this.defaultOptions.keyFieldClassName);
				selectedEntries.push(recIdElem);
			}
		}
		
		if(selectedEntries.length<=0) {
			this.disableDeleteButton(true);
		} else {
			this.disableDeleteButton(false);
		}
		this.updateSerialized();
	},
	
	this.disableDeleteButton = function(disable) {
		if(disable) {
			$('#deleteButton').attr('disabled','disabled');
			$('#deleteButton').removeAttr('data-toggle');
		} else { 
			$('#deleteButton').removeAttr('disabled','');
			$('#deleteButton').attr('data-toggle','modal');
		}
	}
	
	this.selectAll = function() {
		var rows = $(this.defaultOptions.tableClassName).find('tbody').find('tr');
		for(var j=0; j<rows.length; j++) {
			this.selectItem(rows[j],true);
		}
	},
	
	this.toggleAll = function(button) {
		if(button.is(':checked')) {
			this.selectAll();
		} else {
			this.resetItems();
		}
	}
	
	this.resetItems = function() {
		$('.wicket_orderUp a span i').remove();	
		$('.wicket_orderDown a span	i').remove();
		$('.wicket_orderUp a span').append('<i class="glyphicon glyphicon-arrow-down pull-right" style="font-size:inherit;"/>');
		$('.wicket_orderDown a span	').append('<i class="glyphicon glyphicon-arrow-up pull-right" style="font-size:inherit;" />');
		//$('.wicket_orderNone a').append('<i class="glyphicon glyphicon-user"></i>');
		var rows = $(this.defaultOptions.tableClassName).find('tbody').find('tr');
		for(var j=0; j<rows.length; j++) {
			this.selectItem(rows[j],false);
		}
		this.selectedEntries = new Array();
		this.updateSerialized();
		this.disableDeleteButton(true);
		/*
		$(this.defaultOptions.tableClassName).find('td').removeClass('highlighted');
		$(this.defaultOptions.tableClassName).find(':checkbox').attr('checked',false);
		this.disableDeleteButton(true);
		this.updateSerialized();
		*/
	}
	
	this.updateSerialized = function() {
		$(this.defaultOptions.serializedField).val(this.serialize());
		//set all other hidden fields to because Wicket generates some auto-fields
		//$('#serializedIds').closest("form").find('input[type=hidden]').val(this.serialize());
	},
	
	this.setDisabled = function(isDisabled) {
		this.defaultOptions.disabled=isDisabled;
	}
};

var tableManager = null;
$(document).ready(function() {
	tableManager = new $.tableManager();
	if($(tableManager.defaultOptions.keyFieldClassName).length==0) {
		//no id, try different identifier (e.g. for projects):
	
	}
	tableManager.resetItems();

	$('body').on('click', tableManager.defaultOptions.tableClassName + ' tbody tr', function() {
	//$(tableManager.defaultOptions.tableClassName).find('tbody').find('tr').bind('click', function() {
		var cb = $(this).find(':checkbox');
		if($(this).find('td').hasClass('highlighted')) {
			tableManager.selectItem($(this),false);
		} else {
			tableManager.selectItem($(this),true);
		}
	});
	
	//replace placeholder by toggle-all
	$(tableManager.defaultOptions.tableClassName).find('thead th').find(":contains('" + tableManager.defaultOptions.toggleAllPlaceholder 
		+"')").each(function() {
		var elem = $(this).html('<input type="checkbox" id="resetAll" onclick="tableManager.toggleAll($(this));" class="resetAll"/></th>');
	});
	

	$('body').on('click', '.edit_label, table .glyphicon, table a', function(event) {
	//$('.edit_label, table .glyphicon, table a').bind('click',function(event) {
		//event.stopPropagation();
		//event.preventDefault();
		var tr = $(this).parent().parent().parent().parent();
		tableManager.selectItem(tr,false); //don't select item when editing
		tableManager.setDisabled(true);
	});

	$('#deleteButton').click(function() {
		$('.no_deletions').text(tableManager.getSelectedItems());
	});

	$('#finallyDelete').click(function() {
		//tableManager.resetItems();
	});

	/* Search Field */
	//icon-remove-circle

	$('.icon-search').on('click',function() {
		 $('#searchForm').submit();
	});
	
	$('#searchForm').on('keypress',function(e) {
		if(e.which == 13) {
			$('#searchForm').submit();
		}
	});
});

$(document).ready(function() {
	if($('.search-query').val() !='') {
		$('#searchForm').append('<span class="click-icon glyphicon glyphicon-remove-circle" title="Remove search term" onclick="$(\'.search-query\').val(\'\');$(\'#searchForm\').submit();$(this).replaceWith(\'\');"></span>')
		//$('#searchForm').find('.icon-search').after...
	}
	
	/* Generally set focus on first input field if exactly one form */
	if($('form').length==1) {
		$('input[type=text]').first().focus();
	} else {
		$('input[type=text]').first().focus();
	}
	
	/* Bootstrap popover extension: popover close on click outside */
	$(':not(#anything)').on('click', function (e) {
		var popoverIcon = $(e.target).parent(); //span class id="...")
		var popoverIconId = popoverIcon.attr('id');
   	 	$('.popover').each(function () {
   	 		var popoverParentIcon = $(this).parent();
   	 		var popoverParentIconId = popoverParentIcon.find('span').attr('id');
   	 		if ((popoverParentIconId != popoverIconId) && $(this).is(':visible')) {
   	 			$('#'+popoverParentIconId).popover('hide');//hide();
   	 		}
    	});
	});
	
	/* Ajax-capable handling of Bootstrap popover elements which are heavily used
	   to add help and information tags to fields and table entries.
	*/
	$('body').on('click',  '.bs-popover', function() {
		var icon = $(this);
		if (!icon.hasClass('popover-init-done')) {
			var iconId = icon.attr('id');
			var iconContent = $('#' + iconId + '-content');
			var spans = iconContent.find('span');
			var title = spans[0];
			var content = spans[1];
			$(icon).popover({title: title, content: content, html:true});
			$(icon).addClass('popover-init-done');
			$(icon).popover('show');
		}
	});
	
	/* move pagination of Wicket default tables to footer */
	$('table').each(function(i,elem) {
		var nav = $(this).find('.navigation');
		if (nav.length != 0) {
			if($(this).closest('.show-no-navigation').length > 0) {
				return;
			}
			var footer = $(this);
			$(this).append('<tfoot></tfoot>');
			$(this).find('tfoot').append(nav);
			$(nav).show();
		}
	});
	
	$(document).ajaxComplete(function() {
		rotateAnimate();
	});
	
	rotateAnimate();
	function rotateAnimate() {
		$('.streaming-status-icon .glyphicon-repeat:not(.visited)').each(function(i,elem) {
		$(elem).addClass('visited');
		var gDegree = 0;
		setInterval(function(){
    		/* Some long block of code... */
    		gDegree = (gDegree > 360) ? (gDegree - 360 + 10) : (gDegree + 10);
    		// For webkit browsers: e.g. Chrome
           	$(elem).css({ WebkitTransform: 'rotate(' + gDegree + 'deg)'});
      		// For Mozilla browser: e.g. Firefox
           	$(elem).css({ '-moz-transform': 'rotate(' + gDegree + 'deg)'});
		}, 90);
		});
	}
});



