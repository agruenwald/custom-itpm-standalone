/**
 * Get the appropriate layout for a graph
**/
var FlexBoxFactory = Class.extend({
    init: function() {
    	this.MAX_LEAF_NUMBER_PER_LEVEL = 10; //switch to other layout
    	this.lastLayout = null;
    },
     
    getLayout: function(graph, layout) {
    	this.lastLayout = null;
    	if (layout == null) {
    		console.error('Layout type in getLayout() is null.');
    	} else if (layout == 'TreeLayout') {
    		this.lastLayout = new TreeLayout(graph);
    	} else if (layout == 'VLayout') {
    		this.lastLayout = new VLayout(graph);
    	} else if (layout == 'StairLayout') {
    		this.lastLayout = new StairLayout(graph);
    	} else if (layout == 'SeqLayout') {
    		this.lastLayout = new SeqLayout(graph);
    	} else {
    		console.error('Unknown FlexBox layout: ' + layout);
    	}
    	return this.lastLayout;
    },
    
    connect: function(flexGraph, fromId, toId) {
    	flexGraph.addUnrenderedLine(fromId, toId, '', null, 'flexline');
    	//improve performance
    	//flexGraph.placeConnectedLine(fromId, toId, '', null, 'flexline');
    },
    
    _layout: function() {
    	this.intelligentLayouting(this.lastLayout);
    },
     
    /* intelligent layouting */
    intelligentLayouting: function(layout) {
    	if (layout.getMaximumLeafNo() > this.MAX_LEAF_NUMBER_PER_LEVEL) {
    		layout = new BreakDownLayout(layout.graph);
    		this.lastLayout = layout;
    		layout.layout();
    	} else {
    		this.lastLayout = layout;
    		layout.layout();
    	}
    }
});

