/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * Represents an attribute of a class. An attribute consists of
 * a name, a datatype (=range; even, if range might be "UNKNOWN"),
 * a visibility status, and some optional information.
 */
var MetaAttribute_VOID          = "void";
var MetaAttribute_DATETIME	    = "datetime";
var MetaAttribute_BOOLEAN 	    = "boolean";
var MetaAttribute_TIMESTAMP     = "timestamp";
var MetaAttribute_TIME  	    = "time";

var MetaAttribute_PUBLIC 		= "public";
var MetaAttribute_PROTECTED 	= "protected";
var MetaAttribute_PRIVATE 	    = "private";
var MetaAttribute_PACKAGE 	    = "package";

var MetaAttribute = MetaElement.extend({
    init:function(name) {
        this._super();
        this.class="org.cdlflex.mdd.umltuowl.metamodel.MetaAttribute"; //necessary for serializing the meta model;

        this.name=name;
        this.visibility=MetaAttribute_PUBLIC;
        this.range=MetaAttribute_VOID;
        this.rangeIsEnum=false;
        this.multMin=1;
        this.multMax=1;
    },
    
    newFlatInstance: function (jattr) {
    	this.name = jattr.name;
    	this.visibility = jattr.visibility;
    	this.range = jattr.range;
    	this.rangeIsEnum = jattr.rangeIsEnum;
    	this.multMin=jattr.multMin;
        this.multMax=jattr.multMax;
    },

    setRange:function(range) {
        if(range.toLowerCase()=="bool" || range.toLowerCase()=="boolean") {
            range = MetaAttribute_BOOLEAN;
        }
        this.range = range;
    },
    
    getName: function() {
    	return this.name;
    },

    parseAttributeText:function(text) {
        var al = text.trim();
        var aName = '';
        var aVisibility='public';
        var aRange = 'void';
        var aMultMin = 1;
        var aMultMax = 1;
        
        if(al.indexOf(':')>0) {
            var alx = al.split(':');
            aRange=alx[1].trim();
            al=alx[0].trim();
        } else {
            al = al.replace(':','');
        }
        if(al.length>0){
            var first = al.substr(0,1);
            var rest = al.substr(1,al.length);
            var xVis='';
            if(first=='+') {
                xVis='public';
            } else if (first=='~'){
                xVis='protected';
            } else if(first=='-') {
                xVis='private';
            } else if (first=='#') {
                xVis='package';
            }
            if(xVis!='') {
                aVisibility=xVis;
                aName=rest;
            } else {
                aName=al;
            }
        }
        
        if(aRange.indexOf("[") >= 0) {
        	var parts = aRange.split("[");
        	if(parts.length == 2) {
        		aRange = parts[0].trim();
        		var l = parts[1].trim().length;
        		if (parts[1].trim()[l-1] == "]") {
        			var multInfo = parts[1].trim().substring(0,l-1).trim();
        			if (multInfo.length > 0 && multInfo.split("..").length == 2) {
        				var ranges = multInfo.split("..")
        				aMultMin = ranges[0].trim();
        				aMultMax = ranges[1].trim();
        				if (aMultMax == '*') {
        					aMultMax = -1; //infinite
        				}
        				if (aMultMin < 0) {
        					aMultMin = 1;
        				}
        				if ((aMultMax != -1) && aMultMax < aMultMin) {
        					aMultMax = aMultMin;
        				}
        			} 
        		}
        	}
        }
        

        this.name=aName;
        this.range=aRange;
        this.visibility=aVisibility;
        this.multMin=parseInt(aMultMin);
        this.multMax=parseInt(aMultMax);
        return this;
    },
    
    _isMulti: function() {
    	if (this.multMin == 1 && this.multMax == 1) {
    		return false;
    	} else {
    		return true;
    	}
    },

    toString:function() {
        var  v="?";
        if(this.visibility == (MetaAttribute_PRIVATE)) {
            v="-";
        } else if (this.visibility == (MetaAttribute_PUBLIC)) {
            v="+";
        } else if (this.visibility == (MetaAttribute_PROTECTED)) {
            v="#";
        } else if (this.visibility == (MetaAttribute_PACKAGE)) {
            v="~";
        }
        if (!this._isMulti()) {
        	return v+this.name+":"+this.range+"\n";
        } else {
        	return v+this.name+":"+this.range+" [" + this.multMin + ".." + (this.multMax == -1 ? "*" : this.multMax) +"]" + "\n";
        }
    }
});