//autocomplete fields

$.hierarchies = function() {
	/** handle collaborator auto menue **/
	this.initAutocomplete = function(rowNumber) {
		$('#row'+rowNumber).find('select').ajaxChosen({
			type: 'GET',
			url: callbackUrl,
			dataType: 'json'
		}, function (data) {
			var terms = {};
				
			$.each(data, function (i, val) {
				terms[val.key] = val.value;
			});	
			return terms;
		});
	},
	
	/** Initializes several click and indicator icons (for one row) and
	 *  registers a change event, so that whenever the content
	 *  of the fields in one row is changed, the appropriate icon is
	 *  displayed (also the reserve add icon is handled).
	**/
	this.initializeOnChangeIcons = function(rowNumber, action) {
		var icon = $('#row'+rowNumber).find('.icon-'+action);
		$(icon).hide();		
		
		//necessary to trigger change events in first row (when changing project manager)
		$('#row'+rowNumber+' select').first().append('<option value=""></option>');
		$('#row'+rowNumber+' select').trigger('liszt:updated')
		 
		$('#row'+rowNumber).on('change.validator', '.chzn-done', function() {		
			var row = $(this).closest('.communication-row');
			var id = $(row).attr('id').replace('row','');						
			hierarchyManager.showIndicator('row'+id);			
			var superiorPlaceholder=$('.communication-row'+id).find('.superior').attr('data-placeholder');
			var supVal = $('#row'+rowNumber).find('.chzn-done').val();
			if(supVal != superiorPlaceholder && supVal != '') {	
				$(icon).show();
				$('.icon-reserve-add').unbind();
				$('.icon-reserve-add').remove();				
				$('#row'+rowNumber).find('input').last().focus();
			}			
		});
	},
	
	/**
	 * Changes the icon from "add" to "edit" and adds a new row.
	 */
	this.handleAdd = function(rowIdPure) {
		var rowId = '#row'+rowIdPure;		
		//old row
		$(rowId).find('.icon-add').unbind();
		$(rowId).find('.icon-add').remove();		
		$(rowId).find('.icon-edit').hide();
		hierarchyManager.initializeOnChangeIcons(rowIdPure,'edit');
		hierarchyManager.initializeIconAjaxRequest(rowIdPure,'edit');		
		//add new row
		this.addNewRow(rowIdPure);
	},
	
	/**
	 * handles the layout of the rows after clicking on edit and
	 * transmitting everything to the server.
	 */
	this.handleEdit = function(rowIdPure) {
		var rowId = '#row'+rowIdPure;
		$(rowId).find('.icon-edit').hide();
		var rows = $('.communication-row:not(#row-template)');
		if(rows.length >= 1) { //at least template + project manager 
			if($(rows[rows.length-1]).attr('id')==('row'+rowIdPure)) { //last row has been edited - no more rows afterwards
				this.addNewRow(rowIdPure);
			}
		}
	}
	
	/**
	 * Handles the representation of rows after clicking on "remove". I.e. the
	 * events are deregistered, the row is removed and furthermore it is ensured
	 * that still one row for adding new entries is available.
	 */
	this.handleRemove = function(rowIdPure) {
		var rowId = '#row'+rowIdPure;
		$(rowId).unbind();
		$(rowId).remove();	
		
		if($('.communication-row:not(#row-template)').find('.icon-add').length==0 
		&& $('.communication-row:not(#row-template)').find('.icon-reserve-add').length==0) {
			var helpIcon = $('.communication-row-template').find('.icon-add').clone();
			helpIcon.removeClass('icon-add');
			helpIcon.removeClass('icon-ok-sign');
			helpIcon.addClass('icon-plus-sign');
			helpIcon.addClass('icon-reserve-add');	
			var r = $('.communication-row:not(#row-template)').last();
			r.find('.iconbar').append(helpIcon); 
			var lastId = r.attr('id').replace('row','');
			hierarchyManager.initializeOnChangeIcons(lastId, 'reserve-add');
			hierarchyManager.initializeIconAjaxRequest(lastId,'reserve-add');
			$('.icon-reserve-add').show();
		}
	},
	
	/**
	 * Adds a new row to the table and intializes it.
	 */
	this.addNewRow = function(rowIdPure) {
		var rowId = '#row'+rowIdPure;
		var rowTemplate = $('<div>').append($('#row-template').clone()).html();						
		rowIdPure=Number(rowIdPure)+1;
		rowTemplate = rowTemplate.replace(/-template/g, rowIdPure);
		$(rowTemplate).insertAfter(rowId);
		hierarchyManager.initAutocomplete(rowIdPure);		
		hierarchyManager.initializeOnChangeIcons(rowIdPure,'add');		
		hierarchyManager.initializeIconAjaxRequest(rowIdPure,'add');
		hierarchyManager.initializeIconAjaxRequest(rowIdPure,'remove');
		$('.communication-row'+rowIdPure).css('display','');
		$('.icon-reserve-add').unbind();
		$('.icon-reserve-add').remove();
		hierarchyManager.enableKeypress();
		
	},
	
	this.enableKeypress = function(rowNumber) {
		/* disabled because handling difficult
		$(".chzn-select chzn-done").find('input').keydown(function(e){
			 if (e.which == 13) {
				 alert('ok!');
			 }
		});
		
		//focus on first select field
		$('.chzn-single').focus(function(e){
		    e.preventDefault();
		});
		$('.chzn-single').first().focus()
		*/
		
		/*
		$('#row'+rowNumber).find('input').keypress(function (e) {
			  alert('ok!');
			  if (e.which == 13) {
				  hierarchyManager.doHierarchyAjaxRequest($(this), rowNumber, 'add');
			  }
		});*/
		
	}
	
	this.initializeIconAjaxRequest = function(rowNumber,action) {
		//$('#row+rowNumber').off('click.validator',".icon-"+action).off();
		$('#row'+rowNumber).on('click.validator', ".icon-"+action, function(data) {
			//dirty bugfix - reserve-add handler seems to fire twice
			if(action=='reserve-add') {
				if($('#row'+rowNumber).find('.icon-reserve-add').length == 0) {
					return;
				}
			}
			hierarchyManager.doHierarchyAjaxRequest($(this), rowNumber, action);
		});
	},
	
	this.resetFeedback = function(rowNumber,action) {
		hierarchyManager.showFeedback("");
		$('#row'+rowNumber).find('.indicator').hide();
	}
	
	this.startCommunication = function(rowNumber, action) {
		
	},
	
	this.showIndicator = function(rowId) {
		$(rowId).find('.indicator').show();
	},
	
	this.findCollaboratorIds = function(rowIdPure, namesAsArray) {
		var collaboratorIds = "";
		var rowId = ".communication-row"+rowIdPure;
		var colls = $(rowId).find('.search-choice').find('span'); //content of multiple text field - without text() it is a li list.
		if(namesAsArray) {
			return colls;
		}
		$(rowId).find('#collaboratorIds'+rowIdPure+' option').each(function() {
			var id = $(this).val();
			var label = $(this).text();
			var found=false;
			for(var c=0; c<colls.length; c++) {
				if($(colls[c]).text()==label) {
					collaboratorIds += "&collaboratorId[]="+id;
				}
			}				
		});
		return collaboratorIds;
	},
	
	this.findSuperiorId = function(rowIdPure, returnName) {
		var rowId = ".communication-row"+rowIdPure;
		var superior=$(rowId).find('.chzn-single').text(); //content of field left (single input)		
		var superiorPlaceholder=$(rowId).find('.superior').attr('data-placeholder');		
		if(superior==superiorPlaceholder || superior=='') {
			superior="";
		} else {
			if(returnName) {
				return superior;
			}
			$(rowId).find('select.superior option').each(function(opt) {
					if($(this).text()==superior) {
						superior=$(this).val();
					}
			});
		}
		return superior;
	},
	
	//element = an element within the row div
	this.doHierarchyAjaxRequest = function(element, rowNumber, action, forwardedMsg, forwardedStatus) {
		var row = element.closest('.communication-row');
		var rowIdPure = $(row).attr('id').replace('row','');
		var rowId = ".communication-row"+rowIdPure;		
		hierarchyManager.showIndicator(rowId);
		
		var collaboratorIds = hierarchyManager.findCollaboratorIds(rowIdPure);
		var oldSuperiorId = $('#oldSuperior'+rowIdPure).val();
		var superior = hierarchyManager.findSuperiorId(rowIdPure);
		if(((superior=='' && collaboratorIds == '') || oldSuperiorId == '') && action == 'remove') {
			//empty entry set - break;
			hierarchyManager.resetFeedback(rowIdPure);
			hierarchyManager.handleRemove(rowIdPure);
			return;
		} else if (action == 'reserve-add') {
			hierarchyManager.handleAdd(rowIdPure)
			hierarchyManager.resetFeedback(rowIdPure);
			return; 
		}
		
		var core=$('form').first().serialize(); //the core project information
		var data = collaboratorIds+"&superiorName="+superior+"&"+core+"&"+$(this).closest('form').serialize()
								  +"&projectId="+$('#id').val() + "&rowId="+rowIdPure
								  +"&oldSuperiorId="+$('#oldSuperior'+rowIdPure).val();
		hierarchyManager.showFeedback("");		
		var jsonAction = action;
		if(action=='reserve-add') {
			jsonAction = 'add';
		}
		
		$.ajax({
			  url: callbackUrl+"&action="+jsonAction,
			  data: data,
			  cache: false,
			  dataType: 'json',
			  async: false,
			  context: document.body
			}).done(function(data) {				
				var obj = data[0];
				var status 	= data[0]['status'];
				var msg    	= data[0]['msg'];
				var fieldId = data[0]['fieldId'];	
				var forceReload = data[0]['forceReload'];
				if(forceReload=="false") {
					forceReload=false;
				} else {
					forceReload=true;
				}
				
				var feedb = '<div class="feedbackPanel inline-block feedbackPanelParent alert alert-%status%" wicket:id="feedbackul">'
			  		 + '<button type="button" class="close" data-dismiss="alert">x</button>'
		    		 + '<div class="feedbackPanel %status%" wicket:id="messages">'
		      		 +	'<span class="feedbackPanel %status%" wicket:id="message">%msg%</span>'  			
		    		 + '</div></div>';
		    		feedb=feedb.replace(/%msg%/g,msg);
		    		
				if(status != 'INFO') {					
		    		feedb=feedb.replace(/%status%/g,'warn');
		    		hierarchyManager.showFeedback(feedb);
		    		if(forceReload && action != 'read') {
						return hierarchyManager.doHierarchyAjaxRequest(row, rowNumber, 'read', feedb ,'warn');
					}
				}  else {
					if(action=='add' || action=='reserve-add') {
						hierarchyManager.handleAdd(rowIdPure);
						if(forceReload || true) {
							return hierarchyManager.doHierarchyAjaxRequest(row, rowNumber, 'read', feedb, 'success');
						}
					} else if (action == 'remove') {
						hierarchyManager.handleRemove(rowIdPure);
						if(forceReload || true) {
							return hierarchyManager.doHierarchyAjaxRequest(row, rowNumber, 'read', feedb,  'success');
						}
					} else if (action == 'edit') {
						hierarchyManager.handleEdit(rowIdPure);
						if(forceReload || true) {
							return hierarchyManager.doHierarchyAjaxRequest(row, rowNumber, 'read', feedb,  'success');
						}
					} else if (action == 'read') {						
						hierarchyManager.reloadData(data[1]);
					}
					
					if(action=='read' && forwardedMsg == null) {
						hierarchyManager.resetFeedback(rowIdPure, "read");
					} else if (forwardedMsg != null) {
						forwardedMsg=forwardedMsg.replace(/%status%/g,forwardedStatus);	
						hierarchyManager.showFeedback(forwardedMsg);
					}
					else {
						feedb=feedb.replace(/%status%/g,'success');	
						hierarchyManager.showFeedback(feedb);						
					}
				}				
					
				hierarchyManager.createGraph();
				$(rowId).find('.indicator').hide();
			});
	},
	
	this.reloadData = function(data) {
		var iGlobal = 0;
		
		var oldArea = $('.communication-row:not(#row-template):not(#row1)');
		$(oldArea).unbind();
		$(oldArea).remove();
		
		//$('#row1').unbind();		
		$('#row1').off('.validator');
		
		$('#row1 select option').remove();
		$('#row1 .search-choice').remove();
		
		$(data).each(function(i) {
			var o = $(data[i]);		
			var id   = o.attr('id');
			var name = o.attr('name');
			var isPM = o.attr('isPM');
			var isStakeholder = o.attr('isStakeholder');
			var collaborators = o.attr('collaborators');
			
			i++; iGlobal++;
			if(i!=1 && isPM != 'true') {
				//hierarchyManager.addNewRow(i-1);								
			} 							
			hierarchyManager.handleAdd(i);
			
			$('#row'+i+' select').first().append('<option selected="selected" value="'+id+'">'+name+'</option>');
			$('#oldSuperior'+i).val(id);
			$('#row'+i+' select').trigger('liszt:updated');													
			$(collaborators).each(function(j) {
				var cId = $(collaborators[j]).attr('colId');
				var cName = $(collaborators[j]).attr('colName');
				$($('#row'+i+' select')[1]).append('<option selected="selected" value="'+cId+'">'+cName+'</option>');
				$('#row'+i+' select').trigger('liszt:updated');
			});
			i--;							
		});		
	}
	
	this.loadHierarchy = function() {
		hierarchyManager.doHierarchyAjaxRequest($('#row1'),1,'read');
	},
	
	this.showFeedback = function(feedb) {
		$('.feedbackpanelrow').html(feedb);
	}
	
	this.createGraph = function() {
		var comGraph = new $.hierarchyGraph();
		$('.communication-row:not(.row-template)').each(function(index,element){
			var rowIdPure = $(element).attr('id').replace('row','');
			var colls = hierarchyManager.findCollaboratorIds(rowIdPure,true);
			var superior = hierarchyManager.findSuperiorId(rowIdPure,true);
			if(superior != '') {
				comGraph.addCollaborator(superior, rowIdPure==1,false);
			}	
			$(colls).each(function(index,element) {
				comGraph.addCollaborator($(element).text(), false, false);
				comGraph.addManagesRelation(superior, $(element).text());
			});
		});
		
		var height = 400;
		if(($('.hierarchy-definition').height())>400) {
			height=$('.hierarchy-definition').height();
		}
		console.log(height);
		comGraph.drawGraph(760,height);
	}
	
};

var hierarchyManager = new $.hierarchies();
$(document).ready(function () {
	$('.core-data').hide();
	hierarchyManager.initAutocomplete(1);
	hierarchyManager.loadHierarchy();
	hierarchyManager.initializeOnChangeIcons(1, 'add');
	hierarchyManager.initializeIconAjaxRequest(1,'add');
	hierarchyManager.enableKeypress(1);
});
	