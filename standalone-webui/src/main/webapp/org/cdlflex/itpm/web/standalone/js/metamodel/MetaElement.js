/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * An abstract class that contains default properties for classes, attributes, associations
 * and other meta model objects.
 */
var MetaElement = Class.extend({
    init:function() {
        this.class="org.cdlflex.mdd.umltuowl.metamodel.MetaElement"; //necessary for serializing the meta model;

        this.comment=Array();
    },

    getComment:function() {
        return this.comment;
    },

    addComment: function(comment, override) {
    	if(override) {
    		for(var i=0; i < this.comment.length; i++) {
    			var c = this.comment[i];
    			if(c.text == comment.text) {
    				this.comment.remove(i);
    				i--;
    			}
    		}
    	}
        this.comment.push(comment);
    },

    setComment: function(comments) {
        this.comment=comments;
    },

    getName: function() {
    	console.error('getName()  not implemented.');
        return 'ABSTRACT CLASS';
    },
   
    /** Remove all comments which match the exact text which is passed in the argument **/
    removeCommentViaText: function(text) {
    	for(var i=0; i < this.comment.length; i++) {
    		var c = this.comment[i];
    		if(c.text == text) {
    			this.comment.remove(i);
    			return true;
    		}
    	}
    	return false;
    },

   /** add layout information as a (usual) comment */
   addLayoutComment: function(annotationName,commentText, creatorName) {
	   var mcom = new MetaComment();
	   mcom.type = MetaComment_TYPE_INTERNAL;
	   mcom.text = '@'+annotationName + ':' + commentText.toString().trim();
	   mcom.creatorName = creatorName;
	   mcom.createdAt = new Date();
	   this.removeCommentsStartingWith('@'+annotationName + ':');
       this.addComment(mcom,true);
   },
   
   /**
    * remove all comments who start with the string str. the comment
    * type is optional and can be used to only remove certain 
    * comment types (use the variables from MetaComment therefor).
    */
   removeCommentsStartingWith: function(str, commentType) {
	   	var total = 0;
	   	for(var i=0; i < this.comment.length; i++) {
   			var c = this.comment[i];
   			if(commentType == null || (commentType == c.type)) {
   				if(c.text.indexOf(str) == 0) {
   					this.comment.remove(i);
   					i--;
   					total++;
   				}
   			}
   		}
   		return total;
   },

   filterAllLayoutComments: function() {
        var res = new Array();
        for(var i=0; i<this.comment.length;i++) {
           var c = this.comment[i];
           if(this.isLayoutComment(c)) {
                res.push(c);
           }
       }
       return res;
   },

    findLayoutCommentByName: function(annotationName) {
        var lc = this.filterAllLayoutComments();
        for(var i=0; i<lc.length;i++) {
            var c = lc[i];
            var x = c.text.split(':');
            if(x[0].length>0) {
                if(x[0].substring(1)==annotationName) {
                    return x[1];
                }
            }
        }
        return "";
    },

   isLayoutComment: function(c) {
	   if(c.text.indexOf('@') == 0) {
		   return true;
	   }
	   return false;
   	},
   	
   	getInternalComment: function(annotationName) {
   		var status = this.findLayoutCommentByName(annotationName);
   		return status;
   	},
   	
   	getStatusComment: function(pkg) {
   		var status = this.findLayoutCommentByName('status');
   		if (status == 'suggestion' 
   		|| status == 'local'
   		|| status == 'local_available'
   		|| status == 'info') {
   			return 'status-' + status;
   		} else {
   			if(pkg != null) {
   				return pkg.getStatusComment();
   			}
   		}
   	}

});
