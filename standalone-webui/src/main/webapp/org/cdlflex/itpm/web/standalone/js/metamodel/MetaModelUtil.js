/**
 * Transforms two values (min and max) of association ranges into a list of string values.
 * Input examples are (min; max): (,*); (1,1); (0,1); (1,2,3,4,5;1,2,3,4,5); (;)
 * Allowed values are empty, * (=max) an numeric ranges.
 * @param minValue the minimum value of a range, e.g. blank, 1 or a vector like 1,2,3 or 1..2
 * @param maxValue the maximum value of a range, e.g. blank, *, 0, 1, *.
 * @throws TransformationException
 * @return an ordered list that contains the range. Contains only blank, "*" or numeric values.
 */
var MetaUtil = Class.extend({
    normalizeRange:function(minValue,maxValue) {
        var range = new Array();
        if(minValue == maxValue) {
            if(minValue == '' || minValue == null) {
                minValue="*";
                maxValue="*";
            }
           minValue=minValue.replace(/\(|\)/g,''); //replace brackets
           var values = minValue.split(",");
            for (var i = 0; i < values.length; i++) {
                var v = values[i].trim();
                //bugfix var rangeSplit = v.split("\\.\\.");
                var rangeSplit = v.split('..');
                if(rangeSplit.length == 2 && rangeSplit[1] != "*") {
                    var start = parseInt(rangeSplit[0].trim());
                    var end   = parseInt(rangeSplit[1].trim());
                    for(var j = start; j <= end; j++) {
                        range.push(j);
                    }
                } else if (rangeSplit.length == 2 && rangeSplit[1]==("*")) {
                    range.push(rangeSplit[0].trim());
                    range.push(rangeSplit[1].trim());
                } else if (v==("*")) {
                    range.push(v);
                } else {
                    if (v==("")) {
                        range.push(v);
                    } else {
                        parseInt(v); 
                        range.push(v);
                    }
                }
            }
        } else {
            range.push(minValue);
            range.push(maxValue);
        }
        range = range.sort();
        return range;
    }
});