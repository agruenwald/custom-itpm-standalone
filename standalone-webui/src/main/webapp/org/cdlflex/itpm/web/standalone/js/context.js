/* 
 * Context.js
 * Copyright Jacob Kelley
 * MIT License
 * extended by: Andreas Gruenwald.
 * This JS script is used in the Semantic Meta Editor to show a context menu on right-click.
 */

var context = context || (function () {
    
	var options = {
		fadeSpeed: 100,
		filter: function ($obj) {
			// Modify $obj, Do not return
		},
		above: 'auto',
		preventDoubleContext: true,
		compress: false
	};
	
	var contextParentElem = null;
	var eventHist = new Array();

	function initialize(opts) {
		
		options = $.extend({}, options, opts);
		
		$(document).on('click', 'html', function () {
			$('.dropdown-submenu, .dropdown-context-sub').removeClass('open'); //reset all
			$('.dropdown-context').fadeOut(options.fadeSpeed, function(){
			});
		});
		if(options.preventDoubleContext){
			$(document).on('contextmenu', '.dropdown-context', function (e) {
				e.preventDefault();
			});
		}
		$(document).on('mouseenter', '.dropdown-submenu', function(){
			var $sub = $(this).find('.dropdown-context-sub:first'),
				subWidth = $sub.width(),
				subLeft = $sub.offset().left,
				collision = (subWidth+subLeft) > window.innerWidth;
			if(collision){
				$sub.addClass('drop-left');
			}
		});
		
	}
	
	function getOrigin() {
		if(this.contextParentElem == null) {
			console.error('Context origin elem is null');
		}
		return this.contextParentElem;
	}

	function updateOptions(opts){
		options = $.extend({}, options, opts);
	}

	function buildMenu(data, id, subMenu) {
		var subClass = (subMenu) ? ' dropdown-context-sub' : '',
			compressed = options.compress ? ' compressed-context' : '',
			$menu = $('<ul class="dropdown-menu dropdown-context' + subClass + compressed+'" id="dropdown-' + id + '"></ul>');
        var i = 0, linkTarget = '';
        for(i; i<data.length; i++) {
        	if (typeof data[i].divider !== 'undefined') {
				$menu.append('<li class="divider"></li>');
			} else if (typeof data[i].header !== 'undefined') {
				$menu.append('<li class="nav-header">' + data[i].header + '</li>');
			} else {
				if (typeof data[i].href == 'undefined') {
					data[i].href = '#';
				}
				if (typeof data[i].target !== 'undefined') {
					linkTarget = ' target="'+data[i].target+'"';
				}
				if (typeof data[i].subMenu !== 'undefined') {
					$sub = ('<li class="dropdown-submenu dropdown-submenu-header"><a tabindex="-1" class="dropdown-submenu-link" href="' + data[i].href 
						+ '">' + data[i].text 
						+ ('<i style="float:right; margin-right: -13px" class="dropdown-forward-icon glyphicon glyphicon-chevron-right"></i>')
						+ '</a>' 
						+ '</li>');
				} else {
					$sub = $('<li><a tabindex="-1" href="' + data[i].href + '"'+linkTarget+'>' + data[i].text + '</a></li>');
				}
				if (typeof data[i].action !== 'undefined') {
					var actiond = new Date(),
						actionID = 'event-' + actiond.getTime() * Math.floor(Math.random()*100000),
						eventAction = data[i].action;
					$sub.find('a').attr('id', actionID);
					if (typeof data[i].icon !== 'undefined') {
						$sub.find('a').addClass('context-menu-icon');
						$sub.find('a').css('padding-left' , '0px');
						$sub.find('a').html('<i class="context-menu-icon glyphicon glyphicon-'+data[i].icon+'" style="padding-left:10px; padding-right:5px"></i>' + $sub.find('a').html());
					}
					$('#' + actionID).addClass('context-event');
					$(document).on('click', '#' + actionID, eventAction);
				}
				$menu.append($sub);
				if (typeof data[i].subMenu != 'undefined') {
					var subMenuData = buildMenu(data[i].subMenu, id, true);
					$menu.find('li:last').append(subMenuData);
				}
			}
			if (typeof options.filter == 'function') {
				options.filter($menu.find('li:last'));
			}
		}
		return $menu;
	}

	function addContext(selector, data) {
		
		var d = new Date(),
			id = d.getTime(),
			$menu = buildMenu(data, id);
			
		$('body').append($menu);
		$('.dropdown-submenu').removeClass('open'); //reset all
		
		$(document).on('contextmenu', selector, function (e) {
			e.preventDefault();
			
			//stop second event, if for instance canvas, umlclass are the events
			//stop also third event, for instance attribute -> class -> canvas
			if (context.eventHistory != null) {
				var clazzes = context.eventHistory.split(' ');
				for (var i=0; i < clazzes.length; i++) {
					var c = clazzes[i].trim();
					if ($(this).find('.'+c).length > 0) {
						//parent element - skip it.
						//context.eventHistory  = null;
						context.eventHistory = $(this).attr('class');
						console.log('skipped event addContext()', $(this));
						return;
					}
				}
			}
			context.eventHistory = $(this).attr('class');
			
			//e.stopPropagation();
			context.contextParentElem = $(this);
			$('.dropdown-context:not(.dropdown-context-sub)').hide();
			
			$dd = $('#dropdown-' + id);
			if (typeof options.above == 'boolean' && options.above) {
				$dd.addClass('dropdown-context-up').css({
					top: e.pageY - 20 - $('#dropdown-' + id).height(),
					left: e.pageX - 13
				}).fadeIn(options.fadeSpeed);
			} else if (typeof options.above == 'string' && options.above == 'auto') {
				$dd.removeClass('dropdown-context-up');
				var autoH = $dd.height() + 12;
				if ((e.pageY + autoH) > $('html').height()) {
					$dd.addClass('dropdown-context-up').css({
						top: e.pageY - 20 - autoH,
						left: e.pageX - 13
					}).fadeIn(options.fadeSpeed);
				} else {
					$dd.css({
						top: e.pageY + 10,
						left: e.pageX - 13
					}).fadeIn(options.fadeSpeed);
				}
			}
			
			$('ul.dropdown-menu .dropdown-submenu-header a').on('click', function(event) {
				console.log('classes:' + $(this).css('class'));
				// Avoid following the href location when clicking
				event.preventDefault(); 
				// Avoid having the menu to close when clicking
				
				//find parent element
				var upperParent = $(this).closest('li');
				
				//An item clicked which is not a header icon
				if (!$(this).hasClass('dropdown-submenu-link')) {
					$('.dropdown-submenu .dropdown-context-sub').removeClass('open'); //global reset
					//event.stopPropagation(); 
					return;
				}
				
				
				var upperParentResult = upperParent;
				while (upperParent.length > 0 ) {
					upperParentResult = upperParent;
					upperParent = $(upperParent).parent().closest('.dropdown-submenu');
					//upperParent = $(this).closest('.dropdown-submenu .dropdown-context-sub');
					if (upperParent.html() == upperParentResult.html()) {
						break;
					}
				}
				var t = $(this);
				$.each($('.dropdown-submenu .dropdown-context-sub'), function(i, e) {
					var found = $(upperParentResult).find($(this));
					if (found.length <= 0) {
						$(this).removeClass('open');
					} else {
						
					}
				});
				
				//$(this).find('.dropdown-submenu .dropdown-context-sub');
				
				//$(this).find('.dropdown-submenu .dropdown-context-sub').show();
				//$(this).find('.dropdown-context-sub').first().show();
				
				// If a menu is already open we close it
				//$('.dropdown-submenu .dropdown-context-sub').parent().removeClass('open');
				//$('.dropdown-submenu .dropdown-context-sub').removeClass('open');
				// opening the one you clicked on
				//$(this).parent().addClass('open');
				//var submenu = $(this).find('.dropdown-context-sub').first();
				var submenu = $(this).closest('li').find('.dropdown-context-sub').first();
				submenu.addClass('open');
				
				var menu = $(this).parent().find("ul").first();
				var parentMenu = $(submenu).parent().parent().first();
				var menupos = $(menu).offset();

				if (menupos.left + menu.width() > $(window).width()) {
					var newpos = -$(menu).width();
					menu.css({ left: newpos });    
				} else {
					var newpos = $(this).parent().width();
					menu.css({ left: newpos });
				}	
				
				var viewHeight = window.innerHeight;
				var menuViewPos = $(parentMenu).offset().top + $(parentMenu).height() - window.pageYOffset;
				var menuBottom = menuViewPos + $(menu).height();
				
				
				console.info(menuBottom + " vs " + viewHeight);
				if (menuBottom >= viewHeight) {
					var topOffset = window.pageYOffset; //top offset
					$(submenu).css('top', $(this).position().top + 7 - $(submenu).height()); //position=relative within menu - place menu above select
				} else {
					$(submenu).css('top', $(this).position().top - 7); //position=relative within menu
				}
				event.stopPropagation(); 

			});
		});
	}
	
	function destroyContext(selector) {
		$(document).off('contextmenu', selector).off('click', '.context-event');
	}
	
	return {
		init: initialize,
		settings: updateOptions,
		attach: addContext,
		destroy: destroyContext,
		getOrigin: getOrigin
	};
})();