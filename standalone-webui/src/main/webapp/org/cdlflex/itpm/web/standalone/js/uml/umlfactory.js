/**
  * Extensions of the Graph factory to store, load and browse umlTUowl metamodels
  * (semantic meta editor).
**/
var UmlFactory =  GraphFactory.extend({

	init: function(parentId){
		this._super(parentId);
		this.metaModel = null;
		this.commentHandler = new CommentsHandler(this);
	},
	
	humanUndoExt:function() {
        this.humanUndo();
        this._refreshPackageSwitcherIcons(this.metaModel);
    },

    humanRedoExt:function() {
        this.humanRedo();
        this._refreshPackageSwitcherIcons(this.metaModel);
    },
	
	/**
	 * Copy a single package from an existing meta-model and give it a default
	 * name. Also, rename all id's to avoid conflicts.
	**/
	humanCopyPackage: function() {
		var factory = this;
		var mm = factory.metaModel;
		var i = factory._getSelectedPackageIndex(mm);
		var currentPackage = mm.packages[i];
		var copiedPackage = $.extend(true, {}, currentPackage);
		//add a prefix to all ids to avoid dupliate ids in different packages
		var genId = Math.ceil(Math.random() * 100000); //otherwise each package can be copied only once
		var json = JSON.stringify(copiedPackage).replace(/\"(id|associationId|metaClassId)\":\"([a-zA-Z0-9_\-]{1,500})\"/gm, "\"\$1\":\"cp" + genId + "-" + "\$2\"");
		copiedPackage = JSON.parse(json); 
		//var copiedPackage = $(currentPackage).clone();
		copiedPackage.name = 'Copy of ' + currentPackage.name;
		mm.packages.push(copiedPackage);
		factory._buildPackageSwitcher(mm);
		factory._setPackageSelectorLabel(copiedPackage.name);
		factory.selectedPackage=copiedPackage.name;
		factory.reloadModel(factory.metaModel);
	},

	/* handling of comment history popups (close them on right-click by default) */
	_canvasBuildingEvent: function (canvasCss) {
		var factory = this;
		
		//hack: to avoid unbinding when moving lines via jQuery we apply plain JS
		//event listening:
		var canvas = document.getElementById($(canvasCss).attr('id'));
		   // add onclick event 
			canvas.onmousedown = function(e) { 
				if( e.button == 2 ) { //2 for Right Click, 3 for mouse wheel click
					factory._closeOpenCommentHistories($(canvasCss));
					console.log('Pressed right mouse button on canvas.');
					return false; 
				} 
				return true; 
		   }
			
		//init event handling		
		factory.eventPatcher();
			
	},

    humanAddAttribute:function(boxId, event) {
        var box = this.getBoxById(boxId);
        var label = new Label("attr:String",'umlAttribute',new UmlAttributeHandler(box));
        label.renderLabel($('#'+boxId));
        this.aeH.registerEvent(new AddLabelEvent(label,box,event));
    },
    
    /** load a simplified version of all available models (list) **/
    loadModelNames: function(selectElem) {
    	var res = new Array();
    	$.ajax({
    		  dataType: "json",
    		  url: semanticDiagramsURL + '&action=get_modelnames',
    		  //data: data,
    		  async: false,
    		  success: function(data) {
    			  res = data;
    		  }, fail: function() {
    			  bootbox.alert('could not load metamodel names from ' + semanticDiagramsURL, function() {});
    			  return new Array();
    		  }
    		});
    	return res;
    },
    
    loadMetaModelFromServer: function(namespacePrefix, canvasId) {
    	var factory = this;
    	$.getJSON(semanticDiagramsURL + '&action=load&namespaceprefix='+namespacePrefix, function(data) {
    		mm=data;
    		factory.clearSpace();
    		factory.initCanvas(mm);
	        _semanticMetaEditorInitContextMenu(factory, canvasId);   
	    }).fail(function() { 
	    	bootbox.alert('could not load metamodel from ' + semanticDiagramsURL + ' - will load dummy model instead...');
		 });
    },
    
    _getEngsbIcon: function(engSbType, metaModel) {
    	var factory = this;
    	var modelIcon = '';
    	if (engSbType == null) {
    		engSbType = jQuery.extend(new MetaModel(), factory.metaModel).getInternalComment('openengsbType');
    	}
    	if (engSbType == 'domain') {
			modelIcon = '<span class="openengsbType"><i class="icon glyphicon glyphicon-transfer"></i></span>&nbsp;';  //.glyphicon-share
		} else if (engSbType == 'connector') {
			modelIcon = '<span class="openengsbType"><i class="icon glyphicon glyphicon-cloud"></i></span>&nbsp;'; //glyphicon-copyright-mark
		}
    	//jQuery.extend(new MetaModel(), factory.metaModel).getInternalComment('openengsbType');
    	return modelIcon;
    },
    
    _refreshPackageSwitcherIcons: function(mm) {
    	var factory = this;
    	var diagramSwitcher = $('#'+factory.getParentId()).find('.diagram-switcher');
    	var listElem = $(diagramSwitcher).find('li.metamodelname:contains(' + factory.metaModel.name + ')');
    	var actIcon = factory._getEngsbIcon(null, factory.metaModel);
    	var prevIcon = listElem.find('.openengsbType');
    	if (prevIcon.length == 0) {
    		listElem.html(actIcon + listElem.html());//.before(actIcon); 
    	} else {
    		listElem.find('.openengsbType').html(actIcon);
    	}
    },

    _buildPackageSwitcher:function(mm) {
        var factory=this;
        this.selectedPackage="";

        /* enable switching between diagrams - load menu */
        var diagramSwitcher = $('#'+factory.getParentId()).find('.diagram-switcher');
        diagramSwitcher.find('ul').find('li').remove(); //remove previous elements
        
        var modelNames = factory.loadModelNames();
    	$.each(modelNames, function(i, elem) {
    		var isActive = (elem.namespace == factory.metaModel.namespace) ? true : false;
    		var hiddenNamespacePrefix = '<span class="elemPrefix" style="display:none;">' + elem.namespacePrefix + '</span>';
    		var modelIcon = '';//'<span><i class="icon icon-tag"></i></span>';
    		var isActiveIcon = '&nbsp;<span><i class="icon glyphicon glyphicon-folder-open"></i></span>';
    		var modelIcon = factory._getEngsbIcon(elem.openengsbType, factory.metaModel);
    		
    		diagramSwitcher.find('ul.dropdown-menu').append('<li class="metamodelname">' + modelIcon + hiddenNamespacePrefix + elem.name + (isActive ? isActiveIcon : '') + '</li>');
    		
    		if (isActive) {
    			var packageLis = '';
	    		var diagramText = diagramSwitcher.find('.switcher-display').text().trim();
	            $.each(mm.packages, function(index,p) {
	                if(index==0) { //preselect first package
	                    factory.selectedPackage= p.name;
	                }
	                var name = '';
	                if(p.name == '') {
	                    name = 'Diagram #' + index+1;
	                } else {
	                    name = p.name;
	                }
	                packageLis += '<li>'+name+'</li>';
	            });
	            diagramSwitcher.find('ul.dropdown-menu').append('<ul>'+packageLis+'</ul>');
    		}
    	});
        factory._handlePackageChangeEvent();
    },

    _getSelectedPackageIndex:function(mm) {
        /* enable switching between diagrams - load menu */
        var o = this.selectedPackage;
        var chooseDiagram=0;
        var result=0;
        $.each(mm.packages, function(index,p) {
            if(p.name==o) {
                result=index;
                return index;
            } else {
                var s = o.split('Diagram # ');
                var cmp = o;
                if(s.length==2) {
                    cmp = s[1].trim()-1;
                }
                if(cmp== p.name) {
                    result=index;
                    return index;
                }
            }
        });
        return result;
    },

    _handlePackageChangeEvent:function() {
        /* Diagram switcher */
        var factory=this;
        var canvasId = factory.getParentId();
        
        $('#'+factory.getParentId()).find('.diagram-switcher').find('li').bind('click', function(e,x) {
        	var metaModelPrefix = $(this).find('.elemPrefix').html();
        	if(metaModelPrefix != null) {
        		bootbox.confirm('Switching the metamodel means reloading the metamodel without the chance to undo previous actions.' +
        				' Unsaved changes are lost.', function(result) {
        			  if(result) {
        				  factory.loadMetaModelFromServer(metaModelPrefix, canvasId);
        			  }
        		});
        		/*
        		if (confirm('Switching the metamodel means reloading the metamodel without the chance to undo previous actions.' +
        				' Unsaved changes are lost.')) {
        			factory.loadMetaModelFromServer(metaModelPrefix, canvasId);
        		}
        		*/
        	} else {
        		factory._setPackageSelectorLabel(factory.metaModel, $(this).html());
        		factory.selectedPackage=$(this).html();
        		factory.reloadModel(factory.metaModel);
        	}
        });
    },

    _setPackageSelectorLabel:function(metaModel, text) {
        $('#'+this.getParentId()).find('.diagram-switcher').find('.switcher-display').html(this._getEngsbIcon(null, metaModel) + metaModel.name + " - " + text+'<span class="caret"></span>'); //bootstrap specific caret
    },
    
    /**
     * reload a model into the canvas. The jsonObj must contain the parsed JSON metamodel.
     */
    reloadModel:function(jsonObj) {
        this.loadModel(jsonObj,true);
    },
    
    /** reload the model by accessing the model which is stored on the top of the undo-stack. 
     *  If the undo-stack is still empty, nothing will be reloaded. */
    reloadModelFromHist:function() {
    	var factory = this;
    	if (factory.aeH.history.length > 0) {
    		var mm = JSON.parse(factory.aeH.history[factory.aeH.history.length-1]._state);
    		factory.reloadModel(mm);
    	}
    },
    
    changeParentHeightDynamically: function(modelPackage) {
    	var modelSize = modelPackage.classes.length;
    	var currentHeight = this.getParent().height();
    	if (modelSize > 40) {
			currentHeight = currentHeight * modelSize/25;
		} else if (modelSize > 30) {
			currentHeight = currentHeight * 2;
		} else if (modelSize > 20) {
			currentHeight = currentHeight * 1.5;
		} else {
		}
		
		console.log("Model size adapted: " + modelSize + "currentHeight" + currentHeight);
		this.getParent().height(currentHeight);
    },
    
    changeParentHeight: function(newHeight) {
    	this.getParent().height(newHeight);
    },
    
    initCanvas: function(mm) {
        mm = this.createDefaultIfEmpty(mm);
        this.loadModel(mm);
    },
    
    createDefaultIfEmpty: function(mm) {
    	if(mm.packages.length == 0) {
        	mm.packages.push(new MetaPackage('Diagram #1'))
        } 
        return mm;
    },
    
    humanRemoveCurrentPackage: function() {
    	var factory = this;
    	var i = this._getSelectedPackageIndex(this.metaModel);
    	this.metaModel.packages.remove(i); //utils.js
    	factory.createDefaultIfEmpty(this.metaModel);
    	factory._buildPackageSwitcher(this.metaModel);
    	factory.reloadModel(this.metaModel);
    	factory.aeH.registerEvent(new RemoveDiagramEvent(factory.getParent(),'remove'));
    },
    
    humanRemoveCurrentMetaModel: function() {
    	var factory = this;
    	bootbox.confirm('Removing a meta model removes all packages as well and cannot be undone.', function(result) {
    		if(result) {
    			factory.removeCurrentMetaModel();
    		}
		});
    },
    
    removeCurrentMetaModel: function() {
    	var factory = this;
    	var mm = factory.metaModel;
    	$.ajax({
    		  dataType: "json",
    		  type: 'POST',
    		  url: semanticDiagramsURL + '&action=delete_metamodel',
    		  data: {mm: JSON.stringify(mm)},
    		  async: false,
    		  success: function(data) {
    			  if (data.status == 'NOTOK') {
    				  	bootbox.alert('Could not remove meta model.', function(){});
    					return;
    			  }
    			  
    			  mm = null;
    			  $.getJSON(semanticDiagramsURL + '&action=load', function(data) {
    				  mm=data;
    				  factory.clearSpace();
    			      factory.initCanvas(mm);
    			      //	_semanticMetaEditorInitContextMenu(umlFactory, canvasId); 
    			      //factory.clearSpace();
        			  //factory.initCanvas(mm);
        			  factory._buildPackageSwitcher(mm);
        			  factory.reloadModel(factory.metaModel);
        			  
    			  }).fail(function() { 
    				  bootbox.alert('could not load metamodel from ' + semanticDiagramsURL + ' - will load dummy model instead...', function() {});
    			  });
    			  
    			  /*
    			  factory.clearSpace();
    			  factory.initCanvas(mm);
    			  factory._buildPackageSwitcher(mm);
    			  factory.reloadModel(factory.metaModel);
    			  */
    		  }, fail: function() {
    			  bootbox.alert('could not update metamodel with URL' + semanticDiagramsURL, function() {});
    			  return new Array();
    		  }
    	});
		
    	
    	/*
    	var i = this._getSelectedPackageIndex(this.metaModel);
    	this.metaModel.packages.remove(i); //utils.js
    	factory.createDefaultIfEmpty(this.metaModel);
    	factory._buildPackageSwitcher(this.metaModel);
    	factory.reloadModel(this.metaModel);
    	factory.aeH.registerEvent(new RemoveDiagramEvent(factory.getParent(),'remove'));
    	*/
    },
    
    
    determineDiagramHeight: function() {
    	var DEFAULT_HEIGHT_BOX = 220;
    	var DEFAULT_WIDTH_BOX = 180;

    	var factory=this;
    	// create all boxes and labels (attributes and classes)
    	var maxH = 0;
    	$.each(factory.boxes, function(index,box) {
    		var cssBox = $('#' +box.getId());
    		var t = cssBox.position().top + cssBox.height();
    		maxH = Math.max(t, maxH);
    	});
    	return maxH;
    },
    
    
    autoAdaptDiagramHeight: function(alsoRemoveHeight) {
    	alsoRemoveHeight = alsoRemoveHeight == null ? false : true;
    	
    	var DEFAULT_HEIGHT_BOX = 220;
    	var CRITICAL_GAP = 25;
    	var maxH = this.determineDiagramHeight();
    	var currH = this.getParent().height();
    	
    	if(maxH >= (currH - CRITICAL_GAP)) {
    		this.changeParentHeight(maxH + DEFAULT_HEIGHT_BOX);
    	} else if (alsoRemoveHeight) {
    		if(currH > (maxH + CRITICAL_GAP)) {
    			this.changeParentHeight(Math.max(200, maxH + CRITICAL_GAP));
    		}
    	}
    },
    

    loadModel:function(jsonObj,isReload) {
    	var DEFAULT_HEIGHT_BOX = 220;
    	var DEFAULT_WIDTH_BOX = 180;
    	
        var factory=this;
        var commentHandler = this.commentHandler;
        factory.metaModel=jsonObj;
        var mm=jsonObj;
		
        if(!isReload) {
            factory._buildPackageSwitcher(mm); //only the first time!?
        } else {
            factory.clearSpace();
        }

        var pIndex =factory._getSelectedPackageIndex(mm);
        factory._setPackageSelectorLabel(mm, factory.selectedPackage);

		this.changeParentHeightDynamically(mm.packages[pIndex]);
		
		var dummyTop = 50; //margin top
		var dummyLeft = 0;
					
        //create all boxes and labels (attributes and classes)
        $.each(mm.packages[pIndex].classes, function(index,c) {
			var metaClass = new MetaElement(c.name);
            metaClass.comment= c.comment;
            metaClass.filterAllLayoutComments();
            var t = parseInt(metaClass.findLayoutCommentByName('top'));
            var l = parseInt(metaClass.findLayoutCommentByName('left'));
            
            var backupDummyTop = dummyTop;
            var backupDummyLeft = dummyLeft;
            if(isNaN(t)) {
                //t = Math.max(0,Math.ceil(Math.random() * h) - 100);//+factory.getParent()+factory.getParent().offset().top;
                t = dummyTop;
            }
            
            if(isNaN(l)) {
                //l = Math.max(0,Math.ceil(Math.random() * w)-80);//+factory.getParent()+factory.getParent().offset().left;
                l = dummyLeft;
                dummyLeft += DEFAULT_WIDTH_BOX;
                if (l >= (factory.getParent().width() - DEFAULT_WIDTH_BOX)) {
                	dummyLeft = 0;
                	dummyTop += DEFAULT_HEIGHT_BOX;
                }
            } 
            //dummyLeft = Math.max(dummyLeft, (l + DEFAULT_WIDTH_BOX));
            dummyTop = Math.max(dummyTop, (t));

            console.log(c.name + '(' + c.id + ')' ,t,l);
            if(c.name=='') {
                console.error('NO NAME of CLASS!!!!');
            } else if (factory.getBoxById(factory._getPrefixed(c.id)) != null) {
                console.error(c.name + ' already exists!!!!');
            }
            var umlC = null;
            if (c.enumClass) {  //Enum must be first!
            	if(c.superclasses.length <= 0)  { //only display enum class itself, not the values as subclasses.
            	//if(c.subclasses.length > 0) { //empty enum class might still be an enum class.
                	umlC = new UmlEnumClass(c.name,l,t,factory._getPrefixed(c.id),false, commentHandler.getClassStyle(c));
                } else {
                	//skip do not apply positioning increasing
                	dummyTop = backupDummyTop;
                	dummyLeft = backupDummyLeft;
                	return;
                }
            } else if (c.abstractClass) {
                umlC = new UmlAbstract(c.name,l,t,factory._getPrefixed(c.id),false, commentHandler.getClassStyle(c));
            } else if (c.interfaceClass) {
                umlC = new UmlInterface(c.name,l,t,factory._getPrefixed(c.id),false, commentHandler.getClassStyle(c));
            } else {
                umlC = new UmlClass(c.name,l,t,factory._getPrefixed(c.id),false, commentHandler.getClassStyle(c));
            }
            factory.register(umlC);
            //classMapping[umlC.getText()]=umlC.getId(); //remember id's (assume each text is unique)
            $.each(c.attributes, function(index,a) {
                var box = factory.getBoxById(umlC.getId());
                var mA = new MetaAttribute(a.name);
                mA.range= a.range;
                mA.visibility= a.visibility;
                mA.multMin = a.multMin;
                mA.multMax = a.multMax;
                mA.comment = a.comment;
                var label = new Label(
                		mA.toString(),
                		'umlAttribute',
                		new UmlAttributeHandler(box),
                		null,
                		commentHandler.getAttributeStyle(mA));
                label.renderLabel($('#'+umlC.getId()));
            });
            
            //change height of parent according to dummy height
            factory.changeParentHeight(dummyTop + DEFAULT_HEIGHT_BOX);
        });


        //all classes created, now connect them
        console.info('Render generalizations...');
        $.each(mm.packages[pIndex].classes, function(index,c) {
            $.each(c.superclasses, function(index,s) {
    	        //only for Enums
            	if(c.enumClass) {
             		var fromBox = factory.getBoxById(factory._getPrefixed(c.id));
             		var toBox   = factory.getBoxById(factory._getPrefixed(s.id));
             		//avoid touching the fromBox (non-existing). instead add relations as attributes
             		var mA = new MetaAttribute(c.name);
                	mA.range= "String";
                	mA.comment = c.comment;
                	mA.multMin = 1; //a.multMin;
                    mA.multMax = 1; //a.multMax;
                	var label = new Label(
                			mA.toString(),
                			'umlAttribute',
                			new UmlAttributeHandler(toBox),
                			null,
                			commentHandler.getAttributeStyle(mA));
                	label.renderLabel($('#'+factory._getPrefixed(s.id)));
            	} else {
                	var fromBox = factory.getBoxById(factory._getPrefixed(c.id));
                	var toBox   = factory.getBoxById(factory._getPrefixed(s.id));
                	factory.placeConnectedLine(fromBox, toBox, 'generalization');
                }
            });
            
            console.info('   Render associations...');
            $.each(c.associations, function(index,a) {
                var assoctype='';
                if(a.from.metaClassId == c.id) { 
                	if(a.inverseAssociation == null) {
                        //directed
                        if(a.composition) {
                            assoctype='composition_uni';
                        } else if (a.aggregation) {
                            assoctype='aggregation_uni';
                        } else {
                            assoctype='association_uni';
                        }
                    }  else {
                        if(a.composition) {
                            assoctype='composition';
                        } else if (a.aggregation) {
                            assoctype='aggregation';
                        }
                    }
                	
                	console.info("Render association: ", a.from.metaClassId, a.to.metaClassId);
                    var fromBox = factory.getBoxById(factory._getPrefixed(a.from.metaClassId));
                    var toBox   = factory.getBoxById(factory._getPrefixed(a.to.metaClassId));
                    
                    if(fromBox == null) {
                    	console.error('Invalid association: ' + a.id + '(' + a.name + '). ' +
                    				  'toBoxType = '+toBox.mainLabel.getText() + ',' +
                    				  'fromBoxId ' + a.from.metaClassId + ' not found ' +
                    				  'assoctype: ' + assoctype + 
                    				  ': You must fix this in the meta model!!!');
                    	return;
                    } else if (toBox == null) {
                    	console.error('Invalid association: ' + a.id + '(' + a.name + '). ' +
                    				  'fromBoxType = '+fromBox.mainLabel.getText() + ',' +
                    				  'toBoxId ' + a.to.metaClassId + ' not found ' +
                    				  'assoctype: ' + assoctype + 
                    				  ': You must fix this in the meta model!!!');
                    	return;
                    }
                    
                    var toClass = jQuery.extend(new MetaModel(), factory.metaModel).findClassById(a.to.metaClassId);
            		if(toClass.enumClass) {
            			//alert yoing yoing
            			//instead of adding a reference we add an attribute to the enum Class
            			var mA = new MetaAttribute(a.name);
                    	mA.range= toClass.name;
                    	mA.comment = toClass.comment;
                    	mA.multMin = 1;
                        mA.multMax = 1;
                    	var label = new Label(
                    			mA.toString(),
                    			'umlAttribute',
                    			new UmlAttributeHandler(fromBox),
                    			null,
                    			commentHandler.getAttributeStyle(mA));
                    	//label.renderLabel($('#'+factory._getPrefixed(fromBox.getId())));
                    	label.renderLabel($('#' + fromBox.getId()));
                    	return;
            		}
            
                    try {
                    	var noCruicalInverseInformation = true;
                    	if(a.inverseAssociation != null
                    	&& a.inverseAssociation.to.metaClassId == a.from.metaClassId) {
                    		//self-reference
                    		if(a.composition || a.aggregation) {
                    			//print
                    		} else if (!(a.inverseAssociation.composition || a.inverseAssociation.aggregation)){
                    			//the inverse reference is also not of any specific kind
                    			if($('#'+factory._getPrefixed(a.inverseAssociation.id)).length <= 0) {
                    				//first time this association is going to be printed - allow it
                    				
                    			} else {
                    				noCruicalInverseInformation = false;
                    				
                    			}
                    		} else {
                    			//do not print - the reference is also known by the to-class. this class has more information about the reference available
                    			noCruicalInverseInformation = false;
                    		}
                    	}
                    	
                    	if(noCruicalInverseInformation) {
                    		factory.placeConnectedLine(fromBox, toBox, assoctype, factory._getPrefixed(a.id));
                    	} else {
                    		//skip the rest
                    		return;
                    	}
                    } catch(e) {
                  		console.error(e + " - You should fix this (fromBox: " + fromBox.mainLabel.getText() + ", toBox: " + toBox.mainLabel.getText() + ", a.id=" + a.id);
                  	}
                  	
                    if(a.name == '') {
                        factory.changeLineLabel(factory._getPrefixed(a.id),''); 
                    } else {
                        factory.changeLineLabel(factory._getPrefixed(a.id),a.name);
                    }
                    var mchelperFrom = new MetaClassPoint();
                    mchelperFrom.setRange(a.from.range);
                    var mchelperTo = new MetaClassPoint();
                    mchelperTo.setRange(a.to.range);
                    factory.addLineLabelsEndPoints(factory._getPrefixed(a.id), mchelperFrom.getRangeNormalized(),  mchelperTo.getRangeNormalized());
                }  else {
                    //will be rendered when the linked class will be threaded
                    //console.log('obviously not the case!!!!');
                }

            });

        });

        if(!isReload) {
            factory.aeH.registerEvent(new InitDiagramEvent(factory.getParent(),'load'));
        }

    },

    humanSaveModel:function() {
        return this.saveModel(true);
    },

    serializeModel:function() {
        return this.saveModel(); 
    },

    saveModel:function(saveAtServer) {
        var factory=this;
        //take existing json model and extend it with the methods available in the JS objects
       	var mm = jQuery.extend(new MetaModel(""), factory.metaModel);
       
       	//creates a new packge when required
       	var activePackage = mm.managePackageByName(factory.selectedPackage);
        //clear current classes so that they can be filled again (also deletes comments)
       	var classesBackup = jQuery.extend(true, new MetaPackage(''), activePackage); //deep copy
       	activePackage.classes = new Array();

        $.each(factory.boxes,function(i,box) {
            var mc = new MetaClass(box.label.getText());
            mc.setId(factory._stripPrefix(box.getId()));
            mc.abstractClass=box.abstractClass;
            mc.interfaceClass=box.interfaceClass;
            mc.enumClass=box.enumClass;
            var prevClass = classesBackup.findClassById(mc.getId());
            if(prevClass != null) {
            	mc.comment = mc.comment.concat(prevClass.comment);
            }
            mc.addLayoutComment('top',  factory.getRelativeTop(box), null);
            mc.addLayoutComment('left', factory.getRelativeLeft(box), null);
            activePackage.classes.push(mc);
            //attributes as subclasses
            if(mc.enumClass) {
            	$.each(box.getSubLabels(), function(i,label) {
            		var a = new MetaAttribute(label.getText());
            		var sid = factory._stripPrefix(generateId(""));
            		a.parseAttributeText(label.getText());
            		var enumValue = new MetaClass(a.name);
            		enumValue.id=sid;
            		enumValue.enumClass=true;
            		enumValue.comment = a.comment;
            		try {
            			var prevVal = classesBackup.findClassByName(a.name);
            			if(prevVal != null) { //could it be that attribute did not exist previously.
            				enumValue.comment = enumValue.comment.concat(prevVal.comment);
            			} 
            		} catch (e) {
            			console.error('Enum subclass (derived from attribute): cannot find attribute with name "' + a.name + '" within class ' + enumValue.name + '(' + e + ').');
            		}
            		
            		enumValue.superclasses.push(new MetaClass('').newFlatInstance(mc));
            		mc.subclasses.push(new MetaClass('').newFlatInstance(enumValue));
            		activePackage.classes.push(enumValue);
            	});
            }
        });
        
        $.each(factory.boxes,function(i,box) {
            var mc = mm.findClassById(factory._stripPrefix(box.getId()));
            //Attributes
            $.each(box.getSubLabels(), function(i,label) {
            	if(!mc.enumClass) {
            		var a = new MetaAttribute(label.getText());
            		a.parseAttributeText(label.getText());
            		
            		
            		if(a.range.length > 0) {
            			//try to locate an Enum class; first always try current package, afterwards
            			//try to find enum class in other packages.
            			var enumClass = jQuery.extend(new MetaPackage(''), activePackage).findEnumClassByName(a.range);
            			if(enumClass == null) {
            				enumClass = mm.findEnumClassByName(a.range);
            			}
            			
            			if(enumClass != null) {
            				//enum link - do not add attribute
            				//instead, add an association to that class
            				var assocId = factory._stripPrefix(generateId(""));
                            var assoc = new MetaAssociation(assocId,assocId, a.name);
                            assoc.from=new MetaClassPoint(mc);
                            
                            var enumClassInActivePackage = jQuery.extend(new MetaPackage(''), activePackage).findClassById(enumClass.id);
                            if(enumClassInActivePackage != null) {
                            	enumClass = enumClassInActivePackage;
                            }
                            
                            assoc.to=new MetaClassPoint(enumClass); 
                            assoc.to.setRangeSmart("1","1");
                            assoc.inverseAssociation=null;
                            mc.associations.push(assoc);
                            
                            //comment handling
                            var fromClass = jQuery.extend(new MetaClass(''), classesBackup).findClassById( assoc.from.metaClassId);
                        	var toClass   = jQuery.extend(new MetaClass(''), classesBackup).findClassById( assoc.to.metaClassId);
                        	var isFound = false;
                        	for(var m=0; m<fromClass.associations.length; m++) {
                        		var oldAssoc = fromClass.associations[m];
                        		if (oldAssoc.name == a.name) {
                        			//a.name = issueType (attribute=issueType:IssueType) } && oldAssoc.toClass.metaClassId == toClass.id) {
                        			assoc.comment = a.comment.concat(oldAssoc.comment);
                        			isFound = true;
                        			break;
                        		}
                        	} 
                        	if (!isFound) {
                        		//error when loading data, but ok when inserting new data hence ignoring it.
                        		console.info('cannot link attribute "' + a.name + '" with enum association (range= ' + enumClass.name + ')');
                    		}
                        	
                            return;
            			} 
            		}
            		
            		try {
            			a.comment = a.comment.concat(jQuery.extend(new MetaClass(''), classesBackup.findClassById(mc.id)).findAttributeByName(a.name).comment);
            		} catch (e) {
            			//do not show because also occurs when creating new entries
            			//console.error('cannot find attribute with name "' + a.name + '" within class ' + mc.name + '(' + e + ').');
            			
            		}
            		mc.attributes.push(a);
            	} else {
            		//already done (pushing as subclasses) 
            		//done earlier to have the information present for the association handling
            	}
            });   
        });
        

        //Generalization and Associations (process after all classes have been added to the metamodel)
        $.each(factory.lines, function(i,line) {
            //console.log('Assoctype='+line.options.assoctype);
            var assoctype=line.options.assoctype;
            if(assoctype == 'generalization') {
                var subclass = mm.findClassById(factory._stripPrefix(line.fromId));  //class is subclass
                var superclass = mm.findClassById(factory._stripPrefix(line.toId));    //class is superclass
                subclass.superclasses.push(new MetaClass("").newFlatInstance(superclass)); //to avoid circularities
                superclass.subclasses.push(new MetaClass("").newFlatInstance(subclass)); //to avoid circularities
            } else {
                var fromClass = mm.findClassById(factory._stripPrefix(line.fromId));
                var toClass   = mm.findClassById(factory._stripPrefix(line.toId));
                var a = new MetaAssociation(factory._stripPrefix(line.getId()),factory._stripPrefix(line.getId()),line.mainLabel != null ? line.mainLabel.getText() : "");
                a.from=new MetaClassPoint(fromClass);
                a.to=new MetaClassPoint(toClass);
                
                //comments
                var prevAss = classesBackup.findAssociationById(a.id);
                if(prevAss.length > 0 && prevAss[0].comment.length > 0) {
                	a.comment = a.comment.concat(prevAss[0].comment);
                }
                
                //handle aggregations
                if(assoctype.indexOf('composition')==0) {
                    a.composition=true;
                } else if (assoctype.indexOf('aggregation') == 0) {
                    a.aggregation=true;
                }
                //endpoints
                if(line.endpointLabels['from']!=null) {
                    var fromLText = line.endpointLabels['from'].getText();
                    a.from.setRangeSmart(fromLText,fromLText);
                }
                if(line.endpointLabels['to']!=null) {
                    var toLText = line.endpointLabels['to'].getText();
                    a.to.setRangeSmart(toLText,toLText);
                }
                //handle unidirectional associations
                if(assoctype=='composition_uni' || assoctype=='aggregation_uni' || assoctype=='association_uni') {
                    a.inverseAssociation=null;
                } else {
                   //must add inverse but take care of creating a loop!
                	var lineIdInverse = generateId('');
                    var inverse = new MetaAssociation(lineIdInverse, lineIdInverse,
                    					line.mainLabel != null ? line.mainLabel.getText() : "");
                    inverse.from= a.to;
                    inverse.to= a.from;
                    a.setInverseAssociation(inverse); //sufficient??

                }
                fromClass.associations.push(a);
            }
        });

        var jsonModel = JSON.stringify(mm);
        
        if(saveAtServer) {
        	$.post(semanticDiagramsURL, { action: "save", mm: jsonModel } )
        		.done(
        			function(data) {
        				if (data.status=='NOTOK') {
        					bootbox.alert('<b>Could not save metamodel.</b> ' + (
        						(data.data == '' || data.data == null)
        						? ' Please see server logs for details.' : data.data));
        				}
        			}
        	);
        }
        
        return jsonModel;
    },


    _getPrefixName:function() {
        return this.getParentId() + "-";
    },

    _getPrefixed:function(text) {
        text=this.getParentId() + "-" + this._stripSpecialChars(text);

        return text;
    },

    _stripSpecialChars:function(text) {
    	return stripSpecialChars(text); //in utils.js
    },

    _stripPrefix:function(text) {
        var p = this._getPrefixName();
        if(text.indexOf(p)==0) {
            text = text.replace(p,"");
            return text;
        } else {
            return text;
        }
    },
    
    _closeOpenCommentHistories: function(parentCanvas) {
    	this.commentHandler._closeOpenCommentHistories(parentCanvas);
    },
    
    removeHighlights: function() {
    	this.getParent().children().removeClass('glow');
    },
    
    highlight: function(pattern) {
    	var box = this.getBoxByMainLabel(pattern);
    	$('#'+box.getId()).addClass('glow');
    },
    
    
    /* event interface: Handles custom events which can be passed from other interfaces.
     * Passes Wicket, because sever interaction is not necessary. This is a more lightweight
     * solution which reaches more performance via direct JS interaction.
     * 
     */
    eventPatcher: function() {
    	var factory = this;
    	
    	/* Interface format of data object:
    	 * var datObj = {
			  keywords: new Array('this', 'is', 'a', 'test2'),
			  format: format,
			  editorContent: editorContent,
			  cursorPosition: curPos,
			  token: token
	  		};
    	 */
    	 
    	factory.getParent().closest('.wicket-enclosing-form').on('comparisonEditor.refresh', function(e, dataObj) {
			//bootbox.alert('fired event comparisonEditor!');
			factory.reloadModel(factory.metaModel);
    	});
    	
    	factory.getParent().closest('.wicket-enclosing-form').on('semanticEditor.highlight', function(e, dataObj) {
    	//factory.getParent().closest('form').on('semanticEditor.highlight', function(e, dataObj) {
    		console.log('Fired event highlight with data ' + dataObj.keywords.length);
    	});
    },
    
 
});