/**
  Elementary events for the Semantic Meta-Editor Undo-Handling including a buffer
  for making undo possible.
**/
var AtomicEvent = Class.extend({
    /* initialize a new event, such as dropping a new element or creating a new line
       elem: the element where the event occured on.

     */
    init:function(elem,trigger, eventName) {
        this.eventName=eventName;
        this.elem=elem;
    }
});

/* move an element from one position to another one, where the
   element remains constantly.
 */
var RelocateEvent = AtomicEvent.extend({
    init:function(elem,trigger,oldPos,newPos) {
        this._super(elem,trigger,'relocate');
        this.oldPos=oldPos;
        this.newPos=newPos;
    },

    undo:function() {
        //move element to oldPos and re-arrange related elements
    },

    redo:function() {
       //move element from oldPos to new Pos and re-arrange related elements
    }
});

/* create a box somewhere on the surface. The element may be of special
   layout and may trigger rearrangements in the neighborhood.
*/
var AddBoxEvent = AtomicEvent.extend({
    init:function(elem,trigger) {
        this._super(elem,trigger,'add-box');
    },

    undo:function() {
        //remove element again (e.g. box) with related elements together
    },

    redo:function() {
        //add element again, including related elements
    }
});

/* create an element somewhere on the surface. The element may be of special
 layout and may trigger rearrangements in the neighborhood.
 */
var AddLineEvent = AtomicEvent.extend({
    init:function(line,boxFrom,boxTo,trigger) {
        this._super(line,trigger,'add-line');
        this.boxFrom=boxFrom;
        this.boxTo=boxTo;
    },

    undo:function() {
        //remove element again (e.g. box or line) with related elements together
    },

    redo:function() {
        //add element again, including related elements
    }
});

var AddLabelEvent= AtomicEvent.extend({
    init:function(label,parent,trigger) {
        this._super(label,trigger,'add-label');
        this.parent=parent;
    },

    undo:function() {
        //remove label from parent
    },

    redo:function() {
        //add/append element to parent again
    }
});

var ChangeLabelEvent= AtomicEvent.extend({
    init:function(elem,oldContent,newContent,trigger) {
        this._super(elem,trigger,'change-label');
        this.oldContent=oldContent;
        this.newContent=newContent;
    },

    undo:function() {
        //restore the previous content of the label
    },

    redo:function() {
        //add/append element to parent again
    }
});

var RemoveLabelEvent= AtomicEvent.extend({
    init:function(label,parent,trigger) {
        this._super(label,trigger,'remove-label');
        this.parent=parent;
    },

    undo:function() {
        //add label to parent again
    },

    redo:function() {
        //remove label
    }
});

var RemoveBoxEvent = AtomicEvent.extend({
    init:function(elem,trigger) {
        this._super(elem,trigger,'remove-box');
    },

    undo:function() {
        //add a box again
    },

    redo:function() {
        //remove box
    }
});

/* remove all elements from the canvas */
var CleanCanvasEvent = AtomicEvent.extend({
    init:function(canvas,trigger) {
        this._super(canvas,trigger,'clean-space');
    },

    undo:function() {
        //add previous state again
    },

    redo:function() {
        //clean surface again
    }
});

var RemoveLineEvent = AtomicEvent.extend({
    init:function(line,boxFrom,boxTo,trigger) {
        this._super(line,trigger,'remove-line');
        this.boxFrom=boxFrom;
        this.boxTo=boxTo;
    },

    undo:function() {
        //add line again, considering all related labels
    },

    redo:function() {
        //remove line again
    }
});

/* leave a diagram/canvas/package by switching
   to another component, saving
 */
var LeaveDiagramEvent = AtomicEvent.extend({
    init:function(canvas,trigger) {
        this._super(canvas,trigger,'exit');
    },

    undo:function() {
        //add line again, considering all related labels
    },

    redo:function() {
        //remove line again
    }
});

/* is loaded initially for the start, before the very first user action takes place.
 */
var InitDiagramEvent = AtomicEvent.extend({
    init:function(canvas,trigger) {
        this._super(canvas,trigger,'init');
    },

    undo:function() {
        //add line again, considering all related labels
    },

    redo:function() {
        //remove line again
    }
});

/* remove a sub-diagram.
 */
var RemoveDiagramEvent = AtomicEvent.extend({
    init:function(canvas,trigger) {
        this._super(canvas,trigger,'remove-diagram');
    },

    undo:function() {
        //add package again
    },

    redo:function() {
        //remove line again
    }
});

var AddCommentEvent = AtomicEvent.extend({
    init:function(canvas,trigger) {
        this._super(canvas,trigger,'add-comment');
    },

    undo:function() {
        //add package again
    },

    redo:function() {
        //remove line again
    }
});

var RemoveCommentEvent = AtomicEvent.extend({
    init:function(canvas,trigger) {
        this._super(canvas,trigger,'remove-comment');
    },

    undo:function() {
        //add package again
    },

    redo:function() {
        //remove line again
    }
});

var ChangeCommentStatusEvent = AtomicEvent.extend({
    init:function(canvas,trigger) {
        this._super(canvas,trigger,'change-comment-status');
    },

    undo:function() {
        //add package again
    },

    redo:function() {
        //remove line again
    }
});

var AtomicEventHandler = Class.extend({
    init:function(graphFactory) {
        this.factory=graphFactory;
        this.MAX_STEPS=10; //can vary
        this.history=new Array();
        this.undoPosition=0;
    },


    /* register a new event that happens) */
    registerEvent:function(atomicEvent) {
        console.log('HISTORY-EVENT:'+atomicEvent.eventName); 
        undoPosition=0;
        if(this.history.length>0) {
            if(atomicEvent.eventName=='clean-space' && this.history[this.history.length-1].eventName=='clean-space') {
                return;
            }
        }
        var ser = this.factory.serializeModel();
        atomicEvent._state=ser;
        this.history.push(atomicEvent);
        while(this.history.length>this.MAX_STEPS) {
            this.history.shift();
        }
        console.log('hist-size is ' + this.history.length);
   },


    triggerUndo:function() {
        var max = this.history.length-1;
        if(max-undoPosition>0) {
            undoPosition++;
            var e = this.history[max-undoPosition];
            this.factory.reloadModel(JSON.parse(e._state));
            this.factory._buildPackageSwitcher(this.factory.metaModel); //rebuild package switcher (e.g. if packages are added this is relevant)
        }
    },

    triggerRedo:function() {
        var max = this.history.length-1;
        if(undoPosition>0) {
            undoPosition--;
            var e = this.history[max-undoPosition];
            this.factory.reloadModel(JSON.parse(e._state));
            this.factory._buildPackageSwitcher(this.factory.metaModel); //rebuild package switcher (not sure yet if relevant)...
        }
    }
 
});



