/**
 * The tree layout aligns a flex-graph of boxes to a hierarchical tree.
**/
var TreeLayout = Class.extend({
    init: function(graph) {
    	this.graph=graph;
    	this.levelAccess = null;
    	this.MARGIN_TOP = 20;
    	this.MARGIN_LEFT = 0;
		this.levelLeft = new Array();
		this.levelGap = new Array();
		this.IS_CENTERED = true;
		this.GAP_MIN = 3;
    },
     
    layout: function() {
    	this.basicLayout(null);
    	this.basicLayout(null); //workaround improves layout
    	
    	//we still have the calculated gap
    	this.improveBoxPositions(); //currently time-expensive (mainly because of straight-ahead positioning)
    	
    	/*
    	var mostRightPoint = this._getLevelPhysicalWidth(this.getMaxLevel());
    	var parentRight = this.graph.getParent().width();
    	console.log(mostRightPoint + " vs " + parentRight);
    	
    	if (mostRightPoint > parentRight) {
    		//experimental only: zoom and readjust
    		$('.flexbox').addClass('flexbox-strong-zoom');
    		$('.flexbox').css('min-width', 70);
    		$('.flexbox').width(70); 
    		this.improveBoxPositions();
    	}
    	*/
    	
    	/*
    	if (this.getBoxesOnLevel(0).length > this.ZOOM_WHEN_PARENT_BOXES_EXCEED) {
    		$('.flexbox').addClass('flexbox-zoom');
    	}
    	
    	//this.adjustParent();
    	*/
    },
    
    basicLayout: function(level) {
    	if(level == null) { //start at deepest level
    		var maxLevel = this.getMaxLevel();
    		level = maxLevel;
    		var parentHeight = this._calculateTopOffset(level) + this._maxHeightLevel(level) + this.MARGIN_TOP;
    		this.graph.getParent().height(parentHeight); //adjust parent height
    	}
    	
    	var leafes = this.getBoxesOnLevel(level);
    	var topOffset = this._calculateTopOffset(level);
    	this.alignLeafes(leafes, topOffset); 
    	
    	//done, go one level up. start placing the nodes above ... 
    	level--;
    	if(level < 0) {
    		return;
    	} else {
    		this.basicLayout(level);
    	}
    },
    
    /* return the maximum number of leafes over all levels (e.g. level 0 = 3 nodes; level 1 = 10 nodes; return 10) */
    getMaximumLeafNo: function() {
    	var maxim = this.getMaxLevel();
    	var maximum = 0;
    	for(var i=0; i <= maxim; i++) {
    		maximum = Math.max(maximum, this.getBoxesOnLevel(i).length);
    	}
    	return maximum;
    },
    
    alignLeafes: function(leafes, topOffset) {
    	var gapInfo = this.calculateGraphDataOnLevel(leafes);
		
		var gap = gapInfo.gap;
		var margin = gapInfo.margin;	
    	var plannedWidth = gapInfo.plannedWidth;
    	var maxWidth = gapInfo.maxWidth;
    	
    	//done, so start placing
    	var left = this.MARGIN_LEFT + margin; 
    	if(this.IS_CENTERED) {
    		left += Math.max(0, (maxWidth - plannedWidth) / 2);
    	}
    	this.levelLeft.unshift(left); //adds element at position zero of array
    	this.levelGap.unshift(gap);
    	
    	for(var i=0; i < leafes.length; i++) {
    		var top = topOffset;
    		this.graph.moveBoxTo(leafes[i], top, left);
    		left += $('#' + leafes[i].getId()).width() + gap;
    	}
    },
    
    /* calculate the most relevant data for a specific level, such as the
       maximum width of the graph/level, the gap size between the elements,
       the margin and the planned with of elements.
    */
    calculateGraphDataOnLevel: function(leafes) {
    	var gap = null;
    	var margin = 0;
    	var runs = 0;
    	var maxWidth = 0;
    	var avgLeafWidth = 0;
    	while (true) {
           	var leafWidth = 0;
            for(var i=0; i < leafes.length; i++) {
                leafWidth += $('#' + leafes[i].getId()).width();
            }
			avgLeafWidth = leafWidth/leafes.length;
            margin = avgLeafWidth/4;
            maxWidth = this.graph.getParent().width() - margin*2;
            //ideally we leave 1/4 of the avgLeafWidth between nodes
            gap = avgLeafWidth/2; //previously: /4
            var plannedWidth = (Math.max(0, leafes.length) - 1) * gap + leafWidth;
            while (plannedWidth > maxWidth && (gap > 10)) { // at least 10 pixels must be left in between
                gap = gap/2; // successively divide space half and a half until fixing into maxWidth
                plannedWidth = (Math.max(0, leafes.length) - 1) * gap + leafWidth;
            }

            if (plannedWidth > maxWidth && runs < 20) {
                //still ?
                for(var i=0; i < leafes.length; i++) {
                    var oldWidth  =$('#' + leafes[i].getId()).width();
                    $('#' + leafes[i].getId()).width(oldWidth-10);
                }
            } else {
                break;
            }
            runs++; //security mechanism
        }
        return {gap: Math.max(gap, this.GAP_MIN), margin: margin, plannedWidth: plannedWidth, maxWidth: maxWidth, avgLeafWidth: avgLeafWidth};
    },

    /* get the offset for a specific element based on its level */
    _calculateTopOffset: function(level) {
        var topOffset = 0; //this.graph.getParent().offset().top; //this.graph.getParent().offset().top;   vs. offset (depends on canvas or not)
    	for(var i=0; i < level; i++) {
    		var mhl = this._maxHeightLevel(i);
    		topOffset += mhl + mhl/2.5; //previously /2
    	}
    	topOffset += this.MARGIN_TOP;
    	return topOffset;
    },
    
    _maxHeightLevel: function(level) {
    	var maxHeight = 0;
    	var nodes = this.getBoxesOnLevel(level);
    	for(var i=0; i < nodes.length; i++) {
    		maxHeight = Math.max(maxHeight, $('#' + nodes[i].getId()).height());
    	}
    	return maxHeight;
    },
    
    _structureByLevels: function() {
    	this.levelAccess = new Array();
    	for(var i=0; i < this.graph.boxes.length; i++) {
    		var box = this.graph.boxes[i];
    		var level = this.getParentChain(box).length;
    		this._maintainLevelAccess(level);
    		this.levelAccess[level].push(box);
    	}
    },
    
    lazyInit: function() {
    	if(this.levelAccess == null) {
    		this._structureByLevels();
    	}
    },
    
    getMaxLevel: function() {
    	this.lazyInit();
    	return this.levelAccess.length - 1;
    },
    
    getBoxesOnLevel: function(depth) {
	    this.lazyInit();
    	return this.levelAccess[depth];
    },
    
    _maintainLevelAccess: function(level) {
    	if(this.levelAccess == null) {
    		this.levelAccess = new Array();
    	}
    	while(this.levelAccess.length <= level) {
    		this.levelAccess.push(new Array());
    	}
    },
    
    /** get the width of a specific level (the most right point in the level) **/
    _getLevelPhysicalWidth: function(level) {
    	var boxesLastLevel = this.getBoxesOnLevel(this.getMaxLevel());
    	var mr = -1;
    	for (var i=0; i < boxesLastLevel.length; i++) {
    		var mostRightCssBox = $('#' + boxesLastLevel[i].getId());
    		var upperRight = mostRightCssBox.offset().left + mostRightCssBox.width();
    		if(upperRight > mr) {
    			mr = upperRight;
    		} 
    	}
    	return mr;	
    },
    
    getParentChain: function(box, parentChain) {
    	parentChain = parentChain == null ? new Array() : parentChain;
    	var incoming = this.graph.getBoxesIncoming(box.getId());
    	
    	//we always take the first parent
    	if(incoming.length > 0) {
    		var parent = incoming[0]; 
    		if($.inArray(parent, parentChain) >= 0) {
    			//graph - loop
    			return parentChain;
    		}
    		parentChain.push(parent);
    		this.getParentChain(parent, parentChain);
    	}
    	return parentChain;
    },
    
    getDistance: function(box1, box2) {
    	//improvement: take the center
    	box1 = jQuery('#' + box1.getId());
    	box2 = jQuery('#' + box2.getId());
    	var a = Math.abs((box1.offset().top + box1.height()/2) - (box2.offset().top + box2.height()/2));
    	var b = Math.abs((box1.offset().left + box1.width()/2) - (box2.offset().left + box2.width()/2));
    	var c = Math.sqrt(a*a + b*b);
    	return c;
    },
    
    /* get the distance between a child node and all its parents in total */
    _getTotalDistanceParent: function(childBox) {
    	//calculate sum of children distance for each box
    	var parents = this.graph.getBoxesIncoming(childBox.getId());
        var totalDistance = 0;
        for (var c = 0; c < parents.length; c++) {
        	totalDistance += this.getDistance(childBox, parents[c]);
        }
        return totalDistance;
    },
    
    /* get the distance between a child node and all its parent nodes and return
       the sum for all nodes throughout an entire level.
    */
    _getTotalDistanceParentsLevel: function(level) {
    	var leafes = this.getBoxesOnLevel(level);
    	var totalDistance = 0;
        for (var i=0; i < leafes.length; i++) {
        	var leaf = leafes[i];
           	totalDistance += this._getTotalDistanceParent(leaf);	
    	}	
    	return totalDistance;
    },
    
    switchBoxes: function (box1, box2) {
    	var cssBox1 = jQuery('#' + box1.getId());
    	var cssBox2 = jQuery('#' + box2.getId());
    	
    	var top1  = cssBox1.position().top;
    	var left1 = cssBox1.position().left;
    	var top2  = cssBox2.position().top;
    	var left2 = cssBox2.position().left;

    	//this.graph.moveBoxTo(box1, top2, left2);
    	//this.graph.moveBoxTo(box2, top1, left1);
    	
    	var offset1 = cssBox1.offset();
		var offset2 = cssBox2.offset();
    	cssBox1.offset(offset2);
    	cssBox2.offset(offset1);

    },
    
    /* improves the position of the boxes after rendering: 
    	try to minimize the distances between a parent and its child elements
    	so that there are less line overlaps etc. Example:
    					   o    u
    					----------   => if (a,b) belong to u and (c d) belong to o
    					a b		c d     then there will be overlapping lines.
    									The algorithm will switch (a,b) and (c,d).			
    */
    improveBoxPositions: function() {
    	//start at level 1, not at level zero.
    	var improved = false;
    	var orderedLevelAccess = new Array();
        for (var level=1; level <= this.getMaxLevel(); level++) {
           	var leafes = this.getBoxesOnLevel(level);

           	var newLeafOrder = new Array();
            for (var xx = 0; xx < leafes.length; xx++) {
            	newLeafOrder[xx] = xx; //by default leaf xx is at position xx.
            }
           	
           	for (var i=0; i < leafes.length; i++) {
            	//shift each box from left to right. Take the position
            	//at which the total distance of the entire level is smaller than
            	//the distance before. Otherwise return the box to its previous position
            	var distance = this._getTotalDistanceParentsLevel(level);
            	var currentBox = leafes[i];
            	var newPos=i;
            	for (var j=i+1; j < leafes.length; j++) {	
           			this.switchBoxes(currentBox, leafes[j]);
           			var newDistance = this._getTotalDistanceParentsLevel(level);
           			if (newDistance < distance) {
           				//keep the block at its position and continue
           				distance = newDistance; //the new distance is relevant now
           				improved = true;
           				newPos = j;
           			} else {
           				this.switchBoxes(leafes[j], currentBox); //return the box to its prior position
           			}
           		}
           		var oldVal = newLeafOrder[newPos];
           		newLeafOrder[newPos]=i;
           		newLeafOrder[i]=oldVal;
           	}
           	
           	//adapt order in stored arrays too so that the gaps can be rearranged below
           	console.log('new leaf order: ' + newLeafOrder);
           	orderedLevelAccess.push(newLeafOrder);
           	
        }
        
        //as long as there is space for improvement continue
        if (improved) {
        	this.improveBoxPositions(); //should work recursive
        }
        
        //rearrange considering gaps
        for (var level=1; level < this.getMaxLevel(); level++) {
        	var leafes = this.orderByPosition(this.getBoxesOnLevel(level));
        	this._printBoxes(leafes);        	
        	var firstLeaf = $('#'+leafes[0].getId());
        	this.alignLeafes(leafes, firstLeaf.position().top);
        } 
        
		for (var i=0; i < this.graph.boxes.length; i++) {
			this.graph.redrawLines(this.graph, this.graph.boxes[i]);
		}
    },
    
    _printBoxes: function(leafes) {
    	var order='';
		for (var i=0; i < leafes.length; i++) {
			order += leafes[i].mainLabel.getText() + ',';
		}
		console.log('order:' + order);
		return order;
    },
    
    /* order a list of boxes based on their horizontal position */
    orderByPosition: function(leafes) {
    	var orderedList = new Array();
    	var rightLimit = 99999999;
    	var i=0;
    	while (i < 9999) { //security limit
			var left = 0;
			var mostRightBox = null;
			for(var i=0; i < leafes.length; i++) {
				var cssBox = jQuery('#' + leafes[i].getId());
				var boxLeft = cssBox.position().left;
				if (boxLeft > left && boxLeft < rightLimit) {
					left = boxLeft;
					mostRightBox = leafes[i];
				}
			}
			
			if(mostRightBox == null) {
				break;
			}
			//got most right box
			orderedList.push(mostRightBox);
			rightLimit = left;
			i++;
    	}
    	return orderedList.reverse();
    },
});

