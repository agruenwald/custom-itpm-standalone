/**
 * Handling of comments / annotations in the Semantic Meta Editor.
**/
var CommentsHandler =  Class.extend({

  init: function(umlFactory){
  	  this.factory = umlFactory;
  },
  
  _addTr: function(a) {
  	var tr = '';
  	$.each(a, function(i,e) {
  		tr += '<td>' + e + '</td>';
  	});
		return '<tr>' + tr + '</tr>';
  },
  
  _handleShowAttributes: function(mA, mc, optionalMetaAttribute, rows) {
	  var factory = this.factory;
	  var commentHandler = this;
		
	  if (mA != null) {
			$.each(jQuery.extend(new MetaAttribute(''), mA).getComment(), function(i, c) {
				rows.push(new Array(mc.id, 'MetaAttribute', mA.name, c, mA)); //fake-meta-attribute
			});
		} else if (mc.enumClass) {  //for enum classes constants/attributes are stored in subclasses.
			$.each(mc.subclasses, function(i, s) {
				var sc = jQuery.extend(new MetaModel(''), factory.metaModel).findClassById(s.id);
				$.each(jQuery.extend(new MetaClass(''), sc).getComment(), function(i, c) {
			  		rows.push(new Array(mc.id, 'MetaAttribute', s.name, c, s.name)); //fake-meta-attribute; pass subclass-name as the last parameter (optionalAttribute)
			  	});
			});
		} else if (optionalMetaAttribute != null ){
			//some attributes may link to enum classes
			var enumAssoc = mc.findAssociationByName(optionalMetaAttribute.name);
			if(enumAssoc != null) {
				var linkTo = jQuery.extend(new MetaModel(), factory.metaModel).findClassById(enumAssoc.to.metaClassId);
				if(linkTo.enumClass) {
					$.each(jQuery.extend(new MetaAssociation(''), enumAssoc).getComment(), function(i, c) {
	  			  		rows.push(new Array(linkTo.id, 'MetaAttribute', linkTo.name, c)); //fake-meta-attribute
	  			  	});
				}
	  		}
		} else {
			//don't react.
		}
	  return rows;
  },
  
  /* */
  showComments: function(id, optionalMetaAttribute, isCanvasSelected, hidePosition) {
  	var factory = this.factory;
  	var commentHandler = this;
  	
  	var parentCanvas = $('#' + id).closest('.canvas');
  	
  	if (hidePosition == undefined) {
  		hidePosition = true;
  	}
  	
  	//remove old instances
  	$(parentCanvas).closest('.wicket-enclosing-form').find('.annotation_table_in_action').remove();
  	
  	var atableTemplate = $(parentCanvas).closest('.wicket-enclosing-form').find('.annotation_table');	
  	var atable = $(atableTemplate).clone();
  	atable.css('display', '');
  	atable.addClass('annotation_table_in_action');
  	commentHandler._closeOpenCommentHistories(parentCanvas);
  	
  	var rows = new Array();
 
  	var isAttribute = optionalMetaAttribute != null ? true : false; 	
  	$.each(jQuery.extend(new MetaModel(''), factory.metaModel).getComment(), function(i, c) {
  		rows.push(new Array(id, 'MetaModel', factory.metaModel.name, c));
  	});
  	
  	var packages = factory.metaModel.packages;
  	var selI = factory._getSelectedPackageIndex(factory.metaModel);
  	$.each(jQuery.extend(new MetaPackage(''), packages[selI]).getComment(), function(i, c) {
  		rows.push(new Array(selI, 'MetaPackage', packages[selI].name, c));
  	});
  	
  	var mc = jQuery.extend(new MetaModel(), factory.metaModel).findClassById(factory._stripPrefix(id));
  	$.each(jQuery.extend(new MetaClass(''), mc).getComment(), function(i, c) {
  		var skipC = false;
  		if (hidePosition) {
  			var ec = jQuery.extend(new MetaComment(), c);
  			
  			if (mc.isLayoutComment(ec) && (ec.text.indexOf('@top') == 0 || ec.text.indexOf('@left') == 0)) {
  				skipC=true; //skip comments about position
  			}
  			
  		}
  		if (!skipC) {
  			rows.push(new Array(mc.id, 'MetaClass', mc.name, c));
  		}
  	});
  	
  	if(mc != null && isAttribute) {
  		var mA = mc.findAttributeByName(optionalMetaAttribute.name);
  		rows = commentHandler._handleShowAttributes(mA, mc, optionalMetaAttribute, rows);
  	} else if (mc != null) {
  		//show attributes in class view
  		if (mc.attributes.length == 0) {
  			rows = commentHandler._handleShowAttributes(null, mc, optionalMetaAttribute, rows); //subclasses will be traversed
  		} else {
	  		$.each(mc.attributes, function(i,mA) {
	  			if (mA != null) {
	  				rows = commentHandler._handleShowAttributes(mA, mc, optionalMetaAttribute, rows); //subclasses will not be traversed because mA is never null
	  				/*
	      			$.each(jQuery.extend(new MetaAttribute(''), mA).getComment(), function(i, c) {
	      				rows.push(new Array(mc.id, 'MetaAttribute', mA.name, c, mA));
	      			});
	      			*/
	      		}
	  		});
  		}
  	}
  	
  	var mc = jQuery.extend(new MetaModel(), factory.metaModel).findAssociationById(factory._stripPrefix(id))[0];
  	$.each(jQuery.extend(new MetaAssociation(''), mc).getComment(), function(i, c) {
  		var name = mc.name
  		if(name == '') {
  			var from = factory.getBoxById(factory._getPrefixed(mc.from.metaClassId));
  			if (from != null) {
  				name = from.mainLabel.getText()
  			}
  			
  			if(mc.to != null) {
  				name += '-';
  				var to = factory.getBoxById(factory._getPrefixed(mc.to.metaClassId));
  				if(to != null) {
  					name += to.mainLabel.getText();
  				}
  			}
  		}
  		rows.push(new Array(mc.id, 'MetaAssociation', name, c));
  	});
  	
  	var content = '';
  	$.each(rows, function(i, row) {
  		var cells = new Array();
  		var createdAt = printableDate(row[3].createdAt);
  		cells = cells.concat(new Array(row[1], row[2], row[3].text, row[3].type, createdAt, row[3].creatorName != null ? row[3].creatorName : ''));
  		cells.push('<input type="hidden" class="metaElementId" value="' + row[0] +'"/>'
  				 + '<input type="hidden" class="metaElementType" value="' + row[1] + '"/>'
  				 + '<input type="hidden" class="metaAttributeName" value="' + (row.length > 4 ? row[4].name : '') + '"/>' //(row.length > 4 ? row[4] : '') + '"/>'
  				 + '<i class="remove-comment-action glyphicon glyphicon-remove-sign" style=""></i>');
  		content += commentHandler._addTr(cells);
  		
  	});
  	
  	atable.find('table').append(content);
  	
  	if (isCanvasSelected) {
  		var po = $('#' +id).popover({
				//container:'body',  //testwise
				 		container: '#' + $(parentCanvas).attr('id'),
						title: '<span class="text-info"><strong>Comments and Annotations</strong></span>'+
						'<button type="button" id="close" class="close" onclick="$(&quot;#'+id + '&quot;).popover(&quot;hide&quot;);">&times;</button>',  
				 html:true, content: atable, trigger: 'manual',
				 placement: 'bottom'});
  		po.popover('show');
  		var cssPopOver = $('#' +id).find('.popover');
  		var canvasOffset  =$($('.canvas')[0]).offset();
  		cssPopOver.css('left', 50); //canvasOffset.left);
  		cssPopOver.css('top', 20); //  canvasOffset.top);
  	} else {
  		var po = $('#' +id).popover({
			//container:'body',  //testwise
			 		container: '#' + $(parentCanvas).attr('id'),
					title: '<span class="text-info"><strong>Comments and Annotations</strong></span>'+
					'<button type="button" id="close" class="close" onclick="$(&quot;#'+id + '&quot;).popover(&quot;hide&quot;);">&times;</button>',  
			 html:true, content: atable, trigger: 'manual',
			 placement: 'right'});
		po.popover('show');
  	}
  	
  	$('.remove-comment-action:not(.bound)').addClass('bound').bind('click', function(e) {
  		var tr = $(this).closest('tr');
  		var metaElementId 		= $(tr).find('input[type=hidden].metaElementId').val();
  		var metaElementType 	= $(tr).find('input[type=hidden].metaElementType').val();
  		var metaAttributeName 	= $(tr).find('input[type=hidden].metaAttributeName').val();
  		var content = $(tr).find('td');
  		
  		var text 		= $(content[2]).html();
  		var metaType 	= $(content[1]).html();
  		
  		var hasBeenRemoved = commentHandler.removeComment(metaElementId, metaElementType, text, metaAttributeName, e);
  		if(hasBeenRemoved) {
  			//refreshing the canvas makes the popup unnavigable.
  			commentHandler._closeOpenCommentHistories(factory.getParent()); //close comment history popover
  	  		commentHandler.refresh(); //simplified: updating the status is threated the same way as adding a comment.
  			commentHandler.showComments(id, optionalMetaAttribute, isCanvasSelected);
  			$(tr).remove();
  		}
  	});
  },
  
  
  /**
   * trigger the adding of a comment to an element. manage it within a single (status field).
   */
  setElementStatus: function(value, boxId, metaClassType, selectedAttribute, creatorName) {
	  return this.setElementCommentAnnotation(value, boxId, metaClassType, selectedAttribute, creatorName, '@status');
  },
  
  setElementCommentAnnotation: function(value, boxId, metaClassType, selectedAttribute, creatorName, uniqueAnnotation) {
	  var factory = this.factory;
	  var commentHandler = this;
	  
	  var a = this._determineElems(factory._stripPrefix(boxId), metaClassType, (selectedAttribute != null ? selectedAttribute.name : null));
	  for(var i=0; i < a.length; i++) {
		  var elem = a[i];
	  	  var noRemoved = elem.removeCommentsStartingWith(uniqueAnnotation + ":", MetaComment_TYPE_INTERNAL); 
	  	  console.info('removed ' + noRemoved + ' entries.');
	  }
	   	
	  var mcom = new MetaComment(uniqueAnnotation + ":" + value, true);
	  mcom.creatorName = creatorName;
	  mcom.type = MetaComment_TYPE_INTERNAL; 
	  var mElem = commentHandler._persistComment(boxId, metaClassType, (selectedAttribute != null ? selectedAttribute.name : null), mcom);
	  factory.aeH.registerEvent(new ChangeCommentStatusEvent(mcom.text,mElem,'click'));
	  commentHandler.refresh(); //simplified: updating the status is treated the same way as adding a comment.
  },
  
  /** form handling for adding new comments */
  addComment: function(elemId, metaClassType, selectedAttribute) {
  	var factory = this.factory;
  	var commentHandler = this;
  	
  	commentHandler._closeOpenCommentHistories(factory.getParent()); //close comment history popover
  	
  	var modal = factory.getParent().closest('.wicket-enclosing-form').next('.comment-add-modal');
  	modal.find('h4').html('Add new comment/annotation');
  	modal.find('.action').val('add');
  	
  	var elemText = '';
  	if (selectedAttribute != null) { 
  		elemText = factory.getBoxById(elemId).mainLabel.getText() + ":" + selectedAttribute.name;
  	} else if (factory.getBoxById(elemId) != null) {
  		elemText = factory.getBoxById(elemId).mainLabel.getText();
  	} else if (factory.getLineById(elemId) != null) {
  		var line = factory.getLineById(elemId);
  		if(line.mainLabel != null) {
  			elemText = line.mainLabel.getText();
  		}
  		if(elemText == '') {
  			elemText = factory.getBoxById(line.fromId).mainLabel.getText() + '-' + factory.getBoxById(line.toId).mainLabel.getText();
  		}
  	} else {
  		elemText = factory.selectedPackage;
  	}
  	
  	modal.find('input[type=text].comment-element').val(elemText);
  	modal.find('input[type=hidden].comment-element-id').val(elemId);
  	modal.find('input[type=hidden].comment-element-type').val(metaClassType);
  	
  	var selName = selectedAttribute != null ? selectedAttribute.name : '';
  	modal.find('input[type=hidden].comment-attribute-name').val(selName);
  	modal.find('textarea.comment-text').val('');
  	modal.find('.submit-button').html('Add Comment');
  	modal.modal('show');
  	
  	$(modal).find('.submit-button:not(.bound)').addClass('bound').bind('click', function(e) {
  		e.preventDefault();
  		//validation
  		var elementType = modal.find('.comment-element-type').val();
  		var elemInfo = modal.find('.comment-element').val();
  		var elemId = modal.find('.comment-element-id').val();
  		var text = modal.find('.comment-text').val();
  		var type = modal.find('select').val();
  		var creatorName = modal.find('.creatorName').val();
  		var selAttributeName = modal.find('.comment-attribute-name').val();
  		
  		if(elemInfo.length <= 0
  		|| (elementType == 'MetaAttribute' && selAttributeName == '')
  		|| text.length <= 0
  		|| type.length <= 0
  		|| elementType.length <= 0
  		|| elemId.length <= 0) {
  			alert('Validation failed.');
  		} else {
  			
  			var mcom = new MetaComment(text,true);
  			mcom.creatorName = creatorName;
  			if(type == 'comment') {
  				mcom.type = MetaComment_TYPE_COMMENT;
  			} else { 
  				mcom.type = MetaComment_TYPE_CONSTRAINT; 
  				//MetaComment_TYPE_INTERNAL;
  			}
  			var mElem = commentHandler._persistComment(elemId, elementType, selAttributeName, mcom);
  			factory.aeH.registerEvent(new AddCommentEvent(mcom.text,mElem,e));
  			factory.getParent().closest('.wicket-enclosing-form').next('.comment-add-modal').modal('hide');
  			commentHandler.refresh(); //simplified: updating the status is treated the same way as adding a comment.
  		}
  	});
  },
  
  _persistComment: function(elemId, elementType, selAttributeName, mcom) {
	var factory = this.factory;
	var mElem = null;
	
	if(elementType == 'MetaModel') {
		mElem = factory.metaModel;
		mElem = jQuery.extend(new MetaModel(''), mElem);
		mElem.addComment(mcom);
	} else if(elementType == 'MetaPackage') {
		mElem = factory.metaModel.packages[factory._getSelectedPackageIndex(factory.metaModel)];
		mElem = jQuery.extend(new MetaPackage(''), mElem);
		mElem.addComment(mcom);
	} else if (elementType == 'MetaClass') {
		mElem = jQuery.extend(new MetaModel(''), factory.metaModel).findClassById(factory._stripPrefix(elemId));
		mElem = jQuery.extend(new MetaClass(''), mElem);
		mElem.addComment(mcom);
		
	} else if (elementType == 'MetaAttribute') {
		mElem = jQuery.extend(new MetaModel(''), factory.metaModel).findClassById(factory._stripPrefix(elemId));
		mElem = jQuery.extend(new MetaClass(''), mElem);
		var mAttr = mElem.findAttributeByName(selAttributeName)
		if(mAttr != null) {
			mElem = jQuery.extend(new MetaAttribute(''), mAttr); 
			mElem.addComment(mcom); 
		} else {
			//it still can be an enum type. 
			//case1: issueType: IssueType (usage of Enum class as an attribute range)
			var assoc = mElem.findAssociationByName(selAttributeName);
			if (assoc != null) {
				mElem = jQuery.extend(new MetaAssociation(''), assoc); 
				mElem.addComment(mcom); 
				jQuery.extend(new MetaModel(''), factory.metaModel).findAssociationById(factory._stripPrefix(assoc.id))[0].comment.push(mcom);
			} else {
				//case 2: enumAttribute defined within Enum Class (e.g. Bug, Feature, ... with IssueType) 
				var listPotentialSubClassesEnum = jQuery.extend(new MetaModel(''), factory.metaModel).findClassesByName(selAttributeName);
				if(listPotentialSubClassesEnum.length == 1) { //exactly one class matching
					mElem = listPotentialSubClassesEnum[0];
					//mElem.addComment(mcom);  (avoid duplicate comment)
					jQuery.extend(new MetaModel(''), factory.metaModel).findClassById(factory._stripPrefix(mElem.id)).comment.push(mcom);
				} else {
					console.error('No attr found for name ' + selAttributeName);
				}
			}
		} 
	} else {
		mElem = jQuery.extend(new MetaModel(''), factory.metaModel).findAssociationById(factory._stripPrefix(elemId));
		mElem = jQuery.extend(new MetaAssociation(''), mElem);
		mElem.addComment(mcom);
		//otherwise does not show up in the model
		jQuery.extend(new MetaModel(''), factory.metaModel).findAssociationById(factory._stripPrefix(elemId))[0].comment.push(mcom);
	}
	return mElem;
  },
  
  refresh: function() {
	  var factory = this.factory;
	  factory.reloadModelFromHist();  
  },
  
  _determineElems: function(metaElementId, metaElementType, metaAttributeName) {
  	var factory = this.factory;
  	var commentHandler = this;
  	
  	var a = new Array();
  	var isRemoved = false;
  	
  	var isRemoved = false;
  	if (metaElementType == 'MetaModel') {
  		a.push(jQuery.extend(new MetaModel(''), factory.metaModel));
  		//isRemoved = jQuery.extend(new MetaModel(''), factory.metaModel).removeCommentViaText(text);
  	} else if (metaElementType == 'MetaPackage') {
  		var pkg = factory.metaModel.packages[metaElementId];
  		if (pkg == null) {
  			var inde = factory._getSelectedPackageIndex(factory.metaModel);
  			pkg = factory.metaModel.packages[inde];
  		}
  		a.push(jQuery.extend(new MetaPackage(''), pkg));
  		//isRemoved = jQuery.extend(new MetaPackage(''), factory.metaModel[metaElementId]).removeCommentViaText(text);
  	} else if (metaElementType == 'MetaClass') {
  		var mc = jQuery.extend(new MetaModel(''), factory.metaModel).findClassById(metaElementId);
  		a.push(jQuery.extend(new MetaClass(''), mc));
  		//isRemoved = jQuery.extend(new MetaClass(''), mc).removeCommentViaText(text);
  	} else if (metaElementType == 'MetaAssociation') {
  		var assoc = jQuery.extend(new MetaModel(''), factory.metaModel).findAssociationById(metaElementId)[0];
  		a.push(jQuery.extend(new MetaAssociation(''), assoc));
  		//isRemoved = jQuery.extend(new MetaAssociation(''), assoc).removeCommentViaText(text);
  	} else if (metaElementType == 'MetaAttribute') {
  		var mc = jQuery.extend(new MetaModel(''), factory.metaModel).findClassById(metaElementId);
  		if(metaAttributeName == null) {
  		}
  		var ma = jQuery.extend(new MetaAttribute(''), mc).findAttributeByName(metaAttributeName);
  		if (ma == null) {
  			//could also be a subclass (Enum constant/attribute)
  			$.each(mc.subclasses, function(i,s) {
  				if(s.name == metaAttributeName) {
  					var subCl = jQuery.extend(new MetaModel(''), factory.metaModel).findClassById(s.id)
  					a.push(jQuery.extend(new MetaClass(''), subCl));
  					//isRemoved = jQuery.extend(new MetaClass(''), subCl).removeCommentViaText(text);
  				}
  			});
  		} else {
  			a.push(jQuery.extend(new MetaAttribute(''), ma));
  			//isRemoved = jQuery.extend(new MetaAttribute(''), ma).removeCommentViaText(text);
  		}
  	} else {
  		console.error('Unknown/unthreaded metaElementType ' + metaElementType);
  	}
  	return a;
  },
  
  removeComment: function(metaElementId, metaElementType, text, metaAttributeName, e) {
	var factory = this.factory;
	var isRemoved = false;
  	var a = this._determineElems(metaElementId, metaElementType, metaAttributeName);
  	for(var i=0; i < a.length; i++) {
  		var elem = a[i];
  		isRemoved = elem.removeCommentViaText(text);
  	}
  	
  	if(isRemoved) {
  		factory.aeH.registerEvent(new RemoveCommentEvent(text, null, e));
  	}
  	return isRemoved;
  },
  
  //remove all previous popovers
  _closeOpenCommentHistories: function(parentCanvas) {
	  parentCanvas.find('.box').popover('destroy');
	  parentCanvas.find('.association_line').popover('destroy');
	  parentCanvas.popover('destroy');
  },
  
  _getActivePackage: function() {
	  var factory = this.factory;
	  var packages = factory.metaModel.packages;
	  var selI = factory._getSelectedPackageIndex(factory.metaModel);
	  return jQuery.extend(new MetaPackage(''), packages[selI]);
  },
  
  getClassStyle: function(metaclass) {
	  metaclass = jQuery.extend(new MetaClass(), metaclass);
	  return metaclass.getStatusComment(this._getActivePackage());
  },
  
  getAttributeStyle: function(metaattribute) {
	  metaAttribute = jQuery.extend(new MetaAttribute(), metaattribute);
	  return metaAttribute.getStatusComment(this._getActivePackage());
  }

});