/**
  * The central graph factory maintains a graph (e.g. a UML class diagram) and enables
  * access to modify, update, query the graph including specific functionality for
  * certain types of graphs.
**/
var GraphFactory =  Class.extend({

  init: function(parentId){
  	  this.parentId = parentId;
      this.boxes = new Array();
      this.lines = new Array();
      this.aeH = new AtomicEventHandler(this);
  },

  getParent: function() {
  	return $('#'+this.parentId);
  },

  getParentId: function() {
      return this.parentId;
  },

  getRelativeTop: function(box) {
       var top = $('#'+box.getId()).position().top;
       //top = top - $('#'+this.parentId).position().top;
      return top;
  },

  getRelativeLeft: function(box) {
      var left = $('#'+box.getId()).position().left;
      //left = left - $('#'+this.parentId).position().left;
      return left;
  }, 
  
  register: function(obj) {
  		obj.setFactory(this);
  		this.boxes.push(obj);
  	    obj.add();
  },

  humanRegister: function(obj) {
      this.register(obj);
      this.aeH.registerEvent(new AddBoxEvent(obj,'click'));
  },
  
  getBoxById: function(id) {
  	for(var i=0; i < this.boxes.length; i++) {
  		if(this.boxes[i].getId() == id) {
  			return this.boxes[i];
  		}
  	}
  	return null;
  },
  
  getBoxByMainLabel: function(name) {
  	for(var i=0; i < this.boxes.length; i++) {
  		if(this.boxes[i].mainLabel.getText() == name) {
  			return this.boxes[i];
  		}
  	}
  	return null;
  },

  getLineById: function(id) {
        for(var l in this.lines) {
            if(this.lines[l].id == id) {
                return this.lines[l];
            }
        }
        return null;
  },

  removeBoxById: function(objId) {
    for(var i=0; i < this.boxes.length; i++) {
        var c = this.boxes[i];
        if(c.getId() == objId) {
            var removeIds = new Array();
            for(var j=0; j<this.lines.length;j++) {
                var l=this.lines[j];
                if(l.fromId==objId || l.toId==objId) {
                    removeIds.push(l.id);
                }
            }
            for(var j=0; j< removeIds.length; j++) {
               this.removeLineById(removeIds[j]);
            }
            this.boxes[i].destroy();
            this.boxes.remove(i);
        }
    }
  },

  humanRemoveBoxById: function(objId,event) {
      this.aeH.registerEvent(new RemoveBoxEvent(this.getBoxById(objId),event));
      var id = this.removeBoxById(objId);
    //experimentally added, but works: undo requires state so that redo is possible.
      this.aeH.registerEvent(new RemoveBoxEvent(this.getBoxById(objId),event)); 
      return id;
  },


  clearSpace: function() {
    while(this.boxes.length>0) {
        var box = this.boxes[this.boxes.length-1];
        var boxId = box.getId();
        this.removeBoxById(boxId);
    }
  },

  humanClearSpace: function() {
      this.clearSpace();
      this.aeH.registerEvent(new CleanCanvasEvent(this.getParent(),'click'));
  },

  removeLineById: function(lineId) {
      for(var j=0; j<this.lines.length;j++) {
          var l=this.lines[j];
          if(l.id==lineId) {
              this.lines[j].destroy();
              this.lines.remove(j);
          }
      }
  },

  humanRemoveLineById: function(lineId) {
      var line = this.getLineById(lineId);
      var f = this.getBoxById(line.fromId);
      var t = this.getBoxById(line.toId);
      this.removeLineById(lineId);
      this.renderIfSelf(line, lineId, f);
      this.aeH.registerEvent(new RemoveLineEvent(line,f,t,'mousedown'));
  },

  renderIfSelf: function(line, lineId, boxFrom) {
  	if(line.classType=='SelfReference') {
  		var selfRefs = this.getLinesConnectedFrom(boxFrom, 'SelfReference');
  		for(var i=0; i < selfRefs.length; i++) {
  			this.redrawSelfReference(boxFrom,selfRefs[i], true);
  		}
  	}
  },
  
  /* returns an array containing all lines associated to a specific box */
  getBoxConnectionsById: function(boxId) {
     // var box = this.getLineById(boxId);
      var foundLines = new Array();
      for(var j=0; j<this.lines.length;j++) {
          var l=this.lines[j];
          if(l.fromId==boxId || l.toId==boxId) {
              foundLines.push(l);
          }
      }
      return foundLines;
  },
  
  moveBoxTo: function (box, top, left) {
  	var cssBox = $('#' + box.getId());
  	cssBox.css('left', left);
  	cssBox.css('top', top);
  	this.redrawLines(this, box);
  	
  },
  
  getBoxConnectionsIncoming: function(boxId) {
  	var lines = this.getBoxConnectionsById(boxId);
  	var res = new Array();
  	for(var i=0; i<lines.length;i++) {
  		var line = lines[i];
  		if(line.toId == boxId) {
  			res.push(line);
  		}
  	}
  	return res;
  },

  getBoxConnectionsOutgoing: function(boxId) {
  	var lines = this.getBoxConnectionsById(boxId);
  	var res = new Array();
  	for(var i=0; i<lines.length;i++) {
  		var line = lines[i];
  		if(line.fromId == boxId) {
  			res.push(line);
  		}
  	}
  	return res;
  },
  
  getBoxesIncoming: function (boxId) {
  	var incoming = this.getBoxConnectionsIncoming(boxId);
  	var res = new Array();
  	for(var i=0; i < incoming.length; i++) {
  		res.push(this.getBoxById(incoming[i].fromId));
  	}
  	return res;
  },

  getBoxesOutgoing: function (boxId) {
  	var outgoing = this.getBoxConnectionsOutgoing(boxId);
  	var res = new Array();
  	for(var i=0; i < outgoing.length; i++) {
  		res.push(this.getBoxById(outgoing[i].toId));
  	}
  	return res;
  },
  

  addLine: function(line) {
      this.lines.push(line);
  },

  /* let the graph factory know about an existing line but do not render it */
  addUnrenderedLine: function(fromBox, toBox, assoctype, lineId, extraClasses) {
  	var factory = this;
	var line = factory.createLine(fromBox, toBox, assoctype, lineId, extraClasses);
  	line.addUnconnectedLine(factory,fromBox,lineId, true);
  	/* add line to factory */
  	line.fromId = fromBox.getId();
  	line.toId = toBox.getId();
  	factory.addLine(line);
  	return line;
  },
  
  createLine: function(fromBox, toBox, assoctype, lineId, extraClasses) {
  	var factory = this;
  	var line = null;
	if(fromBox == toBox) {
		line = new UmlAssociationSelf(factory, {assoctype: assoctype, extraClasses: extraClasses});
	} else {
		line = new UmlAssociation(factory,{assoctype: assoctype, extraClasses: extraClasses});
	}
	return line;
  },
  
  placeConnectedLine: function(fromBox, toBox, assoctype, lineId, extraClasses) {
	var factory = this;
	var line = factory.createLine(fromBox, toBox, assoctype, lineId, extraClasses);
	var association = line.addUnconnectedLine(this,fromBox,lineId, true);
	factory.endLinkMode(line.id, factory.parentId); //unbind all events
	if(fromBox==toBox) {
		factory.redrawSelfReference(fromBox,line);
	}  else {
		factory.redrawLine(fromBox,toBox,line, false, true);
		//hack: otherwise multiple lines between two concepts are not rendered correctly
		factory.moveBoxTo(fromBox, $('#'+fromBox.getId()).position().top, $('#'+fromBox.getId()).position().left);
	}
  },

  /* starts the process of connecting a new line from a certain box to another (yet unknown) box */
  startConnectionProcess: function(assoctype, fromId) {
  	var box = this.getBoxById(fromId);
  	var factory = this;

    var line = new UmlAssociation(factory,{assoctype: assoctype});
    var con = box.getBestSuitedConnector(line, null, null, true);
    var association = line.addUnconnectedLine(this,box,null,true);

	$('#'+this.parentId).bind('mousemove',function(event) {
		factory.linkMouseMoveEvent(event,$('#'+line.id), con)
	});     
	
	$('#'+this.parentId).bind('mousedown',function(event) {
		console.log('mousedown (right or left)');
        
		if(event.which == 2 || event.which > 2) { //>2 safety
			console.log('right mouse key pressed.');
			line.unregisterLineDragging(factory);
			factory.quitLinkMode(line.id,factory.parentId);
		} else if (event.which == 1) {
			console.log('left mouse key pressed.');
			var to = $(event.toElement).attr('id');
			var elementTo = factory.getBoxById(to);
            if(elementTo==null) {
                elementTo=factory.getBoxById($(event.toElement).closest('.box').attr('id')); //parent element (Firefox?)
            }
			if(elementTo != null) {
				factory.endLinkMode(line.id, factory.parentId); //unbind all events
				if(elementTo == box) {
                    var res = factory.redrawSelfReference(box,line);
                    if(res != false) {
                    	factory.aeH.registerEvent(new AddLineEvent(line,box,elementTo,'mousedown'));
                    }
                }  else {
            		line.unregisterLineDragging(factory);
                    factory.redrawLine(box,elementTo,line);
                    //hack: otherwise multiple lines between two concepts are not rendered correctly
					factory.moveBoxTo(box, $('#'+box.getId()).position().top, $('#'+box.getId()).position().left);
                    factory.aeH.registerEvent(new AddLineEvent(line,box,elementTo,'mousedown'));
                }
				$('#'+line.id).bind('click',function(event) {
					//alert('clicked the line');
				});
				//factory.aeH.registerEvent(new AddLineEvent(line,box,elementTo,'mousedown'));
			} else {
				
			}
		}
	}); 
	$(document).bind('keyup',function(e) {
		if(e.keyCode == 13) {
			console.log('ENTER - same as left mouse - connect');
			factory.quitLinkMode(line.id,factory.parentId); 
		} else if (e.keyCode == 27) {
			console.log('ESC - same as right mouse');
			factory.quitLinkMode(line.id,factory.parentId);
		}
	});     
  },

  linkMouseMoveEvent: function (event, lineObj, con) {
    if($(lineObj).length > 0) {
    	//offsetX does not work in all browsers
    	var positionX = event.pageX - this.getParent().offset().left;
    	var positionY = event.pageY - this.getParent().offset().top;
    	this.drawStaticLine(con.getTop(), con.getLeft(), positionY, positionX, lineObj);
    }
  },
  
  getLinesConnectedFrom: function(boxFrom, classType) {
  	var res = new Array();
  	for(var i=0; i < boxFrom._factory.lines.length; i++) { //#1 possible improvement here
  		var line = boxFrom._factory.lines[i];
  		if(line.fromId == boxFrom.getId()) {
  			if((classType == null) || classType == line.classType) {
  				res.push(line);
  			}
  		}
  	}
  	return res;
  },

  /* redraw a single line (connection) and all its related elements, such as arrows, labels. */
  redrawLine: function(boxFrom,boxTo,line,isRedrawWhenDrag, isPlaceConnected) {
  	//console.log('line.classType='+line.classType);
      if(line.classType == 'Line') {
      } else if (line.classType == 'SelfReference') {
      	//this.redrawSelfReference(boxFrom,line); //,is
      	return; //CSS does it for us
      } else {
      	console.error('Unknown class type ' + line.classType);
      }
      
	  var conFrom=null;
	  var conTo=null;
	  try {
	  	conFrom = boxFrom.getBestSuitedConnector(line, null, boxTo, isPlaceConnected);
	  	conTo=boxTo.getBestSuitedConnector(line, null, boxFrom, isPlaceConnected);
      } catch (e) {
      	console.error('This should not happen at all!' + boxFrom.mainLabel.getText() + ' details: ' + conFrom + "-" + conTo);
      	var skip = true;
      	if(skip) { //to ease debugging
      		return;
      	}
      }
      var assocLine = this.drawStaticLine(conFrom.getTop(), conFrom.getLeft(), conTo.getTop(), conTo.getLeft(), $('#'+line.id));
      //var pOffset = $('#'+this.parentId).offset();
      //mark(pOffset.top + conFrom.getTop(), pOffset.left + conFrom.getLeft(), 'red');
      //mark(pOffset.top + conTo.getTop(), pOffset.left + conTo.getLeft(), 'blue');
      if(!isRedrawWhenDrag) {
          line.fromId=boxFrom.getId();
          line.toId=boxTo.getId();
          this.addLine(line);
      }
      var mathProcessor = new MathProcessor();
      line.renderConnectedLine(this,mathProcessor,boxTo);
  },

  /* redraw self-reference */   
    redrawSelfReference: function(box,line,isRedrawWhenDrag) {
        if( line.options.assoctype == 'generalization') { //no self-connected generalization!
        	line.destroy();
        	return false;
        }
        if(isRedrawWhenDrag) {
        	var id = line.id;
        	var cssBox = $('#'+box.getId());
        	var sr = $('#'+id);
        	var cssRef = $('#'+id);
        	var thisSelfReferences = this.getLinesConnectedFrom(box, 'SelfReference'); //self-references connected to that entity
        	var noSelfReferences = thisSelfReferences.indexOf(line);
        	/*
        	if(isRedrawWhenDrag) {
        		noSelfReferences = Math.max(0, noSelfReferences - 1);
       		 }
       		 */
        	var scale =  40;
        	var offsetS = scale/2 * (noSelfReferences);
        	cssRef.css('width', cssBox.width());
        	cssRef.css('height', scale);
       
        	sr.css('top',cssBox.position().top + offsetS + cssBox.height() - scale/2);
        	sr.css('left', cssBox.position().left + (scale/2)*((-1)*(1+noSelfReferences)));
			sr.css('z-index', cssBox.css('z-index') - 1 - noSelfReferences);
		
            var mathProcessor = new MathProcessor();
        	line.renderConnectedLine(this,mathProcessor,box);
        	return true;
        }
        
        var con=box.getBestSuitedConnector(line);
        var oldId = line.getId();
        line.destroy();
        var id = oldId;// generateId('self-ref');
        var r = line.buildLine(box._factory , id, 'self-reference'); //'<div id="'+id+'" class="self-reference association_line"></div>';
        
        $('#'+box._factory.getParent().attr('id')).append(r);
    	line.id = id; //update id otherwise the old elements will not be removed from surface when drag&dropping
        //$('#'+box.getId()).append(r);
        var cssBox = $('#'+box.getId());
        var sr = $('#'+id);
        
        var cssRef = $('#'+id);
        
        var noSelfReferences = this.getLinesConnectedFrom(box, 'SelfReference').length; //number of self-references connected to that entity
        if(isRedrawWhenDrag) {
        	noSelfReferences = Math.max(0, noSelfReferences - 1);
        }
        
        var scale =  40;
        var offsetS = scale/2 * (noSelfReferences);
        cssRef.css('width', cssBox.width());
        cssRef.css('height', scale);
       
        sr.css('top',cssBox.position().top + offsetS + cssBox.height() - scale/2);
        sr.css('left', cssBox.position().left + (scale/2)*((-1)*(1+noSelfReferences)));
		sr.css('z-index', cssBox.css('z-index') - 1 - noSelfReferences);
        var assoctype= line.options.assoctype;
        if(!isRedrawWhenDrag) {
			line = new SelfReference(box._factory ,{assoctype: assoctype});
        	line.id=id;
            line.fromId=box.getId();
            line.toId=box.getId();

            this.addLine(line);
        } else {
        	 line.fromId=box.getId();
          	line.toId=box.getId();
        }
        var mathProcessor = new MathProcessor();
        line.renderConnectedLine(this,mathProcessor,box);
        return true;
    },


  /* redraw all lines connected to a specific box in spite of movement */
  redrawLines: function(factory,box) {
       var lines = factory.getBoxConnectionsById(box.getId());
        for(var i = 0; i < lines.length; i++) {
        	var line = lines[i];
        	var conFrom = box.getBestSuitedConnector(line);
            if(line.classType == 'SelfReference') {
            	factory.redrawSelfReference(box,factory.getLineById(line.id),true);
            	continue;
            }
            
            var toId = (line.toId==box.getId()) ? line.fromId : line.toId;
            var boxTo = factory.getBoxById(toId);
            var conTo = boxTo.getBestSuitedConnector(line);

            var lineObj = $('#'+lines[i].id);
            //this fixed point is not the initial point then we have a problem.
            lines[i].destroy();
            var association = line.addUnconnectedLine(this,boxTo, lines[i].id,false);
            factory.redrawLine(boxTo,box,factory.getLineById(line.id),true);
        }
    },

    /* draw a static line from (x,y) to (x1,y2) */
    drawStaticLine: function (originTop, originLeft, toTop, toLeft, lineObj) {
        var length = Math.sqrt((toLeft - originLeft) * (toLeft - originLeft) 
            + (toTop - originTop) * (toTop - originTop));
    
        var angle = 180 / 3.1415 * Math.acos((toTop - originTop) / length);
        if(toLeft > originLeft) {
            angle *= -1;
        }
    
        $(lineObj)
            .css('height', length)
            .css('-webkit-transform', 'rotate(' + angle + 'deg)')
            .css('-moz-transform', 'rotate(' + angle + 'deg)')
            .css('-o-transform', 'rotate(' + angle + 'deg)')
            .css('-ms-transform', 'rotate(' + angle + 'deg)')
            .css('transform', 'rotate(' + angle + 'deg)');
        return $(lineObj);

    },
    
  	endLinkMode: function(lineId, parentId) {
    	$('#'+this.parentId).unbind('mousedown');
    	$('#'+this.parentId).unbind('mousemove');
    	$(document).unbind('keyup'); 
  	},
  	
  	quitLinkMode: function(lineId,parentId) {
    	$('#'+lineId).remove();
    	$('#'+this.parentId).unbind('mousedown');
    	$('#'+this.parentId).unbind('mousemove');
    	$(document).unbind('keyup'); 
	},

    changeLineLabel: function(lineId, content) {
        var line = this.getLineById(lineId);
        line.refreshMainLabel(this,content);
    },

    humanChangeLineLabel: function(lineId,content,event) {
        this.changeLineLabel(lineId,content);
        this.aeH.registerEvent(new ChangeLabelEvent(lineId,this.getParent(),event)); 
    },

    addLineLabelsEndPoints: function(lineId,textFrom,textTo) {
        var line = this.getLineById(lineId);
        if(line.endpointLabels['from'] == null || line.endpointLabels['from']._text=='') {
            line.refreshLineLabelEndpoint(this,textFrom==null ? textFrom : textFrom,'from');
        }
        if(line.endpointLabels['to'] == null || line.endpointLabels['to']._text=='') {
            line.refreshLineLabelEndpoint(this,textTo==null ? textTo : textTo,'to')
        }
    },

    humanAddLineLabelsEndPoints: function(lineId, defaultText) {
        return this.addLineLabelsEndPoints(lineId,defaultText,defaultText);
    },

    zoomIn: function() {
        this._setScale(this._zoom+0.1);
    },

    zoomOut: function() {
        this._setScale(this._zoom-0.1);
    },

    resetZoom: function() {
        this._setScale(1.0);
    },

    /** provide a serializable format of the model, e.g. as JSON so that it can be persistent and reloaded on demand */
    serializeModel:function() {
        alert('NOT IMPLEMENTED YET!');
    },

    /** reloads the model, e.g. when undo-ing */
    loadModel:function(json,isReload) {
        alert('NOT IMPLEMENTED YET!');
    },

    humanUndo:function() {
        this.aeH.triggerUndo();
    },

    humanRedo:function() {
        this.aeH.triggerRedo();
    },


    _setScale: function(zoom) {
        var el = $('#'+this.getParentId());
        this._zoom=zoom;
        el.css('zoom',zoom);
        el.css('MozTransform','scale('+zoom+')');
        el.css('WebkitTransform','scale('+zoom+')');
        /*
        el.style.zoom = zoom;
        el.style.MozTransform = 'scale('+zoom+')';
        el.style.WebkitTransform = 'scale('+zoom+')';
        */
    }
});