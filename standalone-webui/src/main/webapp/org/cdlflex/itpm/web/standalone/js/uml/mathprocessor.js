/**
  * provides mathematical functions for calculation of certain graph behavior.
**/
var MathProcessor =  Class.extend({
	
    _intersectionPoint: function(x1, y1, x2, y2, x3, y3, x4, y4){
  		var DONT_INTERSECT = null;
		var COLLINEAR = null;
		var DO_INTERSECT = 2;

  		var a1, a2, b1, b2, c1, c2;
  		var r1, r2 , r3, r4;
  		var denom, offset, num;

  		// Compute a1, b1, c1, where line joining points 1 and 2
  		// is "a1 x + b1 y + c1 = 0".
		a1 = y2 - y1;
		b1 = x1 - x2;
		c1 = (x2 * y1) - (x1 * y2);

		// Compute r3 and r4.
		r3 = ((a1 * x3) + (b1 * y3) + c1);
		r4 = ((a1 * x4) + (b1 * y4) + c1);

		// Check signs of r3 and r4. If both point 3 and point 4 lie on
		// same side of line 1, the line segments do not intersect.
		if ((r3 != 0) && (r4 != 0) && this._sameSign(r3, r4)){
			return DONT_INTERSECT;
		}

  		// Compute a2, b2, c2
  		a2 = y4 - y3;
  		b2 = x3 - x4;
  		c2 = (x4 * y3) - (x3 * y4);

  		// Compute r1 and r2
  		r1 = (a2 * x1) + (b2 * y1) + c2;
  		r2 = (a2 * x2) + (b2 * y2) + c2;

  		// Check signs of r1 and r2. If both point 1 and point 2 lie
  		// on same side of second line segment, the line segments do
  		// not intersect.
  		if ((r1 != 0) && (r2 != 0) && (this._sameSign(r1, r2))){
    		return DONT_INTERSECT;
  		}

  		//Line segments intersect: compute intersection point.
  		denom = (a1 * b2) - (a2 * b1);

  		if (denom == 0) {
  			console.info('Colinarity of lines: ' + new Array(x1,y1,x2,y2,x3,y3,x4,y4));
    		return COLLINEAR;
  		}

  		if (denom < 0){ 
    		offset = -denom / 2; 
  		} 
  		else {
    		offset = denom / 2 ;
  		}

  		// The denom/2 is to get rounding instead of truncating. It
  		// is added or subtracted to the numerator, depending upon the
  		// sign of the numerator.
  		num = (b1 * c2) - (b2 * c1);
  		if (num < 0){
  		  x = (num - offset) / denom;
  		} 
  		else {
    		x = (num + offset) / denom;
  		}

  		num = (a2 * c1) - (a1 * c2);
  		if (num < 0){
   			y = ( num - offset) / denom;
  		} 
  		else {
    		y = (num + offset) / denom;
  		}

  		// lines_intersect
  		return new Array(x,y);
    },
    
    _sameSign: function(a, b){
  		return (( a * b) >= 0);
	},
	

    intersectionPointAdvanced: function(p1X, p1Y, p2X, p2Y, p1XB, p1YB, p2XB, p2YB, isHorizontal, isDebug) {
        var x = this._intersectionPoint(p1X, p1Y, p2X, p2Y, p1XB, p1YB, p2XB, p2YB, isHorizontal);
      	
      	if(isDebug) {
      		drawHelperLine(p1X, p1Y, p2X, p2Y, 'purple');
      		drawHelperLine(p1XB, p1YB, p2XB, p2YB, 'green');
       	 	$('body').append('<div style="position:absolute;background-color:purple;width:20px;height:20px;z-index:500000;' + 
        		'top:'+p1X+'px;left:'+p1Y+'px"></div>')
        }
        if(x==null) {
            return x;
        } else {
            return x;
            /*    geht nicht so
             // if((p1XB-5 < x[0] < p2XB+ 5) && p || (p2XB-5 < x[0] < p2XB+ 5) || (p1YB-5 < x[1] < p1YB+ 5) ||(p2YB-5 < x[1] < p2YB+ 5))   {
             if((p1XB-5 < x[0] < p1XB+ 5) || (p2XB-5 < x[0] < p2XB+ 5) || (p1YB-5 < x[1] < p1YB+ 5) ||(p2YB-5 < x[1] < p2YB+ 5))   {
             return x;
             } else {
             return null; //not on straight line
             }*/


        }
    },

    /* returns the center of the line, i.e. the length 
    not in use any more (?)
    getCenterOfLine: function(boxFrom,boxTo) {
        var b0 = boxFrom.getBestSuitedConnector()
        var b1 = boxTo.getBestSuitedConnector();
        var length = this.computeDistance(b0.getTop(),b0.getLeft(),b1.getTop(),b1.getLeft());
        return length/2;
    },
    */

	/**
	  * lineElem is the shape-element, not the css element.
	**/
    closestIntersectionPoint: function(boxFrom,boxTo,lineElem, isDebug) {
        var conFrom = boxFrom.getBestSuitedConnector(lineElem);
        return this.closestIntersectionPointCoordinates(conFrom.getTop(),conFrom.getLeft(),boxTo, lineElem, isDebug);
    },


    closestIntersectionPointCoordinates: function(fromTop,fromLeft, boxTo, lineElem, isDebug) {
        //var conFrom = boxFrom.getBestSuitedConnector();
        var conTo = boxTo.getBestSuitedConnector(lineElem);
        this._closestBorder='';
        var eTo = $('#'+boxTo.getId());
        var t = eTo.position().top; //important: position=relative to parent element; otherwise it would be offset()
        var l = eTo.position().left;
        
        if(isDebug) {
        	console.log('mark fromTop, fromLeft purple...');
        	mark(fromTop, fromLeft, 'purple', 10);
        }

        var leftBorder = new Array(t+eTo.height(), l, t, l);
        var rightBorder = new Array(t+eTo.height(),l+eTo.width(), t, l + eTo.width());
        var topBorder = new Array(t,l, t, l + eTo.width());
        var bottomBorder = new Array(t+eTo.height(),l,t+eTo.height(),l+eTo.width());

        var borders = new Array(leftBorder,rightBorder,topBorder,bottomBorder);
        for(var i=0; i < borders.length; i++) {
        	var b = borders[i];
        	if(isDebug) {
            	drawHelperLine(b[0], b[1], b[2], b[3]);
            } else {
            	//drawHelperLine(b[0], b[1], b[2], b[3], 'green');	
            }
        }
        var minDistance = -1;
        var clip=null;
        var minText = 'UNKNOWN';
        var texti = new Array('LEFT','RIGHT','TOP','BOTTOM');
        for(var i = 0; i < borders.length; i++) {
            var b = borders[i];
            if(isDebug) {
            	console.log("intersectionPointAdvanced: (" + fromTop + "," + fromLeft + "), (" + 
            										  	 conTo.getTop() + "," + conTo.getLeft() + ")", "(" +
            										  	 b[0] + "," + b[1] + ")", "(" +
            										  	 b[2] + "," + b[3] + ")");
            }
           
            var x = this.intersectionPointAdvanced(fromTop,fromLeft,conTo.getTop(),conTo.getLeft(),
                b[0],b[1],b[2],b[3],i>1
            );

            if(x==null) {
                continue;
            }

            if(!((Math.min(b[0],b[2])-5 <= x[0] && x[0] <= Math.max(b[0],b[2])+5)
                && (Math.min(b[1],b[3])-5 <= x[1] && x[1] <= Math.max(b[1],b[3])+5))) {

                continue; //skip entries outside the range
            }
            //x[0]=top; x[1]=left

            /*
             var x = this.intersectionPoint(fromTop,fromLeft,conTo.getTop(),conTo.getLeft(),
             b[0],b[1],b[2],b[3],i>1
             );
             */
            /*
             if(i>1) {
             $('body').append('<span style="position:absolute;z-index:10000;width:10px;height:10px;background-color:red;top:'+x[0]+';left:'+x[1]+'"></span>');
             //$('body').append('<span style="position:absolute;z-index:10000;width:10px;height:10px;background-color:red;top:'+b[2]+';left:'+b[3]+'"></span>');
             } else {
             $('body').append('<span style="position:absolute;z-index:10000;width:10px;height:10px;background-color:green;top:'+x[0]+';left:'+x[1]+'"></span>');
             }
             */

            var distance = this.computeDistance(x[0],x[1],fromTop,fromLeft);

            if(minDistance==-1 || distance < minDistance) {
                minDistance=distance;
                clip=x; //closest intersection point
                minText=texti[i];
                this._closestBorder=minText;
            }
        }
        //console.log('CLOSEST BORDER IS ' + minText);
        return clip;
    },

    labelPositionEndpoint: function(boxFrom,boxTo, lineElem) {
        var conFrom = boxFrom.getBestSuitedConnector(lineElem);
        var point = this.closestIntersectionPointCoordinates(conFrom.getTop(),conFrom.getLeft(),boxTo, lineElem);
        var b=this._closestBorder;
        if(b=='TOP') {
              point[0]-=25;
              point[1]+=5;
        } else if (b=='BOTTOM') {
            point[0]+=20;
            point[1]-=5;
        } else if (b=='LEFT') {
            point[0]-=25;
            point[1]-=35;
        } else if (b=='RIGHT') {
            point[0]-=25;
            point[1]+=20;
        } else {
            console.error('no closest border found for ' + boxFrom.mainLabel.getText() + ' and ' + boxTo.mainLabel.getText());
        }
        return point;
    },

    closestIntersectionPointDistance: function(boxFrom,boxTo, lineElem) {
        var clip = this.closestIntersectionPoint(boxFrom,boxTo);
        ///console.log(":::"+clip);
        var conE = boxTo.getBestSuitedConnector(lineElem);
        var iTop = clip[0];
        var iLeft = clip[1];
        var c = this.computeDistance(conE.getTop(),conE.getLeft(),iTop,iLeft);
        return c;
    },

    computeDistanceEfficientViaClip: function(boxFrom,boxTo,clip, lineElem) {
    	if(clip==null) {
    		var c = null;	
    	}
        var conE = boxTo.getBestSuitedConnector(lineElem);
        if(clip == null) {
        	console.error('We have a problem: boxTo does not return a valid connector (boxToId=' + boxTo.getId() + ')');
        }
        var iTop = clip[0];
        var iLeft = clip[1];
        var c = this.computeDistance(conE.getTop(),conE.getLeft(),iTop,iLeft);
        return c;
    },

    /* Euclidean distance */
    computeDistanceViaPoints: function(p1,p2) {
         return this.computeDistance(p1[0],p1[1],p2[0],p2[1]);
    },

    /* Euclidean distance */
    computeDistance: function(x1,y1,x2,y2) {
        var a = Math.max(x1, x2) - Math.min(x1, x2);
        var b = Math.max(y1,y2) - Math.min(y1,y2);
        var c = Math.sqrt(a*a + b*b);
        return c;
    },

    /* returns point in the middle of the line */
    getMiddleOfLine: function(conFrom,conTo) {
        //mark(conFrom.getTop(),conFrom.getLeft(),'pink');
        //mark(conTo.getTop(),conTo.getLeft(),'red');
        var top  =  Math.min(conFrom.getTop(),conTo.getTop()) + Math.abs(conFrom.getTop()-conTo.getTop())/2;
        var left =  Math.min(conFrom.getLeft(),conTo.getLeft()) + Math.abs(conFrom.getLeft()-conTo.getLeft())/2;
        return new Array(top,left);
    },
    
    _push: function(a, x, y) {
    	var exists=false;
    	for(var i=0; i<a.length; i++) {
    		if(a[i][0]==x && a[i][1] == y) {
    			exists=true;
    			break;
    		}
    	}
    	
    	if(!exists) {
    		a.push(new Array(x,y));
    	}
    },
    
    computeIntersectionRectangulars: function(boxFrom, boxTo) {
    	var intersections = new Array();
    	
    	var boxFromCss  = $('#' + boxFrom.getId());
    	var boxToCss 	= $('#' + boxTo.getId());
    	
    	//arrays: point 1, point2, and x (or y)
    	var a1s = this._getLines(boxFromCss);
    	var a2s = this._getLines(boxToCss);
    	
    	for(var i=0; i < a1s.length; i++) {
    		var x = a1s[i];
    		for(var j=0; j < a2s.length; j++) {
    			var y = a2s[j];
    			if(x[0] != y[0]) { //intersection can only take place between horizontal and vertical line
    				var fromx = Math.min(x[1],x[2]);
    				var tox   = Math.max(x[1],x[2]);
    				var fromy = Math.min(y[1],y[2]);
    				var toy   = Math.max(y[1],y[2]);
    				
    				if((fromx <= y[3]) && (tox >= y[3]) //in range
    				&& (fromy <= x[3]) && (toy >= x[3])) {
    					//intersection
    					//intersections.push(new Array(x[2],y[2]));
    					var p=0,q=0;
    					if(x[0] == 'horizontal') {
    						p=x[3];
    						q=y[3];
    						//intersections.push(new Array(x[3],y[3]));
    					} else {
    						//intersections.push(new Array(y[3],x[3]));
    						p=y[3];
    						q=x[3];
    					}
    					this._push(intersections, p, q);
    				} 
    			}
    		}
    	}
    	return intersections;
    },
    
    /* get an array of lines. the last position in the line states always the horizontal/vertical position, the second and the third two points indicate the range */
    _getLines: function(box) {
    	var dc = new Array('horizontal', box.position().left,  box.position().left + box.width(), box.position().top); //horizontal top line
    	var ab = new Array('horizontal', box.position().left,  box.position().left + box.width(), box.position().top + box.height()); //horizontal bottom line
    	var ad = new Array('vertical',   box.position().top,   box.position().top + box.height(), box.position().left);
    	var bc = new Array('vertical',   box.position().top,   box.position().top + box.height(), box.position().left + box.width());
    	var a = new Array(dc, ab, ad, bc);
    	return a;
    }
});