/* Handler which can be used/extended to implement specific label events */
var LabelHandler = Class.extend({
    /* either box, line, etc. */
    init:function(elemContainer) {
         this.elemContainer=elemContainer;
    },

    /* Persists a new label somewhere */
    addToRepository:function(label) {
       console.error('must be overriden.');
    },

    /* Removes a label from any (central) repository */
    removeFromRepository:function(label) {
        console.error('must be overriden.');
    },

    /* Updates existing Label in (central) repo */
    updateInRepository:function(l,editableElem, val) {
        console.error('must be overriden.');
    },

    /* Additional events can be attached to the editing element (for instance key press, change, etc.) */
    additionalEditingEvents: function(l, editableElem) {

    },

    /* Validate and process the data before storing
     returns null if data was invalid, otherwise the processed string.
     */
    validateInput: function(inputData) {
        if(inputData.length>=2) {
            return inputData;
        } else {
            return null;
        }
    }
});


/* Handler which can be used/extended to implement specific label events */
var UmlAttributeHandler = LabelHandler.extend({
    init:function(box) {
        this._box=box;
        this._super(box);
    },

    /* Persists a new label somewhere */
    addToRepository:function(label) {
        this._box._subLabels.push(label);
    },

    /* Removes a label from any (central) repository */
    removeFromRepository:function(label) {
        this._box.removeSubLabelById(label._id);
    },

    /* Updates existing Label in (central) repo */
    updateInRepository:function(l,editableElem, val) {
        var label = this._box.getSubLabelById(editableElem.closest('.'+l._cssClass).attr('id'));
        label.setText(val);
    },

    /* Additional events can be attached to the editing element (for instance key press, change, etc.) */
    additionalEditingEvents: function(l, editableElem) {
        var handler = this;
        editableElem.find('input[type=text]').on('keydown',function(e) {
            if(e.which==9) { //tab - save and create a new attribute
                //check if current line is not empty and longer/greater than 2 characters at least
                if(l.escapeEditableModel(l,true,editableElem,true)) {
                    var label = new Label(l._DEFAULT_ATTR, l._cssClass, handler);
                    label.renderLabel($('#'+handler._box.getId()));
                    label.clickEvent();
                }
                handler._focusPocus(handler._box.getId());
                /*
                $('#'+handler._box.getId()).find('input[type=text]').focus(); //in any case put focus on current field,
                //otherwise the hidden dummy field will be shown
                */
            }
        });
        
        handler._focusPocus(l._id);
    },
    
    /* necessary for tab and the related blur-event, at least in Chrome (hack)  */
    _focusPocus: function(id) {
    	setTimeout(function() {
            $('#'+id).find('input[type=text]').focus();
        }, 100); 
    }
});

/* Handler which can be used/extended to implement specific label events */
var UmlClassMainLabelHandler = LabelHandler.extend({
    init:function(box) {
        this._box=box;
        this._super(box);
    },

    /* Persists a new label somewhere */
    addToRepository:function(label) {
        this._box.mainLabel=label;
    },

    /* Removes a label from any (central) repository */
    removeFromRepository:function(label) {
        console.error('not implemented!');
    },

    /* Updates existing Label in (central) repo */
    updateInRepository:function(l,editableElem, val) {
        this._box.mainLabel.setText(val);
    }
});


/* Handler which can be used/extended to implement specific label events */
var MainLabelHandler = LabelHandler.extend({
    init:function(line) {
        this._line=line;
        this._super(line);
    },

    /* Persists a new label somewhere */
    addToRepository:function(label) {
        this._line.mainLabel=label;
    },

    /* Removes a label from any (central) repository */
    removeFromRepository:function(label) {
        console.error('not implemented yet!');
    },

    /* Updates existing Label in (central) repo */
    updateInRepository:function(l,editableElem, val) {
        this._line.mainLabel.setText(val);
    }
});


var EndpointLabelHandler = LabelHandler.extend({
    init:function(line,labelType) {
        this._line=line;
        this._labelType=labelType;
        this._super(line);
    },

    /* Persists a new label somewhere */
    addToRepository:function(label) {
        this._line.endpointLabels[this._labelType]=label;
    },

    /* Removes a label from any (central) repository */
    removeFromRepository:function(label) {
        this._line.endpointLabels[this._labelType].destroy();
        this._line.endpointLabels[this._labelType]=null;
    },

    /* Updates existing Label in (central) repo */
    updateInRepository:function(l,editableElem, val) {
        this._line.endpointLabels[this._labelType].setText(val);
    },

    /* Validate and process the data before storing
     returns null if data was invalid, otherwise the processed string.
     */
    validateInput: function(inputData) {
        if(inputData.length<=8) {
            return inputData;
        } else {
            return null;
        }
    }
});