var redraw, g, renderer;

$.hierarchyGraph = function(width,height) {
	var g = new Graph();
	var aEntered = new Array();
	
	var imageRenderer = function(r, n) {
        /* the Raphael set is obligatory, containing
            all you want to display */
        var set = r.set().push(
            r.image("../img/collaborator.png", 20, 20, 32, 32))
            .push(r.text(40, 60, n.label || node.id));
        return set;
    };
    
	var imageRendererPM = function(r, n) {
        /* the Raphael set is obligatory, containing
            all you want to display */
        var set = r.set().push(
            r.image("../img/superior.png", 20, 20, 25, 32))
            .push(r.text(40, 60, n.label || node.id));
        return set;
    };
    
    var defaultRenderer = function(r, node) {
        /* the default node drawing */
        var color = '#7cbf00';//Raphael.getColor();
        var stroke = '#7cbf00';
        var agruenwaldOffset=35;
        var ellipse = r.ellipse(agruenwaldOffset, agruenwaldOffset, 20, 15).attr({fill: color, stroke: stroke	, "stroke-width": 2});
        /* set DOM node ID */
        ellipse.node.id = node.label || node.id;
        shape = r.set().
            push(ellipse).
            push(r.text(agruenwaldOffset, agruenwaldOffset+30, node.label || node.id));
        return shape;
    };
    
    var defaultRendererPM = function(r, node) {
        /* the default node drawing */
        var color = '#bf5600';//Raphael.getColor();
        var stroke = '#bf5600';
        var agruenwaldOffset=35;
        var ellipse = r.ellipse(agruenwaldOffset, agruenwaldOffset, 20, 15).attr({fill: color, stroke: stroke	, "stroke-width": 2});
        /* set DOM node ID */
        ellipse.node.id = node.label || node.id;
        shape = r.set().
            push(ellipse).
            push(r.text(agruenwaldOffset, agruenwaldOffset+30, node.label || node.id));
        return shape;
    };
    
	this.resetGraph = function() {
		g= new Graph();
		aEntered = new Array();
	},
	
	this.drawGraph = function(width,height) {
		var gParent = $('#hierarchy_graph').parent();
	    $('#hierarchy_graph').remove();
	    gParent.append('<div id="hierarchy_graph"></div>')
	    if(aEntered.length<=1) { //only one node - don't visualize it
	    	return;
	    }
		var layouter = new Graph.Layout.Spring(g);
		var renderer = new Graph.Renderer.Raphael('hierarchy_graph', g, width, height);    
		redraw = function() {
	        layouter.layout();
	        renderer.draw();
	    };
	},
	
	this.addCollaborator = function(collaborator,isPM,isStakeholder) {
		for(var i=0; i<aEntered.length;i++) {
			if(aEntered[i]==collaborator) {
				return;
			}
		}
		aEntered.push(collaborator);
		if(isPM) {
			g.addNode(collaborator, {render: defaultRendererPM, label: collaborator});
		} else {
			g.addNode(collaborator, {render: defaultRenderer, label: collaborator});	
		}
			    

	},
	
	this.addManagesRelation = function(superior,collaborator) {
		g.addEdge(superior, collaborator, { directed : true } );
	}
}
	