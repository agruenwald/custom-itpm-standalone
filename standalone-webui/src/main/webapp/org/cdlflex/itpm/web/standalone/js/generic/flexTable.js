/*
 * Reduces the columns of any marked table based on a priority heuristic and the
 * column names as long as the table exceeds the max-size.
 * class="flextable" activates the algorithm.
*/
var FlexTable = Class.extend({
	init: function(table, firstColIsCheckbox) {
		this.table = table;
		this.firstColIsCheckbox = (firstColIsCheckbox == null? true : firstColIsCheckbox);
		this.ignoreTitles = new Array();
		this.ignoreTitles.push('details');
		this.ignoreTitles.push('origin');
//		this.ignoreTitles.push('name');
	},
	
	/**
	 * Get the names of the titles in natural order
	**/
	getTitles: function() {
		var ths = this.table.find('th');
		var titles = new Array();
		var flexTab = this;
		$(ths).each(function() {
			var title = $(this).text().toLowerCase();
			var cssTitle = $(this).find('input:checkbox').hasClass();
			if (flexTab.ignoreTitles.indexOf(title) < 0
			&& $(this).find('input:checkbox').length <= 0) {
				titles.push(title);
			}
		});
		return titles;
	},
	
	getMaxWidth: function() {
		return this.table.parent().width();
	},
	
	getActWidth: function() {
		return this.table.width() - 100;
	},
	
	getCol: function(idx, includeHeader) {
		var sel = null;
		idx = this.getColIOffset(idx);
		if(includeHeader) {
			sel = this.table.find('tr').find('td:eq('+idx+'),th:eq('+idx+')');
		} else {
			sel = this.table.find('tr').find('td:eq('+idx+')');
		}
		return sel;
	},
	
	getColIOffset: function(idx) {
		idx = this.firstColIsCheckbox ? (idx + 1) : idx;
		return idx;
	},
	
	/**
	  * detect boolean columns (all content is either true or false) and
	  * return the indices of the matched columns.
	**/
	getBooleanColIndices: function() {
		var titles = this.getTitles();
		var boolCols = new Array();
		for (var i = 0; i < titles.length; i++) {
			var col = this.getCol(i, false);
			var onlyTrueFalse = col.length > 0 ? true : false;
			for (var j=0; j < col.length; j++) {
				var text = $(col[j]).text().trim();
				if (text == 'true' || text == 'false') {
					//ok
				} else {
					onlyTrueFalse = false;
					break;
				}
			}
			
			if (onlyTrueFalse) {
				boolCols.push(i);
			}
		}
		return boolCols;
	},
	
	/**
	  * get those columns which undercut a certain string length in all of their fields
	**/
	getColWithLowLength: function(minLength) {
		var titles = this.getTitles();
		var candidates = new Array();
		for (var i = 0; i < titles.length; i++) {
			var col = this.getCol(i, false);
			var satisfied = true;
			for (var j=0; j < col.length; j++) {
				var text = $(col[j]).text().trim();
				if (text.length >= minLength) {
					satisfied = false;
					break;
				}
			}
			
			if (satisfied) {
				candidates.push(i);
			}
		}
		return candidates;
	},
	
	/**
	  * get those columns which are empty to a certain percentage
	**/
	getColEmptyPerc: function(perc) {
		var titles = this.getTitles();
		var candidates = new Array();
		for (var i = 0; i < titles.length; i++) {
			var col = this.getCol(i, false);
			var empty = 0;
			for (var j=0; j < col.length; j++) {
				var text = $(col[j]).text().trim();
				if (text.length <= 0) {
					empty++;
				}
			}
			
			var emptyPerc = 0;
			if (col.length > 0) {
				emptyPerc = (empty/col.length) * 100;
			}
			
			if (emptyPerc > perc) {
				candidates.push(i);
			}
		}
		return candidates;
	},
	
	/**
		get those cols which have the same content all over 
		@param ignoreEmpty if ignoreEmpty is on then empty fields will not count
		as different (they will be ignored literally).
		@param numberOfDistinctValues
	**/
	getColSameContent: function(ignoreEmpty, minValues) {
		var titles = this.getTitles();
		var candidates = new Array();
		for (var i = 0; i < titles.length; i++) {
			var col = this.getCol(i, false);
			var distinctValues = new Array();
			var empty = 0;
			for (var j=0; j < col.length; j++) {
				var text = $(col[j]).text().trim();
				if (distinctValues.indexOf(text) < 0) {
					if (ignoreEmpty || text != '') {
						distinctValues.push(text);
					}
				}
			}
			
			var maximum = minValues == null ? 1 : minValues;
			if (distinctValues.length <= maximum) {
				candidates.push(i);
			}
		}
		return candidates;
	},
	
	removeColumn: function(idx) {
		//var offset = this.firstColIsCheckbox ? 1 : 0;
		//idx = idx + offset;
		var col = this.getCol(idx,true);
		col.remove();
	},
	
	reduce: function() {
		var i = 0;
		var FULLTEST = false;
		this.reduceIdIfNameAvailable(); //remove id in any case if name available
		var iRemoved = 100;
		var phase1Done = false;
		//maximum of 6 cols (?)
		var MAX_COLS = 6;
		while ((FULLTEST || (this.getTitles().length > MAX_COLS) 
		|| (iRemoved > 0 && ((this.getActWidth() > this.getMaxWidth())) && i < 20))) {
			console.log(this.getActWidth() + '>' + this.getMaxWidth());
			if (!phase1Done) {
				var iRemoved = this.heuristic();
				if (iRemoved == 0) {
					phase1Done = true;
					iRemoved=1; //to continue
					continue;
				}
			} else {
				//remove the last column successifely
				var lastCol = this.getCol(this.getTitles().length-1, true);
			
				//var lastInd = this.getColIOffset(this.getTitles().length-1);
				//var lastCol = this.getCol(lastInd, false);
				lastCol.remove();
				//this.removeColumn(lastInd);
				iRemoved=1;
				console.log('Remove last columns..' + this.getTitles().length);
			}
			console.log('Iteration ' + i + ' (heuristic): ' + iRemoved + ' removed.');
			i++;
		}
		console.log("Finished with i=" + i + " and width " + this.getActWidth() + '>' + this.getMaxWidth());
	},
	
	removeIndices: function(indices) {
		for (var b = indices.length -1; b >= 0; b--) {
			this.removeColumn(indices[b]);
		}
	},
	
	reduceIdIfNameAvailable: function() {
		var titles = this.getTitles();
		if (titles.indexOf('name') >= 0) {
			if (titles.indexOf('id') >= 0) {
				this.removeColumn(titles.indexOf('id'));
				return 1;
			} 
		}
		return 0; 
	},
	
	heuristic: function() {
		var totallyRemoved = this.reduceIdIfNameAvailable();	
		var booleanColumns = this.getBooleanColIndices();
		totallyRemoved += booleanColumns.length;
		this.removeIndices(booleanColumns);
		if (booleanColumns.length <= 0) {
			var shortColumns = this.getColWithLowLength(5);
			totallyRemoved += shortColumns.length;
			this.removeIndices(shortColumns);
			if (shortColumns.length <= 0) {
				var emptyPerc = this.getColEmptyPerc(50);
				totallyRemoved += emptyPerc.length;
				this.removeIndices(emptyPerc);
				if (emptyPerc.length <= 0) {
					var sameContent = this.getColSameContent();
					totallyRemoved += sameContent.length;
					this.removeIndices(sameContent);
				}
			}
		}
		return totallyRemoved;
	},
});


var lastDebugFlexTable = null;	
$(document).ready(function() {
	$('table.flextable.autoprune').each(function() {
		var flexTable = new FlexTable($(this));
		lastDebugFlexTable = flexTable;
		flexTable.reduce();
		console.log(flexTable.getTitles($(this)));
	});
});