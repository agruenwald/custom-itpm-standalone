/**
  UML class diagram shapes (Semantic MetaModel Editor)
**/
var UmlBox = Box.extend({
    init: function(title,cssClass,x,y,id,addDefaultAttribute, additionalCssClasses) {
    	var cssClasses = additionalCssClasses == null ? cssClass : cssClass.concat(' ', additionalCssClasses);
        
        this._super(title,cssClasses,x,y,id,addDefaultAttribute);
        this.abstractClass=false;
        this.interfaceClass=false;
        this.enumClass=false;
        if(addDefaultAttribute) {
            var attr = new Label("+id:String",'umlAttribute',new UmlAttributeHandler(this));
        }
    }
});

var UmlClass = UmlBox.extend({
    init: function(title,x,y,id,addDefaultAttribute,additionalCssClasses) {
    	this._super(title,'umlClass',x,y,id,addDefaultAttribute,additionalCssClasses);
    }
});

var UmlInterface = UmlBox.extend({
    init: function(title,x,y,id,addDefaultAttribute,additionalCssClasses) {
        this._super(title,'umlInterface',x,y,id,addDefaultAttribute,additionalCssClasses);
        this.interfaceClass=true;
    }
});

var UmlAbstract = UmlBox.extend({
    init: function(title,x,y,id,addDefaultAttribute,additionalCssClasses) {
        this._super(title,'umlAbstract',x,y,id,addDefaultAttribute,additionalCssClasses);
        this.abstractClass=true;
    }
});

var UmlEnumClass = UmlBox.extend({
    init: function(title,x,y,id,addDefaultAttribute,additionalCssClasses) {
        this._super(title,'umlEnum',x,y,id,false,additionalCssClasses);
        this.enumClass=true;
        if(addDefaultAttribute) {
        	var attr = new Label("+Constant1:String",'umlAttribute',new UmlAttributeHandler(this));
        }
    }
})

var UmlAssociationSelf = SelfReference.extend({
    init: function(factory,options) {
        this._super(factory,options);
    },
    
    /* exactly the same as in UmlAssociation */
    buildLine: function(factory, lineId, additionalClasses) {
 		var span = '';
        additionalClasses = additionalClasses == null ? '' : additionalClasses;
        if(this.options.assoctype=='association_uni') {
           span='<span class="arrow-simple arrow-to"><span class="arrow-arrow"></span></span></div>';
        } else if (this.options.assoctype=='generalization') {
            span='<span class="arrow-simple arrow-to"><span class="arrow-generalization"></span></span></div>';
        } else if (this.options.assoctype=='generalization') {
           span='<span class="arrow-simple arrow-to"><span class="arrow-generalization"></span></span></div>';
        } else if (this.options.assoctype=='composition') {
            span='<span class="arrow-simple arrow-from"><span class="arrow-composition"></span></span></div>';
        } else if (this.options.assoctype=='aggregation') {
            span='<span class="arrow-simple arrow-from"><span class="arrow-aggregation"></span></span></div>';
        } else if (this.options.assoctype=='aggregation_uni') {
            span='<span class="arrow-simple arrow-from"><span class="arrow-aggregation"></span></span>' +
                '<span class="arrow-simple arrow-to"><span class="arrow-arrow"></span></span>'
                '</div>';
        } else if (this.options.assoctype=='composition_uni') {
            span='<span class="arrow-simple arrow-from"><span class="arrow-composition"></span></span>' +
                '<span class="arrow-simple arrow-to"><span class="arrow-arrow"></span></span>'
            '</div>';
        } 
        var line = '<div class="association_line ' + additionalClasses +' line_fresh" id="'+lineId+'">'+span+'</span></div>';
        return line;
    }
});

var UmlAssociation = Line.extend({
    init: function(factory,options) {
        this._super(factory,options);
    },

    buildLine: function(factory, lineId, additionalClasses) {
        var span = '';
        additionalClasses = additionalClasses == null ? '' : additionalClasses;
        if(this.options.assoctype=='association_uni') {
           span='<span class="arrow-simple arrow-to"><span class="arrow-arrow"></span></span></div>';
        } else if (this.options.assoctype=='generalization') {
            span='<span class="arrow-simple arrow-to"><span class="arrow-generalization"></span></span></div>';
        } else if (this.options.assoctype=='generalization') {
           span='<span class="arrow-simple arrow-to"><span class="arrow-generalization"></span></span></div>';
        } else if (this.options.assoctype=='composition') {
            span='<span class="arrow-simple arrow-from"><span class="arrow-composition"></span></span></div>';
        } else if (this.options.assoctype=='aggregation') {
            span='<span class="arrow-simple arrow-from"><span class="arrow-aggregation"></span></span></div>';
        } else if (this.options.assoctype=='aggregation_uni') {
            span='<span class="arrow-simple arrow-from"><span class="arrow-aggregation"></span></span>' +
                '<span class="arrow-simple arrow-to"><span class="arrow-arrow"></span></span>'
                '</div>';
        } else if (this.options.assoctype=='composition_uni') {
            span='<span class="arrow-simple arrow-from"><span class="arrow-composition"></span></span>' +
                '<span class="arrow-simple arrow-to"><span class="arrow-arrow"></span></span>'
            '</div>';
        } 
        this.options.extraClasses = this.options.extraClasses == null ? '' : this.options.extraClasses;
        var line = '<div class="association_line ' + additionalClasses + ' ' + this.options.extraClasses 
        	+ ' line_fresh" id="'+lineId+'">'+span+'</span></div>';
        return line;
    }
});
