/* A basic label which can be edited and deleted, etc. Needs a handler to work with */
/* Most general Label implementation */
var Label = Class.extend({
    init:function(text,cssClasses,labelHandler, icon, additionalCssClasses) {
        this._labelHandler=labelHandler;
        this._labelHandler.addToRepository(this);
        this._DEFAULT_ATTR='+';
        this._text=text;
        this._style="";
        this._cssClass=cssClasses;
        this._additionalCssClasses=additionalCssClasses;
        this._isEditable = true; //isEditable == null ? true : isEditable;
        this._isRemovable = true; //isRemovable == null? true : isRemovable;
        this._icon = icon;
    },
    
    setEditable: function(editable) {
    	this._isEditable = editable;
    },
    
   	setRemovable: function(rem) {
   		this._isRemovable=rem;
   	},

    setText:function(text) {
        this._text=text.trim();
    },

    getText:function() {
        return this._text.trim();
    },

    getId:function() {
        return this._id;
    },

    _buildHtml:function() {
        this._id = 'label-'+ Math.ceil(Math.random() * 10000000);
        var i = this._icon == null ? '' : this._icon;
        return '<div id="'+this._id+ '" class="single-label '
            +this._cssClass
            + (this._additionalCssClasses == null ? '' : (' ' + this._additionalCssClasses))
            +'" style="'+this._style+'">'+this._buildHtmlLine() + i + '</div>';
    },

    _buildHtmlLine:function() {
        var res = this._text + '<span class="edit iconbox">';
        if(this._isEditable) {
            res += '<span><i class="edit-icon glyphicon glyphicon-pencil change-button"></i></span>';
        }
        if(this._isRemovable){
            res += '<span class="remove"><i class="edit-icon glyphicon glyphicon-minus-sign remove-button"></i></span>'
        }
        res += '</span>';
        return res;
    },

    binds: function() {
        var l = this;
        if(this._isEditable) {
            $('#'+this._id).children().not('.remove').bind('click',function(event) {
                l.clickEvent();
            });
            $('#'+this._id).bind('dblclick',function(event) {  //double click on label triggers edit mode
               l.clickEvent();
            });

            /* handling of remove button */
            $('#'+this._id).find('.remove').on('click', function(event) {
                $(this).off();
                l._labelHandler.removeFromRepository(l);
                l._labelHandler.elemContainer._factory.aeH.registerEvent(new RemoveLabelEvent(l,l._labelHandler.elemContainer,'click'));
                l.destroy();
            });
        }
    },

    clickEvent: function() {
        var htmlElem = $('#'+this._id);
        $('#'+this._id).unbind('click');
        var editableId = 'editable-'+this._id;
        var previousText = this._text;
        htmlElem.html('<span id="'+editableId+'"><input type="text" value="'+this._text+'"/><span class="iconbox"><i class="glyphicon glyphicon-ok save-button"></i></span></span>');
        $(htmlElem).find('input[type=text]').focus(); //put cursor on field
        var l = this;
        var editableElem = $('#'+editableId);
        editableElem.find('.iconbox').on('click',function(event) {
            l.escapeEditableModel(l,true,editableElem,true);  //save and exit
            l._labelHandler.elemContainer._factory.aeH.registerEvent(new ChangeLabelEvent(l,previousText, l._text,''));
        });

        editableElem.find('input[type=text]').on('keyup',function(e) {
            if(e.which==27) {      //ESC
                //escape without saving
                l.escapeEditableModel(l,false,editableElem);
                if(l._text== l._DEFAULT_ATTR) {
                    //empty attribute - remove it
                    l._labelHandler.removeFromRepository(l);
                    l._labelHandler.elemContainer._factory.aeH.registerEvent(new RemoveLabelEvent(l,l._labelHandler.elemContainer,'keyup'));
                    l.destroy();
                    editableElem.off();
                }
            } else if (e.which==13) { //enter
                //save and exit
                l.escapeEditableModel(l,true,editableElem,true);
                l._labelHandler.elemContainer._factory.aeH.registerEvent(new ChangeLabelEvent(l,previousText, l._text,''));
            } else {
                //tab does not work here
            }
        });

        /* additional events, for instance when exist, create a new field */
        this._labelHandler.additionalEditingEvents(l,editableElem);

    },

    escapeEditableModel: function(l,save,editableElem, checkContent) {
        var isValid=false;
        var htmlElem = $('#'+l._id);
        if(save) {
            //var val = $('#'+label.getId()).find('input[type=text]').val();
            var val = htmlElem.find('input[type=text]').val();
            val=val.trim();
            if(checkContent) {
                var res = this._labelHandler.validateInput(val);
                if(res==null) {
                    return false; //wrong
                } else {
                    isValid=true;
                    val=res;
                }
            } else {
                isValid=true;
            }
            this._labelHandler.updateInRepository(l,editableElem,val);
        }
        l.switchToViewMode(l,editableElem);
        return isValid;
    },

    /* changes from edit mode into view mode again */
    switchToViewMode: function(l,editableElem) {
        var htmlElem = $('#'+l._id);
        htmlElem.html(l._buildHtmlLine());
        l.binds();
        $(editableElem).off();
    },

    renderLabel: function(parentE) {
        var id = this._cssClass + Math.ceil(Math.random() * 100000);
        var x = parentE.append(this._buildHtml());
        this.binds();
    },

    /* destroy the HTML elements */
    destroy: function() {
        $('#'+this._id).unbind('click');
        $('#'+this._id).remove();
    }

});

/*
 Box is facilitating the meta objects with graphical shapes
 */
var Box = Class.extend({
    init: function(title,cssClass,x,y,id, cssStyle, icon, additionalCssClasses){
        this.label=new Label(title,cssClass,new UmlClassMainLabelHandler(this), icon);
        this.label._isRemovable=false;
        this._factory=null; //will be initialized correctly afterwards
        
        //improvement 
        x = this._alignHeight(x);
        y = this._alignHeight(y);
        
        this._x=x;
        this._cssClass = cssClass; //main cssClass
        this._additionalCssClasses = additionalCssClasses;
        this._cssStyle = cssStyle;
        this._isDraggable = true;
        
        this._y=y;
        if(id==null) {
            this._id=0;
        } else {
            this._id=id;
        }
        this._subLabels=new Array();
    },
    
    _alignHeight:function(v) {
    	var x = v/10;
    	x = Math.ceil(x);
    	if (x%2 == 1) {
    		x=x-1; //only position in steps of 20 (raster of jquery UI drag)
    	}
    	return x*10;
    },

    getTop:function() {
        return $('#'+this._id).position().top;
    },

    getLeft:function() {
        return $('#'+this._id).position().left;
    },

    getSubLabels:function() {
        return this._subLabels;
    },

    setFactory: function(factory) {
        this._factory=factory;
    },

    getId: function() {
        return this._id;
    },

    setId: function(id) {
        this._id=id;
    },

    setDragging: function(isDraggable) {
    	this._isDraggable = isDraggable;
        $('#'+this.getId()).draggable( "option", "disabled", !isDraggable);
    },

    /* Adds a new Box to the canvas and manages the events of the box */
    add: function() {
        var thisBox=this;

        var id = 0;
        if(this._id==0) {
            id = generateId('box-');//Math.ceil(Math.random() * 100000);
            this.setId(id);
        } else {
            id = this._id;
        }

        var parent = $('#'+this._factory.getParent().attr('id'));
        this._addConnector();
        var off = parent.position();
        var x = this._y; //relative ! + off.top;
        var y = this._x; //relative ! + off.left;
        var absolute = 'position:absolute;top:'+(x)+';'+'left:'+(y)+';' + this._cssStyle;
        var x = $('#'+this._factory.getParent().attr('id'))
            .append('<div class="'+thisBox._cssClass 
            		+ (this.additionalCssClasses == null ? '' : (' ' + this.additionalCssClasses))
            		+ ' box" id="'+id+'" style="'+absolute+'">'+
                //this.label.display()+
            '</div>');
        this.mainLabel.renderLabel($('#'+id));
        
        if(this._isDraggable) {
        	$('#'+id).draggable({ containment: "parent",grid: [ 20,20 ] });
        }
        
        $.each(this._subLabels, function(index,a) {
            a.renderLabel($('#'+id));
        });


        $('#'+id).bind('dblclick',function(event) {
//         	$(this).html('<input type="text" value="'+$(this).html()+'"/>');
            console.log('doublelclicked!' + $(this));
        });

        var connector = this._connector;
        var isDragging=false;
        var tmpTop=0;
        var tmpLeft=0;
        
        if(this._isDraggable) {
			$('#'+id).mousedown(function(e) {
				if(e.which==1) { //actually drag
					//var theSelectedBox=thisBox._factory.getBoxById(id);
					$(window).mousemove(function() {
						isDragging = true;
						tmpTop=thisBox.getTop();
						tmpLeft=thisBox.getLeft();
						thisBox._factory.redrawLines(thisBox._factory,thisBox);
					});
				}
			}).mouseup(function() {
					var wasDragging = isDragging;
					isDragging = false;
					$(window).unbind("mousemove");
					if (!wasDragging) { //was clicking
						console.log('was clicking');
					} else {
						//console.log('was dragging'); - drop
						thisBox._factory.aeH.registerEvent(new RelocateEvent(thisBox,'mouseup',new Array(tmpTop,tmpLeft),new Array(thisBox.getTop(),thisBox.getLeft())));
						thisBox._factory.autoAdaptDiagramHeight(); //increase height of diagram automatically if necessary
					}
				});
		}
    },

    destroy: function() {
        $('#'+this._id).remove();
    },

    changeTitle: function(title) {
        this.label._text=title;
    },

    getSubLabelById: function(id) {
        for(var i=0; i < this._subLabels.length;i++) {
            if(this._subLabels[i].getId()==id) {
                return this._subLabels[i];
            }
        }
    },

    removeSubLabelById: function(id) {
        for(var i=0; i < this._subLabels.length;i++) {
            if(this._subLabels[i].getId()==id) {
                return this._subLabels.remove(i);
            }
        }
    },
    
    _addConnector: function() {
    	this._connector = new CenterConnector(this._id,this._factory.getParent().attr('id'));
    },

    getBestSuitedConnector: function(con, connections, boxTo, isNew, relMousePosTop, relMouseTopLeft) {
        if (connections == undefined) {
        	//console.error('Fix that: ' + con);
        	connections = this._factory.getBoxConnectionsById(this.getId());
        }
        
        if (isNew == undefined && (con != undefined && con.toId == 0)) {
        	isNew = true;
        }
        isNew=false; 
        if (connections != undefined && con != undefined) { 
        	if (connections.length > 1 || (isNew && connections.length > 0)) {
        		//con.id
        		//
        		var sameElemCon = new Array();
        		for (var i=0; i < connections.length; i++) {
        			var c = connections[i];
        			
        			var targetBoxId = con.toId;
        			//look for all associations which link with this box
        			if ((c.toId == targetBoxId && c.fromId == con.fromId) 
        			||  (c.fromId == targetBoxId && c.toId == con.fromId)) {
        				sameElemCon.push(c)
        			}
        		}

        		if (sameElemCon.length > 1 || (isNew && sameElemCon.length > 0)) {
        			var pos = sameElemCon.indexOf(con);
        			if (pos < 0 && isNew) {
        				pos = sameElemCon.length; 
        			}
        			return new CenterConnector(this._id,this._factory.getParent().attr('id'), pos);
        		}
       		} 
       	}
        //then the connector should move depending on the number of connections
        return this._connector; //several connectors may be possible, however one is sufficient at the moment
    }
});

var Connector = Class.extend({
    init:function(elementId,parentId, shifts) {
        this.elementId=elementId;
        this.parentId=parentId;
        if(shifts == null) {
        	this.shifts = 0;
        } else {
        	this.shifts = shifts;
        }
    },

    getLeft:function() {
        alert('X');
    },

    getTop:function() {
        alert('Y');
    },
    
    getShifts:function() {
    	return this.shifts;
    }
});

var CenterConnector = Connector.extend({
    init: function(e,p,shifts) {
        this._super(e,p,shifts);
        this.shifts=shifts;
        if (this.shifts==null) {
        	this.shifts = 0;
        }
    },

    getTop:function() {
    	if($('#'+this.elementId).position() == null) {
    		console.error("Cannot read offset properties of element with id " + this.elementId);
    	}
    	//console.info('---> shift:' + this.getShifts());
        var top 	= $('#'+this.elementId).position().top;
        var height 	= $('#'+this.elementId).height();
        var res 	= top + height/2;
        var multiplier = (this.getShifts() % 2) ? 1 : -1;
        var div = 2;
        if (multiplier > 0 && this.getShifts() < 2) {
        	div = 1;
        }
        res = res + ((height/6) * this.getShifts()/div * multiplier);
        return res;
    },

    getLeft:function() {
        var left = $('#'+this.elementId).position().left;
        var width = $('#'+this.elementId).width();
        var res = left + width/2;
        return res;
    }
});

/*
    Lines connect boxes (2)
 */
var Line = Class.extend({
    init: function(factory,options) {
    	this.classType='Line';
        this.id=0;
        this.fromId=0;
        this.toId=0;
        this.mainLabel=null;
        this.arrowFrom=null;
        this.arrowTo=null;
        this.endpointLabels = new Array(); //from and to keys
        //this.labelTo=null;
        this.options=options;
        this._factory=factory;
    },

    getId: function() {
        return this.id;
    },

    refreshMainLabel: function(factory,text) {
        var from = factory.getBoxById(this.fromId);
        var to = factory.getBoxById(this.toId);
        if(this.mainLabel== null) {
            this.mainLabel=new Label(text,'lineMainLabel',new MainLabelHandler(this));
            this.mainLabel._isRemovable=false;;
        } else {
            this.mainLabel.destroy();
        }
        //Position the main label at the center of the line
        var lineCenter = this.mainLabelPositionAndRender(from,to, factory, text);
        
        $('#'+this.mainLabel.getId()).css('top',lineCenter[0]-$('#'+this.mainLabel.getId()).height()/2);
        $('#'+this.mainLabel.getId()).css('left',lineCenter[1]-$('#'+this.mainLabel.getId()).width()/2);
    },
    
    mainLabelPositionAndRender: function(from, to, factory, text) {
    	var coordinates = new MathProcessor().getMiddleOfLine(from.getBestSuitedConnector(this),to.getBestSuitedConnector(this));
    	this.mainLabel.setText(text);
        this.mainLabel.renderLabel($('#'+factory.parentId));
        return coordinates;
    },

    refreshLineLabelEndpoint: function(factory,text,labelType) {
        var from = factory.getBoxById(this.fromId);
        var to   = factory.getBoxById(this.toId);
        //var pos = new MathProcessor().getCenterOfLine(from,to);
        if(this.endpointLabels[labelType]== null) {
            this.endpointLabels[labelType]=new Label(text,'lineEndpointLabel',new EndpointLabelHandler(this,labelType));
            this.endpointLabels[labelType]._isRemovable=true;;
        } else {
            this.endpointLabels[labelType].destroy();
        }

        //position the label at the end of the line
        var mP = new MathProcessor();
        var x = null;
        if(labelType=='to') { //take care: to is shown at the other end of the line and vice versa!!!
            x = mP.labelPositionEndpoint(from,to,this);
        } else {
            x = mP.labelPositionEndpoint(to,from,this);
        }

        var lineCenter = new MathProcessor().getMiddleOfLine(from.getBestSuitedConnector(this),to.getBestSuitedConnector(this));
        this.endpointLabels[labelType].setText(text);
        this.endpointLabels[labelType].renderLabel($('#'+factory.parentId));
        $('#'+this.endpointLabels[labelType].getId()).css('top',x[0]);
        $('#'+this.endpointLabels[labelType].getId()).css('left',x[1]);
    },

    destroy: function() {
        $('#'+this.id).remove();
        if(this.mainLabel!=null) {
            this.mainLabel.destroy();
        }
        if(this.endpointLabels['from']!=null) {
            this.endpointLabels['from'].destroy();
        }
        if(this.endpointLabels['to']!=null) {
            this.endpointLabels['to'].destroy();
        }
    },

    buildLine: function(factory, lineId, additionalClasses) {
    	additionalClasses = additionalClasses == null ? '' : additionalClasses;
        var renderedHtml = '<div class="association_line line_fresh ' + additionalClasses
        				 + '" id="'+lineId+'"><span class="arrow-simple"><img id="xx" src="images/diagram/arrow.png"/></span></div>';
        return renderedHtml;
    },

    renderConnectedLine: function(factory,mathProcessor,movedBox) {
        var boxFrom = factory.getBoxById(this.fromId);
        var boxTo = factory.getBoxById(this.toId);

        var clipTo=mathProcessor.closestIntersectionPoint(boxFrom,boxTo, this);
        var clipFrom=mathProcessor.closestIntersectionPoint(boxTo,boxFrom, this);
        
        if((clipFrom == null) || (clipTo == null)) {
        	var isDebug = false;
        	if (isDebug) {
        		//this is just natural because it occurs whenever two boxes are moved and they are overlapping
        		//so they cannot be connected.
        		console.error('shapes.js: No intersection point found for ' + boxFrom.mainLabel.getText() + ' and ' + boxTo.mainLabel.getText() + '.');
        		var conFrom = boxFrom.getBestSuitedConnector(this);
        		var conTo   = boxTo.getBestSuitedConnector(this);
        	
        		console.log('Mark ' + boxFrom.mainLabel.getText() + ' with red');
        		mark(conFrom.getTop(),  conFrom.getLeft(), 'red', 20);
        	
        		console.log('Mark ' + boxTo.mainLabel.getText() + ' with blue');
        		mark(conTo.getTop(), 	conTo.getLeft(), 'blue', 20);
        	
        		if(clipFrom != null) {
        			console.log('Mark clipFrom ' + ' with pink');
        			mark(clipFrom[0], 	clipFrom[1], 'pink', 20);
        		} else {
        			//try again...
        			clipFrom = mathProcessor.closestIntersectionPoint(boxTo,boxFrom, this, true);
        		}
        	
        		if(clipTo != null) {
        			console.log('Mark clipTo ' + ' with yellow');
        			mark(clipTo[0], clipTo[1], 'yellow', 20);
        		} else {
        			//try again...
        			clipTo=mathProcessor.closestIntersectionPoint(boxFrom,boxTo, this, true);
        		}
        	}
        } else {  
        	//connection will be preserved even though overlapping 
			var dTo = mathProcessor.computeDistanceEfficientViaClip(boxFrom,boxTo,clipTo, this); //check all 4 borders of box
			var dFrom = mathProcessor.computeDistanceEfficientViaClip(boxTo,boxFrom,clipFrom, this);
			var lineLength = dTo+dFrom+mathProcessor.computeDistanceViaPoints(clipFrom,clipTo); //line length, clever right? ;-)
			$('#'+this.id).find('.arrow-simple').css('position','absolute');
			var d = 0;
			if(movedBox.getId()==this.fromId) {
				c = clipTo;
				d= lineLength-dTo;
				$('#'+this.id).find('.arrow-to').addClass('rotate-180');
				$('#'+this.id).find('.arrow-to').css('bottom',d);
				$('#'+this.id).find('.arrow-from').css('bottom',dFrom);
				//mark(c[0],c[1],'blue');
			} else {
				c = clipTo; //lineLength - clipTo;
				//mark(c[0],c[1],'red');
				d=dFrom;
			
				//test - check
				d = dTo; //depends on where the line started originally (?)
			
				$('#'+this.id).find('.arrow-to').css('bottom',d);
				$('#'+this.id).find('.arrow-from').addClass('rotate-180');
				//test 2
				var dist = lineLength -dTo;
				dist = lineLength-dFrom;
				
				$('#'+this.id).find('.arrow-from').css('bottom',dist);
				
				//var mp = new MathProcessor();
				//mp.computeDistance(x1,y1,x2,y2)
			}

			/* performance test: removing labels increases performance quite a lot.	 */
			if(this.mainLabel != null) {
				this.refreshMainLabel(factory,this.mainLabel._text);
			}

			if(this.endpointLabels['from'] != null) {
				//console.log('Refresh linelabel endpoint for ' + this.id);
				this.refreshLineLabelEndpoint(factory,this.endpointLabels['from']._text,'from');
			}

			if(this.endpointLabels['to'] != null) {
				//console.log('Refresh linelabel endpoint for ' + this.id);
				this.refreshLineLabelEndpoint(factory,this.endpointLabels['to']._text,'to');
			}
			
		}
    },

    /* unregister the dragging of a new line - very important! */
    unregisterLineDragging: function(factory) {
        $(factory.getParent()).off('.dragnewline');;
    },

    addUnconnectedLine: function(factory, fromBox, lineId, isNewAction) {
        var con = fromBox.getBestSuitedConnector(this, null, null, isNewAction);
        var lineId = lineId == null ? 'association-line'+ Math.ceil(Math.random() * 100000): lineId;
        this.id=lineId;
        var renderedHtml = this.buildLine(factory,lineId);
        var association = $(renderedHtml).appendTo('#'+factory.parentId);     //add to canvas
        association.css('top', con.getTop()).css('left', con.getLeft());

        if(isNewAction) { //entirely new, not just recreated for help
            $(factory.getParent()).on('mousemove.dragnewline', function(e) {
            	//as offsetX is not defined in all browsers
            	var positionX = e.pageX - factory.getParent().offset().left;
            	var positionY = e.pageY - factory.getParent().offset().top;
            	
                $('#'+lineId).find('.arrow-simple').css('position','absolute');
                $('#'+lineId).find('.arrow-to').css('bottom',0);
                //$('#'+this.id).find('.arrow-from').css('bottom',100);
                var mP=new MathProcessor();
                
                var clipFrom=mP.closestIntersectionPointCoordinates(positionY,positionX,fromBox,factory.getLineById(lineId));  //fromBox is given, but target point instead of to-class as well
                //mark(clipFrom[0],clipFrom[1]);
                
                if(clipFrom == null) {
                	//overlapping (e.g. when creating a new line moving around within a box)
                } else {
                	var d = mP.computeDistance(clipFrom[0],clipFrom[1], positionY, positionX)-20;  
                	$('#'+lineId).find('.arrow-from').css('bottom',d);
                }
            });
        }
        return association;
    }
});

/* Self Reference */
var SelfReference = Line.extend({
    init: function(factory,options) {
    	this._super(factory,options);
    	this.classType='SelfReference';
    	this.location='bottom';
    },
    
    //override
	mainLabelPositionAndRender: function(from, to, factory, text) {
		var cssElem = $('#' + this.id);
		this.mainLabel.setText(text);
        this.mainLabel.renderLabel($('#'+factory.parentId));
        //$('#label-9046041').appendTo($('#self-ref41432'))
        
        var h=0, w=0;
        h=$(cssElem).height() - $('#'+this.mainLabel.getId()).height()/1.5 + 3;
		w=w+$(cssElem).width()*1/2;
		$('#'+this.mainLabel.getId()).appendTo($(cssElem));
		var res = new Array();
		res.push(h);
		res.push(w);
		
		return res;
    },
    
    refreshLineLabelEndpoint: function(factory,text,labelType) {
        var from = factory.getBoxById(this.fromId);
        var to = factory.getBoxById(this.toId);
        var mp = new MathProcessor(); //.getCenterOfLine(from,to);
        if(this.endpointLabels[labelType]== null) {
            this.endpointLabels[labelType]=new Label(text,'lineEndpointLabel',new EndpointLabelHandler(this,labelType));
            this.endpointLabels[labelType]._isRemovable=true;;
        } else {
            this.endpointLabels[labelType].destroy();
        }
        
        var x = new Array(0,0);
        if(labelType == 'from') {
        	x[0] = $('#'+this.getId()).position().top - 17;
        	x[1] = $('#'+this.getId()).position().left - 20;
        } else {
        	x[0] = $('#'+this.getId()).position().top + $('#'+this.getId()).height() - 10;
        	x[1] = $('#'+this.getId()).position().left + $('#'+this.getId()).width() + 5;
        }
        
        this.endpointLabels[labelType].setText(text);
        this.endpointLabels[labelType].renderLabel($('#'+factory.parentId));
        $('#'+this.endpointLabels[labelType].getId()).css('top',x[0]);
        $('#'+this.endpointLabels[labelType].getId()).css('left',x[1]);
  
    },
    
    renderConnectedLine: function(factory,mathProcessor,movedBox) {
        var boxFrom = factory.getBoxById(this.fromId);
        var boxTo = factory.getBoxById(this.toId);
        
		$('#'+this.id).find('.arrow-from').addClass('rotate-180');
		$('#'+this.id).find('.arrow-from').css('position', 'absolute');
        //$('#'+this.id).find('.arrow-from').css('top',topPoint[0]);
        //$('#'+this.id).find('.arrow-from').css('left',topPoint[1]);
        $('#'+this.id).find('.arrow-from').css('left','10px');
        $('#'+this.id).find('.arrow-from').css('margin-left','3px');
        $('#'+this.id).find('.arrow-from').css('margin-top','-11px');
        $('#'+this.id).find('.arrow-from').css('bottom', 'auto');
        
        
        $('#'+this.id).find('.arrow-to').addClass('rotate-180');
		$('#'+this.id).find('.arrow-to').css('position', 'absolute');
        //$('#'+this.id).find('.arrow-from').css('top',topPoint[0]);
        //$('#'+this.id).find('.arrow-from').css('left',topPoint[1]);
        $('#'+this.id).find('.arrow-to').css('top','20px');
        $('#'+this.id).find('.arrow-to').css('left',$('#'+boxFrom.getId()).width());
        $('#'+this.id).find('.arrow-to').css('bottom', 'auto');
        
		//check if boxes are not overlapping (next two lines) - then refresh
		var clipTo=mathProcessor.closestIntersectionPoint(boxFrom,boxTo, this);
        var clipFrom=mathProcessor.closestIntersectionPoint(boxTo,boxFrom, this);
        
        /*
        if(clipFrom == null || clipTo == null) {
        	console.log('Skip refreshing of ' 
        		+ this.id + '(' + boxFrom.mainLabel.getText() + '-' + boxTo.mainLabel.getText() + ')');
        }
        */
        
        if(this.mainLabel != null) {
            this.refreshMainLabel(factory,this.mainLabel._text);
        }

        if(this.endpointLabels['from'] != null) {
        	//console.log('Endpoint refreshing 1 ' + this.id);
            this.refreshLineLabelEndpoint(factory,this.endpointLabels['from']._text,'from');
        }

        if(this.endpointLabels['to'] != null) {
        	//console.log('Endpoint refreshing 2 ' + this.id);
            this.refreshLineLabelEndpoint(factory,this.endpointLabels['to']._text,'to');
        }

    },
    
   	unregisterLineDragging: function(factory) {
   		console.log('do not disable line dragging (self-reference)');
        //override $(factory.getParent()).off('.dragnewline');;
    },
});