/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * Represents an attribute of a class. An attribute consists of
 * a name, a datatype (=range; even, if range might be "UNKNOWN"),
 * a visibility status, and some optional information.
 */
var MetaAttribute_VOID          = "void";
var MetaAttribute_DATETIME	    = "datetime";
var MetaAttribute_BOOLEAN 	    = "boolean";
var MetaAttribute_TIMESTAMP     = "timestamp";
var MetaAttribute_TIME  	    = "time";

var MetaAttribute_PUBLIC 		= "public";
var MetaAttribute_PROTECTED 	= "protected";
var MetaAttribute_PRIVATE 	    = "private";
var MetaAttribute_PACKAGE 	    = "package";

var MetaAssociation = MetaElement.extend({
    init:function(associationId,id,name) {
        this._super();
        this.class="org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation"; //necessary for serializing the meta model;

        this.from=null;
        this.to=null;
        this.name=name;
        this.id=id;
        this.associationId = associationId;
        this.inverseAssociation=null;
        this.composition=false;
        this.aggregation=false;
    },
    
    newFlatInstance: function(assoc) {
    	this.name = assoc.name;
    	this.id = assoc.id;
    	this.associationId = assoc.associationId;
    	this.composition = assoc.composition;
    	this.aggregation = assoc.aggregation;
    	this.from = new MetaClassPoint();
    },


    getFrom:function(){
        return this.from;
    },

    setFrom:function(classPointFrom) {
        this.from = classPointFrom;
    },

    getTo:function() {
        return this.to;
    },

    setTo:function(classPointTo) {
        this.to = classPointTo;
    },

    getInverseAssociation:function() {
        return this.inverseAssociation;
    },

    setInverseAssociation:function(inverseAssociation) {
        this.inverseAssociation = inverseAssociation;
    },

    getName: function() {
    	return this.name;
    },

    toString:function() {
        var s;
        s = sprintf(" %s%s%s-->%s (%s%s%s).",
            from.getMetaClass().getName(),
            this.inverseAssociation == null ? "---" : "<--",
            name.isEmpty() ? "noname" : name,
            to.getMetaClass().getName(),
            to.getRange().toString(),
            this.aggregation ? " - aggregation" : "",
            this.composition ? " - composition" : ""
        );

        for(var i=0; i< this.getComment().length; i++) {
            var c = this.getComment()[i];
            s += sprintf("\n       Comment: %s", c);
        }
        return s;
    }

});