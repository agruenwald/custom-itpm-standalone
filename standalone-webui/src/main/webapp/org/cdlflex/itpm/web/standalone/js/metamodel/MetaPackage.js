/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * Represents a single UML package an is transformed into a single ontology.
 * Packages are defined differently in UML tools: In Visual Paradigm a package
 * usually is presented in a new class diagram. Therefore, one has to differ
 * between default UML packages and UML tool constructs. Packages therefore not
 * always might be supported.
 */
var MetaPackage = MetaElement.extend({
    init:function(name) {
    	this._super();
        this.class="org.cdlflex.mdd.umltuowl.metamodel.MetaPackage"; //necessary for serializing the meta model;

        this.name=name;
        this.classes=new Array();
    },

    /**
     * find a meta class by its identifier (internal) an return it.
     * @param classId unique class id (!= class name).
     * @return the meta class with the given class id or {@code null},
     * if no meta class matched.
     */
    findClassById:function(classId) {
        for(var i=0; i < this.classes.length;i++) {
            var c = jQuery.extend(new MetaClass(), this.classes[i]);
            if(stripSpecialChars(c.getId()) == stripSpecialChars(classId)) {
                return c;
            }
        }
        return null;
    },
    
    findEnumClassByName: function(classId) {
    	var p = jQuery.extend(new MetaPackage(''), this);
        var metaClass = p.findClassByName(classId);  
        if(metaClass != null && metaClass.enumClass) {
        	return metaClass;
        }
        return null;
    },

    /**
     * find a meta class by its name and return it.
     * @param className unique class name
     * @return the meta class with the given class name or {@code null},
     * if no meta class matched.
     */
    findClassByName:function(className) {
        for(var i=0; i < this.classes.length;i++) {
        	var c = jQuery.extend(new MetaClass(), this.classes[i]);
            if(c.getName()==(className)) {
                return c;
            }
        }
        return null;
    },

    /**
     * find a meta class by its name and return it. In some cases it can
     * be, that a class names occurs multiple time, e.g. in ArgoUML all
     * classes (from different drawings) are merged into a single XMI file.
     * @param className unique class name
     * @return a list of the meta classes found  with the given class name or an empty
     * list, if if no meta classes matched.
     */
     findMultipleClassesByName:function(className) {
        for(var i=0; i < this.classes.length;i++) {
        	var c = jQuery.extend(new MetaClass(), this.classes[i]);
            if(c.getName()==(className)) {
                result.push(c);
            }
        }
        return result;
    },

    /**
     * @return return a list of all classes sorted (interfaces,abstracts,concrete classes).
     */
    findAllClassesSortedByAbstract:function() {
        var interfaces = new Array();
        var abstracts = new Array()
        var concretes = new Array()

        for(var i=0;i<this.classes.length;i++) {
            var metaClass = jQuery.extend(new MetaClass(), this.classes[i]);
            if(metaClass.abstractClass) {
                abstracts.push(metaClass);
            } else if (metaClass.interfaceClass) {
                interfaces.push(metaClass);
            } else {
                concretes.push(metaClass);
            }
        }
        var list = new Array();
        list = list.concat(interfaces,abstracts,concretes);
        return list;
    },

    /**
     * find an association based on its ID. Should not be used to
     * often, because this method is computational expensive.
     * @param associationId name of the association ID
     * @return list of found meta associations. In case of unidirectional relationship,
     * 		   two associations are found. If ID is unknown, an empty list is returned.
     */
    findAssociationById: function(associationId) {
        var list = new Array();
        var p = jQuery.extend(new MetaPackage(''), this);
        for(var j=0; j< p.classes.length;j++) {
            var metaClass = p.classes[j];
            for(var k=0; k<metaClass.associations.length;k++) {
                var ass = jQuery.extend(new MetaAssociation(), metaClass.associations[k]);
                if(stripSpecialChars(ass.associationId) == stripSpecialChars(associationId)) {
                    list.push(ass);
                    if(ass.getInverseAssociation() != null
                        && stripSpecialChars(ass.getInverseAssociation().associationId) == stripSpecialChars(associationId)) {
                        list.push(ass.getInverseAssociation());
                        return list;
                    }
                }
            }
        }
        return list;
    },

    /**
     * @return whole model is returned as a string containing package, class and
     * attribute information.
     */
    toString:function() {
        var s = "";
        for(var i=0;i<this.classes.length;i++) {
            var clazz = jQuery.extend(new MetaClass(), this.classes[i]);

            var attrs = "";
            if (clazz.abstractClass || clazz.interfaceClass) {
                attrs = clazz.abstractClass ? "abstract" : "";
                if(!attrs=='') {
                    attrs = attrs + (clazz.interfaceClass() ? ",interface" : "");
                } else {
                    attrs = attrs + (clazz.interfaceClass() ? "interface" : "");
                }
                attrs = " (" + attrs + ")";
            }

            s += "Class " + clazz + attrs + "\n";
            for(var j=0;j<clazz.subclasses.length;j++) {
                var subclass = this.subclasses[i];
                s+= "==> subclass " + subclass + "\n";
            }

            for(var j=0;j<clazz.superclasses.length;j++) {
                var subclass = this.superclasses[i];
                s+= "==> superclass " + superclass + "\n";
            }

            for(var j=0;j<clazz.comments().length;j++) {
                var c = this.comments[i];
                s+= "         Comment: " + c + "\n";
            }

            for(var j=0;j<clazz.attributes.length;j++) {
                var attr = clazz.attributes[i];
                s += "   " + attr;
            }

            for(var j=0;j<clazz.associations.length;j++) {
                var ass = clazz.associations[i];
                s += "  + " + ass + "\n";
            }
        }
        return s;
    }


});