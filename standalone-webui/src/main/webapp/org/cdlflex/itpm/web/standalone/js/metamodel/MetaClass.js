/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * An abstract class that contains default properties for classes, attributes, associations
 * and other meta model objects.
 */
var MetaClass = MetaElement.extend({
    init:function(name) {
        this._super();
        this.class="org.cdlflex.mdd.umltuowl.metamodel.MetaClass"; //necessary for serializing the meta model;

        this.id="";
        this.name=name;
        this.originalName=name;
        this.abstractClass=false;
        this.interfaceClass=false;
        this.enumClass=false;
        this.superclasses=new Array();
        this.subclasses = new Array();
        this.attributes = new Array();
        this.associations = new Array();
    },

    /* avoid additional circularities for JSON serialization */
    newFlatInstance:function(metaClass) {
        this.init(metaClass.name);
        this.id=metaClass.id;
        this.name=metaClass.name;
        this.originalName=metaClass.originalName;
        this.abstractClass=metaClass.abstractClass;
        this.interfaceClass=metaClass.interfaceClass;
        this.enumClass=metaClass.enumClass;
        this.superclasses=new Array();
        this.subclasses=new Array();
        this.attributes=new Array();
        this.associations=new Array();
        return this;
    },

    setId: function(id) {
        this.id=id;
    },

    getId: function(id) {
        return this.id;
    },

    getName:function() {
        return this.name;
    },

    setName:function(name) {
        this.name=name;
        this.originalName=name;
    },

    setTransformedName:function(name) {
        this.name=name;
    },

    findAttributeByName:function(attributeName) {
        for(var i=0; i < this.attributes.length; i++) {
            var a = this.attributes[i];
            if(a.name == (attributeName)) {
                return a;
            }
        }
        return null;
    },

    findAssociationByName:function(associationName) {
        for(var i=0; i < this.associations.length; i++) {
            var a = this.associations[i];
            if (a.name == associationName) {
            //if(a.getName() == (associationName)) {
            //if(a.getTo().getMetaClass().getName() == (associationName)) {
                return a;
            }
        }
        return null;
    },
    
    findViaClassName: function(className, metaClassArray) {
    	for(var i=0; i < metaClassArray.length; i++) {
    		var c = metaClassArray[i];
    		if(c.name == className) {
    			return c;
    		}
    	}
    	return null;
    },

    removeAssociation:function(association) {
        for(var i = 0; i < this.associations.length; i++) {
            var a = this.associations[i];
            if(a == association) {
                this.associations.remove(i);
                return;
            }
        }
    }
});
