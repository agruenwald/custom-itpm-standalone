/* Menu */
var _semanticMetaEditorRegisteredMenus = new Array();
function _semanticMetaEditorInitContextMenu(umlFactory, canvasId) {
	canvasId = '#' + canvasId;
	
	if (_semanticMetaEditorRegisteredMenus.indexOf(canvasId) >= 0) {
		return;  //only init each canvas once
	} else {
		_semanticMetaEditorRegisteredMenus.push(canvasId); 
	}
	
	umlFactory._canvasBuildingEvent($(canvasId));
	/*
	context.init({preventDoubleContext: false, compress: true, filter:function(e) { 
		//alert('filter ...');
	}});
	*/
	context.init({preventDoubleContext: false, compress: true});
	context.settings({compress: true});
	
	function menuHandlerUML(key, e) {
			e.preventDefault();
			var boxId = context.getOrigin().attr('id');
			var selectedAttribute = null;
            var metaClassType = 'MetaClass';
            if ($('#' + boxId).hasClass('umlAttribute')) {
				boxId  = $('#' + boxId).closest('.box').attr('id');
				selectedAttribute = new MetaAttribute().parseAttributeText(context.getOrigin().text().trim());
				metaClassType = 'MetaAttribute';
			}
			console.log($(this).text());
			//window.console && console.log(m) || alert(m);
			if(key=='delete') {
				umlFactory.humanRemoveBoxById(boxId,'click');
			} else if (key == 'add_association') {
				umlFactory.startConnectionProcess('association',boxId);
			} else if (key == 'add_attribute') {
				umlFactory.humanAddAttribute(boxId,'click');
			} else if (key == 'add_association_uni') {
				umlFactory.startConnectionProcess('association_uni',boxId);
			} else if (key == 'add_generalization') {
				umlFactory.startConnectionProcess('generalization',boxId);
			} else if (key == 'add_composition') {
				umlFactory.startConnectionProcess('composition',boxId);
			} else if (key == 'add_aggregation') {
				umlFactory.startConnectionProcess('aggregation',boxId);
			} else if (key == 'add_composition_uni') {
				umlFactory.startConnectionProcess('composition_uni',boxId);
			}else if (key == 'add_aggregation_uni') {
				umlFactory.startConnectionProcess('aggregation_uni',boxId);
			} else if (key == 'add_comment') {
				umlFactory.commentHandler.addComment(boxId, metaClassType, selectedAttribute);
			} else if (key == 'show_comments') {
				umlFactory.commentHandler.showComments(boxId, selectedAttribute, false);
			} else if (key.match("^annotate")) {
				var status = key.replace(/^annotate_/, '');
				umlFactory.commentHandler.setElementStatus(status, boxId, metaClassType, selectedAttribute, 'Andreas Gruenwald');
			} else if (key.match("^make_")) {
				var status = key.replace(/^make_/, '');
				if (status == 'abstract') {
					$('#' + boxId).find('.umlClass').addClass('umlAbstract');
					umlFactory.getBoxById(boxId).abstractClass = true;
				} else {
					$('#' + boxId).find('.umlClass').removeClass('umlAbstract');
					umlFactory.getBoxById(boxId).abstractClass = false;
				}
			}
	}
	
	context.attach(canvasId + ' .box, ' + canvasId + ' .umlAttribute', [
		{text: 'Add Attribute', icon: 'plus', 					action: function(e) { menuHandlerUML('add_attribute', e) }},
		{text: 'Add Association', icon: 'resize-horizontal', 	action: function(e) { menuHandlerUML('add_association', e) }},
		{text: 'Add Generalization', icon: 'plus', 				action: function(e) { menuHandlerUML('add_generalization', e) }},
		{divider: true},
		{text: 'Add Unidirectional association', icon: 'none', 	action: function(e) { menuHandlerUML('add_association_uni', e) }},
		{text: 'Add Composition', icon: 'none', 				action: function(e) { menuHandlerUML('add_composition', e) }},
		{text: 'Add Aggregation', icon: 'none', 				action: function(e) { menuHandlerUML('add_aggregation', e) }},
		{divider: true},
		{text: 'Delete', 			icon: 'minus', 				action: function(e) { menuHandlerUML('delete', e) }},
		{divider: true},
		{text: 'More', subMenu: [
				{text: 'Add Unidirectional Composition', icon: 'none', 				action: function(e) { menuHandlerUML('add_composition_uni', e) }},
				{text: 'Add Unidirectional Aggregation', icon: 'none', 				action: function(e) { menuHandlerUML('add_aggregation_uni', e) }},
				{divider: true},
				{text: 'Make class abstract', icon: 'none', 						action: function(e) { menuHandlerUML('make_abstract', e) }},
				{text: 'Make class concrete', icon: 'none', 						action: function(e) { menuHandlerUML('make_concrete', e) }}
		]},
		{text: 'Annotate', subMenu: [
				{text: 'Show comments', icon: 'list', 				action: function(e) { menuHandlerUML('show_comments', e) }},
				{text: 'Add comment', icon: 'comment', 				action: function(e) { menuHandlerUML('add_comment', e) }},
				{divider: true},
				{text: "Mark as local concept", subMenu: [
					{text: 'Local (tool-specific)', icon: 'cloud-download', 				action: function(e) { menuHandlerUML('annotate_local', e) }},
					{text: 'Local and available', icon: 'check', 				action: function(e) { menuHandlerUML('annotate_local_available', e) }},
					{text: 'Tool-internal (info purpose)', icon: 'certificate', 				action: function(e) { menuHandlerUML('annotate_info', e) }}
				]},
				{text: 'Suggest as available', icon: 'heart-empty', 				action: function(e) { menuHandlerUML('annotate_suggestion', e) }},
				{text: 'Global availability (default)', icon: 'check', 				action: function(e) { menuHandlerUML('annotate_available', e) }},
		]}
		]
	);
	
	
	
	function menuHandlerCanvas(key, e) {
		var m = "clicked: " + key;
	   	e.preventDefault();
		var boxId = context.getOrigin().attr('id');
	   // console.log($(this).text());
		var x =e.pageX;
		var y = e.pageY;
		var parentCanvas = $(context.getOrigin()).closest('.canvas');
		
		y = Math.max(0,y - $(parentCanvas).offset().top);
		x = Math.max(0,x - $(parentCanvas).offset().left);
		
		console.log('position: ' + x + '-' + y);
		if(key=='add_class') {
			var umlE = new UmlClass("Class",x,y,null,true);
			umlFactory.humanRegister(umlE);
		} else if (key == 'add_interface') {
			var umlE = new UmlInterface("Interface",x,y,null,true);
			umlFactory.humanRegister(umlE);
		} else if (key == 'add_abstract') {
			var umlE = new UmlAbstract("AbstractClass",x,y,null,true);
			umlFactory.humanRegister(umlE);
		} else if (key == 'add_enum') {
			var umlE = new UmlEnumClass("Enum", x,y,null,true);
			umlFactory.humanRegister(umlE);
		} else if (key == 'save') {
			umlFactory.humanSaveModel();
		} else if (key == 'clear') {
			umlFactory.humanClearSpace();
		}  else if (key == 'zoom_in') {
			umlFactory.zoomIn();
		} else if (key == 'zoom_out') {
			umlFactory.zoomOut();
		} else if (key == 'reset_zoom') {
			umlFactory.resetZoom();
		} else if (key == 'undo') {
			umlFactory.humanUndoExt();
		} else if (key == 'redo') {
			umlFactory.humanRedoExt();
		} else if (key == 'add_comment') {
			umlFactory.commentHandler.addComment(boxId, 'MetaPackage');
		} else if (key == 'show_comments') {
			umlFactory.commentHandler.showComments(boxId, null, true);
		} else if (key == 'add_package') {
			var addModal = $(parentCanvas).closest('.wicket-enclosing-form').find('.modal-add');
			addModal.find('h4').html('Add new package to metamodel ' + umlFactory.metaModel.name);
			addModal.find('.action').val('add');
			addModal.find('input[type=text].package-name').val('');
			addModal.find('.submit-button').html('Add diagram');
			addModal.modal();	
		} else if (key == 'rename_package') {
			var addModal = $(parentCanvas).closest('.wicket-enclosing-form').find('.modal-add');
			addModal.find('h4').html('Rename current package');
			addModal.find('.action').val('rename');
			addModal.find('input[type=text].package-name').val(umlFactory.selectedPackage);
			addModal.find('.submit-button').html('Rename diagram');
			addModal.modal();
		} else if (key == 'copy_package') {
			umlFactory.humanCopyPackage();
		} else if (key == 'remove_package') {
			umlFactory.humanRemoveCurrentPackage();
		} else if (key == 'add_metamodel' || key == 'copy_metamodel') {
			var addModal = $(parentCanvas).closest('.wicket-enclosing-form').find('.modal-add-metamodel');
			if (key == 'copy_metamodel') {
				addModal.find('h4').html('Copy metamodel ' + umlFactory.metaModel.name);
				addModal.find('.action').val('copy');
				addModal.find('.submit-button').html('Copy metamodel');
			} else {
				addModal.find('h4').html('Add new metamodel');
				addModal.find('.action').val('add');
				addModal.find('.submit-button').html('Add metamodel');
			}
			addModal.find('input[type=text].model-name').val('');
			addModal.find('input[type=text].model-iri').val('');
			addModal.find('textarea.model-description').val('');
			addModal.find('input[type=text].model-iri').removeAttr('disabled');
			addModal.modal();
		} else if (key == 'rename_metamodel') {
			var addModal = $(parentCanvas).closest('.wicket-enclosing-form').find('.modal-add-metamodel');
			addModal.find('h4').html('Rename current metamodel');
			addModal.find('.action').val('rename');
			addModal.find('input[type=text].model-name').val(umlFactory.metaModel.name);
			addModal.find('input[type=text].model-iri').val(umlFactory.metaModel.namespace);
			addModal.find('textarea.model-description').val(umlFactory.metaModel.description.trim());
			addModal.find('input[type=text].model-iri').attr('disabled', 'disabled');
			addModal.find('.submit-button').html('Update metamodel');
			addModal.modal();
		} else if (key == 'remove_metamodel') {
			umlFactory.humanRemoveCurrentMetaModel();
		} else if (key == 'auto_height') {
			umlFactory.autoAdaptDiagramHeight(true);
		} else if (key == 'add_space') {
			umlFactory.getParent().height(umlFactory.getParent().height() + 300);
		} else if (key.match("^annotate")) {
			var status = key.replace(/^annotate_/, '');
			umlFactory.commentHandler.setElementStatus(status, boxId, 'MetaPackage', 
					null, 'Andreas Gruenwald');
		} else if (key.match("^type")) {
			var type = key.replace(/^type_/, '');
			umlFactory.commentHandler.setElementCommentAnnotation(type, boxId, 'MetaModel', 
							null, 'Andreas Gruenwald', '@openengsbType');
			umlFactory._refreshPackageSwitcherIcons(umlFactory.metaModel);
			//umlFactory._buildPackageSwitcher(umlFactory.metaModel);
		} else if (key == 'comparison_editor') {
			var toggleUrl = panelComparisonEditorURL;
			var editor2 = $('.comparison-editor-unique'); //hack; Wicket will name a comparison editor with this class if available
			if (editor2.length > 0) {
				editor2 = $(editor2).first();
				if (editor2.css('display') == 'none') {
					editor2.show();
					var event = 'comparisonEditor.refresh';
					editor2.find('.wicket-enclosing-form').trigger( event, [  ] ); //will not work because not rendered yet
				} else {
					editor2.hide();
				}
				
			}
			/*
			$.getJSON( toggleUrl, function( data ) { // a simple call is sufficient
				var items = [];
				bootbox.alert('responded');
			});
			var editor2 = $('.comparison-editor');
			if (editor2.length > 0) {
				editor2 = $(editor2).first();
				if (editor2.css('display') == 'none') {
					editor2.show();
				} else {
					editor2.hide();
				}
				
			}
			*/
		} else if (key.match("^generate_code")) {
			if (key == 'generate_code_model' || key == 'generate_code' || key == 'generate_code_call_routine') {
				var alertMsg = 'Are you sure that you want to start the code generation ' + 
									'in the background (might influence the running applications) ?';
				if (key == 'generate_code_call_routine') {
					alertMsg = 'Warning: The execution of the bin/codgen script in the background may take up ' 
					  		 + 'to 10 minutes, depending on the memory size and speed of your system environment. '
					  		 + 'Also, it might be necessary to restart the running application if the app is built on '
					  		 + 'generated resources.';
				}
				
				bootbox.confirm(alertMsg,
								function(ok) {
									var res = new Array();
									if(ok) {
										$.ajax({
											dataType: "json",
											url: semanticDiagramsURL + '&action=' + key 
													+ "&mm=" + umlFactory.metaModel.namespacePrefix , //prefix (?)
											//data: data,
											async: true,
											success: function(data) {
												if (data.status == 'OK') {
													if (key == 'generate_code_model' || key == 'generate_code') {
														bootbox.alert('Code generation succeeded. Find below a list ' +
														'of the locations which contain the generated sources. <br /> ' + data.data);
													} else {
														bootbox.alert('Executed the bin/gencode/script: <br />' + data.data);
													}
												} else {
													bootbox.alert('Code generation/building failed: ' + data.data);
												}
											}, fail: function() {
												bootbox.alert('Could not start or perform code generation.', function() {});
												return new Array();
											}
										});
									}
								});
			} else {
				bootbox.alert('Code generator not supported yet.');
			}
			//generate_code_all
			//generate_code_current
			//generate_code_deploy
		}
	}

	context.attach(canvasId, [
		{text: 'New Class', icon: 'plus', 					action: function(e) { menuHandlerCanvas('add_class', e) }},
		{text: 'New Interface', icon: 'plus', 				action: function(e) { menuHandlerCanvas('add_interface', e) }},
		{text: 'New Abstract', icon: 'plus', 				action: function(e) { menuHandlerCanvas('add_abstract', e) }},
		{text: 'New Enum', icon: 'plus', 					action: function(e) { menuHandlerCanvas('add_enum', e) }},
		{divider: true},
		{text: 'Undo', icon: 'share-alt', 					action: function(e) { menuHandlerCanvas('undo', e) }},
		{text: 'Redo', icon: 'repeat', 						action: function(e) { menuHandlerCanvas('redo', e) }},
		{text: 'Save', icon: 'floppy-save', 				action: function(e) { menuHandlerCanvas('save', e) }},
		{divider: true},
		{text: 'Annotate', subMenu: [
				{text: 'Show comments', icon: 'list', 				action: function(e) { menuHandlerCanvas('show_comments', e) }},
				{text: 'Add comment', icon: 'comment', 				action: function(e) { menuHandlerCanvas('add_comment', e) }},
				{divider: true},
				{text: "Mark as local concept", subMenu: [
					{text: 'Local (tool-specific)', icon: 'cloud-download', 				action: function(e) { menuHandlerCanvas('annotate_local', e) }},
					{text: 'Local and available', icon: 'check', 				action: function(e) { menuHandlerCanvas('annotate_local_available', e) }},
					{text: 'Tool-internal (info purpose)', icon: 'certificate', 				action: function(e) { menuHandlerCanvas('annotate_info', e) }}
				]},
				{text: 'Suggest as available', icon: 'heart-empty', 				action: function(e) { menuHandlerCanvas('annotate_suggestion', e) }}, //hand-right
				{text: 'Global availability (default)', icon: 'check', 				action: function(e) { menuHandlerCanvas('annotate_available', e) }},
				{divider: true},
				{text: 'EngSb connector', icon: 'cloud', 			action: function(e) { menuHandlerCanvas('type_connector', e) }},
				{text: 'EngSb domain', icon: 'transfer',			action: function(e) { menuHandlerCanvas('type_domain', e) }},
				{text: 'EngSb default', icon: 'none', 				action: function(e) { menuHandlerCanvas('type_default', e) }}
		]},
		{text: 'Modify Metamodels', subMenu: [
				{text: 'Diagram Settings', subMenu: [
					{text: 'Add additional space', icon: 'plus', 		action: function(e) { menuHandlerCanvas('add_space', e) }},
					{text: 'Adapt height', icon: 'plus',				action: function(e) { menuHandlerCanvas('auto_height', e) }},
					{divider: true},
					{text: 'Toggle second editor', icon: 'globe',		action: function(e) { menuHandlerCanvas('comparison_editor', e) }},
					{divider: true},
					{text: 'Clear Space', icon: 'remove-sign', 				action: function(e) { menuHandlerCanvas('clear', e) }}
				]},	
				{text: 'Add new package', icon: 'plus', 			action: function(e) { menuHandlerCanvas('add_package', e) }},
				{text: 'Rename package', icon: 'edit', 				action: function(e) { menuHandlerCanvas('rename_package', e) }},
				{text: 'Copy   package', icon: 'floppy-save',				action: function(e) { menuHandlerCanvas('copy_package', e) }},
				{text: 'Remove package', icon: 'minus',				action: function(e) { menuHandlerCanvas('remove_package', e) }},
				{divider: true},
				{text: 'Add new metamodel', icon: 'plus', 			action: function(e) { menuHandlerCanvas('add_metamodel', e) }},
				{text: 'Rename metamodel', icon: 'edit', 			action: function(e) { menuHandlerCanvas('rename_metamodel', e) }},
				{text: 'Copy metamodel', icon: 'floppy-save',		action: function(e) { menuHandlerCanvas('copy_metamodel', e) }},
				{text: 'Remove metamodel', icon: 'minus', 			action: function(e) { menuHandlerCanvas('remove_metamodel', e) }}
		]},
		{text: 'Generate Code', subMenu: [
				{text: 'Generate active metamodel', icon: 'cog', 	 action: function(e) { menuHandlerCanvas('generate_code_model', e) }},
				{text: 'Generate all', icon: 'cog', 				 action: function(e) { menuHandlerCanvas('generate_code', e) }},
				{text: 'Reinstall code via mvn (bin/codegen)', icon: 'cog', action: function(e) { menuHandlerCanvas('generate_code_call_routine', e) }}
		]}	
		
		]
	);


function menuHandlerAssociations(key, e) {
	e.preventDefault();
	var disableComments = false;
    var boxId = context.getOrigin().attr('id');
    if($(context.getOrigin()).find('.arrow-generalization').length > 0) {
     	disableComments = true; //disable something comments for generalization (?)
    }
    
    var m = "clicked: " + key;
	console.log($(this).text());
	var x = e.pageX;
	var y = e.pageY;
	var parentCanvas = $(context.getOrigin()).closest('.canvas');
	
	y = Math.max(0,y - $(parentCanvas).offset().top);
	x = Math.max(0,x - $(parentCanvas).offset().left);

	console.log('position: ' + x + '-' + y);
	if(key=='delete_association') {
	 umlFactory.humanRemoveLineById(boxId);
	} else if(key=='change_label') {
		umlFactory.humanChangeLineLabel(boxId,'UnnamedAssoc', 'click');
	} else if (key=='add_cardinality') {
		umlFactory.humanAddLineLabelsEndPoints(boxId,'0..*');
	} else if (key == 'add_comment') {
		umlFactory.commentHandler.addComment(boxId, 'MetaAssociation');
	} else if (key == 'show_comments') {
		umlFactory.commentHandler.showComments(boxId, null, false);
	}
                    
}

context.attach(canvasId + ' .association_line:not(:has(.arrow-generalization))', [
//context.attach(canvasId + ' .association_line', [
		{text: 'Change (or add) Label', icon: 'edit', 					action: function(e) { menuHandlerAssociations('change_label', e) }},
		{text: 'Add Cardinality', icon: 'plus', 				action: function(e) { menuHandlerAssociations('add_cardinality', e) }},
		{text: 'Delete', icon: 'minus', 					action: function(e) { menuHandlerAssociations('delete_association', e) }},
		{divider: true},
		{text: 'Add comment', icon: 'comment', 				action: function(e) { menuHandlerAssociations('add_comment', e) }},// disabled: disableComments},
		{text: 'Show comments', icon: 'list', 					action: function(e) { menuHandlerAssociations('show_comments', e)}} // , disabled: disableComments}}
		]
	);

context.attach(canvasId + ' .association_line:has(.arrow-generalization)', [
//context.attach(canvasId + ' .association_line', [
		{text: 'Delete', icon: 'minus', 	action: function(e) { menuHandlerAssociations('delete_association', e) }}
		]
	);
	
/*
$.contextMenu({
        selector: canvasId + ' .association_line',
        build: function($trigger, e) {
            this._e=e;
            
            var disableComments = false;
            if($trigger.find('.arrow-generalization').length > 0) {
            	disableComments = true;
            }
            
            // this callback is executed every time the menu is to be shown
            // its results are destroyed every time the menu is hidden
            // e is the original contextmenu event, containing e.pageX and e.pageY (amongst other data)
            return {
                callback: function(key, options) {
                    var m = "clicked: " + key;
                    console.log($(this).text());
                    var x = options._e.pageX;
                    var y = options._e.pageY;
                    var parentCanvas = $('#'+$(options.$trigger).attr('id')).closest('.canvas');
                    
                    y = Math.max(0,y - $(parentCanvas).offset().top);
                    x = Math.max(0,x - $(parentCanvas).offset().left);

                    console.log('position: ' + x + '-' + y);
                    if(key=='delete_association') {
                     umlFactory.humanRemoveLineById($(this).attr('id'));
                    } else if(key=='change_label') {
                        umlFactory.humanChangeLineLabel($(this).attr('id'),'UnnamedAssoc', 'click');
                    } else if (key=='add_cardinality') {
                        umlFactory.humanAddLineLabelsEndPoints($(this).attr('id'),'0..*');
                    } else if (key == 'add_comment') {
                    	umlFactory.commentHandler.addComment($(options.$trigger).attr('id'), 'MetaAssociation');
                    } else if (key == 'show_comments') {
                    	var elem = $(options.$trigger);
                    	var id = elem.attr('id');
                    	umlFactory.commentHandler.showComments(id, null, false);
                    }
                },
                items: {
                    "change_label": {name: "Change (or add) Label", icon: "edit"},
                    "add_cardinality": {name: "Add Cardinality", icon: "add"},
                    "delete_association": {name: "Delete", icon: "delete"},
                    "add_comment": {name: "Add comment", icon: "add", disabled: disableComments},
                    "show_comments": {name: "Show comments", icon: "info", disabled: disableComments}

                }
            };
        }
    });
*/
    
}