(function () {
  var Pos = CodeMirror.Pos;

  function forEach(arr, f) {
    for (var i = 0, e = arr.length; i < e; ++i) f(arr[i]);
  }
  
  function arrayContains(arr, item) {
    if (!Array.prototype.indexOf) {
      var i = arr.length;
      while (i--) {
        if (arr[i] === item) {
          return true;
        }
      }
      return false;
    }
    return arr.indexOf(item) != -1;
  }
  
  
  function filterSPARQL(items,itemClasses,token) {
	  var selectedItems = new Array();
	  var selectedItemClasses = new Array();
	  var i = 0;
	  $.each(items, function(x,y) {
		  if((y.toLowerCase().indexOf(token.string.toLowerCase()) >= 0) || (token.string.trim().length==0)) {     //contains
		  //if((y.toLowerCase().indexOf(token.string.toLowerCase()) == 0) || (token.string.trim().length==0)) {  //startsWith
		    if ((y.split(":").length - 1) > 1) {
		        console.log("Skipped anonym class " + y);
		    } else {
			    selectedItems.push(y);
			    selectedItemClasses.push(itemClasses[i]);
			}
		  } else {
			  
		  }
		  i++;
	  });
	  return new Array(selectedItems,selectedItemClasses) ;
  }
  
  var cachedSPARQLItems = null;
  var cachedSPARQLItemClasses = null;
  function retrieveOntology(enclosingForm,format,editorContent,cur,token) {
	  if(cachedSPARQLItems != null) {
		  return filterSPARQL(cachedSPARQLItems,cachedSPARQLItemClasses,token);
	  }
	  
	  var datObj = {
			  keywords: new Array('this', 'is', 'a', 'test2'),
			  format: format,
			  editorContent: editorContent,
			  cursorPosition: cur,
			  token: token
	  };
	  
	  $(enclosingForm).trigger("semanticEditor.highlight", datObj);
	  
	  var url = jsQueryURL + '&req=rule_editor&f=get_ontology&format='+format+'&content='+editorContent+'&cursor_position='+cur.ch+'&token='+token.string;
	  $.ajax(
			  {
			  	url: url,
			  	dataType: "json",
			  	async: false,
			  	success: function(data)
			  	{
			  	  $('#ajaxveil').hide();
				  items = $.merge([], data.objectProperties);
				  items = $.merge(items,data.dataProperties);
				  items = $.merge(items,data.classes);
				  cachedSPARQLItems = items;
				  cachedSPARQLItemClasses = new Array();
				  for(var i=0; i < data.objectProperties.length; i++) {
					  cachedSPARQLItemClasses.push('owlObjectProperty');
				  }
				  for(var i=0; i < data.dataProperties.length; i++) {
					  cachedSPARQLItemClasses.push('owlDataProperty');
				  }
				  for(var i=0; i < data.classes.length; i++) {
					  cachedSPARQLItemClasses.push('owlClass');
				  }
				  
				  //interact with semantic Meta Editor (if available)
				  $(enclosingForm).trigger("semanticEditor.highlight", new Array('this', 'is', 'still', 'a', 'test', 'event'));
			  	}
		  });
	  $('#ajaxveil').hide();
	  return filterSPARQL(cachedSPARQLItems,cachedSPARQLItemClasses,token);
	  
  }
  
  function retrieveOntologySimplified(enclosingForm,format,editorContent,cur,token) {
	  return retrieveOntology(enclosingForm,format,editorContent,cur,token);
  }

  function scriptHint(editor, keywords, getToken, options) {
    // Find the token at the cursor
    var cur = editor.getCursor(), token = getToken(editor, cur), tprop = token;
    // If it's not a 'word-style' token, ignore the token. Only accepted char: ":" (double point)
		if (!/^[\w|:$_]*$/.test(token.string)) {
      token = tprop = {start: cur.ch, end: cur.ch, string: "", state: token.state,
                       type: token.string == "." ? "property" : null};
    }
	console.log("INPUT TOKEN (PARTLY): " + token.string);
	
	//Future work: Implementation at the moment for SPARQL (DEMO)
	var qlFormatSelect = editor.options['qlFormat'];
	var qlFormat = $(qlFormatSelect).val();
	$(qlFormatSelect).find('option').each(function() { 
  		if ($(this).val() == qlFormat) {
  			qlFormat = ($(this).text());
  		}
  	});

	if(qlFormat == null) {
	    qlFormat = $(editor.getTextArea()).closest('form').find('.rule-format option:selected').text();
	}

	switch(qlFormat) {
	case 'SPARQL': break; //supported
	default: 
		return {list: new Array(),
        from: Pos(cur.line, token.start),
        to: Pos(cur.line, token.end)};	
	}
	
	var content = editor.getValue();
	var enclosingForm = $(editor.getInputField()).closest('form');
	
	var list = new Array();
	var listClasses = new Array();
	switch(content) {
	case '' : list.push('SELECT'); break;
	case 'SELECT ':
	    list.push("?x");
	    list.push("?x ?y");
	    list.push("?x ?y WHERE { ?c itpm:hasCollaboratorName ?x. ?c itpm:hasCollaboratorId ?y} ");
	    list.push("(COALESCE(SUM(?integerVal), '0.0') AS ?intExample)");
	    break;
	default: 
		lists = retrieveOntologySimplified(enclosingForm,qlFormat,content,cur,token);
		list = lists[0]; //items
		listClasses = lists[1]; //concept types
	}
	return {list: list,
            from: Pos(cur.line, token.start),
            to: Pos(cur.line, token.end),
            owlClasses: listClasses};	
		
		
    // If it is a property, find out what it is a property of.
    while (tprop.type == "property") {
      tprop = getToken(editor, Pos(cur.line, tprop.start));
      if (tprop.string != ".") return;
      tprop = getToken(editor, Pos(cur.line, tprop.start));
      if (tprop.string == ')') {
        var level = 1;
        do {
          tprop = getToken(editor, Pos(cur.line, tprop.start));
          switch (tprop.string) {
          case ')': level++; break;
          case '(': level--; break;
          default: break;
          }
        } while (level > 0);
        tprop = getToken(editor, Pos(cur.line, tprop.start));
	if (tprop.type.indexOf("variable") === 0)
	  tprop.type = "function";
	else return; // no clue
      }
      if (!context) var context = [];
      context.push(tprop);
    }
    return {list: getCompletions(token, context, keywords, options),
            from: Pos(cur.line, token.start),
            to: Pos(cur.line, token.end)};
  }

  CodeMirror.qlAPIHint = function(editor, options) {
    return scriptHint(editor, javascriptKeywords,
                      function (e, cur) {return e.getTokenAt(cur);},
                      options);
  };

 

  var stringProps = ("charAt charCodeAt indexOf lastIndexOf substring substr slice trim trimLeft trimRight " +
                     "toUpperCase toLowerCase split concat match replace search").split(" ");
  var arrayProps = ("length concat join splice push pop shift unshift slice reverse sort indexOf " +
                    "lastIndexOf every some filter forEach map reduce reduceRight ").split(" ");
  var funcProps = "prototype apply call bind".split(" ");
  var javascriptKeywords = ("break case catch continue debugger default delete do else false finally for function " +
                  "if in instanceof new null return switch throw true try typeof var void while with").split(" ");
  var coffeescriptKeywords = ("and break catch class continue delete do else extends false finally for " +
                  "if in instanceof isnt new no not null of off on or return switch then throw true try typeof until void while with yes").split(" ");

  function getCompletions(token, context, keywords, options) {
    var found = [], start = token.string;
    function maybeAdd(str) {
      if (str.indexOf(start) == 0 && !arrayContains(found, str)) found.push(str);
    }
    function gatherCompletions(obj) {
      if (typeof obj == "string") forEach(stringProps, maybeAdd);
      else if (obj instanceof Array) forEach(arrayProps, maybeAdd);
      else if (obj instanceof Function) forEach(funcProps, maybeAdd);
      for (var name in obj) maybeAdd(name);
    }

    if (context) {
      // If this is a property, see if it belongs to some object we can
      // find in the current environment.
      var obj = context.pop(), base;
      if (obj.type.indexOf("variable") === 0) {
        if (options && options.additionalContext)
          base = options.additionalContext[obj.string];
        base = base || window[obj.string];
      } else if (obj.type == "string") {
        base = "";
      } else if (obj.type == "atom") {
        base = 1;
      } else if (obj.type == "function") {
        if (window.jQuery != null && (obj.string == '$' || obj.string == 'jQuery') &&
            (typeof window.jQuery == 'function'))
          base = window.jQuery();
        else if (window._ != null && (obj.string == '_') && (typeof window._ == 'function'))
          base = window._();
      }
      while (base != null && context.length)
        base = base[context.pop().string];
      if (base != null) gatherCompletions(base);
    }
    else {
      // If not, just look in the window object and any local scope
      // (reading into JS mode internals to get at the local and global variables)
      for (var v = token.state.localVars; v; v = v.next) maybeAdd(v.name);
      for (var v = token.state.globalVars; v; v = v.next) maybeAdd(v.name);
      gatherCompletions(window);
      forEach(keywords, maybeAdd);
    }
    return found;
  }
})();