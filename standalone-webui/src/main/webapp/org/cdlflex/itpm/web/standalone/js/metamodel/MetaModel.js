/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com>
 * Represents the global model that contains all packages and hence all
 * classes and constructs of a  UML class model or an Ontology and
 * is used to transfer between UML and OWL (or reverse).
 */
var MetaModel = MetaElement.extend({
    init:function(name, namespace) {
    	this._super();
        this.class="org.cdlflex.mdd.umltuowl.metamodel.MetaModel"; //necessary for serializing the meta model;
        this.name=name;
        this.namespace=namespace;
        if(this.namespace == null) {
        	this.namespace = '';
        }
        this.description="";
        this.packages=new Array();
    },

    /**
     * find a meta class by its identifier (internal) an return it.
     * @param classId unique class id (!= class name).
     * @return the meta class with the given class id or {@code null},
     * if no meta class matched.
     */
    findClassById:function(classId) {
        for(var i=0; i < this.packages.length; i++) {
            var p = jQuery.extend(new MetaPackage(''), this.packages[i]);
            var metaClass = p.findClassById(classId); 
            if(metaClass != null) {
                return metaClass;
            }
        }
        return null;
    },
    
    findClassesByName:function(className) {
    	var list = new Array();
        for(var i=0; i < this.packages.length; i++) {
            var p = jQuery.extend(new MetaPackage(''), this.packages[i]);
            var metaClass = p.findClassByName(className);  
            if(metaClass != null) {
                list.push(metaClass);
            }
        }
        return list;
    },
    
    findEnumClassByName: function(classId) {
    	for(var i=0; i < this.packages.length; i++) {
            var p = jQuery.extend(new MetaPackage(''), this.packages[i]);
            var metaClass = p.findClassByName(classId);  
            if(metaClass != null && metaClass.enumClass) {
                return metaClass;
            }
        }
        return null;
    },
    
    getName: function() {
    	return this.name;
    },
    

    /**
     * @return number of class elements that are contained in the whole meta model.
     */
    getSize:function() {
        var size = 0;
        for(var i=0; i < this.packages.length; i++) {
            var p = this.packages[i];
            size = size + p.classes.length;
        }
        return size;
    },

    /**
     * find a meta package by its name and return it.
     * @param packageName name of the package, case sensitive.
     * @return meta package or {@code null}, if package does not exist.
     */
    findPackageByName:function(packageName) {
        for(var i=0; i < this.packages.length; i++) {
            var p = this.packages[i];
            if(p.name==(packageName)) {
                return p;
            }
        }
        return null;
    },

    /**
     * find an association based on its ID. Should not be used to
     * often, because this method is computational expensive.
     * @param associationId name of the association ID
     * @return list of found meta associations. In case of unidirectional relationship,
     * 		   two associations are found. If ID is unknown, an empty list is returned.
     */
    findAssociationById:function(associationId) {
        var list = new Array();
        for(var i=0; i < this.packages.length; i++) {
            var p = jQuery.extend(new MetaPackage(''), this.packages[i]);
            for(var j=0; j< p.classes.length;j++) {
                var metaClass = p.classes[j];
                for(var k=0; k<metaClass.associations.length;k++) {
                    var ass = jQuery.extend(new MetaAssociation(), metaClass.associations[k]);
                    if(stripSpecialChars(ass.associationId) == stripSpecialChars(associationId)) {
                        list.push(ass);
                        if(ass.getInverseAssociation() != null
                            && stripSpecialChars(ass.getInverseAssociation().associationId) == stripSpecialChars(associationId)) {
                            list.push(ass.getInverseAssociation());
                            return list;
                        }
                    }
                }
            }
        }
        return list;
    },

    /**
     * same as {@code findPackagByName}, but creates a new package and
     * returns it, if package is not already existing.
     * @param packageName name of the package, case sensitive.
     * @return meta package; either existing one or a new one with empty class set.
     */
    managePackageByName:function(packageName) {
        for(var i=0; i < this.packages.length; i++) {
            var p = this.packages[i];
            if(p.name==(packageName)) {
                return p;
            }
        }
        var newPackage = new MetaPackage(packageName);
        this.packages.push(newPackage);
        return newPackage;
    },


    /**
     * @return whole model is returned as a string containing package, class and
     * attribute information.
     */
    toString:function() {
        var s = "";
        for(var i=0; i < this.packages.length; i++) {
            var pack = this.packages[i];
            s += "================= Package: " + pack.name + "=================\n";
            s += pack.toString();
        }
        return s;
    }

});