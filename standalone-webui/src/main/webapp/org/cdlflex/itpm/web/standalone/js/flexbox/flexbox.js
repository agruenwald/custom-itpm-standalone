/**
 * Flexibly configurable elements (logo, color, etc.)
**/
var FlexBox = Box.extend({
    init: function(title, color, fontColor, cssClasses, icon, x,y, id) {
	  	if(color == null) {
    		color='';
    	}
    	
    	if(icon == null || icon == '') {
    		icon = 'glyphicon glyphicon-user';
    	}
    	
    	if (fontColor == null) {
            switch (color) {
                case 'blue':
                    fontColor='white';
                    break;
                default:
                    fontColor = '#000';
            };
    	}

    	if (color.length > 0) {
    		color = 'background-color:' + color + ';';
    	}
    	color += 'color:'+fontColor + ';';

     	if (cssClasses == null) {
     	    cssClasses = 'flexbox ';
     	} else {
     	    cssClasses = 'flexbox ' + cssClasses.trim() + ' ';
     	}

     	this._super(title,cssClasses,x,y,id,color, '<span style="float:right;font-size:1.5em"><i class="'+icon+'"></i></span>');
        this.mainLabel.setEditable(false);
        this.setDragging(false);
    }
});


