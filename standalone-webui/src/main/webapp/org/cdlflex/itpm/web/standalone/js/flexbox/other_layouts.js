/**
  * Additional layout algorithms for the Flexbox.
**/
//SeqLayout, VLayout, StairLayout
var SeqLayout = TreeLayout.extend({
    init: function(graph) {
    	this._super(graph);
    	this.MARGIN_TOP = 20;
    	this.MARGIN_LEFT=0;
    	this.IS_CENTERED= true;
    	this.GAP_MIN = 20;
  		
    },
     
    layout: function() {
    	this.basicLayout(null);
    	//this.basicLayout(null); //workaround improves layout
    	
    	//we still have the calculated gap
    	this.improveBoxPositions(); //currently time-expensive (mainly because of straight-ahead positioning)
    	
    	this.lazyInit();
    	
    	//move all nodes into one array - ordered by sequence
    	var nodes = new Array();
    	for (var level = 0; level <= this.getMaxLevel(); level++) {
    		nodes = nodes.concat(this.levelAccess[level]);
    	}
    	var adaptParentHeight = $('#' + this.graph.getParentId()).height(this._maxHeightLevel(0) + this.MARGIN_TOP*2);
    	this.alignLeafes(nodes, this.MARGIN_TOP); 
    }
    
});

/**
 * The breakdown layout is an extension of the tree layout and is applied whenever there 
 *  are too many parent boxes (a hierarchical tree in that case would get too broad).
**/
var BreakDownLayout = TreeLayout.extend({
    init: function(graph) {
    	this._super(graph);
    	this.MARGIN_TOP = 10;
    	this.MARGIN_LEFT=20;
    	this.IS_CENTERED= false;
    	this.GAP = 10;
    	this.GAP_MIN = 20;
  		this.VER_GAP = 12;
  		this.BOX_MAXHEIGHT = 60;
  		this.STEP_LEFT = 15;
  		this.ZOOM_WHEN_PARENT_BOXES_EXCEED=4;
    },
    
    _adaptHeight: function(cssBox) {
    	cssBox.css('min-height',40); //clear min-height
    	cssBox.height(this.BOX_MAXHEIGHT);
    },
    
    /* adjust the height of the parent container */
    adjustParent: function() {
    	var boxes = this.getBoxesOnLevel(this.getMaxLevel());
    	var maxTop = 0;
    	for (var i=0; i < boxes.length; i++) {
    		maxTop = Math.max(maxTop, $('#' + boxes[i].getId()).position().top);
    	}
    	$('#' + this.graph.getParentId()).height(maxTop + 150);
    },
    
    /* align the boxes and give each box the same space */
    alignLevelEqually: function(levelNo, marginTop) {
    	var boxes = this.getBoxesOnLevel(levelNo);
    	var gapInfo = this.calculateGraphDataOnLevel(boxes);
    	var plannedWidth = gapInfo.plannedWidth; //maxWidth
		
		var newMaxWidth = gapInfo.maxWidth - this.MARGIN_LEFT;
		var boxWidth = (newMaxWidth - this.GAP_MIN * (boxes.length - 1))/boxes.length;
		
    	var cssBox = null;
    	var leftCount = this.MARGIN_LEFT;
    	for (var i=0; i < boxes.length; i++) {
    		var box = boxes[i];
    		var cssBox = $('#' + box.getId());
    		cssBox.width(boxWidth); //gapInfo.avgLeafWidth
    		var containerCss = $('#' + this.graph.getParentId());
    		cssBox.offset({left: containerCss.offset().left + leftCount, top: containerCss.offset().top +  this.MARGIN_TOP});
    		leftCount += this.GAP_MIN + cssBox.width();
    	}
    	//now take care about gaps and margin
    	//this.alignLeafes(boxes, marginTop);
    },
     
    layout: function() {
    	var maxLevel = this.getMaxLevel();
    	if (maxLevel < 0) {
    		console.info('No nodes!');
    		return;
    	}
    	
    	this.alignLevelEqually(0, this.MARGIN_TOP);
    	//this.alignLeafes(this.getBoxesOnLevel(0), this.MARGIN_TOP);
    	for (var level = 0; level <= this.getMaxLevel(); level++) {
    		var parents = this.getBoxesOnLevel(level);
    		for (var p = 0; p < parents.length; p++) {
    			var parent = parents[p];
    			var parentCss = $('#' + parent.getId());
    			this._adaptHeight(parentCss);
    			//get direct children of parent
    			//var incoming = this.graph.getBoxesIncoming(box.getId());
    			var children = this.graph.getBoxesOutgoing(parent.getId());
    			var top = this.graph.getParent().offset().top 
    				+ parentCss.height()
    				+ parentCss.position().top
    				+ this.VER_GAP;
    			//top += 	this.graph.getParent().position().top;
    			
    			for (var c = 0; c < children.length; c++) {
    				var parentCss = $('#' + parent.getId());
    				var childCss  = $('#' + children[c].getId());
    				var left1 = parentCss.position().left;
    				var stepl = (this.STEP_LEFT < 0 ? $(parentCss).width()/6 : this.STEP_LEFT);
    				childCss.offset({
    						top:  top, 
    						left: left1 + stepl + this.graph.getParent().offset().left
    				});
    				this._adaptHeight(childCss);
    				childCss.width(parentCss.width());
    				top += childCss.height() + this.VER_GAP;
    			}
    		}
    	}	
    	$('.flexline').remove(); //remove all lines (not needed)
    	//zoom out a little bit (decrease font-size) if too many boxes
    	
    	if (this.getBoxesOnLevel(0).length > this.ZOOM_WHEN_PARENT_BOXES_EXCEED) {
    		$('.flexbox').addClass('flexbox-zoom');
    	}
    	
    	this.adjustParent();
    	
    }
    
});

