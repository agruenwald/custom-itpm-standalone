package org.cdlflex.itpm.web.generic.chart;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.cdlflex.itpm.generated.model.Diagram;
import org.cdlflex.itpm.generated.model.Kpi;
import org.cdlflex.itpm.generated.model.OutputFormat;
import org.cdlflex.itpm.generated.model.PMRule;
import org.cdlflex.itpm.generated.model.SPARQL;
import org.cdlflex.itpm.generated.model.Table;
import org.cdlflex.itpm.web.queryEditor.RuleService;
import org.cdlflex.mdd.sembase.MDDException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Get different types of visualization elements which can be rendered in the Dashboard. (e.g. charts, tables, blocks).
 * 
 */
public class DashboardFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(DashboardFactory.class);
    private final RuleService ruleService;
    private List<PMRule> rules;
    private boolean refresh;

    /**
     * Create a new dashboard factory and reset the cache.
     * 
     * @param ruleService
     */
    public DashboardFactory(RuleService ruleService) {
        this.ruleService = ruleService;
        refresh = true;
        rules = null;
    }

    /**
     * Get all dashboard elements for a certain type of rule format and for SPARQL.
     * 
     * @param format the format, e.g. diagram
     * @return a list with (abstract) renderable components
     * @throws MDDException
     */
    public List<AbstractDashboardComponent> retrieveDashboardComponentsOfType(List<OutputFormat> formats)
        throws MDDException {
        List<AbstractDashboardComponent> list = new ArrayList<AbstractDashboardComponent>();
        if (refresh) {
            rules = ruleService.readRules(refresh);
            Collections.sort(rules, new DashboardComponentSorter());
        }
        refresh = false;
        List<PMRule> filteredRules = this.filterShowInDashboard(rules);
        filteredRules = ruleService.filterRulesByOutputFormat(filteredRules, formats);
        filteredRules = ruleService.filterRulesByQueryFormat(filteredRules, new SPARQL());
        for (PMRule rule : filteredRules) {
            List<List<String>> ruleResults = new ArrayList<List<String>>();
            try {
                ruleResults = ruleService.getRuleResults(rule);
                list.add(this.createComponent(ruleResults, rule, rule.getOutput()));
            } catch (MDDException e) {
                LOGGER.error("Rule {}: {}", rule.getName(), e.getMessage());
                continue;
            }
        }
        return list;
    }

    private List<PMRule> filterShowInDashboard(List<PMRule> rules) {
        List<PMRule> filteredRules = new ArrayList<PMRule>();
        for (PMRule rule : rules) {
            if (rule.getDashboard()) {
                filteredRules.add(rule);
            }
        }
        return filteredRules;
    }

    private AbstractDashboardComponent createComponent(List<List<String>> ruleResults, PMRule rule,
        OutputFormat format) throws MDDException {
        if (rule.getOutput().getId().equals(new Diagram().getId())) {
            return new ChartService().readChart(rule, ruleResults, false);
        } else if (format.getId().equals(new Kpi().getId())) {
            return readKpi(ruleResults, rule);
        } else if (format.getId().equals(new Table().getId())) {
            return readTable(ruleResults, rule);
        } else {
            throw new MDDException(String.format("Unknown format %s.", format));
        }
    }

    private AbstractDashboardComponent readTable(List<List<String>> ruleResults, PMRule rule) {
        return new DashboardBasicComponent(rule.getName(), rule);
    }

    private AbstractDashboardComponent readKpi(List<List<String>> list, PMRule rule) throws MDDException {
        // one dimension, one record
        // this must be a KPI
        if (list.size() == 1) {
            List<String> rec = list.get(0);
            String value = rec.get(0);
            String cssStatus = rec.size() > 1 ? rec.get(1) : "";
            String icon = rec.size() > 2 ? rec.get(2) : "";
            KpiModel kpiModel = new KpiModel(rule.getName(), value, rule);
            kpiModel.setIcon(icon);
            kpiModel.setStatusCssClass(cssStatus);
            return kpiModel;
        } else {
            throw new MDDException(String.format(
                    "The query %s is not a valid KPI. A KPI must contain exactly one record.", rule.getName()));
        }
    }
}
