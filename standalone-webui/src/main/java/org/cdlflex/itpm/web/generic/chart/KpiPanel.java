package org.cdlflex.itpm.web.generic.chart;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.cdlflex.itpm.web.queryEditor.QueryEditorIntegratedPanel;
import org.cdlflex.itpm.web.queryEditor.QueryEditorStandalonePanel;
import org.cdlflex.itpm.web.queryEditor.RuleService;

/**
 * Panel for the visualization of a single Key Performance Indicator (KPI) within a box.
 */
public class KpiPanel extends Panel {
    private static final long serialVersionUID = 1L;
    private final KpiModel kpi;
    private final RuleService ruleService;
    private final FeedbackPanel notificationPanel;

    public KpiPanel(String id, KpiModel kpi, RuleService ruleService, FeedbackPanel notificationPanel) {
        super(id);
        this.kpi = kpi;
        this.ruleService = ruleService;
        this.notificationPanel = notificationPanel;
        plot();
    }

    public String plot() {
        WebMarkupContainer box = new WebMarkupContainer("kpi-container");
        String status = kpi.getStatusCssClass().isEmpty() ? "flexBox-warning" : kpi.getStatusCssClass();
        status += " anybox";
        String glyphicon = kpi.getIcon().isEmpty() ? "icon-star" : kpi.getIcon();
        box.add(new AttributeModifier("class", String.format("dashboard-stat %s", status)));
        Label title = new Label("title", Model.of(kpi.getRule().getName()));
        Label text = new Label("text", Model.of(kpi.plot()));

        WebMarkupContainer icon = new WebMarkupContainer("icon");
        icon.add(new AttributeModifier("class", String.format("glyphicon glyph%s", glyphicon)));
        box.add(title);
        box.add(icon);
        box.add(text);
        add(box);

        // integrated query editor.
        QueryEditorStandalonePanel queryEditor =
            new QueryEditorStandalonePanel("query-editor", notificationPanel, kpi.getRule(), ruleService);
        QueryEditorIntegratedPanel integratedQueryEditor =
            new QueryEditorIntegratedPanel("query-editor-integrated", box, queryEditor, null);
        add(integratedQueryEditor);
        return null;
    }
}
