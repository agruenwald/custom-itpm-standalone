package org.cdlflex.itpm.web.queryEditor.accordionStructure;

import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.feedback.IFeedbackMessageFilter;

/**
 * The error filter is used to bubble up error and info messages in the nested accordion structure of the Query Editor.
 */

public class FilterError implements IFeedbackMessageFilter {
    private static final long serialVersionUID = 1L;

    public boolean accept(FeedbackMessage message) {
        return message.getLevel() >= FeedbackMessage.WARNING ? true : false;
    }
}
