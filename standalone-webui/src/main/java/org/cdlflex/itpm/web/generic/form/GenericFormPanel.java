package org.cdlflex.itpm.web.generic.form;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.Model;
import org.cdlflex.itpm.web.generic.table.IDataContainer;
import org.cdlflex.itpm.web.metamodeltools.MetaModelBrowser;
import org.cdlflex.itpm.web.reusable.NotificationPanelBootstrap;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAO;
import org.cdlflex.mdd.sembase.semaccess.MDDBeanInterface;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.cdlflex.mdd.umltuowl.metamodel.MetaComment;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Form Panel for the creation of generic forms based either on the metaModel or based on data source configurations
 * defined by the end user. Rendering forms based on data source configurations however has been removed because it is
 * currently not implemented completely.
 */
public class GenericFormPanel extends Panel {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(GenericFormPanel.class);
    private static final String BUTTON_TEXT_MAIN_FORM_ADD = "Add Entry";
    private static final String BUTTON_TEXT_MAIN_FORM_EDIT = "Save Changes";
    private static final String BUTTON_TEXT_SUB_FORM_ADD = "Add Subentry";
    private static final String BUTTON_TEXT_SUB_FORM_EDIT = "Update Changes in Sub Entry";
    private static final boolean SHOW_COMMENT_SUBFORM_OFF = false;
    private FormBuilderMetaClass formBuilder;
    private Button btn;
    private final IDataContainer rootDataContainer;
    private final Form<GenericFormPanel> form;
    private final RepeatingView repeating;
    private final String modelName;
    private MetaClass metaClass;
    private final MetaAssociation subBeanAssociation;
    private final String recId;
    private MDDBeanInterface rootBean;
    private final FormBuilderMetaClass rootFormBuilder;
    private final int level;
    private boolean isSubForm;
    private String metaClassName;

    /**
     * Create a new form panel
     * 
     * @param panelId the panel id
     * @param modelName the name of the metamodel, e.g. itpm or config
     * @param metaClassName the meta class name the way it is stored in the umlTUowl meta mdoel
     * @param recId the record id or empty if there is none (to add entries)
     * @param isSubForm set to true if it is a subform and use false for the root form (the very first form)
     * @param rootBean set to null if it is the root form and pass the root bean to subforms, subsequently.
     * @param subBeanAssociation the association that links subforms to the main form.
     * @param iDataContainer anchor to navigate to certain states within the overview<->form hierarchy.
     */
    public GenericFormPanel(String panelId, String modelName, String metaClassName, String recId, boolean isSubForm,
            FormBuilderMetaClass rootFormBuilder, MetaAssociation subBeanAssociation, IDataContainer iDataContainer) {
        this(panelId, modelName, metaClassName, recId, isSubForm, rootFormBuilder, subBeanAssociation,
                iDataContainer, 0);
    }

    public GenericFormPanel(String panelId, String modelName, String metaClassName, String recId, boolean isSubForm,
            FormBuilderMetaClass rootFormBuilder, MetaAssociation subBeanAssociation, IDataContainer iDataContainer,
            int level) {
        super(panelId);
        this.modelName = modelName;
        this.subBeanAssociation = subBeanAssociation;
        this.recId = recId;
        this.level = level;
        this.rootDataContainer = iDataContainer;
        this.rootFormBuilder = rootFormBuilder;
        this.isSubForm = isSubForm;
        this.metaClassName = metaClassName;
        form = new Form<GenericFormPanel>("form");
        form.setOutputMarkupId(true);
        add(form);
        repeating = new RepeatingView("line");
        repeating.setOutputMarkupId(true);
        form.add(repeating);
        NotificationPanelBootstrap notificationPanel = new NotificationPanelBootstrap("notify");
        form.add(notificationPanel);

    }

    /**
     * Render the form. Because this action is quite expensive it should only be triggered when the component is
     * actually visible.
     */
    public void renderForm() {
        MetaClass metaClass = null;
        MDDBeanInterface rootBean = null;
        try {
            rootBean = rootFormBuilder == null ? null : rootFormBuilder.getRootBean();
            if (recId.isEmpty()) {
                // workaround for clone
                if (rootFormBuilder == null && !isSubForm) {
                    rootBean =
                        new ConnectionUtil().getFactory().createBean(metaClassName,
                                new ConnectionUtil().getOpenConnection(modelName));
                }
            } else {
                // workaround for clone
                if (rootFormBuilder == null && !isSubForm) {
                    DAO<?> dao =
                        new ConnectionUtil().getFactory().create(metaClassName,
                                new ConnectionUtil().getOpenConnection(modelName));
                    rootBean = (MDDBeanInterface) dao.readRecord(recId);
                }
            }

            try {
                MetaModel mm =
                    new MetaModelBrowser().getMetaModelClasses(new ConnectionUtil().getOpenConnection(modelName)
                            .getNamespacePrefix());
                metaClass = mm.findClassByName(metaClassName);
                if (metaClass == null) {
                    throw new MDDException(String.format("Could not find metaClass %s.", metaClass));
                }
            } catch (MDDException e) {
                logger.error(e.getMessage());
            }

            String infoText = "";
            for (MetaComment c : metaClass.getCommentByType(MetaComment.TYPE_COMMENT)) {
                String ct = c.getText().isEmpty() ? infoText : c.getText();
                infoText = infoText.isEmpty() ? ct : " " + ct;
            }

            Label optionalForm = new Label("optionalDescr", infoText);
            if (infoText.isEmpty() || (isSubForm && SHOW_COMMENT_SUBFORM_OFF)) {
                optionalForm.setVisible(false);
            }

            this.metaClass = metaClass;
            this.rootBean = rootBean;

            form.add(optionalForm);
            buildForm(isSubForm);

            String buttonText = getButtonText(!recId.isEmpty(), isSubForm);
            btn = formBuilder.getSubmitButton(form, buttonText);
            form.add(btn);
        } catch (MDDException e) {
            logger.error(e.getMessage());
        }
    }

    public void buildForm(boolean isSubForm) throws MDDException {
        repeating.removeAll();
        formBuilder =
            isSubForm ? new FormBuilderMetaClass(isSubForm, rootBean, rootFormBuilder, recId, rootDataContainer,
                    level) : new FormBuilderMetaClass(isSubForm, rootBean, null, recId, rootDataContainer, level);
        formBuilder.buildForm(repeating, modelName, metaClass, subBeanAssociation, recId);
    }

    public FormBuilderMetaClass getFormBuilder() {
        return formBuilder;
    }

    public void updateButton(boolean isUpdate, boolean isSubForm) {
        String text = getButtonText(isUpdate, isSubForm);
        btn.setModel(Model.of(text));
    }

    public String getButtonText(boolean isUpdate, boolean isSubForm) {
        String buttonText = "";
        if (isSubForm) {
            if (!isUpdate) {
                buttonText = BUTTON_TEXT_SUB_FORM_ADD;
            } else {
                buttonText = BUTTON_TEXT_SUB_FORM_EDIT;
            }
        } else {
            if (!isUpdate) {
                buttonText = BUTTON_TEXT_MAIN_FORM_ADD;
            } else {
                buttonText = BUTTON_TEXT_MAIN_FORM_EDIT;
            }
        }
        return buttonText;
    }

    public IDataContainer getRootDataContainer() {
        return rootDataContainer;
    }

    public Form<GenericFormPanel> getForm() {
        return form;
    }
}
