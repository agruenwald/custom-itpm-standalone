package org.cdlflex.itpm.web.reusable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.ISortableDataProvider;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.cdlflex.itpm.web.standalone.BasePage;
import org.cdlflex.itpm.web.utils.Util;
import org.cdlflex.mdd.sembase.MDDException;

/**
 * Default table template with convenient methods to display simple tables with titles based on generic data.
 */
public abstract class TableTemplatePage extends BasePage {
    private static final long serialVersionUID = 1L;
    private final List<Map<String, String>> data;
    private final String defaultSortAttr;

    public TableTemplatePage(String defaultSortAttr) {
        this(new PageParameters(), defaultSortAttr);
    }

    public TableTemplatePage(final PageParameters parameters, String defaultSortAttr) {
        add(new NotificationPanelBootstrap("infoFeedback"));
        this.data = new ArrayList<Map<String, String>>();
        this.defaultSortAttr = defaultSortAttr;
    }

    /**
     * @return a list of ordered titles of the table
     * @throws MDDException
     */
    public abstract List<String> getDataTitles() throws MDDException;

    /**
     * a list of records. each records contains a list with the values. The length of the values should be equals to the
     * length of the titles.
     * 
     * @return
     * @throws MDDException
     */
    public abstract List<List<String>> getDataRecords() throws MDDException;

    public List<Map<String, String>> getTableData() {
        return data;
    }

    /**
     * get a table component which can be added to the page.
     * 
     * @param tableId the wicket id of the table.
     * @return
     * @throws MDDException
     */
    public DefaultDataTable<Map<String, String>, String> getDataTableComponent(String tableId) throws MDDException {
        data.clear();
        data.addAll(Util.listToMap(getDataTitles(), getDataRecords(), false));
        ISortableDataProvider<Map<String, String>, String> dataProvider =
            new GenericSortableDataProvider(data, defaultSortAttr);
        List<InteractiveTableColumn> columns = new ArrayList<InteractiveTableColumn>();
        for (String key : getDataTitles()) {
            columns.add(new InteractiveTableColumn(Model.of(key), key));
        }
        if (columns.isEmpty()) {
            columns.add(new InteractiveTableColumn(Model.of("-"), "-"));
        }
        DefaultDataTable<Map<String, String>, String> defaultDataTable =
            new DefaultDataTable<Map<String, String>, String>(tableId, columns, dataProvider, 20);
        defaultDataTable.setOutputMarkupId(true);
        return defaultDataTable;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void refreshTable(DefaultDataTable<Map<String, String>, String> table) throws MDDException {
        data.clear();
        data.addAll(Util.listToMap(getDataTitles(), getDataRecords(), false));
        List columns = new ArrayList<InteractiveTableColumn>();
        for (String key : getDataTitles()) {
            columns.add(new InteractiveTableColumn(Model.of(key), key));
        }
        if (columns.isEmpty()) {
            columns.add(new InteractiveTableColumn(Model.of("-"), "-"));
        }
        table.getColumns().clear();
        table.getColumns().addAll(columns);
    }
}
