package org.cdlflex.itpm.web.generic.flexGraph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Representation of a Flexbox, a single node within the Flex-graph which is used to transfer between the Java-world and
 * JavaScript.
 */
public class FlexBox {
    /** use these classes to provide the boxes with a default color/layout */
    public static final List<String> VALID_CLASSES = Arrays.asList(new String[] { "flexBox-success",
        "flexBox-danger", "flexBox-warning", "flexBox-info", "flexBox-primary", "flexBox-default" });

    private String id;
    private String title;
    private String text;
    private String cssClass;
    private List<String> parentIds;
    private String icon;

    public FlexBox() {
        this("", "", "", "");
    }

    public FlexBox(String id, String title, String text, String icon) {
        this(id, title, text, icon, "");
    }

    public FlexBox(String id, String title, String text, String icon, String cssClass) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.cssClass = cssClass;
        this.icon = icon;
        this.parentIds = new ArrayList<String>();

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCssClass() {
        return cssClass;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public List<String> getParentIds() {
        return parentIds;
    }

    public void setParentIds(List<String> parentIds) {
        this.parentIds = parentIds;
    }

    public String getIcon() {
        return icon;
    }

    /**
     * use glyphicons {@see http://getbootstrap.com/components/#glyphicons}
     * 
     * @param icon the name without glyph, e.g. icon-user for "glyphicon glyphicon-user"
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

}
