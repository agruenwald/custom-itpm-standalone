package org.cdlflex.itpm.web.synchronizer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AbstractDefaultAjaxBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.AjaxSelfUpdatingTimerBehavior;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptContentHeaderItem;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.IRequestParameters;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.util.time.Duration;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.config.generated.model.SyncRecord;
import org.cdlflex.itpm.web.reusable.TableTemplatePage;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.itpm.web.utils.HtmlUtil;
import org.cdlflex.itpm.web.utils.IconUtil;
import org.cdlflex.itpm.web.utils.Util;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAO;
import org.openengsb.core.api.security.annotation.SecurityAttribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//@PaxWicketMountPoint(mountPoint = "synchronizer")
@SecurityAttribute(key = "org.openengsb.ui.component", value = "USER_ADMIN")
public class SynchronizerPage extends TableTemplatePage {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(SynchronizerPage.class);
    private static final String PARAM_ACTION = "action";
    private static final String PARAM_CONFIG_ID = "configId";
    private static final String DEFAULT_SORT = "name";
    private static final String PARAM_STATUS_ON_OFF = "toggle-activation-status";
    private static final String DEACTIVATE_ALL = "Deactivate All";
    private static final String ACTIVATE_ALL = "Activate All";
    private static final Integer REFRESH_SECS = 25; // 45
    private List<DataSourceConfig> cachedRecords = null;
    private DefaultDataTable<Map<String, String>, String> table;
    private boolean detail;
    private final Form<?> form;

    public SynchronizerPage() {
        super(DEFAULT_SORT);
        detail = false;
        try {
            table = this.getDataTableComponent("dataTable");
        } catch (MDDException e1) {
            logger.error(e1.getMessage());
            error(e1.getMessage());
        }
        table.setOutputMarkupId(true);
        table.setOutputMarkupPlaceholderTag(true);
        form = new Form<Object>("button-form");

        /* auto-refresh */
        AjaxSelfUpdatingTimerBehavior ajaxSelfUpdatingTimerBehavior =
            new AjaxSelfUpdatingTimerBehavior(Duration.seconds(REFRESH_SECS)) {
                private static final long serialVersionUID = 1L;

                @Override
                protected void onPostProcessTarget(AjaxRequestTarget target) {
                    logger.trace("Auto-refresh synchronizer view...");
                    try {
                        refreshTable(table);
                        target.add(table);
                        target.add(form);
                    } catch (MDDException e) {
                        logger.error("Failed in refreshing the synchronization page: {}", e.getMessage());
                    }
                }
            };
        // currently deactivated because it destroys registered JS
        table.add(ajaxSelfUpdatingTimerBehavior); // keep because required to import wicketJS

        form.add(table);
        form.setOutputMarkupId(true);
        final SyncButton syncButtonModel = new SyncButton();
        final String startText = initToggleActivationText();
        syncButtonModel.setButton(startText, "cog");
        AjaxLink<Void> syncButton = new AjaxLink<Void>("trigger-sync") {
            private static final long serialVersionUID = 1L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                boolean deactivateAll = isAllRecordsDeactivated();
                if (deactivateAll) {
                    syncButtonModel.setButton(DEACTIVATE_ALL, "cog");
                } else {
                    syncButtonModel.setButton(ACTIVATE_ALL, "off");
                }

                DAO<DataSourceConfig> configDAO = null;
                Connection configCon = null;
                try {
                    configCon = new ConnectionUtil().getOpenConfigConnection();
                    configDAO = new ConnectionUtil().getFactory().create(DataSourceConfig.class, configCon);
                    configCon.begin();
                    for (DataSourceConfig c : configDAO.readFulltext("")) {
                        c.setDeactivated(!deactivateAll);
                        configDAO.update(c);
                    }
                    configCon.commit();
                    refreshTable(table);
                } catch (MDDException e) {
                    try {
                        configCon.rollback();
                    } catch (MDDException e1) {
                    }
                    logger.error(e.getMessage());
                    error(e.getMessage());
                    return;
                }
                target.add(form);
                target.add(table);
                target.add(this);
            }
        };
        Label buttonText = new Label("trigger-sync-label", new PropertyModel<String>(syncButtonModel, "value"));
        buttonText.setEscapeModelStrings(false);
        syncButton.add(buttonText);
        form.add(syncButton);

        ReloadButton reloadButton = new ReloadButton("reload-data");
        form.add(reloadButton);

        ToggleSyncButton toggleSyncButton = new ToggleSyncButton("toggle-sync");
        form.add(toggleSyncButton);

        ToggleMsgButton toggleMsgButton = new ToggleMsgButton("toggle-msg", this);
        form.add(toggleMsgButton);

        this.add(form);
        /* interaction between icons and Wicket backend */
        final AbstractDefaultAjaxBehavior behave = createJSInteractionBehavior();
        table.add(behave);
    }

    private String initToggleActivationText() {
        String buttonText = "Toggle Configs";
        if (isAllRecordsDeactivated()) {
            // all deactivated
            buttonText = ACTIVATE_ALL;
        } else {
            buttonText = DEACTIVATE_ALL;
        }
        return buttonText;
    }

    private boolean isAllRecordsDeactivated() {
        if (cachedRecords != null) {
            int noActivated = 0;
            for (DataSourceConfig c : cachedRecords) {
                if (!c.getDeactivated()) {
                    noActivated++;
                }
            }
            return noActivated == 0;
        }
        return false;
    }

    @Override
    public List<String> getDataTitles() throws MDDException {
        if (!detail) {
            return Arrays.asList(new String[] { "Active", "Name", "Connector", "Status", "Stream", "Last Poll",
                "Last Sync" });
        } else {
            return Arrays.asList(new String[] { "Active", "Name", "Connector", "Status", "Message / Update info",
                "Stream", "Last Poll", "Last Sync" });
        }
    }

    private List<String> createRecord(DataSourceConfig c, SyncRecord syncRecord) {
        List<String> record = new ArrayList<String>();
        String firstCol = c.getName() + IconUtil.addHiddenId("hidden-record-id", c.getId());
        firstCol =
            (syncRecord == null || syncRecord.getDetectedSourceUrl().isEmpty() ? (HtmlUtil.a(c.getUrl(), firstCol,
                    "_blank")) : (HtmlUtil.a(syncRecord.getDetectedSourceUrl(), firstCol, "_blank")));
        firstCol = HtmlUtil.span(firstCol, "config-title");
        String status = this.getActivationStatus(c);
        record.add(this.getStatusOnOff(c));
        record.add(firstCol);
        record.add(c.getConnectorType());
        record.add(status);
        if (detail) {
            if (syncRecord == null) {
                record.add("");
            } else {
                record.add(HtmlUtil.small(syncRecord.getMsg() == null ? "" : syncRecord.getMsg()
                    + HtmlUtil.small(syncRecord.getOptMsg().isEmpty() ? "" : " / " + (syncRecord.getOptMsg()))));
            }
        }
        record.add(this.getStreamType(c));
        String lastUpdate = "-";
        String lastExec = "-";
        if (syncRecord != null) {
            lastUpdate = Util.getDateDistanceInfo(syncRecord.getLastChange());
            lastExec =
                syncRecord.getLastExecution() == null ? lastExec : Util.getDateDistanceInfo(syncRecord
                        .getLastExecution());
        }
        record.add(lastUpdate);
        record.add(lastExec);
        return record;
    }

    @Override
    public List<List<String>> getDataRecords() throws MDDException {
        List<List<String>> table = new ArrayList<List<String>>();
        ConnectionUtil cU = new ConnectionUtil();
        try {
            DAO<DataSourceConfig> configDAO =
                cU.getFactory().create(DataSourceConfig.class, cU.getOpenConfigConnection());
            List<DataSourceConfig> result = configDAO.readFulltext("");
            cachedRecords = result;
            for (DataSourceConfig c : result) {
                SyncRecord syncRecord = c.getSyncInfo();
                if (syncRecord != null) {
                    syncRecord =
                        cU.getFactory().create(SyncRecord.class, cU.getOpenConfigConnection())
                                .readRecordViaBean(syncRecord);
                    if (syncRecord == null) {
                        throw new MDDException(String.format("Sync record for %s not found!", c));
                    }
                }
                List<String> record = createRecord(c, syncRecord);
                table.add(record);
            }
        } catch (NullPointerException e) {
            // concurrent access and/or other Jena access issues may cause temporary exceptions which
            // should not bother the user.
            logger.error("Suppressed exception from the datastore: " + e.getMessage());
            cU.getOpenConfigConnection().rollback();
            cU.getFreshConfigConnection(); // try to reset connection
        }
        return table;
    }

    private String getActivationStatus(DataSourceConfig c) {
        SyncRecord syncRecord = c.getSyncInfo();
        if (syncRecord == null) {
            return "-";
        }
        String optMsg = syncRecord.getMsg().isEmpty() ? "" : String.format(" (%s)", syncRecord.getMsg());
        optMsg =
            syncRecord.getOptMsg().isEmpty() ? optMsg : optMsg + String.format("Info: %s", syncRecord.getOptMsg());
        optMsg = StringEscapeUtils.escapeHtml(optMsg);
        String matchedService = syncRecord == null ? "" : syncRecord.getMatchedServiceName();
        String connectorType = c.getConnectorType();
        String matchIcon = "";
        if (matchedService.isEmpty()) {
            matchIcon =
                IconUtil.createBSIcon(String.format("No service available for connector type %s.", connectorType),
                        "itpm-yellow", "flash", false);
        } else {
            matchIcon = "-";
        }
        boolean isClickableStatus = true;
        String status = "";
        if (syncRecord.getRunning()) {
            status =
                IconUtil.createBSIconOnOff("The synchronization process is currently taking place.",
                        "streaming-status-icon", "repeat", true, isClickableStatus);
        } else if (syncRecord.getStatusOk()) {
            status =
                IconUtil.createBSIconGoodBad("Up and running" + optMsg, "streaming-status-icon", "ok-sign", true,
                        isClickableStatus);
        } else if (matchedService.isEmpty()
            || (!syncRecord.getLastActionDownstream() && !syncRecord.getLastActionUpstream())) {
            // either no service available yet or no run performed yet
            status = matchIcon;
        } else {
            status =
                IconUtil.createBSIconGoodBad("Last action failed" + optMsg, "streaming-status-icon", "remove-sign",
                        false, isClickableStatus);
        }
        return status;
    }

    /**
     * Get the html markup for an icon which shows the activation state.
     * 
     * @param c
     * @return
     */
    private String getStatusOnOff(DataSourceConfig c) {
        String icon = "";
        boolean isClickableStatus = true;
        if (c.getDeactivated()) {
            icon =
                (IconUtil.createBSIconOnOff("The configuration record is currently deactivated.",
                        "streaming-icon-on-off", "off", c.getDeactivated(), isClickableStatus));
        } else {
            icon =
                (IconUtil.createBSIconOnOff("The configuration is activated.", "streaming-icon-on-off",
                        "play-circle", !c.getDeactivated(), isClickableStatus));
        }
        return icon;
    }

    private String getStreamType(DataSourceConfig c) {
        String icon = "";
        boolean isClickableStatus = true;
        if (c.getDownstream() && c.getUpstream()) {
            icon =
                (IconUtil.createBSIcon("Upstream and downstream are activated.", "streaming-icon", "sort",
                        isClickableStatus));
        } else if (c.getDownstream()) {
            icon =
                (IconUtil.createBSIcon("Downstream is activated.", "streaming-icon", "cloud-download",
                        isClickableStatus));
        } else if (c.getUpstream()) {
            icon =
                (IconUtil.createBSIcon("Upstream is activated.", "streaming-icon", "cloud-upload", isClickableStatus));
        } else {
            icon =
                (IconUtil.createBSIcon("No upstream and no downstream (?)", "streaming-icon", "question-sign",
                        isClickableStatus));
        }
        return icon;
    }

    private AbstractDefaultAjaxBehavior createJSInteractionBehavior() {
        return new AbstractDefaultAjaxBehavior() {
            private static final long serialVersionUID = 1L;

            public void renderHead(Component component, IHeaderResponse response) {
                String script = "var synchTableURL ='" + this.getCallbackUrl() + "';";
                response.render(new JavaScriptContentHeaderItem(script, "", ""));
            }

            @Override
            protected void respond(final AjaxRequestTarget target) {
                ConnectionUtil cU = new ConnectionUtil();
                // Wicket.Ajax.get({ u: synchTableURL + '&action=toggle-status&configId=10' })
                // var x = Wicket.Ajax.get({ u: synchTableURL + '&action=toggle-status&configId=team-a', sh:
                IRequestParameters params = RequestCycle.get().getRequest().getRequestParameters();
                String action = params.getParameterValue(PARAM_ACTION).toString("");
                String configId = params.getParameterValue(PARAM_CONFIG_ID).toString("");
                if (configId.isEmpty()) {
                    logger.error(String.format("Empty configId in ajax request on synchronizer."));
                    return;
                }

                try {
                    DAO<DataSourceConfig> dsDAO =
                        cU.getFactory().create(DataSourceConfig.class, cU.getOpenConfigConnection());
                    dsDAO.setReadLinkedObjectAttributesLevel1(true);
                    DataSourceConfig config = dsDAO.readRecord(configId);
                    if (config == null) {
                        logger.error(String.format("No configId with %s exists.", configId));
                    } else {
                        if (action.equals("toggle-status")) {
                            if (!config.getUpstream() && config.getDownstream()) {
                                config.setDownstream(false);
                                config.setUpstream(true);
                            } else if (config.getUpstream() && !config.getDownstream()) {
                                config.setDownstream(true);
                                config.setUpstream(true);
                            } else {
                                config.setDownstream(true);
                                config.setUpstream(false);
                            }
                            dsDAO.update(config);
                        } else if (action.equals(PARAM_STATUS_ON_OFF)) {
                            config.setDeactivated(!config.getDeactivated());
                            dsDAO.update(config);
                        } else if (action.equals("reset-status")) {
                            if (config.getSyncInfo() != null) {
                                config.getSyncInfo().setLastExecution(null);
                                config.getSyncInfo().setRunning(false);
                                config.getSyncInfo().setLastExecutedBy("--");
                                cU.getFactory().create(SyncRecord.class, cU.getOpenConfigConnection())
                                        .update(config.getSyncInfo());
                            }
                        } else {
                            logger.error(String.format("Empty configId in ajax request on synchronizer."));
                            return;
                        }
                        logger.debug(String.format("Updated %s.", config));
                        table = getDataTableComponent("dataTable");
                        target.add(table);
                        target.add(getComponent());
                    }
                } catch (MDDException e) {
                    logger.error(e.getMessage());
                }
            }
        };
    }

    public boolean isDetail() {
        return detail;
    }

    public void setDetail(boolean d) {
        this.detail = d;
    }

    public Form<?> getForm() {
        return form;
    }

    protected DefaultDataTable<Map<String, String>, String> getTable() {
        return table;
    }
}
