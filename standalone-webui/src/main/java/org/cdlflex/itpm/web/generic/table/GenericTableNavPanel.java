package org.cdlflex.itpm.web.generic.table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.list.AbstractItem;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.cdlflex.itpm.generated.model.PMRule;
import org.cdlflex.itpm.generated.model.SPARQL;
import org.cdlflex.itpm.web.metamodeltools.MetaModelBrowser;
import org.cdlflex.itpm.web.reusable.NotificationPanelBootstrap;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAO;
import org.cdlflex.mdd.sembase.semquery.QueryInterface;
import org.cdlflex.mdd.sembase.semquery.QueryMap;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;

/**
 * Navigation for the generic table. The navigation either lists all defined queries, or all available meta classes in
 * the central ITPM model which have instances associated.
 */
public class GenericTableNavPanel extends Panel {
    private static final String DEFAULT_MODEL_NAME = "itpm";
    private static final String CONFIG_MODEL_NAME = "config";

    private static final String BUTTON_ACTIVATE_CLASS = "label-itpm";
    private static final String SPARQL_ALL_CLASSES = "SELECT ?className (count(?instance) as ?no) WHERE { "
        + "?className a owl:Class. " + "?instance a ?className. "
        + "} GROUP by (?className) ORDER by asc (?className)";
    private static final boolean SHOW_NO_RECORDS = true;
    private static final boolean SKIP_EMPTY_METACLASSES = true;
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(GenericTableNavPanel.class);

    private final MetaModelBrowser metaModelBrowser;
    private NotificationPanelBootstrap notificationPanel;

    private final RepeatingView repeating;
    private final PageParameters parameters;
    private final Class<? extends Page> pageClass;
    private SearchForm searchForm;
    private boolean expandedMenu;
    private boolean expandMenuEnabled = true;

    private final BookmarkablePageLink<Object> itpmViewButton;
    private final BookmarkablePageLink<Object> configViewButton;
    private final BookmarkablePageLink<Object> definedQueriesView;
    private final AjaxLink<String> expandToggler;

    public GenericTableNavPanel(String panelId, final PageParameters parameters, Class<? extends Page> pageClass,
            GenericTablePage genericTablePage, IDataContainer iDataContainer, boolean triggerManually) {
        super(panelId);
        this.metaModelBrowser = new MetaModelBrowser();
        this.setOutputMarkupId(true);
        PageParameters itpmViewPars = new PageParameters();
        itpmViewPars.add("view", DEFAULT_MODEL_NAME);
        itpmViewButton = new BookmarkablePageLink<Object>("itpm-view", pageClass, itpmViewPars);
        add(itpmViewButton);

        definedQueriesView = new BookmarkablePageLink<Object>("default-view", pageClass, new PageParameters());
        add(definedQueriesView);

        PageParameters configViewPars = new PageParameters();
        configViewPars.add("view", CONFIG_MODEL_NAME);
        configViewButton = new BookmarkablePageLink<Object>("config-view", pageClass, configViewPars);
        add(configViewButton);

        repeating = new RepeatingView("table-nav");
        repeating.setOutputMarkupId(true);
        this.pageClass = pageClass;
        this.parameters = parameters;
        add(repeating);
        renderMenu(triggerManually, SKIP_EMPTY_METACLASSES);
        expandedMenu = true;

        searchForm = new SearchForm(iDataContainer, "searchForm");
        add(searchForm);

        expandToggler = new AjaxLink<String>("nav-collapse") {
            private static final long serialVersionUID = 1L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                renderMenu(false, expandedMenu);
                target.add(repeating.getParent());
                if (expandedMenu) {
                    this.add(new AttributeModifier("class", "navbar-expand-icon-span"));
                } else {
                    this.add(new AttributeModifier("class", "icon-flipped navbar-expand-icon-span"));
                }
            }
        };
        add(expandToggler);
        expandToggler.setVisible(expandMenuEnabled);
    }

    public SearchForm getSearchForm() {
        return searchForm;
    }

    protected void renderMenu(boolean triggerManually, boolean skipEmptyMetaClasses) {
        repeating.removeAll(); // reset
        if (parameters.get("view").toString("").equals(DEFAULT_MODEL_NAME)) {
            itpmViewButton.add(new AttributeAppender("class", new Model<String>(BUTTON_ACTIVATE_CLASS), " "));
            if (!triggerManually) {
                this.renderMetaModelMenu(DEFAULT_MODEL_NAME, new ConnectionUtil().getOpenITPMConnection(),
                        skipEmptyMetaClasses);
            }
        } else if (parameters.get("view").toString("").equals(CONFIG_MODEL_NAME)) {
            configViewButton.add(new AttributeAppender("class", new Model<String>(BUTTON_ACTIVATE_CLASS), " "));
            if (!triggerManually) {
                this.renderMetaModelMenu(CONFIG_MODEL_NAME, new ConnectionUtil().getOpenConfigConnection(),
                        skipEmptyMetaClasses);
            }
        } else {
            definedQueriesView.add(new AttributeAppender("class", new Model<String>(BUTTON_ACTIVATE_CLASS), " "));
            if (!triggerManually) {
                this.renderRuleMenu();
            }
        }
    }

    protected PMRule renderRuleMenu() {
        expandToggler.setVisible(false);
        List<PMRule> rules = new LinkedList<PMRule>();
        int idParameter = parameters.get("id").toInt(0);
        try {
            DAO<PMRule> dao =
                new ConnectionUtil().getFactory().create(PMRule.class, new ConnectionUtil().getOpenITPMConnection());
            Map<String, String> comp = new HashMap<String, String>();
            comp.put("output", "Table");
            rules = dao.readRecordByAttributeValues(comp);
        } catch (MDDException e) {
            logger.error(e.getMessage());
            notificationPanel.error(e.getMessage());
        }
        List<NavEntry> navEntries = new ArrayList<NavEntry>();
        PMRule selectedRule = null;
        for (PMRule rule : rules) {
            logger.info(String.format("Render navigation entry for rule \"%s\".", rule.getName()));
            NavEntry navEntry =
                new NavEntry(String.valueOf(rule.getId()), rule.getName(), 0, false, rule.getOutput().getId());
            if (idParameter == rule.getId()) {
                selectedRule = rule;
            }
            int noRecords = 0;
            if (SHOW_NO_RECORDS) {
                try {
                    noRecords = getSimpleDataFromRule(rule).size();
                    navEntry.setEntries(noRecords);
                } catch (MDDException e) {
                    logger.error(e.getMessage());
                }
            }
            navEntries.add(navEntry);
        }

        Collections.sort(navEntries, new AlphabeticNavEntrySorter());
        if (selectedRule == null) {
            for (PMRule rule : rules) {
                if (String.valueOf(rule.getId()).equals(navEntries.get(0).getLinkId())) {
                    selectedRule = rule;
                    break;
                }
            }
        }
        if (selectedRule != null) {
            this.generateNavBar(navEntries, String.valueOf(selectedRule.getId()), "", true);
        }
        return selectedRule;
    }

    protected MetaClass renderMetaModelMenu(String modelName, Connection con, boolean skipEmptyMetaClasses) {
        expandedMenu = !skipEmptyMetaClasses;
        MetaModel mm = null;
        try {
            mm = metaModelBrowser.getMetaModelClasses(modelName);
            Map<String, String> comp = new HashMap<String, String>();
            comp.put("output", "Table");
        } catch (MDDException e) {
            logger.error(e.getMessage());
            notificationPanel.error(e.getMessage());
        }

        MetaClass selectedClass = null;
        String idParameter = parameters.get("id").toString("");
        List<QueryMap> queryMap = new ArrayList<QueryMap>();
        try {
            QueryInterface queryInterface = new ConnectionUtil().getQueryInterface(new SPARQL().getId(), con);
            queryMap = queryInterface.queryMap(SPARQL_ALL_CLASSES);
        } catch (MDDException e) {
            logger.error(e.getMessage());

        } catch (Exception eOther) {
            try {
                con.rollback();
            } catch (Exception e) {

            }
        }

        List<NavEntry> navEntries = new ArrayList<NavEntry>();
        try {
            con.begin();
            for (MetaClass metaClass : mm.getAllClasses()) {
                NavEntry navEntry = new NavEntry(metaClass.getName(), metaClass.getName(), 0, false);
                if (metaClass.isEnumClass() || metaClass.isAbstractClass() || metaClass.isInterfaceClass()) {
                    continue;
                }
                if (itpmFilter(metaClass.getName(), mm.getNamespacePrefix())) {
                    continue;
                }

                int noRecords = 0;
                if (SHOW_NO_RECORDS) {
                    noRecords = this.getNumberOfRecords(metaClass.getName().replaceAll(" ", ""), queryMap);
                    navEntry.setEntries(noRecords);
                }

                if (SHOW_NO_RECORDS && skipEmptyMetaClasses && noRecords <= 0) {
                    continue;
                }

                if (idParameter.equals(metaClass.getName())) {
                    selectedClass = metaClass;
                }
                if (selectedClass != null && metaClass.getName().equals(selectedClass.getName())) {
                    selectedClass = metaClass;
                }
                navEntries.add(navEntry);
            }
            con.rollback();
        } catch (MDDException e) {
            logger.error(e.getMessage());
            try {
                con.rollback();
            } catch (MDDException e1) {
            }
        }

        Collections.sort(navEntries, new AlphabeticNavEntrySorter());
        if (selectedClass == null && navEntries.size() > 0) {
            for (MetaClass metaClass : mm.getAllClasses()) {
                if (metaClass.getName().equals(navEntries.get(0).getLinkId())) {
                    selectedClass = metaClass;
                    break;
                }
            }
        }
        this.generateNavBar(navEntries, selectedClass == null ? "" : selectedClass.getName(), modelName, false);
        return selectedClass;
    }

    /**
     * A specific filter do hide some of the concepts within the ITPM and the CONFIG model which are irrelevant for the
     * end-user.
     * 
     * @param metaClassName the name of the meta class (original name)
     * @param namespacePrefix the prefix of the meta-model, e.g itpm or config.
     * @return true if the the concept should be filtered and false if it should not be filtered (not filtered means
     *         that it will be displayed;
     */
    private static boolean itpmFilter(String metaClassName, String namespacePrefix) {
        if (namespacePrefix.equals("itpm")) {
            if (metaClassName.endsWith("Management") || metaClassName.equals("PMRule")) {
                return true;
            }
        } else if (namespacePrefix.equals("config")) {
            if (metaClassName.contains("Field") || metaClassName.contains("SyncInfo")) {
                return true;
            }
        }
        return false;
    }

    private void generateNavBar(List<NavEntry> navEntries, String selectedId, String view, boolean highlightGroups) {
        repeating.removeAll();
        String grpChange = "";
        for (NavEntry navEntry : navEntries) {
            if (highlightGroups && (!navEntry.getType().startsWith(grpChange) || grpChange.isEmpty())) {
                grpChange = navEntry.getType();
                AbstractItem item = new AbstractItem(repeating.newChildId());
                repeating.add(item);
                Label separator = new Label("table-nav-link", Model.of(navEntry.getType() + "s"));
                item.add(new AttributeAppender("class", new Model<String>("nav-separator"), " "));
                item.add(separator);
            }

            AbstractItem item = new AbstractItem(repeating.newChildId());
            repeating.add(item);
            PageParameters pars = new PageParameters();
            pars.add("id", navEntry.getLinkId());
            pars.add("view", view);
            pars.add("expandedmenu", this.expandedMenu);
            BookmarkablePageLink<Object> link = new BookmarkablePageLink<Object>("table-nav-link", pageClass, pars);
            if (navEntry.getLinkId().equals(selectedId)) {
                link.add(new AttributeAppender("class", new Model<String>("active"), " "));
            }
            item.add(link);
            Label label = new Label("no-records", navEntry.getEntries());
            if (navEntry.getEntries() <= 0) {
                label.setVisible(false);
            }
            link.setBody(new Model<String>(String.format("%s%s", navEntry.getName(), navEntry.getEntries() <= 0 ? ""
                : String.format(" (%s)", navEntry.getEntries()))));
        }
    }

    private List<List<String>> getSimpleDataFromRule(PMRule rule) throws MDDException {
        QueryInterface queryInterface =
            new ConnectionUtil().getQueryInterface(rule.getFormat().getId(), rule.getRuleDefinition());
        return queryInterface.query(rule.getRuleDefinition());
    }

    private int getNumberOfRecords(String metaClassName, List<QueryMap> queryMapList) {
        int res = -1;
        for (QueryMap qm : queryMapList) {
            if (qm.get("className").equalsIgnoreCase(metaClassName)) {
                res = Integer.parseInt(qm.get("no"));
                break;
            }

        }
        return res;
    }
}
