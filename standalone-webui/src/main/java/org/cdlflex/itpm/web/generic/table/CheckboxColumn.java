package org.cdlflex.itpm.web.generic.table;

import java.util.Map;

import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.TextFilteredPropertyColumn;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
 * Add a column with checkboxes to the table. The header element will contain a placeholder which will be replaced via
 * JS into a checkbox if required. The header-checkbox will toggle all other checkboxes and table rows on click.
 */
public class CheckboxColumn extends TextFilteredPropertyColumn<Object, String, String> {
    private static final long serialVersionUID = 1L;
    private static final String CHECKBOX_PLACEHOLDER_TOGGLE_ALL = "[Checkbox-Placeholder]";

    public CheckboxColumn(String propertyExpression, String optMetaClassName) {
        super(getTitleModel(), propertyExpression);
    }

    public static IModel<String> getTitleModel() {
        IModel<String> model = Model.of(CHECKBOX_PLACEHOLDER_TOGGLE_ALL);
        return model;
    }

    // add the LinkPanel to the cell item
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void populateItem(Item cellItem, String componentId, IModel model) {
        String firstAttr = "";
        try {
            firstAttr = ((Map<String, String>) (model.getObject())).values().iterator().next().toString();
        } catch (Exception e) {

        }
        if (firstAttr.isEmpty()) {
            cellItem.add(new Label(componentId, "not found"));
        } else {
            CheckboxCell checkboxCell = new CheckboxCell(componentId, "metaClassName", firstAttr);
            checkboxCell.setEscapeModelStrings(false);
            cellItem.add(checkboxCell);
        }
    }
}
