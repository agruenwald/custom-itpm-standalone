package org.cdlflex.itpm.web.reusable;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.wicket.extensions.markup.html.repeater.data.sort.SortOrder;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;

/**
 * Default Data provider for generic tables.
 */
public class GenericSortableDataProvider extends SortableDataProvider<Map<String, String>, String> {
    private static final long serialVersionUID = 1L;
    private final List<Map<String, String>> data;
    private final GenericSorter genericSorter;

    public GenericSortableDataProvider(List<Map<String, String>> data) {
        this(data, "");
    }

    public GenericSortableDataProvider(List<Map<String, String>> data, String defaultSortColumn) {
        this.data = data;
        this.genericSorter = new GenericSorter();
        this.setSort(defaultSortColumn, SortOrder.ASCENDING);
    }

    public long size() {
        return data.size();
    }

    @Override
    public Iterator<Map<String, String>> iterator(long first, long count) {
        String title = this.getSort() == null ? "" : this.getSort().getProperty();
        boolean asc = this.getSortState().getPropertySortOrder(title) == SortOrder.ASCENDING;
        // do not sort if the title is empty (preserve id order)
        if (!title.isEmpty()) {
            genericSorter.setColumnName(title);
            genericSorter.setDescending(!asc);
            Collections.sort(data, genericSorter);
        }
        return data.subList((int) first, (int) Math.min(first + count, data.size())).iterator();
    }

    @Override
    public IModel<Map<String, String>> model(Map<String, String> object) {
        return new CompoundPropertyModel<Map<String, String>>(object);
    }
}
