package org.cdlflex.itpm.web.generic.table;

import java.util.Map;

import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.TextFilteredPropertyColumn;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * Column for links in generic tables.
 */
@SuppressWarnings("rawtypes")
public class TableLinkColumn extends TextFilteredPropertyColumn {
    private String optMetaClassName;
    private PageParameters parameters;

    @SuppressWarnings("unchecked")
    public TableLinkColumn(IModel displayModel, String propertyExpression, String optMetaClassName,
            PageParameters parameters) {
        super(displayModel, propertyExpression);
        this.optMetaClassName = optMetaClassName;
        this.parameters = parameters;
    }

    private static final long serialVersionUID = -7257681204371916136L;

    // add the LinkPanel to the cell item
    @SuppressWarnings("unchecked")
    public void populateItem(Item cellItem, String componentId, IModel model) {
        String firstAttr = "";
        try {
            firstAttr = ((Map<String, String>) (model.getObject())).values().iterator().next().toString();
        } catch (Exception e) {

        }
        if (firstAttr.isEmpty()) {
            cellItem.add(new Label(componentId, "not found"));
        } else {
            cellItem.add(new CellLink(componentId, GenericTablePage.class, optMetaClassName, firstAttr, parameters));
            cellItem.setEscapeModelStrings(false);
        }
    }
}
