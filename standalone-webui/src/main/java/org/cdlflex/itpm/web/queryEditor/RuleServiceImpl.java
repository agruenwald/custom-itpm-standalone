package org.cdlflex.itpm.web.queryEditor;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.cdlflex.itpm.generated.model.KnowledgeArea;
import org.cdlflex.itpm.generated.model.OutputFormat;
import org.cdlflex.itpm.generated.model.PMRule;
import org.cdlflex.itpm.generated.model.Project;
import org.cdlflex.itpm.generated.model.QueryFormat;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Basic service for the simplified storage and retrieval of PM rules.
 * 
 */
public class RuleServiceImpl implements RuleService, Serializable {
    private static final long serialVersionUID = -2753092966958861450L;
    private static final Logger logger = LoggerFactory.getLogger(RuleServiceImpl.class);
    private List<PMRule> rules = null;

    /**
     * Constructor, containing the parameters which are injected via blueprint.
     * 
     * @param pmRuleDAO a DAO for managing project management rules (SPARQL, etc.).
     * @param qI a query interface for performing SPARQL queries in the knowledge base
     * @param kbCon the connection to the knowledge base, which can be reused
     * @throws MDDException is thrown if anything goes wrong during the initialization of the connection and the
     *         knowledge base interfaces.
     */
    public RuleServiceImpl() throws MDDException {
        super();
    }

    /**
     * Service shutdown method, is executed when stopping the bundle.
     */
    public void destroy() {
        logger.info("Shutdown {}", RuleService.class.getName());
    }

    public void insertRule(PMRule rule) throws MDDException {
        ConnectionUtil cu = new ConnectionUtil();
        Project project1 = readAndCreateProjectOnDemand("itpm");
        KnowledgeArea area = readAndCreateKnowledgeAreaOnDemand(rule.getBelongsTo().getName());
        rule.setBelongsTo(area);
        rule.setIsPMRuleOfProject(project1);
        cu.getFactory().create(PMRule.class, cu.getOpenITPMConnection()).insert(rule);
    }

    public void updateRule(PMRule rule) throws MDDException {
        ConnectionUtil cu = new ConnectionUtil();
        Project project1 = readAndCreateProjectOnDemand("itpm");
        KnowledgeArea area = readAndCreateKnowledgeAreaOnDemand(rule.getBelongsTo().getName());
        rule.setBelongsTo(area);
        rule.setIsPMRuleOfProject(project1);
        cu.getFactory().create(PMRule.class, cu.getOpenITPMConnection()).update(rule);
    }

    public void removeRule(PMRule rule) throws MDDException {
        ConnectionUtil cu = new ConnectionUtil();
        cu.getFactory().create(PMRule.class, cu.getOpenITPMConnection()).remove(rule);
    }

    @Override
    public List<PMRule> filterRulesByKnowledgeAreas(List<PMRule> allRules, List<String> knowledgeAreaNames) {
        List<PMRule> rules = new LinkedList<PMRule>();
        for (PMRule r : allRules) {
            for (String k : knowledgeAreaNames) {
                if (k.startsWith(r.getBelongsTo().getName())) {
                    rules.add(r);
                    break;
                }
            }
        }
        return rules;
    }

    public List<PMRule> filterRulesByKnowledgeArea(List<PMRule> allRules, String knowledgeAreaName) {
        return this.filterRulesByKnowledgeAreas(allRules, Arrays.asList(new String[] { knowledgeAreaName }));
    }

    @Override
    public List<PMRule> filterRulesByOutputFormat(List<PMRule> allRules, List<OutputFormat> outputFormats) {
        List<PMRule> rules = new LinkedList<PMRule>();
        for (PMRule r : allRules) {
            for (OutputFormat outputFormat : outputFormats) {
                if (outputFormat.getId().equals(r.getOutput().getId())) {
                    rules.add(r);
                    break;
                }
            }
        }
        return rules;
    }

    @Override
    public List<PMRule> filterRulesByQueryFormat(List<PMRule> allRules, QueryFormat format) {
        List<PMRule> rules = new LinkedList<PMRule>();
        for (PMRule r : allRules) {
            if (r.getFormat().getId().equals(format.getId())) {
                rules.add(r);
            }
        }
        return rules;
    }

    public List<PMRule> readRules() throws MDDException {
        return this.readRules(true); // no cache
    }

    public List<PMRule> readRules(boolean refresh) throws MDDException {
        if (this.rules == null || refresh) {
            rules =
                new ConnectionUtil().getFactory().create(PMRule.class, new ConnectionUtil().getOpenITPMConnection())
                        .readFulltext("");
        }
        return rules;
    }

    private KnowledgeArea readAndCreateKnowledgeAreaOnDemand(String classNameBean) throws MDDException {
        ConnectionUtil cu = new ConnectionUtil();
        DAO<Object> dao = cu.getFactory().create(classNameBean, cu.getOpenITPMConnection());
        KnowledgeArea bean = (KnowledgeArea) cu.getFactory().createBean(classNameBean, cu.getOpenITPMConnection());
        bean.setName(classNameBean);
        KnowledgeArea persistentBean = (KnowledgeArea) dao.readRecord(bean.getName());
        if (persistentBean == null) {
            dao.insert(bean);
            logger.debug("INSERTED " + classNameBean + " * for the very first * time.");
            return bean;
        } else {
            return persistentBean;
        }
    }

    private Project readAndCreateProjectOnDemand(String projectId) throws MDDException {
        ConnectionUtil cu = new ConnectionUtil();
        DAO<Project> dao = cu.getFactory().create(Project.class.getSimpleName(), cu.getOpenITPMConnection());
        Project bean = new Project();
        bean.setId(projectId);
        Project persistentBean = dao.readRecord(bean.getId());
        if (persistentBean == null) {
            throw new MDDException("Project " + projectId + " is missing.");
        } else {
            return persistentBean;
        }
    }

    @Override
    public List<List<String>> getRuleResults(PMRule rule) throws MDDException {
        List<List<String>> list = new LinkedList<List<String>>();
        list =
            new ConnectionUtil().getQueryInterface(rule.getFormat().getId(), rule.getRuleDefinition()).query(
                    rule.getRuleDefinition());
        // list = Util.parseAnnotations(list);
        return list;
    }
}
