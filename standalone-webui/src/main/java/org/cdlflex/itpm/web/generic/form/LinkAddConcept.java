package org.cdlflex.itpm.web.generic.form;

import java.util.Map;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaynberg.wicket.select2.Select2MultiChoice;

/**
 * Represents the icon/link which appears to add new/linked records within a subform of the root form.
 */
public class LinkAddConcept extends AjaxLink<String> {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(LinkAddConcept.class);
    private final Map<String, AssociationContainer> containers;
    private final GenericFormPanel subForm;
    private final String modelName;
    private final Select2MultiChoice<OptionEntry> select2Elem;
    private final MetaAssociation defaultMetaAssociation;
    private final FormBuilderMetaClass formBuilderMetaClass;
    private final MetaClass defaultSubclassType;
    private final AjaxLink<String> editConceptLink;
    private boolean open;

    public LinkAddConcept(String panelId, String modelName, Map<String, AssociationContainer> containers,
            GenericFormPanel subForm, Select2ClickBehavior tmpSelect2ClickBehavior,
            Select2MultiChoice<OptionEntry> select2Elem, MetaAssociation defaultMetaAssociation,
            final FormBuilderMetaClass formBuilderMetaClass, MetaClass defaultSubclassType,
            AjaxLink<String> editConceptLink) {
        super(panelId);
        this.setOutputMarkupId(true);
        this.setOutputMarkupPlaceholderTag(true);
        this.containers = containers;
        this.subForm = subForm;
        this.modelName = modelName;
        this.select2Elem = select2Elem;
        this.defaultMetaAssociation = defaultMetaAssociation;
        this.formBuilderMetaClass = formBuilderMetaClass;
        this.defaultSubclassType = defaultSubclassType;
        this.editConceptLink = editConceptLink;
        this.open = false;
    }

    @Override
    public void onClick(AjaxRequestTarget target) {
        FormBuilderMetaClass subFormBuilder =
            containers.get(defaultMetaAssociation.getName()).getSubForm().getFormBuilder();
        containers.get(defaultMetaAssociation.getName()).getSubForm().updateButton(false, true);
        subFormBuilder.getRepeatingView().removeAll();
        try {
            formBuilderMetaClass.setSelectedItemId("");
            subFormBuilder.buildForm(subFormBuilder.getRepeatingView(), modelName, defaultSubclassType,
                    defaultMetaAssociation, "");
        } catch (MDDException e) {
            logger.error(e.getMessage());
        }
        target.add(subFormBuilder.getRepeatingView().getParent());
        subForm.setVisible(!open);
        select2Elem.setVisible(open);
        target.add(subForm.getParent());
        target.add(select2Elem);
        editConceptLink.setVisible(false);
        open = !open; // toggle
    }
};
