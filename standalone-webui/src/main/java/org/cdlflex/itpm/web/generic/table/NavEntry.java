package org.cdlflex.itpm.web.generic.table;

import java.io.Serializable;

/**
 * A (sortable) entry in the navigation menu.
 */
public class NavEntry implements Serializable {
    private static final long serialVersionUID = -3275015934230210338L;
    private String linkId;
    private String name;
    private int entries;
    private boolean selected;
    private String type;

    public NavEntry(String linkId, String name, int entries, boolean selected) {
        this(linkId, name, entries, selected, "");
    }

    public NavEntry(String linkId, String name, int entries, boolean selected, String type) {
        this.linkId = linkId;
        this.name = name;
        this.entries = entries;
        this.selected = selected;
        this.type = type;
    }

    public String getLinkId() {
        return linkId;
    }

    public void setLinkId(String linkId) {
        this.linkId = linkId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEntries() {
        return entries;
    }

    public void setEntries(int entries) {
        this.entries = entries;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
