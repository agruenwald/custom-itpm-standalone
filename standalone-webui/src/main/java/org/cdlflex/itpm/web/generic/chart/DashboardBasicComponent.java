package org.cdlflex.itpm.web.generic.chart;

import org.cdlflex.itpm.generated.model.PMRule;

/**
 * Abstract interface to display charts/single diagrams of any type.
 */
public class DashboardBasicComponent implements AbstractDashboardComponent {
    private static final long serialVersionUID = 1L;
    private String title;
    private PMRule rule;

    public DashboardBasicComponent(String title, PMRule rule) {
        this.title = title;
        this.rule = rule;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String plot() {
        throw new IllegalAccessError("Please override this method.");
    }

    @Override
    public PMRule getRule() {
        return rule;
    }

}
