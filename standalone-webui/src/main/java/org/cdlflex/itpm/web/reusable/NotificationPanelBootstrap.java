package org.cdlflex.itpm.web.reusable;

import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.markup.html.panel.FeedbackPanel;

public class NotificationPanelBootstrap extends FeedbackPanel {

    public NotificationPanelBootstrap(String id) {
        super(id);
        this.setOutputMarkupId(true);
    }

    private static final long serialVersionUID = 1L;

    @Override
    protected String getCSSClass(final FeedbackMessage message) {
        String level = message.getLevelAsString().toLowerCase();
        if (level.equals("info")) {
            level = "success";
        } else if (level.equals("error")) {
            level = "warning";
        }
        return "feedbackPanel " + level;
    }

    /*
     * @Override public void newMessageDisplayComponent(id, message) {
     * 
     * }
     */

}
