package org.cdlflex.itpm.web.standalone;

import org.apache.wicket.markup.html.form.Form;
import org.cdlflex.itpm.web.synchronizer.ReloadButton;
import org.cdlflex.itpm.web.synchronizer.ResetOntButton;
import org.cdlflex.mdd.sembase.MDDException;

/**
 * Page is shown whenever an unexpected excepøion occurs. It overs ways to fix invalid db states etc.
 */
public class ErrorHandlingPage extends BasePage {
    private static final long serialVersionUID = -1580566678595285459L;

    public ErrorHandlingPage() throws MDDException {
        super();
        final Form<?> form = new Form<Object>("button-form");
        form.setOutputMarkupId(true);
        this.add(form);
        ReloadButton reloadButton = new ReloadButton("reload-data");
        form.add(reloadButton);
        ResetOntButton resetOntButton = new ResetOntButton("reset-ont");
        form.add(resetOntButton);
        // new ConnectionUtil().getOpenITPMConnection().toString();
    }

}
