package org.cdlflex.itpm.web.reusable;

import java.io.Serializable;

import org.apache.wicket.ajax.AjaxRequestTarget;

/**
 * This object can be passed to inject callback behavior on form submissions in the query editor.
 * 
 */
public interface ISubmitCallback extends Serializable {
    public void submit(boolean isNew, AjaxRequestTarget target);
}
