package org.cdlflex.itpm.web.generic.flexGraph;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.cdlflex.itpm.generated.model.Graph;
import org.cdlflex.itpm.generated.model.GraphSeq;
import org.cdlflex.itpm.generated.model.GraphStairs;
import org.cdlflex.itpm.generated.model.GraphV;
import org.cdlflex.itpm.generated.model.OutputFormat;

/**
 * A FlexGraphPanel enables the representation of tables in graph patterns. For instance hierarchies (team structures,
 * software architecture), sequences (e.g. Waterfall diagram) can be represented. Each node contains a title, some
 * optional description, an icon (Bootstrap Glyphicons are supported) and can be styled. While this panel has been
 * implemented natively it can be replaced by any other 3rd party library.
 */
public class FlexGraphPanel extends Panel {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(FlexGraphPanel.class);
    private static final String JS_TREE_LAYOUT = "TreeLayout";
    private static final String JS_V_LAYOUT = "VLayout";
    private static final String JS_SEQ_LAYOUT = "SeqLayout";
    private static final String JS_STAIR_LAYOUT = "StairLayout";
    private static final String DEFAULT_CSS_STATUS = "flexBox-info";
    private static final String ERROR_CSS_STATUS = "flexBox-warning";
    private final String markupId;
    private final Model<String> jsModel;

    public FlexGraphPanel(final String id, OutputFormat outputFormat, List<Map<String, String>> graphData) {
        super(id);
        markupId = String.valueOf(UUID.randomUUID().getMostSignificantBits());
        logger.trace(String.format("Called FlexGraphPanel with id=%s. Generated markupId=%s.", id, markupId));
        WebMarkupContainer container = new WebMarkupContainer("flexgraph");
        container.setOutputMarkupId(true);
        container.setMarkupId(markupId); // used for communication with JS
        this.add(container);
        jsModel = Model.of("");
        add(new Label("js-script", jsModel).setEscapeModelStrings(false));
        renderFlexGraph(outputFormat, graphData);
    }

    public void renderFlexGraph(OutputFormat outputFormat, List<Map<String, String>> graphData) {
        Map<String, FlexBox> flexGraph = mapDataToFlexGraph(graphData);
        String layout = getJSLayout(outputFormat);
        String graphJs = createJS(flexGraph, markupId, layout);
        jsModel.setObject(graphJs);
    }

    private String getMapValue(Map<String, String> map, String key, String defaultValue) {
        if (map.containsKey(key)) {
            return map.get(key);
        } else {
            return defaultValue;
        }
    }

    /**
     * translates the output format of a rule into the JS world.
     * 
     * @param outputFormat the output format in Java
     * @return a string containing a JavaScript keyword for referencing a specific layout or empty if the output format
     *         is either unknown or the output format is not a graph.
     */
    public String getJSLayout(OutputFormat outputFormat) {
        if (outputFormat.getId().equals(new Graph().getId())) {
            return JS_TREE_LAYOUT;
        } else if (outputFormat.getId().equals(new GraphV().getId())) {
            return JS_V_LAYOUT;
        } else if (outputFormat.getId().equals(new GraphSeq().getId())) {
            return JS_SEQ_LAYOUT;
        } else if (outputFormat.getId().equals(new GraphStairs())) {
            return JS_STAIR_LAYOUT;
        } else {
            return "";
        }
    }

    private Map<String, FlexBox> mapDataToFlexGraph(List<Map<String, String>> graphData) {
        final String ICON = "icon-user";

        Map<String, FlexBox> flexGraph = new HashMap<String, FlexBox>();
        int i = 0;
        for (Map<String, String> record : graphData) {
            if (i == 0 && (!record.containsKey("id") || !record.containsKey("parentId"))) {
                String errorText =
                    String.format("The graph data is missing some column keys: [id, title, parentId]"
                        + " are mandatory. text, icon, status] are optional.");
                logger.error(errorText);
                FlexBox box =
                    new FlexBox("invalid-flexgraph-query", "Parameters are missing",
                            "If you want to create a graph then you need to "
                                + "provide columns which match exactly the names (id,title,parentId).  "
                                + "Additionally text, icon (=glyphicons) and status (=style) may be used.", ICON,
                            ERROR_CSS_STATUS);
                flexGraph.put("invalid-flexgraph-query", box);
                break;
            } else if (!record.containsKey("title")) {
                logger.warn(String.format("Please add a title to the graph query. Currently the id will be used."));

            }

            String id = escape(record.get("id"));
            String parentId = escape(getMapValue(record, "parentId", ""));
            FlexBox box =
                new FlexBox(id, readable(escape(getMapValue(record, "title", "?title missing (id=" + id + ")"))),
                        readable(escape(getMapValue(record, "text", ""))), getMapValue(record, "icon", ICON),
                        getMapValue(record, "status", DEFAULT_CSS_STATUS));

            if (!parentId.isEmpty()) {
                // already existing. it is possible that several equivalent records exist with different superiors.
                if (flexGraph.containsKey(id)) {
                    flexGraph.get(id).getParentIds().add(parentId);
                    continue;
                } else {
                    box.getParentIds().add(parentId);
                }
            }
            flexGraph.put(id, box);
            i++;
        }
        return flexGraph;
    }

    /**
     * take care that strings become not too long ("unbreakable")
     * 
     * @param escape
     * @return
     */
    private String readable(String escape) {
        final boolean activated = false;
        if (activated) {
            String[] words = escape.split("\\s");
            String cleanedWord = "";
            if (words.length <= 0) {
                words = new String[] { escape };
            }
            for (String word : words) {
                if (word.length() > 15) {
                    word = word.replace("-", " ");
                    word = word.replace(":", " ");
                    cleanedWord += word + " ";
                } else {
                    cleanedWord = word;
                }
                cleanedWord = cleanedWord.trim();
            }
            return cleanedWord;
        } else {
            return escape;
        }
    }

    private String escape(String string) {
        string = string.replaceAll(":", ""); // jquery problem (pseudo-elem detection)
        string = string.replaceAll("\\.", "-"); // would result in invalid id elements
        string = string.replaceAll("\\s+", "-"); // would result in invalid id elements
        return string;
    }

    /**
     * Create the Javascript which renders the tree graph based on the given Java flexgraph.
     * 
     * @param flexGraph a map containing the nodes.
     * @param markupId the ID which identifies the div in which the graph is rendered ambigiously.
     * @param layout the layout which determines the graph structure. Use one of the static JS_LAYOUT variables.
     * @return executable javascript which can be passed to the wicket renderer.
     */
    private String createJS(Map<String, FlexBox> flexGraph, String markupId, String layout) {
        String flexboxPrefix = "flexbox" + (int) (Math.random() * 1000 + 1) + "-";
        String js = "var debugFlexFactory=null; //last layout for debugging \n";
        js += "jQuery(document).ready(function() {\n";
        js += String.format(" var flexContainerId = '%s';\n", markupId);
        js += " var flexGraph = new GraphFactory(flexContainerId);\n";
        js += " var flexBoxFactory = new FlexBoxFactory();\n";
        js += String.format(" var layout = flexBoxFactory.getLayout(flexGraph, '%s');\n", layout);
        js += String.format(" debugFlexFactory=flexBoxFactory;\n");
        int i = 0;
        for (FlexBox box : flexGraph.values()) {
            js +=
                String.format("     var flexBox%d = new FlexBox('%s', '', '', '%s', "
                    + "'glyphicon glyph%s', 100*(%d),100*(%d), '%s');\n", i, box.getTitle(), box.getCssClass(),
                        box.getIcon(), i, i, flexboxPrefix + box.getId());
            js += String.format("     flexGraph.register(flexBox%d);\n", i);
            js +=
                String.format(
                        "     var label%d = new Label('%s','umlAttribute' ,new UmlAttributeHandler(flexBox%d));\n",
                        i, box.getText(), i);
            js += String.format("     label%d.setEditable(false);\n", i);
            js += String.format("     label%d.setRemovable(false);\n", i);
            js += String.format("     label%d.renderLabel($('#'+flexBox%d.getId()));\n", i, i);
            i++;
        }

        for (FlexBox box : flexGraph.values()) {
            for (String p : box.getParentIds()) {
                js += " try {\n";
                js +=
                    String.format("     flexBoxFactory.connect(flexGraph, flexGraph.getBoxById('%s'), "
                        + "flexGraph.getBoxById('%s'), '', null, 'flexline');\n", flexboxPrefix + p, flexboxPrefix
                        + box.getId());
                js += " } catch (ex) { \n";
                js +=
                    String.format("		console.error(ex.toString(), 'tried to link %s and %s'); \n", flexboxPrefix + p,
                            flexboxPrefix + box.getId());
                js += "}\n";
            }
        }
        js += "     flexBoxFactory.intelligentLayouting(layout);\n";
        js += "});";
        return js;
    }
}
