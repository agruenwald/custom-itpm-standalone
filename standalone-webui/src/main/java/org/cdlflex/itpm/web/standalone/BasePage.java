package org.cdlflex.itpm.web.standalone;

import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

/**
 * All other pages must extend this class. The base page follows the same structure as the CDLFlex-EngSb base page to
 * ease integration. It's main purpose is to load the Javascript and CSS bundles into the HTML header.
 */
public class BasePage extends WebPage {
    private static final long serialVersionUID = 1L;

    public BasePage() {
        add(new NavMenu("navigation"));
    }

    public BasePage(PageParameters parameters) {
        super(parameters);
        add(new NavMenu("navigation"));
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);

        for (CssResourceReference bundle : WicketApplication.CSS_BUNDLES) {
            response.render(CssHeaderItem.forReference(bundle));
        }

        for (JavaScriptResourceReference bundle : WicketApplication.JS_BUNDLES) {
            response.render(JavaScriptHeaderItem.forReference(bundle));
        }
    }
}
