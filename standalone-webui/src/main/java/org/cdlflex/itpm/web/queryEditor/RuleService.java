package org.cdlflex.itpm.web.queryEditor;

import java.util.List;

import org.cdlflex.itpm.generated.model.OutputFormat;
import org.cdlflex.itpm.generated.model.PMRule;
import org.cdlflex.itpm.generated.model.QueryFormat;
import org.cdlflex.mdd.sembase.MDDException;

/**
 * Service abstraction to persist and retrieve rules efficiently (can be used in Blueprint and Spring)
 */
public interface RuleService {
    public void insertRule(PMRule rule) throws MDDException;

    public void updateRule(PMRule rule) throws MDDException;

    public void removeRule(PMRule rule) throws MDDException;

    public List<PMRule> filterRulesByKnowledgeArea(List<PMRule> allRules, String knowledgeAreaName);

    public List<PMRule> filterRulesByKnowledgeAreas(List<PMRule> allRules, List<String> knowledgeAreaName);

    public List<PMRule> filterRulesByQueryFormat(List<PMRule> allRules, QueryFormat format);

    public List<PMRule> filterRulesByOutputFormat(List<PMRule> allRules, List<OutputFormat> outputFormats);

    public List<PMRule> readRules() throws MDDException;

    public List<PMRule> readRules(boolean refresh) throws MDDException;

    public List<List<String>> getRuleResults(PMRule rule) throws MDDException;
}
