package org.cdlflex.itpm.web.queryEditor.accordionStructure;

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;

/**
 * Representation of a single accordion tab.
 */
public abstract class AccordionTab implements IAccordionTab {
    private static final long serialVersionUID = 1L;
    private final IModel<String> title;
    private boolean isNewActive;
    private String knowledgeAreaName;

    public AccordionTab(IModel<String> title, String knowledgeAreaName) {
        this.title = new TitleModel(title);
        this.knowledgeAreaName = knowledgeAreaName;
    }

    public IModel<String> getTitle() {
        return title;
    }

    public void setNewActive(boolean x) {
        this.isNewActive = x;
    }

    public boolean isNewActive() {
        return isNewActive;
    }

    public boolean isVisible() {
        return true;
    }

    public String getKnowledgeArea() {
        return knowledgeAreaName;
    }

    public abstract int getCount();

    private class TitleModel extends LoadableDetachableModel<String> {
        private static final long serialVersionUID = 1L;
        private final IModel<String> delegate;

        public TitleModel(IModel<String> delegate) {
            this.delegate = delegate;
        }

        @Override
        protected String load() {
            return this.delegate.getObject() + " (" + getCount() + ")";
        }

        @Override
        public void detach() {
            super.detach();
            delegate.detach();
        }
    }
}
