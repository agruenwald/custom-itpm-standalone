package org.cdlflex.itpm.web.queryEditor;

import org.apache.wicket.markup.html.WebPage;
import org.cdlflex.base.api.WebAppModule;

/**
 * Module for the Visualization of the page in the EngSb cockpit.
 */
public class QueryEditorModule implements WebAppModule {
    @Override
    public String getName() {
        return "Query Editor";
    }

    @Override
    public Class<? extends WebPage> getPageClass() {
        return QueryEditorPage.class;
    }

    @Override
    public String[] getAuthorities() {
        return new String[0];
    }
}
