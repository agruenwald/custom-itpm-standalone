package org.cdlflex.itpm.web.generic.form;

import java.io.Serializable;

import org.apache.wicket.markup.html.form.FormComponent;

/**
 * Container which simplifies the access to attributes and their rendering in meta-forms.
 */
public class AttributeContainer implements Serializable {
    private static final long serialVersionUID = 1L;
    private FormComponent<?> attributeValue;
    private String attributeName;

    public AttributeContainer(String attributeName, FormComponent<?> attributeTf) {
        this.attributeValue = attributeTf;
        this.attributeName = attributeName;
    }

    public FormComponent<?> getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(FormComponent<?> attributeValue) {
        this.attributeValue = attributeValue;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }
}
