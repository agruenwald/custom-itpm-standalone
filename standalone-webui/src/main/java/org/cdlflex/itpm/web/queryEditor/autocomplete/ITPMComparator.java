package org.cdlflex.itpm.web.queryEditor.autocomplete;

import java.util.Comparator;

/**
 * Order a list of data concepts (<prefixName>:<conceptName>) for convenient representation in the auto completion menu
 * of the query editor. For instance entities with the prefix "itpm" (=main Ontology) are listed first.
 */
public class ITPMComparator implements Comparator<String> {
    private static final String ITPM = "itpm";

    @Override
    public int compare(String s1, String s2) {
        if (s1.startsWith(ITPM) && !s2.startsWith(ITPM)) {
            return -1;
        } else {
            if (!s1.startsWith(ITPM) && s2.startsWith(ITPM)) {
                return 1;
            } else {
                // either both or neither starts with itpm
                return s1.compareTo(s2);
            }
        }
    }
}
