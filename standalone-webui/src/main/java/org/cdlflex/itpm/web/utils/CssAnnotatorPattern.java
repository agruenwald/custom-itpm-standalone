package org.cdlflex.itpm.web.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Patterns/shortcuts of the internal (simple) markup language to enable users to add icons or html tags to a table
 * (which has been generated via a query).
 */
public class CssAnnotatorPattern {
    private String domain;
    private String tag;
    private Map<String, String> attributes;
    private String matchedPattern;

    public CssAnnotatorPattern() {
        this.attributes = new HashMap<String, String>();
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public void setMatchedPattern(String group) {
        this.matchedPattern = group;
    }

    public String getMatchedPattern() {
        return matchedPattern;
    }
}
