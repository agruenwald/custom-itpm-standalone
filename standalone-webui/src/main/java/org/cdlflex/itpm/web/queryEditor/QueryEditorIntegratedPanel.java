package org.cdlflex.itpm.web.queryEditor;

import java.io.Serializable;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.cdlflex.itpm.web.generic.table.IDataRefreshContainer;
import org.cdlflex.itpm.web.reusable.ISubmitCallback;

/**
 * Integration of the query editor into the Web UI based on some JS. Enables editing of elements in place (edit icon
 * will show up in the right corner on mouse over).
 */
public class QueryEditorIntegratedPanel extends Panel implements Serializable, ISubmitCallback {
    private static final long serialVersionUID = 1L;
    private final QueryEditorStandalonePanel editor;
    private final Component hoverPanel;
    private Label hoverIcon;
    private final IDataRefreshContainer iDataContainer;
    private final ISubmitCallback iClickIconCallback;

    public QueryEditorIntegratedPanel(String panelId, Component hoverPanel, QueryEditorStandalonePanel queryEditor,
            IDataRefreshContainer iDataContainer) {
        this(panelId, hoverPanel, queryEditor, iDataContainer, null);
    }

    /**
     * Create a new integrated panel which will wrap an existing component.
     * 
     * @param panelId the panel id
     * @param hoverPanel the existing panel which will be enriched with the query editor behavior on hover or click.
     * @param queryEditor the query editor component.
     * @param iDataContainer an optional data container that can be passed to refresh data which is shown on the same
     *        page.
     * @param iClickIconCallback if not null then this method can be used to add additional behavior whenever the
     *        edit-icon/button is going to be clicked.
     */
    public QueryEditorIntegratedPanel(String panelId, Component hoverPanel, QueryEditorStandalonePanel queryEditor,
            IDataRefreshContainer iDataContainer, ISubmitCallback iClickIconCallback) {
        super(panelId);
        this.editor = queryEditor;
        this.hoverPanel = hoverPanel;
        queryEditor.setCallbackObj(this);
        renderPanel(queryEditor, hoverPanel);
        this.iDataContainer = iDataContainer;
        this.iClickIconCallback = iClickIconCallback;
    }

    public void renderPanel(final QueryEditorStandalonePanel editor, final Component hoverPanel) {
        add(editor);
        setOutputMarkupId(true);

        editor.setVisible(false);
        editor.setOutputMarkupId(true);
        hoverPanel.setOutputMarkupId(true);
        String markupIdElementOfConcern = hoverPanel.getMarkupId();

        hoverIcon = new Label("hover-icon");
        hoverIcon.setOutputMarkupId(true);
        add(hoverIcon);
        hoverIcon.add(new AjaxEventBehavior("onclick") {
            private static final long serialVersionUID = 1L;

            protected void onEvent(AjaxRequestTarget target) {
                if (iClickIconCallback != null) {
                    iClickIconCallback.submit(true, target);
                }
                editor.setVisible(true);
                hoverPanel.setVisible(false);
                hoverIcon.setVisible(false);
                target.add(editor);
                target.add(hoverPanel);
                target.add(QueryEditorIntegratedPanel.this);
                target.add(QueryEditorIntegratedPanel.this.getParent());
            }
        });

        String jsHook =
            String.format("jQuery('#%s').append(jQuery('#%s'));", markupIdElementOfConcern, hoverIcon.getMarkupId());
        jsHook +=
            String.format("jQuery('#%s')" + ".hover(function() { jQuery('#%s').show(); }, "
                + "  function() { jQuery('#%s').fadeOut(3000); }" + ");", hoverPanel.getMarkupId(),
                    hoverIcon.getMarkupId(), hoverIcon.getMarkupId());

        Label jsPart = new Label("js-part", Model.of(jsHook));
        jsPart.setEscapeModelStrings(false);
        add(jsPart);
    }

    @Override
    public void submit(boolean isNew, AjaxRequestTarget target) {
        editor.setVisible(false);
        hoverPanel.setVisible(true);
        hoverIcon.setVisible(true);
        if (iDataContainer != null) {
            iDataContainer.refreshGenericData();
        }
        target.add(this.getPage());
    }
}
