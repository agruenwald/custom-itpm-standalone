package org.cdlflex.itpm.web.generic.form;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.WordUtils;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.AbstractTextComponent;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.AbstractItem;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.cdlflex.itpm.generated.mddconnector.MDDFunctions;
import org.cdlflex.itpm.web.generic.table.IDataContainer;
import org.cdlflex.itpm.web.metamodeltools.MetaModelBrowser;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.itpm.web.utils.IconUtil;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAO;
import org.cdlflex.mdd.sembase.semaccess.DAOUtil;
import org.cdlflex.mdd.sembase.semaccess.MDDBeanInterface;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAttribute;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.cdlflex.mdd.umltuowl.metamodel.MetaComment;
import org.cdlflex.mdd.umltuowl.metamodel.MetaElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaynberg.wicket.select2.DragAndDropBehavior;
import com.vaynberg.wicket.select2.Select2MultiChoice;

/**
 * Creation of generic forms for visualization and modification of data. Currently only visualization is implemented.
 * The form is created based on the known umlTUowl metamodel. Note: The form is still in an experimental state, hence
 * the code may need refactoring.
 */
public class FormBuilderMetaClass implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final String EXAMPLE_MAX_CONSTRAINT_PATTERN = "pattern[.{0,200}]";
    private static final DateFormat CHANGE_DATE_FORMAT = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
    private static final MDDFunctions MDD_FUNCTIONS = new MDDFunctions();
    private static final Logger logger = LoggerFactory.getLogger(FormBuilderMetaClass.class);
    private static final String ADD_CONCEPT = "add-concept";
    private static final String EDIT_CONCEPT = "edit-concept";
    private static final String FORM_ENTRY_TF = "formentry-tf";
    private static final String FORM_ENTRY_TA = "formentry-ta";
    private static final String SUB_FORM_ID = "subform";
    private static final List<String> RECORD_INFO = Arrays.asList(new String[] { "createdAt", "lastChange",
        "versionInfo", "origin", "originLink" });
    private static final List<String> RECORD_INFO_HELP_TEXTS = Arrays.asList(new String[] {
        "The date of the first creation of the record.", "The date of the last update.",
        "At each update the version of the record is increased by one automatically.",
        "The origin if the record if specified. The origin remains the same over time.",
        "A reference to the origin of the record if available (e.g. a link to a PM suite or a file." });
    private static final String ID_AUTO_HINT =
        "You may want to leave the ID empty so that a value will be assigned automatically.";
    private static final String LINE_FULL_WIDTH_CSS = "form-group col-md-12";
    private static final String LINE_TWO_COLUMNS_CSS = "form-group col-md-6";
    private final List<AttributeContainer> attributeContainers = new ArrayList<AttributeContainer>();
    private String modelName;
    private MetaClass metaClass;
    private boolean isSubForm;
    private Map<String, AssociationContainer> containers = new LinkedHashMap<String, AssociationContainer>();
    private AssociationContainer subClassSelection;
    private MDDBeanInterface rootBean;
    private MetaAssociation subBeanAssociation;
    private FormBuilderMetaClass rootFormBuilder;
    private MetaClass metaClassTypeOfBean;
    private RepeatingView repeatingView;
    private String selectedItemId;
    private Select2ClickBehavior tmpSelect2ClickBehavior;
    private final IDataContainer rootDataContainer;
    private final int level;

    public FormBuilderMetaClass(boolean isSubmForm, MDDBeanInterface rootBean, FormBuilderMetaClass rootFormBuilder,
            String selectedItemId, IDataContainer rootDataContainer, int level) {
        this.isSubForm = isSubmForm;
        this.rootBean = rootBean;
        this.rootFormBuilder = rootFormBuilder == null ? this : rootFormBuilder;
        subClassSelection = null;
        metaClassTypeOfBean = null;
        selectedItemId = "";
        tmpSelect2ClickBehavior = null;
        this.rootDataContainer = rootDataContainer;
        this.level = level;
    }

    public void buildForm(RepeatingView repeating, final String modelName, final MetaClass metaClass,
        MetaAssociation subBeanAssociation, String selectedItemId) throws MDDException {
        this.repeatingView = repeating;
        this.modelName = modelName;
        this.subBeanAssociation = subBeanAssociation;
        this.metaClass = metaClass;
        subClassSelection = null;
        this.selectedItemId = selectedItemId;
        tmpSelect2ClickBehavior = null;

        MDDBeanInterface beanInstance = renderSubclassSelectionOnAdd(repeating, subBeanAssociation);
        renderAttributes(repeating, beanInstance, false);
        renderAssociations(repeating, beanInstance, metaClass);
        renderAttributes(repeating, beanInstance, true);
        if (!isSubForm) {
            renderGenericInfoAttributes(repeating, (MDDBeanInterface) beanInstance);
        }
    }

    private void renderAssociations(RepeatingView repeating, MDDBeanInterface beanInstance, MetaClass metaClass)
        throws MDDException {
        Iterator<Entry<MetaAssociation, MetaClass>> iterator =
            new MetaModelBrowser().getMDDSimpleAccess().getAllSuperAssociations(metaClass).entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<MetaAssociation, MetaClass> associationEntry = iterator.next();
            renderAssociation(repeating, beanInstance, metaClass, associationEntry);
        }
    }

    private AbstractItem addAbstractItem(RepeatingView repeating) {
        return addAbstractItem(repeating, isSubForm);
    }

    /**
     * Add a new line to the form.
     * 
     * @param repeating the repeating element (abstract)
     * @param twoColumns if true then two form lines/blocks will be rendered per row.
     * @return the abstract item which already has been added to the repeating element.
     */
    private AbstractItem addAbstractItem(RepeatingView repeating, boolean twoColumns) {
        final AbstractItem item = new AbstractItem(repeating.newChildId());
        item.setOutputMarkupId(true);
        repeating.add(item);
        item.add(new AttributeModifier("class", new Model<String>(isSubForm ? LINE_FULL_WIDTH_CSS
            : LINE_TWO_COLUMNS_CSS)));
        return item;
    }

    private void renderAssociation(RepeatingView repeating, MDDBeanInterface beanInstance, MetaClass metaClass,
        Entry<MetaAssociation, MetaClass> associationEntry) throws MDDException {
        final AbstractItem item = addAbstractItem(repeating);
        final MetaAssociation metaAssociation = associationEntry.getKey();

        MetaClass defaultSubclassType = null;
        MetaClass refClassLinked = associationEntry.getKey().getTo().getMetaClass();
        if (refClassLinked.isAbstractClass() || refClassLinked.isInterfaceClass()) {
            List<MetaClass> subClasses =
                new MetaModelBrowser().getMDDSimpleAccess().getAllSubClasses(refClassLinked, false);
            defaultSubclassType = subClasses.size() > 0 ? subClasses.get(0) : null;
        } else {
            defaultSubclassType = refClassLinked;
        }
        item.add(new Label("label", MetaModelBrowser.labelFormatter(associationEntry.getKey().getName())));
        FormComponent<?> tf = createField(item, true, false, "");
        tf.setVisible(false);
        List<MetaElement> commentElems = new ArrayList<MetaElement>();
        commentElems.add(metaAssociation);
        // for the super-class the comment is added. for subclasses the comment will be shown next to the subtype field.
        commentElems.add(metaAssociation.getTo().getMetaClass());
        renderHelpIconMulti(item, commentElems, metaAssociation.getName());

        Set<Object> linkedEntities =
            MDD_FUNCTIONS.getBeanAssociationDynamically(beanInstance,
                    associationEntry.getKey().getName().replaceAll("\\s", ""));
        linkedEntities.remove(null);
        List<OptionEntry> possibleChoices = new ArrayList<OptionEntry>();
        // for compositions only the directly linked entities must be shown
        // for all others the whole range of possibilities need to be captured.
        if (metaAssociation.isComposition()) {
            for (Object linkedEntity : linkedEntities) {
                MDDBeanInterface beanInterface = (MDDBeanInterface) linkedEntity;
                OptionEntry oE = new OptionEntry(beanInterface.toString(), beanInterface.getDisplayFriendlyName());
                oE.setOptionalBean(beanInterface);
                possibleChoices.add(oE);
            }
        } else if (refClassLinked.isEnumClass()) {
            // for all enum classes provide the list of enum choices only.
            for (MetaClass enumMetaClass : refClassLinked.getSubclasses()) {
                OptionEntry oE =
                    new OptionEntry(WordUtils.uncapitalize(enumMetaClass.getName()), enumMetaClass.getName());
                possibleChoices.add(oE);
            }
        } else { // if (refClassLinked.isAbstractClass() || refClassLinked.isInterfaceClass()) {
            // for concrete classes provide all entries and all entries of subclasses
            List<MetaClass> classAndSubclasses =
                new MetaModelBrowser().getMDDSimpleAccess().getAllSubClasses(refClassLinked);
            for (MetaClass c : classAndSubclasses) {
                if (!(c.isAbstractClass() || c.isInterfaceClass())) {
                    DAO<? extends MDDBeanInterface> dao =
                        new ConnectionUtil().getFactory().create(c.getName().replaceAll("\\s", ""),
                                new ConnectionUtil().getOpenConnection(modelName));
                    List<? extends MDDBeanInterface> subList = dao.readFulltext("");
                    for (MDDBeanInterface inst : subList) {
                        OptionEntry oE = new OptionEntry(inst.toString(), inst.getDisplayFriendlyName());
                        oE.setOptionalBean(inst);
                        possibleChoices.add(oE);
                    }
                }
            }
        }

        AssociationContainer selectContainer = new AssociationContainer(possibleChoices);
        for (OptionEntry oE : possibleChoices) {
            for (Object linkedEntity : linkedEntities) {
                if (linkedEntity == null) {
                    // single entries which are empty are null-valued.
                    continue;
                }
                // ignore case: enum classes feature vs. Feature
                if (oE.getId().equalsIgnoreCase(linkedEntity.toString())) {
                    selectContainer.getSelected().add(oE);
                    break;
                }
            }
        }

        containers.put(metaAssociation.getName(), selectContainer);
        final Select2MultiChoice<OptionEntry> selected =
            new Select2MultiChoice<OptionEntry>("selectentry", new PropertyModel<Collection<OptionEntry>>(
                    selectContainer, "selected"), new GenericChoiceProvider(possibleChoices));
        selected.getSettings().setMultiple(associationEntry.getKey().isMaximumRangeGreaterThan(1));
        if (!associationEntry.getKey().isMaximumRangeGreaterThan(1)) {
            selected.getSettings().setMaximumSelectionSize(1);
        }
        selected.setOutputMarkupId(true);
        selected.setOutputMarkupPlaceholderTag(true);
        selected.add(new DragAndDropBehavior());
        selectContainer.setSelect2(selected);
        this.addSubformBehavior(item, selected, metaAssociation, defaultSubclassType, selectContainer, level);
        // required because otherwise select2 does not fire as expected
        selected.add(new AjaxFormComponentUpdatingBehavior("onchange") {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onUpdate(AjaxRequestTarget target) {
            }
        });
        item.add(selected);
    }

    /**
     * Add a select2 menu which shows a list of available subclass types the user can choose to add an associated
     * sub-entry.
     * 
     * @param repeating
     * @param beanInstance
     * @param ma
     * @throws MDDException
     */
    private MDDBeanInterface renderSubclassSelectionOnAdd(RepeatingView repeating, MetaAssociation ma)
        throws MDDException {
        MDDBeanInterface beanInstance = null;
        if (ma == null) {
            this.metaClassTypeOfBean = this.metaClass;
            return MDDFormBridge.createBean(modelName, this.metaClass, this.selectedItemId);
        } else if (!this.selectedItemId.isEmpty()) {
            this.metaClassTypeOfBean = this.metaClass;
        }

        List<OptionEntry> subclassOptions = new ArrayList<OptionEntry>();
        List<MetaClass> metaSubClasses =
            new MetaModelBrowser().getMDDSimpleAccess().getAllNonAbstractSubClasses(ma.getTo().getMetaClass());
        int i = 0;
        OptionEntry selectedOption = null;
        for (MetaClass msc : metaSubClasses) {
            OptionEntry option = new OptionEntry(msc.getId(), msc.getOriginalName());
            option.setOptionalMetaClass(msc);
            subclassOptions.add(option);
            if (this.metaClassTypeOfBean == null) {
                if (i == 0) {
                    selectedOption = option; // first by default
                }
            } else {
                if (this.metaClassTypeOfBean.getName().equals(msc.getName())) {
                    selectedOption = option;
                }
            }
            i++;
        }
        this.metaClassTypeOfBean = selectedOption.getOptionalMetaClass();
        this.subClassSelection = new AssociationContainer(subclassOptions);
        this.subClassSelection.getSelected().add(selectedOption);
        if (isSubForm && !selectedItemId.isEmpty()) {
            Set<Object> os = MDD_FUNCTIONS.getBeanAssociationDynamically(rootBean, ma.getName());
            for (Object o : os) {
                if (o != null && DAOUtil.deserializeIdFromBean(o.toString()).equals(selectedItemId)) {
                    beanInstance = (MDDBeanInterface) o;
                    break;
                }
            }
        } else {
            beanInstance = MDDFormBridge.createBean(modelName, selectedOption.getOptionalMetaClass(), selectedItemId); // selectedOption.getId());
        }
        if (metaSubClasses.size() <= 1) {
            // no need to select if only one choice
            return beanInstance;
        }

        final AbstractItem item = addAbstractItem(repeating);
        item.add(new Label("label", "Subtype"));
        renderHelpIcon(item, selectedOption.getOptionalMetaClass(), selectedOption.getOptionalMetaClass()
                .getOriginalName());
        renderAttributeFakeFields(item);
        renderAssociationSubformFake(item);

        final Select2MultiChoice<OptionEntry> selected =
            new Select2MultiChoice<OptionEntry>("selectentry", new PropertyModel<Collection<OptionEntry>>(
                    subClassSelection, "selected"), new GenericChoiceProvider(subclassOptions));
        subClassSelection.setSelect2(selected);
        selected.getSettings().setAllowClear(false);
        selected.getSettings().setMaximumSelectionSize(1);
        selected.getSettings().setMultiple(false);
        selected.add(new DragAndDropBehavior());
        selected.add(new AjaxFormComponentUpdatingBehavior("onchange") {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                if (subClassSelection.getSelected().size() <= 0) {
                    // remain unchanged
                } else {
                    metaClassTypeOfBean = subClassSelection.getSelected().get(0).getOptionalMetaClass();
                    repeatingView.removeAll();
                    try {
                        String recId = "";
                        buildForm(repeatingView, modelName, metaClassTypeOfBean, subBeanAssociation, recId);
                    } catch (MDDException e) {
                        logger.error(e.getMessage());
                    }
                    target.add(repeatingView.getParent());
                }
            }
        });
        if (!this.selectedItemId.isEmpty()) {
            // edit: type cannot be changed anymore
            selected.setEnabled(false);
        }
        item.add(selected);
        return beanInstance;
    }

    public Button getSubmitButton(Form<?> form, String buttonText) {
        AjaxFallbackButton ajaxButton = new AjaxFallbackButton("submit", Model.of(buttonText), form) {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                processOnSubmit(this, target, form);
            }
        };
        ajaxButton.setVisible(true);
        return ajaxButton;
    }

    /**
     * Deserialize the form input into a Java bean which can be processed by the generated MDD DAOs and the
     * MMDBeanConnector (takes care about creating and linking associations, etc.) The data is first marshalled into
     * adequate data source configuration format and then automatically processed by the MDD connector.
     * 
     * @param button
     */
    public void processOnSubmit(Button button, AjaxRequestTarget target, Form<?> form) {
        MDDFormBridge bridge =
            new MDDFormBridge(modelName, metaClassTypeOfBean, rootFormBuilder, attributeContainers, containers);
        bridge.processOnSubmit(button, target, form, isSubForm, selectedItemId, subBeanAssociation);
    }

    /**
     * Render all meta attributes, which is compared to the association rendering and storage handling a quite-forward
     * task.
     * 
     * @param repeating
     * @param beanInstance
     */
    private void renderAttributes(RepeatingView repeating, MDDBeanInterface beanInstance, boolean renderBooleans) {
        MDDFunctions mddf = new MDDFunctions();
        final boolean isUpdate = !this.selectedItemId.isEmpty();
        List<MetaAttribute> metaAttributes =
            new MetaModelBrowser().getMDDSimpleAccess().getAllSuperAttributes(metaClass);
        int i = 0;
        for (MetaAttribute ma : metaAttributes) {
            boolean isBoolean = ma.getRange().toLowerCase().contains("bool");
            if (!((renderBooleans && isBoolean) || (!renderBooleans && !isBoolean))) {
                continue;
            }
            AbstractItem item = addAbstractItem(repeating);
            item.add(new Label("label", MetaModelBrowser.labelFormatter(ma.getOriginalName())));
            String val = "";
            try {
                val = mddf.getBeanAttributeDynamically(beanInstance, ma.getOriginalName());
            } catch (MDDException e) {
                logger.error(e.getMessage());
            }
            FormComponent<?> tf =
                createField(item, !isMultiLineCheckConstraint(ma), ma.getRange().toLowerCase().contains("bool"), val);
            tf.setEnabled(isBoolean || (i != 0) || !isUpdate);
            attributeContainers.add(new AttributeContainer(ma.getOriginalName(), tf));
            renderHelpIcon(item, ma, ma.getOriginalName());
            renderAssociationFakeFields(item);
            i++;
        }
    }

    private FormComponent<?> createField(AbstractItem item, boolean isSingleLine, boolean isCheckbox, String val) {
        AbstractTextComponent<String> tf = new TextField<String>(FORM_ENTRY_TF, Model.of(val));
        AbstractTextComponent<String> ta = new TextArea<String>(FORM_ENTRY_TA, Model.of(val));
        final FormComponent<?> cb = new CheckBox("formentry-cb", Model.of(Boolean.parseBoolean(val)));
        item.add(tf);
        item.add(ta);
        item.add(cb);
        if (isCheckbox) {
            tf.setVisible(false);
            ta.setVisible(false);
            return cb;
        } else if (!isSingleLine) {
            tf.setVisible(false);
            cb.setVisible(false);
            return ta;
        } else {
            ta.setVisible(false);
            cb.setVisible(false);
            return tf;
        }
    }

    /**
     * Render the generic info attributes of the {@link MDDBeanInterface} class which exist for in any class and are
     * mainly for info purpose. The fields are added manually because by default the timestamps are rendered as dates
     * without hours, minutes, seconds.
     * 
     * @param repeating
     * @param beanInstance
     */
    private void renderGenericInfoAttributes(RepeatingView repeating, MDDBeanInterface beanInstance) {
        List<String> stringifiedValues = new ArrayList<String>();
        stringifiedValues.add(beanInstance.getCreatedAt() == null ? "" : CHANGE_DATE_FORMAT.format(beanInstance
                .getCreatedAt()));
        stringifiedValues.add(beanInstance.getLastChange() == null ? "" : CHANGE_DATE_FORMAT.format(beanInstance
                .getLastChange()));
        stringifiedValues.add(String.valueOf(beanInstance.getVersionInfo()));
        stringifiedValues.add(String.valueOf(beanInstance.getOrigin()));
        stringifiedValues.add(String.valueOf(beanInstance.getOriginLink()));
        int i = 0;
        for (String attr : RECORD_INFO) {
            String value = stringifiedValues.get(i);
            value = value.equals("null") ? "" : value;
            addTextualValue(repeating, WordUtils.capitalize(attr), value, WordUtils.capitalize(attr),
                    RECORD_INFO_HELP_TEXTS.get(i));
            i++;
        }
    }

    private void addTextualValue(RepeatingView repeating, String label, String value, String infoTitle,
        String infoText) {
        AbstractItem item = addAbstractItem(repeating);
        item.add(new Label("label", WordUtils.capitalize(label)));
        FormComponent<?> tf = createField(item, true, false, value);
        tf.setEnabled(false);
        renderHelpIcon(item, infoTitle, infoText);
        tf.setVisible(true);
        renderAssociationFakeFields(item);
    }

    private void renderAssociationFakeFields(AbstractItem item) {
        Select2MultiChoice<?> ddc = null;
        ddc =
            new Select2MultiChoice<OptionEntry>("selectentry", new PropertyModel<Collection<OptionEntry>>(
                    new AssociationContainer(new ArrayList<OptionEntry>()), "selected"), new GenericChoiceProvider(
                    new ArrayList<OptionEntry>()));
        ddc.setVisible(false);
        item.add(ddc);
        renderAssociationSubformFake(item);
    }

    private void renderAssociationSubformFake(AbstractItem item) {
        Label addConceptFake = new Label(ADD_CONCEPT, ADD_CONCEPT);
        item.add(addConceptFake);
        addConceptFake.setVisible(false);

        Label editConceptFake = new Label(EDIT_CONCEPT, EDIT_CONCEPT);
        item.add(editConceptFake);
        editConceptFake.setVisible(false);

        Label fakeSubForm = new Label(SUB_FORM_ID, SUB_FORM_ID);
        item.add(fakeSubForm);
        fakeSubForm.setVisible(false);
    }

    private void renderAttributeFakeFields(AbstractItem item) {
        FormComponent<?> tf = createField(item, true, false, "");
        tf.setVisible(false);
    }

    private void renderHelpIcon(AbstractItem item, String title, String text) {
        String infoText =
            text.isEmpty() ? "" : IconUtil.createBSPopover(MetaModelBrowser.labelFormatter(title),
                    "formbuilder-help", "", true, text);

        Label popup = new Label("popup", infoText);
        popup.setEscapeModelStrings(false);
        item.add(popup);
    }

    private void renderHelpIcon(AbstractItem item, MetaElement ma, String labelName) {
        List<MetaElement> elems = new ArrayList<MetaElement>();
        elems.add(ma);
        renderHelpIconMulti(item, elems, labelName);
    }

    private void renderHelpIconMulti(AbstractItem item, List<MetaElement> elems, String labelName) {
        String infoText = "";
        for (MetaElement elem : elems) {
            for (MetaComment c : elem.getCommentByType(MetaComment.TYPE_COMMENT)) {
                String ct = c.getText().isEmpty() ? infoText : c.getText();
                infoText = infoText.isEmpty() ? ct : " " + ct;
            }
        }
        if (infoText.isEmpty() && labelName.toLowerCase().equals("id")) {
            infoText = ID_AUTO_HINT;
        }
        renderHelpIcon(item, labelName, infoText);
    }

    /**
     * Hack for demonstration: Constraints, e.g. regular expressions can be used to derive information about the input
     * data. E.g. some Java libraries exist which can create test data or patterns that match the criteria of the
     * regular expression. In the same way regular expressions can be used to get information about the maximum string
     * length that is supported. To exemplify the usage of constraints this methods looks for attribute constraints that
     * specify the maximum length of an attribute explicitly.
     * 
     * @param elem the meta attribute
     * @return true if the demo pattern matched exactly. true indicates that the pattern has been found and that the
     *         user specified the field content to be very long hence a textfield is appropriate for rendering.
     */
    private boolean isMultiLineCheckConstraint(MetaAttribute elem) {
        for (MetaComment c : elem.getCommentByType(MetaComment.TYPE_CONSTRAINT)) {
            String ct = c.getText();
            if (ct.equals(EXAMPLE_MAX_CONSTRAINT_PATTERN)) {
                return true;
            }
        }
        return false;
    }

    private void addSubformBehavior(AbstractItem item, final Select2MultiChoice<OptionEntry> selected,
        final MetaAssociation metaAssociation, final MetaClass defaultSubclassType,
        final AssociationContainer selectContainer, int level) {
        final AjaxLink<String> editConceptLink;
        int nextLevel = level + 1;
        if (level >= 1 || (defaultSubclassType != null && metaClass.getName().equals(defaultSubclassType.getName()))) {
            logger.info(String.format("The creation of nested self-references (or levels greater than 1) "
                + "is not supported (association %s).", defaultSubclassType.getName()));
            Label label = new Label(SUB_FORM_ID);
            item.add(label);
            label.setVisible(false);
            Label addConceptLink = new Label(ADD_CONCEPT);
            item.add(addConceptLink);
            editConceptLink = null;
            Label hiddenEditLabel = new Label(EDIT_CONCEPT);
            item.add(hiddenEditLabel);
            hiddenEditLabel.setVisible(false);
            addConceptLink.setVisible(false);
        } else {
            final GenericFormPanel subForm =
                new GenericFormPanel(SUB_FORM_ID, modelName, metaAssociation.getTo().getMetaClass().getName(), "",
                        true, this.rootFormBuilder, metaAssociation, rootDataContainer, nextLevel);
            subForm.renderForm();
            subForm.setOutputMarkupId(true);
            subForm.setOutputMarkupPlaceholderTag(true);
            selectContainer.setSubForm(subForm);
            item.add(subForm);
            subForm.setVisible(false);

            editConceptLink =
                new LinkEditConcept(EDIT_CONCEPT, modelName, containers, subForm, tmpSelect2ClickBehavior, selected,
                        this);
            item.add(editConceptLink);

            final LinkAddConcept addConceptLink =
                new LinkAddConcept(ADD_CONCEPT, modelName, containers, subForm, tmpSelect2ClickBehavior, selected,
                        metaAssociation, this, defaultSubclassType, editConceptLink);
            ;
            item.add(addConceptLink);
        }

        selected.add(new Select2ClickBehavior(metaAssociation) {
            private static final long serialVersionUID = 1L;

            @Override
            public void selectedItemsClick(AjaxRequestTarget target, MetaAssociation association, String itemId) {
                AssociationContainer ac = containers.get(association.getName());
                for (OptionEntry oE : ac.getSelected()) {
                    if (oE.getId().equals(itemId)) {
                        this.setOptionEntry(oE);
                        tmpSelect2ClickBehavior = this;
                        break;
                    }
                }
                selectedItemId = itemId;
                // editConceptLink is null if level to high or if the association is a self-reference
                if (editConceptLink != null) {
                    editConceptLink.setVisible(true);
                    target.add(editConceptLink);
                }
            }

            @Override
            public void blur(AjaxRequestTarget target) {
            }
        });
    }

    /**
     * Get the root bean (the main bean) of the form, even for nested subforms.
     * 
     * @return
     */
    public MDDBeanInterface getRootBean() {
        return this.rootBean;
    }

    /**
     * Get access to the root form builder for nested forms.
     * 
     * @return the root form builder for nested forms or {@link this} if the current form is the root form.
     */
    public FormBuilderMetaClass getRootFormBuilder() {
        return this.rootFormBuilder;
    }

    /**
     * Get an association container via the name of the meta association. The container includes all selectable choices
     * and the actually selected choices for an association field.
     * 
     * @param name the association name (must exist)
     * @return the container which can be used to add new choices or selected values.
     */
    public AssociationContainer getAssociationContainerByName(String name) {
        return this.containers.get(name);
    }

    public RepeatingView getRepeatingView() {
        return repeatingView;
    }

    public MetaClass getRootMetaClass() {
        return metaClass;
    }

    public IDataContainer getRootDataContainer() {
        return rootDataContainer;
    }

    protected void setSelectedItemId(String selectedItemId) {
        this.selectedItemId = selectedItemId;
    }

    protected Select2ClickBehavior getEditSelectClickBehavior() {
        return tmpSelect2ClickBehavior;
    }
}
