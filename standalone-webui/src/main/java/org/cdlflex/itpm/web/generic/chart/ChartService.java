package org.cdlflex.itpm.web.generic.chart;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.cdlflex.itpm.generated.model.PMRule;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.itpm.web.utils.Util;
import org.cdlflex.mdd.sembase.MDDException;

/**
 * The Chart service class provides the Java interface to the Flash Chart API which is built on Flash and JavaScript and
 * has been the most popular open source API in recent years.
 */
public class ChartService {
    private static final Logger logger = Logger.getLogger(ChartService.class);

    /**
     * Get a map with different types of "charts" or visualization representations
     * 
     * @param ruleService
     * @return
     * @throws MDDException
     */
    public AbstractDashboardComponent readChart(PMRule rule, List<List<String>> list, boolean showTitle)
        throws MDDException {
        // heuristic for estimating if the query can be translated into a chart
        List<Integer> dimensions = new LinkedList<Integer>();
        for (List<String> l : list) {
            // one entry:
            int iDimension = 0;
            for (String col : l) {
                try {
                    Double.parseDouble(col);
                    if (!col.startsWith("+")) { // phone number
                        iDimension++;
                    }
                } catch (Exception e) {
                    if (col.isEmpty()) {
                        iDimension++; // consider it as a possible dimension
                    }
                }
            }
            dimensions.add(iDimension);
        }
        int estimatedDimensions = Util.median(dimensions);
        List<String> sampleSet = null;
        boolean escape = false;
        for (int d : dimensions) {
            if (d == estimatedDimensions) {
                for (List<String> record : list) {
                    try {
                        record.get(d);
                        sampleSet = record;
                        escape = true;
                        break;
                    } catch (IndexOutOfBoundsException e) {
                        logger.error(String.format(
                                "Although %s dimensions detected the first column (description) is missing"
                                    + " in rule %s (%s).", d, rule.getName(), e.getMessage()));
                        break;
                    }
                }

                if (escape) {
                    break;
                }
            }
        }

        if (estimatedDimensions == 1 && sampleSet.size() >= 2) {
            int nums = getFirstDoubleCol(sampleSet);
            int titles = 0;
            for (int i = 0; i < sampleSet.size(); i++) {
                if (i != nums) {
                    titles = i;
                    break;
                }
            }

            if (sampleSet.size() >= 3) {
                int z = 0;
                for (int i = 0; i < sampleSet.size(); i++) {
                    if (i != nums && i != titles) {
                        z = i;
                        break;
                    }
                }
                BarChartExt barchart = new BarChartExt(rule.getName(), rule, showTitle);
                barchart.setY(Util.numColToList(list, nums));
                barchart.setX(Util.colToList(list, titles));
                barchart.setZ(Util.colToList(list, z));
                return barchart;
            } else {
                BarChart barchart = new BarChart(rule.getName(), rule, showTitle);
                barchart.setY(Util.numColToList(list, nums));
                barchart.setX(Util.colToList(list, titles));
                return barchart;
            }
        } else if (sampleSet != null) {
            int nums = getFirstDoubleCol(sampleSet);
            int titles = 0;
            for (int i = 0; i < sampleSet.size(); i++) {
                if (i != nums) {
                    titles = i;
                    break;
                }
            }

            List<Integer> doubleColRows = getAllDoubleCols(sampleSet);
            List<String> subtitles =
                new ConnectionUtil().getDefaultQueryInterface().extractNames(rule.getRuleDefinition());

            BarChartExt barchart = new BarChartExt(rule.getName(), rule, showTitle);
            List<String> titles2 = new ArrayList<String>();
            List<String> titles1 = new ArrayList<String>();
            List<String> titlestmp = new ArrayList<String>();
            List<Integer> doubleCols = getAllDoubleCols(sampleSet);
            List<Double> values = Util.numColToList(list, doubleCols);
            titlestmp.addAll(Util.colToList(list, titles));
            boolean varyZ = true;
            int j = 0;
            int posi = 0;
            for (int i = 0; i < values.size(); i++) {
                int pos = posi; // i % x.length;
                if (varyZ) {
                    pos = i % doubleCols.size();
                }
                String assignedTitle = subtitles.get(doubleColRows.get(pos)); // important [1,2,3,4,1,2,3,4,...]
                titles2.add(assignedTitle);
                titles1.add(titlestmp.get(posi)); // important [cdlflex,cdlflex,cdlflex,cdlflex]

                j++;
                if (j >= (subtitles.size() - 1)) {
                    j = 0;
                    posi++;
                }
            }
            barchart.setY(values);
            barchart.setX(titles2);
            barchart.setZ(titles1);
            return barchart;
        } else {
            throw new MDDException(String.format("Undetected range of dimensions for %s. Needs to be implement.",
                    rule.getName()));
        }
    }

    private List<Integer> getAllDoubleCols(List<String> entry) {
        List<Integer> doubleCols = new ArrayList<Integer>();
        int i = 0;
        for (String x : entry) {
            try {
                Double.parseDouble(x);
                doubleCols.add(i);
            } catch (Exception e) {

            }
            i++;
        }
        return doubleCols;
    }

    private int getFirstDoubleCol(List<String> entry) {
        List<Integer> allDoubleColls = getAllDoubleCols(entry);
        return allDoubleColls.size() <= 0 ? 0 : allDoubleColls.get(0);
    }
}
