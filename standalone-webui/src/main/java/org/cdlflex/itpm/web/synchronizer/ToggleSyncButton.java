package org.cdlflex.itpm.web.synchronizer;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.PropertyModel;
import org.cdlflex.itpm.web.backend.SynchronizerJob;

/**
 * Start and stop the synchronizer thread, which is running in the background, manually.
 */
public class ToggleSyncButton extends AjaxLink<Void> {
    private static final long serialVersionUID = -5562712640504628574L;
    private SyncButton buttonModel;
    private final String startText = "Start Synchronizer";
    private final String stopText = "Stop  Synchronizer";

    public ToggleSyncButton(String id) {
        super(id);
        buttonModel = new SyncButton();
        buttonModel.setButton(getStatusBasedText(), getStatusBasedIcon());
        Label reloadButtonText = new Label("toggle-sync-label", new PropertyModel<String>(buttonModel, "value"));
        reloadButtonText.setEscapeModelStrings(false);
        this.add(reloadButtonText);
    }

    @Override
    public void onClick(AjaxRequestTarget target) {
        // final SyncButton buttonModel = new SyncButton();
        if (SynchronizerJob.isRunning()) {
            SynchronizerJob.stop();
        } else {
            // do not waint and force start.
            SynchronizerJob.restart(false, true);
        }
        buttonModel.setButton(getStatusBasedText(), getStatusBasedIcon());
        target.add(this);
    }

    private String getStatusBasedText() {
        return SynchronizerJob.isRunning() ? stopText : startText;
    }

    private String getStatusBasedIcon() {
        return SynchronizerJob.isRunning() ? "stop" : "play";
    }
}
