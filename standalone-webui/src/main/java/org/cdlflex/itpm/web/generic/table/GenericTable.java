package org.cdlflex.itpm.web.generic.table;

import org.apache.wicket.markup.html.WebPage;
import org.cdlflex.base.api.WebAppModule;

/**
 * This is an element which is used by the EngSb to add navigation elements automatically.
 */
public class GenericTable implements WebAppModule {
    @Override
    public String getName() {
        return "Generic Table";
    }

    @Override
    public Class<? extends WebPage> getPageClass() {
        return GenericTablePage.class;
    }

    @Override
    public String[] getAuthorities() {
        return new String[0];
    }
}
