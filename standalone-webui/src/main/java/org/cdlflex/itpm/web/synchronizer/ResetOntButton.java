package org.cdlflex.itpm.web.synchronizer;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.PropertyModel;
import org.cdlflex.itpm.web.store.TriggerLoadConfigData;
import org.cdlflex.mdd.sembase.MDDException;

/**
 * Button for completely dropping the ontology files and reloading.
 */
public class ResetOntButton extends AjaxLink<Void> {
    private static final long serialVersionUID = -5562712640504628574L;
    private SyncButton reloadButtonModel;
    private final String reloadText = "Flush Ontologies";

    public ResetOntButton(String id) {
        super(id);
        reloadButtonModel = new SyncButton();
        reloadButtonModel.setButton(reloadText, "floppy-open");
        Label reloadButtonText = new Label("reset-ont-label", new PropertyModel<String>(reloadButtonModel, "value"));
        reloadButtonText.setEscapeModelStrings(false);
        this.add(reloadButtonText);
    }

    @Override
    public void onClick(AjaxRequestTarget target) {
        final SyncButton reloadButtonModel = new SyncButton();
        reloadButtonModel.setButton(reloadText, "floppy-open");
        String eMsg;
        try {
            eMsg = TriggerLoadConfigData.loadAppDataFlushOntologies();
        } catch (MDDException e) {
            eMsg = e.getMessage();
        }
        if (!eMsg.isEmpty()) {
            error(eMsg);
        }
        reloadButtonModel.setButton(reloadText, "floppy-open");
        target.add(this);
    }
}
