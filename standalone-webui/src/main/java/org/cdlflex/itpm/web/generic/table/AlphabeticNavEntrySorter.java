package org.cdlflex.itpm.web.generic.table;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Sorts the navigation menu for generic tables (metamodel and/or manually defined query entries).
 */
public class AlphabeticNavEntrySorter implements Comparator<NavEntry> {
    private static final List<String> SORT = Arrays.asList(new String[] { "table", "graph", "kpi", "diagram" });

    private static int findTypePos(String type) {
        for (int pos = 0; pos < SORT.size(); pos++) {
            if (type.toLowerCase().startsWith(SORT.get(pos))) {
                return pos;
            }
        }
        return -1;
    }

    @Override
    public int compare(NavEntry e1, NavEntry e2) {
        int typeComparision = findTypePos(e1.getType()) - findTypePos(e2.getType());
        if (typeComparision == 0) {
            return e1.getName().compareTo(e2.getName());
        } else {
            return typeComparision;
        }
    }
}
