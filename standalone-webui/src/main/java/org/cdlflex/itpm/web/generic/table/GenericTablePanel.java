package org.cdlflex.itpm.web.generic.table;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.ISortableDataProvider;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.cdlflex.itpm.web.generic.form.GenericFormPanel;
import org.cdlflex.itpm.web.metamodeltools.MetaModelBrowser;
import org.cdlflex.itpm.web.reusable.GenericSortableDataProvider;
import org.cdlflex.itpm.web.reusable.InteractiveTableColumn;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;

/**
 * Panel to visualize generic data tables based on umlTUowl meta models.
 */
public class GenericTablePanel extends Panel implements IDataContainer {
    private static final long serialVersionUID = 1L;
    private String titleName;
    private String optMetaClassName;
    protected String optModelName;
    private final MetaModelBrowser metaModelBrowser;
    private List<String> titles = new LinkedList<String>();
    protected final List<Map<String, String>> data;
    private GenericFormPanel genericFormPanel;
    @SuppressWarnings("rawtypes")
    private DefaultDataTable table;
    private final SearchForm searchForm;
    private final DeleteButtonPanel deleteButton;
    private final Button addButton;
    private final Form<?> form;

    public GenericTablePanel(String panelId, String modelName, MetaClass metaClass, SearchForm searchForm) {
        this(panelId, modelName, metaClass, searchForm, new PageParameters(), true);
    }

    public GenericTablePanel(String panelId, String modelName, MetaClass metaClass, SearchForm searchForm,
            boolean renderGenericForm) {
        this(panelId, modelName, metaClass, searchForm, new PageParameters(), renderGenericForm);
    }

    public GenericTablePanel(String panelId, String modelName, MetaClass metaClass, SearchForm searchForm,
            final PageParameters parameters) {
        this(panelId, modelName, metaClass, searchForm, true);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public GenericTablePanel(String panelId, String modelName, MetaClass metaClass, SearchForm searchForm,
            final PageParameters parameters, boolean renderGenericForm) {
        super(panelId);
        form = new Form("generic-table-form");
        form.setOutputMarkupId(true);
        add(form);

        String recId = parameters.get("recId").toString("");
        boolean isOverView = parameters.get("recId").toString("").isEmpty();

        this.data = new ArrayList<Map<String, String>>();
        this.metaModelBrowser = new MetaModelBrowser();
        this.searchForm = searchForm;
        this.optMetaClassName = metaClass.getName();
        this.optModelName = modelName;
        readGenericData(modelName, this.optMetaClassName, searchForm == null ? "" : searchForm.getValue());
        titles = metaModelBrowser.getTitles(metaClass);
        titleName =
            String.format(optMetaClassName == null ? "Generic Table" : MetaModelBrowser
                    .labelFormatter(this.optMetaClassName));
        table = createTable(data);
        form.add(table);

        table.getColumns().add(new TableLinkColumn(new Model("Details"), "id", optMetaClassName, parameters));
        table.getColumns().add(0, new CheckboxColumn("id", optMetaClassName));
        table.setVisible(isOverView);

        addButton = getAddButton();
        form.add(addButton);
        addButton.setVisible(isOverView);

        deleteButton = new DeleteButtonPanel("deletePanel", modelName, metaClass, this);
        deleteButton.setVisible(isOverView);
        form.add(deleteButton);

        genericFormPanel =
            new GenericFormPanel("add-form", optModelName, optMetaClassName, recId, false, null, null, this);
        form.add(genericFormPanel);
        if (renderGenericForm) {
            genericFormPanel.renderForm();
        }
        genericFormPanel.setVisible(renderGenericForm);
    }

    @Override
    public void showOverview() {
        genericFormPanel.setVisible(false);
        deleteButton.setVisible(true);
        addButton.setVisible(true);
        table.setVisible(true);
    }

    public DefaultDataTable<Map<String, String>, String> createTable(final List<Map<String, String>> data) {
        ISortableDataProvider<Map<String, String>, String> dataProvider = new GenericSortableDataProvider(data);
        List<InteractiveTableColumn> columns = new ArrayList<InteractiveTableColumn>();
        for (String key : titles) {
            columns.add(new InteractiveTableColumn(Model.of(key), key));
        }
        if (columns.isEmpty()) {
            columns.add(new InteractiveTableColumn(Model.of("-"), "-"));
        }
        DefaultDataTable<Map<String, String>, String> defaultDataTable =
            new DefaultDataTable<Map<String, String>, String>("dataTableGenericPanel", columns, dataProvider, 20);
        defaultDataTable.setOutputMarkupId(true);
        return defaultDataTable;
    }

    /**
     * @return a button which, when clicked, adds a form for adding entries generically.
     */
    private Button getAddButton() {
        Button button = new Button("add-button", Model.of("")) {
            private static final long serialVersionUID = 1L;

            @Override
            public void onSubmit() {
                PageParameters parameters = new PageParameters();
                parameters.add("view", optModelName);
                parameters.add("metaClass", optMetaClassName);
                genericFormPanel.setVisible(true);
                table.setVisible(false);
                deleteButton.setVisible(false);
                // rebuild form to clear previous results.
                form.remove("add-form");
                genericFormPanel =
                    new GenericFormPanel("add-form", optModelName, optMetaClassName, "", false, null, null,
                            GenericTablePanel.this);
                genericFormPanel.renderForm();
                form.add(genericFormPanel);
                genericFormPanel.setVisible(true);
                this.setVisible(false);
            }
        };
        return button;
    }

    /**
     * Trigger refreshment of data after entries have been deleted or added.
     */
    public void refreshGenericData() {
        data.clear();
        data.addAll(metaModelBrowser.getTableData(optModelName, optMetaClassName, searchForm == null ? ""
            : searchForm.getValue()));
    }

    @Override
    public List<Map<String, String>> readGenericData(String modelName, String metaClassName, String fulltext) {
        data.clear();
        data.addAll(metaModelBrowser.getTableData(modelName, metaClassName, fulltext));
        return data;
    }

    @Override
    public String getModelName() {
        return optModelName;
    }

    @Override
    public String getMetaClassName() {
        return optMetaClassName;
    }

    public String getTitle() {
        return titleName;
    }
}
