package org.cdlflex.itpm.web.standalone;

import java.util.Arrays;
import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.cdlflex.itpm.generated.model.Diagram;
import org.cdlflex.itpm.generated.model.Kpi;
import org.cdlflex.itpm.generated.model.OutputFormat;
import org.cdlflex.itpm.generated.model.Table;
import org.cdlflex.itpm.web.generic.chart.AbstractDashboardComponent;
import org.cdlflex.itpm.web.generic.chart.BarChart;
import org.cdlflex.itpm.web.generic.chart.ChartPanel;
import org.cdlflex.itpm.web.generic.chart.DashboardBasicComponent;
import org.cdlflex.itpm.web.generic.chart.DashboardFactory;
import org.cdlflex.itpm.web.generic.chart.DashboardTablePanel;
import org.cdlflex.itpm.web.generic.chart.KpiModel;
import org.cdlflex.itpm.web.generic.chart.KpiPanel;
import org.cdlflex.itpm.web.queryEditor.QueryEditorIntegratedPanel;
import org.cdlflex.itpm.web.queryEditor.QueryEditorStandalonePanel;
import org.cdlflex.itpm.web.queryEditor.RuleService;
import org.cdlflex.mdd.sembase.MDDException;

/**
 * The homepage is the entry point of the Wicket application. The homepage lists all queries which are of type "Diagram"
 * to provide an overview dashboard to the project manager.
 */
public class HomePage extends BasePage {
    private static final long serialVersionUID = 1L;

    @SpringBean
    private RuleService ruleService;
    private static boolean firstInit = false;

    public HomePage() throws MDDException {
        super();
        if (firstInit) {
            firstInit = true;
            setResponsePage(StartGuidePage.class);
        }

        DashboardFactory dashboardFactory = new DashboardFactory(ruleService);
        List<AbstractDashboardComponent> kpis =
            dashboardFactory.retrieveDashboardComponentsOfType(Arrays.asList(new OutputFormat[] { new Kpi() }));

        ListView<AbstractDashboardComponent> kpiListView =
            new ListView<AbstractDashboardComponent>("kpi_area", kpis) {
                private static final long serialVersionUID = 1L;

                @Override
                protected void populateItem(ListItem<AbstractDashboardComponent> item) {
                    KpiPanel kpiPanel = new KpiPanel("kpi", ((KpiModel) item.getModelObject()), ruleService, null);
                    kpiPanel.setOutputMarkupId(true);
                    item.add(kpiPanel);
                }
            };
        add(kpiListView);
        kpiListView.setVisible(kpis.size() > 0);

        List<AbstractDashboardComponent> tables =
            dashboardFactory.retrieveDashboardComponentsOfType(Arrays.asList(new OutputFormat[] { new Table(),
                new Diagram() }));
        ListView<AbstractDashboardComponent> tableListView =
            new ListView<AbstractDashboardComponent>("table_area", tables) {
                private static final long serialVersionUID = 1L;

                @Override
                protected void populateItem(ListItem<AbstractDashboardComponent> item) {
                    if (item.getModelObject() instanceof DashboardBasicComponent) {
                        DashboardTablePanel panel =
                            new DashboardTablePanel("table-output", item.getModelObject(), ruleService, null);
                        panel.setOutputMarkupId(true);
                        item.add(panel);
                    } else if (item.getModelObject() instanceof BarChart) {
                        ChartPanel c = new ChartPanel("table-output", Model.of("GOOG"), item.getModelObject());
                        // integrated query editor.
                        QueryEditorStandalonePanel queryEditor =
                            new QueryEditorStandalonePanel("query-editor", null, item.getModelObject().getRule(),
                                    ruleService);
                        QueryEditorIntegratedPanel integratedQueryEditor =
                            new QueryEditorIntegratedPanel("query-editor-integrated", c.getForm(), queryEditor, null);
                        c.add(integratedQueryEditor);
                        // min range for diagrams required. otherwise they are cut.
                        c.add(new AttributeModifier("class", "chart-area-part"));
                        item.add(c);
                    }
                }
            };
        add(tableListView);
    }
}
