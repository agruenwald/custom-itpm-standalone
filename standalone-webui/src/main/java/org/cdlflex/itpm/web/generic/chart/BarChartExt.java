package org.cdlflex.itpm.web.generic.chart;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.cdlflex.itpm.generated.model.PMRule;
import org.cdlflex.itpm.web.utils.Util;

/**
 * Display any data records with more than 2 dimensions as a bar chart.
 * 
 */
public class BarChartExt extends BarChart {
    private static final long serialVersionUID = 1L;
    private static final int FONT_SIZE_X = 15;
    private List<String> z;

    public BarChartExt(String title, PMRule rule, boolean showTitle) {
        super(title, rule, showTitle);
        this.z = new LinkedList<String>();
    }

    public void setZ(List<String> z) {
        this.z = z;
    }

    public List<String> getZ() {
        return z;
    }

    private class Entity {
        private String name;
        private Double value;

        protected Entity(String name, Double value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public String toString() {
            return String.valueOf(name + ":" + value);
        }
    }

    private Map<String, List<Entity>> groupData() {
        // attention: preserve order
        Map<String, List<Entity>> map = new LinkedHashMap<String, List<Entity>>();
        for (int i = 0; i < this.getX().size(); i++) {
            String x = this.getX().get(i);
            Double y = this.getY().get(i);
            String z = this.getZ().isEmpty() ? "empty" : this.getZ().get(i);

            List<Entity> list = new LinkedList<Entity>();
            if (!map.containsKey(x)) {
                map.put(x, list);
            } else {
                list = map.get(x);
            }

            list.add(new Entity(z, y));
            map.put(x, list);
        }
        return map;
    }

    private Set<String> getAllZValues() {
        Map<String, List<Entity>> groups = this.groupData();
        Iterator<Entry<String, List<Entity>>> it = groups.entrySet().iterator();

        SortedSet<String> xLabelSet = new TreeSet<String>();
        while (it.hasNext()) {
            Map.Entry<String, List<Entity>> entry = it.next();
            for (Entity d : entry.getValue()) {
                xLabelSet.add(cleanSparqlTimestamps(d.name));
            }
        }
        return xLabelSet;
    }

    @Override
    public String plot() {
        Map<String, List<Entity>> groups = this.groupData();
        Iterator<Entry<String, List<Entity>>> it = groups.entrySet().iterator();

        String bars = "";
        int i = 1;
        List<String> zValues = new LinkedList<String>(getAllZValues());
        SortedSet<String> xLabelSet = new TreeSet<String>();
        while (it.hasNext()) {
            Map.Entry<String, List<Entity>> entry = it.next();
            // is it possible to specify opacity?
            bars +=
                String.format("&bar%s=%s," + "" + "%s,%s,%s&", i > 1 ? "_" + i : "", ALPHA_BARS, COLOR_SCHEME[(i - 1)
                    % COLOR_SCHEME.length], entry.getKey(), FONT_SIZE_X);

            // every list needs to have the same length (add 0.0-values)
            List<String> valueList = new LinkedList<String>();
            for (String existingZValue : zValues) {
                double val = 0.0;
                for (Entity x : entry.getValue()) {
                    if (cleanSparqlTimestamps(x.name).equals(existingZValue)) {
                        val = x.value;
                        break;
                    }
                }
                valueList.add(String.valueOf(val));
            }

            bars += String.format("&values%s=%s&", i > 1 ? "_" + i : "", Util.asString(valueList, ','));
            for (Entity d : entry.getValue()) {
                xLabelSet.add(cleanSparqlTimestamps(d.name));
            }
            i++;
        }
        String xLabels = Util.asString(new LinkedList<String>(new TreeSet<String>(xLabelSet)), ',');

        double min = Collections.min(this.getY());
        double max = Collections.max(this.getY());

        int minRange = (min != 0 ? (int) Math.ceil(max / 25.0) : 0);
        int maxRange = (max != 0 ? (int) Math.ceil(max / 25.0) : 0);
        min = Math.max(0, min - minRange);
        if (max <= 100) {
            min = 0.0;
        }
        max = max + maxRange;
        String res = getChartMarkup(getTitle(), min, max) + String.format("%s" + "&x_labels=%s&", bars, xLabels);
        return res;
    }

    private String cleanSparqlTimestamps(String str) {
        return str.replaceFirst("T00:00:00", "").replaceFirst("T23:00:00Z", "");
    }
}
