package org.cdlflex.itpm.web.queryEditor.autocomplete;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.cdlflex.itpm.generated.ext.ITPMDAOFactory;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.itpm.web.utils.Util;
import org.cdlflex.mdd.modelrepo.AdvancedModelStoreImpl;
import org.cdlflex.mdd.modelrepo.ModelStore;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.sembase.semaccess.DAOFactory;
import org.cdlflex.mdd.sembase.semquery.QueryInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Returns specific concept types of an Ontology, such as all class names, all data property names, etc. The specific
 * (single) Ontology must be provided.
 */
public class ConceptService {
    private static final Logger logger = LoggerFactory.getLogger(ConceptService.class);
    private QueryInterface qI;
    private String ontologyPrefix;
    private ConfigLoader configLoader = new ConfigLoader(false);
    private DAOFactory itpmDAOFactory = new ITPMDAOFactory(configLoader);
    private String format;

    private static final String DEFAULT_PROJECT = "";
    public static final String OWL_CLASS = "owlClass";
    public static final String OWL_OBJECT_PROPERTY = "owlObjectProperty";
    public static final String OWL_DATA_PROPERTY = "owlDataProperty";

    public ConceptService(String format, String ontologyPrefix) throws MDDException {
        this.format = format;
        qI = new ConnectionUtil().getQueryInterface(format, new ConnectionUtil().getOpenITPMConnection());
        this.ontologyPrefix = ontologyPrefix;
    }

    /**
     * @return all classes of the underlying ontology.Might include other concepts as well (data properties or so).
     */
    public List<String> getAllClasses() throws MDDException {
        String query = "SELECT ?class WHERE { ?class a owl:Class . } ORDER BY ASC(?class)";
        try {
            return prefixListEntries(Util.colToList(qI.query(query), 0), ontologyPrefix);
        } catch (MDDException e) {
            logger.error(e.getMessage());
        }
        return new LinkedList<String>();
    }

    /**
     * @return all Object property names of the underlying ontology
     */
    public List<String> getAllObjectProperties() {
        String query = "SELECT ?x WHERE { ?x a owl:ObjectProperty .} ORDER BY ASC(?x)";
        try {
            List<String> result = prefixListEntries(Util.colToList(qI.query(query), 0), ontologyPrefix);
            return result;
        } catch (MDDException e) {
            logger.error(e.getMessage());
        }
        return new LinkedList<String>();
    }

    /**
     * @return all datatype property names of the underlying ontology
     */
    public List<String> getAllDataProperties() {
        String query = "SELECT ?class WHERE { ?class a owl:DatatypeProperty.} ORDER BY ASC(?class)";
        try {
            return prefixListEntries(Util.colToList(qI.query(query), 0), ontologyPrefix);
        } catch (MDDException e) {
            logger.error(e.getMessage());
        }
        return new LinkedList<String>();
    }

    /**
     * prefix a list with a namespace.
     * 
     * @param list a list of strings
     * @param prefix the prefix, e.g. "itpm" without ":"
     * @return a list of strings where each string is prefixed.
     */
    public List<String> prefixListEntries(List<String> list, String prefix) {
        List<String> pList = new ArrayList<String>();
        for (String s : list) {
            pList.add(prefix + ":" + s);
        }
        return pList;
    }

    private void addOrPut(Map<String, List<String>> concepts, List<String> result, String mapKey) {
        if (concepts.containsKey(mapKey)) {
            concepts.get(mapKey).addAll(result);
        } else {
            concepts.put(mapKey, result);
        }
    }

    /**
     * get all concepts of all available models.
     * 
     * @return a map where the key contains the type of the concept and each value is a list of concepts (e.g. classes)
     *         from all models.
     * @throws MDDException
     */
    public Map<String, List<String>> retrieveConceptsAllModels() throws MDDException {
        Map<String, List<String>> concepts = new LinkedHashMap<String, List<String>>();

        ModelStore modelStore = new AdvancedModelStoreImpl(configLoader);
        List<String> modelNames = modelStore.findAllMetaModelNames();
        for (String prefix : modelNames) {
            this.ontologyPrefix = prefix;
            Connection c = itpmDAOFactory.createConnection(DEFAULT_PROJECT, prefix);
            c.connect();
            qI = new ConnectionUtil().getQueryInterface(format, c);

            List<String> resultC = this.getAllClasses();
            this.addOrPut(concepts, resultC, OWL_CLASS);

            List<String> resultD = this.getAllDataProperties();
            this.addOrPut(concepts, resultD, OWL_DATA_PROPERTY);

            List<String> resultO = this.getAllObjectProperties();
            this.addOrPut(concepts, resultO, OWL_OBJECT_PROPERTY);

            c.close();
        }
        for (List<String> list : concepts.values()) {
            Collections.sort(list, new ITPMComparator());
        }

        return concepts;
    }
}
