package org.cdlflex.itpm.web.semanticEditor;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AbstractDefaultAjaxBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptContentHeaderItem;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.request.IRequestParameters;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.TextRequestHandler;
import org.cdlflex.itpm.web.metamodeltools.MetaModelBrowser;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.itpm.web.utils.BashUtil;
import org.cdlflex.mdd.modelrepo.AdvancedModelStore;
import org.cdlflex.mdd.modelrepo.AdvancedModelStoreImpl;
import org.cdlflex.mdd.modelrepo.NamingManager;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.umltumany.CodeGenerator;
import org.cdlflex.mdd.umltuowl.metamodel.MetaComment;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodeltoowl.JsonMaker;

import flexjson.JSONSerializer;

/**
 * Single Semantic MetaEditor instance. Within a single page several instances can be loaded. They all interact in their
 * own environment and they are not in conflict with each other.
 */
public class SemanticMetaEditorPanel extends Panel {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(SemanticMetaEditorPanel.class);
    private static ConfigLoader configLoader = new ConfigLoader(false);

    private static final String PARAM_METAMODEL = "mm";
    private static final String PARAM_ACTION = "action";
    private static final String PARAM_NAMESPACE_PREFIX = "namespaceprefix";
    private static final String VALUE_LOAD = "load";
    private static final String VALUE_SAVE = "save";
    private static final String ADD_METAMODEL = "add_metamodel";
    private static final String COPY_METAMODEL = "copy_metamodel";
    private static final String UPDATE_METAMODEL = "update_metamodel";
    private static final String DELETE_METAMODEL = "delete_metamodel";
    private static final String RETRIEVE_MODEL_NAMES = "get_modelnames";
    private static final String GENERATE_CODE = "generate_code";
    private static final String GENERATE_CODE_MODEL = "generate_code_model";
    private static final String GENERATE_CODE_CALL_ROUTINE = "generate_code_call_routine";

    private static final String OPENENGSB_TYPE = "openengsbType";

    private static final String OK = "OK";
    private static final String NOTOK = "NOTOK";
    private static final List<String> CORE_MODELS = Arrays.asList(new String[] { "itpm", "config" });
    private static final String PERMISSION_DENIED_CORE_MODELS =
        "The permission for modifiying core models is not set (%s is a core model). Because modifying the core "
            + "models may require a rebuilt of the Web Application this feature is deactivated by default. "
            + "You can activate the permission in the sembase configuration files.";
    private static final String MAIN_CONFIG_FILE = "main-config";
    private static final String IS_ADMIN = "is-admin";

    public SemanticMetaEditorPanel(final String id) {
        super(id);
        final String markupId = String.valueOf(UUID.randomUUID().getMostSignificantBits());
        logger.trace(String.format("Called FlexGraphPanel with id=%s. Generated markupId=%s.", id, markupId));
        Form<SemanticMetaEditorPanel> form = new Form<SemanticMetaEditorPanel>("form");
        form.setOutputMarkupId(true);
        form.setMarkupId(markupId); // used for communication with JS
        this.add(form);

        final AbstractDefaultAjaxBehavior behave = new AbstractDefaultAjaxBehavior() {
            private static final long serialVersionUID = 2923087393268148392L;

            public void renderHead(Component component, IHeaderResponse response) {
                // both variables are used to interface with JS.
                String script =
                    "var semanticDiagramsURL ='" + this.getCallbackUrl() + "';"
                        + "var semanticMetaEditorPosition = 0;" + "var semanticMetaEditorId = '" + markupId + "';";
                response.render(new JavaScriptContentHeaderItem(script, "", ""));
            }

            private AdvancedModelStore initStore() {
                try {
                    return new AdvancedModelStoreImpl(configLoader);
                } catch (MDDException e1) {
                    logger.error(String.format("Could not initialize model store (%s).", e1.getMessage()));
                    return null;
                }
            }

            @Override
            protected void respond(final AjaxRequestTarget target) {
                AdvancedModelStore store = this.initStore();
                if (store == null) {
                    return;
                }
                IRequestParameters params = RequestCycle.get().getRequest().getRequestParameters();
                String action = params.getParameterValue(PARAM_ACTION).toString("");

                try {
                    if (action.toLowerCase().equals(VALUE_LOAD)) {
                        logger.info("Load jsonModel...");
                        String namespacePrefix = params.getParameterValue(PARAM_NAMESPACE_PREFIX).toString("");
                        MetaModel mm = store.findMetaModel(namespacePrefix);
                        if (mm == null) {
                            // take first meta model if empty
                            List<MetaModel> allModels = store.findAllMetaModels();
                            if (allModels.size() > 0) {
                                mm = allModels.get(0);
                            } else {
                                // no models available - initialize one at the beginning
                                mm = new MetaModel("default", NamingManager.DEFAULT_NAMESPACE);
                                mm.setDescription("Created via " + SemanticMetaEditorPanel.class.getSimpleName()
                                    + " because not existing.");
                            }
                        }
                        String jsonModel = JsonMaker.toJson(mm);
                        renderString(jsonModel);
                    } else if (action.toLowerCase().equals(VALUE_SAVE)) {
                        String metamodelJson = params.getParameterValue(PARAM_METAMODEL).toString("");
                        logger.info("Save jsonModel...");
                        if (metamodelJson.isEmpty()) {
                            logger.error(String.format("Invalid metamodel: Parameter %s is empty.", PARAM_METAMODEL));
                        } else {
                            MetaModel mm = JsonMaker.toMetaModel(metamodelJson);
                            checkPermission(mm.getNamespacePrefix());
                            store.persistOWLMetaModel(mm, false);
                            MetaModelBrowser.invalidateCache();
                        }
                    } else if (action.toLowerCase().equals(ADD_METAMODEL)) {
                        String mm = params.getParameterValue(PARAM_METAMODEL).toString("");
                        MetaModel metaModel = JsonMaker.toMetaModel(mm);
                        if (store.findMetaModel(metaModel.getNamespacePrefix()) != null) {
                            renderStatus(NOTOK); // already existing
                        } else {
                            store.persistOWLMetaModel(metaModel, false);
                            renderStatus(OK);
                            MetaModelBrowser.invalidateCache();
                        }
                    } else if (action.toLowerCase().equals(COPY_METAMODEL)) {
                        String mm = params.getParameterValue(PARAM_METAMODEL).toString("");
                        MetaModel metaModel = JsonMaker.toMetaModel(mm);
                        List<MetaModel> existingModels = store.findAllMetaModels();
                        int i = 0;
                        for (MetaModel m : existingModels) {
                            if (m.getName().toLowerCase().equals(metaModel.getName().toLowerCase())
                                || m.getNamespacePrefix().toLowerCase()
                                        .equals(metaModel.getNamespacePrefix().toLowerCase())) {
                                i++; // do not allow namespace to change
                            }
                        }
                        if (i != 0) {
                            renderStatus(NOTOK); // duplicate
                        } else {
                            store.persistOWLMetaModel(metaModel, false);
                            renderStatus(OK);
                            MetaModelBrowser.invalidateCache();
                        }

                    } else if (action.toLowerCase().equals(UPDATE_METAMODEL)) {
                        String mm = params.getParameterValue(PARAM_METAMODEL).toString("");
                        MetaModel metaModel = JsonMaker.toMetaModel(mm);
                        checkPermission(metaModel.getNamespacePrefix());
                        List<MetaModel> existingModels = store.findAllMetaModels();
                        int i = 0;
                        for (MetaModel m : existingModels) {
                            if (m.getName().toLowerCase().equals(metaModel.getName().toLowerCase())
                                || m.getNamespacePrefix().toLowerCase()
                                        .equals(metaModel.getNamespacePrefix().toLowerCase())) {
                                i++; // do not allow namespace to change
                            }
                        }
                        if (i != 1) {
                            renderStatus(NOTOK); // duplicates or not existing at all.
                        } else {
                            store.persistOWLMetaModel(metaModel, false);
                            renderStatus(OK);
                            MetaModelBrowser.invalidateCache();
                        }
                    } else if (action.toLowerCase().equals(DELETE_METAMODEL)) {
                        String mm = params.getParameterValue(PARAM_METAMODEL).toString("");
                        MetaModel metaModel = JsonMaker.toMetaModel(mm);
                        checkPermission(metaModel.getNamespacePrefix());
                        store.removeMetaModel(metaModel.getNamespacePrefix());
                        renderStatus(OK);
                        MetaModelBrowser.invalidateCache();
                    } else if (action.toLowerCase().equals(RETRIEVE_MODEL_NAMES)) {
                        JSONSerializer serializer = new JSONSerializer();
                        List<MetaModel> models = store.findAllMetaModels();
                        List<Map<String, String>> compactModels = new LinkedList<Map<String, String>>();
                        for (MetaModel m : models) {
                            Map<String, String> map = new HashMap<String, String>();
                            map.put("name", m.getName());
                            map.put("namespace", m.getNamespace());
                            map.put("namespacePrefix", m.getNamespacePrefix());
                            map.put("size", String.valueOf(m.getSize()));
                            map.put("noPackages", String.valueOf(m.getPackages().size()));
                            List<MetaComment> comments =
                                m.findComments(MetaComment.TYPE_INTERNAL_ANNOTATION, "@" + OPENENGSB_TYPE);
                            map.put(OPENENGSB_TYPE, comments.size() == 1 ? comments.get(0).getAnnotationSubtype()
                                    .get(1) : "");
                            compactModels.add(map);
                        }
                        String jsonStr = serializer.serialize(compactModels);
                        renderString(jsonStr);
                    } else if (action.toLowerCase().equals(GENERATE_CODE)) {
                        CodeGenerator generator = new CodeGenerator(configLoader);
                        try {
                            List<String> res = generator.generateCode();
                            String x = "";
                            for (String y : res) {
                                x += y + "<br />";
                            }
                            renderStatus(OK, x);
                        } catch (MDDException e) {
                            renderStatus(NOTOK, e.getMessage());
                        }
                    } else if (action.toLowerCase().equals(GENERATE_CODE_MODEL)) {
                        CodeGenerator generator = new CodeGenerator(configLoader);
                        try {
                            List<String> res =
                                generator.generateCodeSingleModel(params.getParameterValue(PARAM_METAMODEL).toString(
                                        ""));
                            String x = "";
                            for (String y : res) {
                                x += y + "<br />";
                            }
                            renderStatus(OK, x);
                        } catch (MDDException e) {
                            renderStatus(NOTOK, e.getMessage());
                        }
                    } else if (action.toLowerCase().equals(GENERATE_CODE_CALL_ROUTINE)) {
                        try {
                            String res = BashUtil.execCodeGenScript(configLoader);
                            renderStatus(OK, res);
                        } catch (Exception e) {
                            renderStatus(NOTOK, e.getMessage());
                        }
                    } else {
                        logger.error(String.format("Invalid parameter %s:%s.", PARAM_ACTION, action));
                    }

                } catch (MDDException e) {
                    logger.error(e.getMessage());
                    renderStatus(NOTOK, e.getMessage());
                }
            }
        };
        add(behave);
    }

    private void checkPermission(String modelPrefix) throws MDDException {
        ConfigLoader configLoader = new ConnectionUtil().getConfigLoader();
        String isAdmin = configLoader.findPropertySmart(MAIN_CONFIG_FILE, IS_ADMIN);
        boolean isBAdmin = Boolean.parseBoolean(isAdmin.isEmpty() ? "true" : isAdmin);
        if (CORE_MODELS.contains(modelPrefix) && !isBAdmin) {
            throw new MDDException(String.format(PERMISSION_DENIED_CORE_MODELS, modelPrefix));
        }
    }

    private void renderStatus(String str) {
        this.renderStatus(str, "");
    }

    private void renderStatus(String str, String optionalData) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("status", str);
        if (!optionalData.isEmpty()) {
            map.put("data", optionalData);
        }
        renderString(new JSONSerializer().serialize(map));
    }

    private void renderString(String str) {
        TextRequestHandler textRequestHandler = new TextRequestHandler("application/json", "UTF-8", str);
        RequestCycle.get().scheduleRequestHandlerAfterCurrent(textRequestHandler);
    }
}
