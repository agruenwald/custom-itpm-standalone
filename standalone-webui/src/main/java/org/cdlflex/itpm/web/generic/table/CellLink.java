package org.cdlflex.itpm.web.generic.table;

import org.apache.wicket.Page;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * Table link to detailed record data (used within cell).
 */
public class CellLink extends Panel {
    private static final long serialVersionUID = -8059214609379218054L;

    public CellLink(String id, Class<? extends Page> linkClass, String metaClassName, String recId,
            PageParameters parameters) {
        super(id);
        final String name = "<i class=\"glyphicon glyphicon glyphicon-edit\"></i>"; // or glyphicon-edit
        PageParameters param = new PageParameters();
        param.add("metaClass", metaClassName);
        param.add("recId", recId);
        // keep existing parameters
        for (String existingP : parameters.getNamedKeys()) {
            if (param.get(existingP).toString("").isEmpty()) {
                param.add(existingP, parameters.get(existingP));
            }
        }
        this.setEscapeModelStrings(false);
        BookmarkablePageLink<?> link = new BookmarkablePageLink<String>("link", linkClass, param);
        link.setEscapeModelStrings(false);
        Label label = new Label("label", name);
        link.add(label);
        label.setEscapeModelStrings(false);
        add(link);
    }
}
