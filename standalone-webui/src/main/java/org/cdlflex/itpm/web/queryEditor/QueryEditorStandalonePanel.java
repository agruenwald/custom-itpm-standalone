package org.cdlflex.itpm.web.queryEditor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AbstractDefaultAjaxBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.ISortableDataProvider;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptContentHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.TextRequestHandler;
import org.apache.wicket.util.value.ValueMap;
import org.cdlflex.itpm.generated.ext.ITPMDAOFactory;
import org.cdlflex.itpm.generated.model.KnowledgeArea;
import org.cdlflex.itpm.generated.model.OutputFormat;
import org.cdlflex.itpm.generated.model.PMRule;
import org.cdlflex.itpm.generated.model.QueryFormat;
import org.cdlflex.itpm.web.queryEditor.accordionStructure.RuleModel;
import org.cdlflex.itpm.web.queryEditor.autocomplete.QueryEditorResponse;
import org.cdlflex.itpm.web.reusable.GenericSortableDataProvider;
import org.cdlflex.itpm.web.reusable.ISubmitCallback;
import org.cdlflex.itpm.web.reusable.InteractiveTableColumn;
import org.cdlflex.itpm.web.semanticEditor.SemanticMetaEditorPanel;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.itpm.web.utils.HtmlUtil;
import org.cdlflex.itpm.web.utils.IconUtil;
import org.cdlflex.itpm.web.utils.Util;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAOFactory;
import org.cdlflex.mdd.sembase.semquery.QueryInterface;

/**
 * Standalone-Panel for the creation of new rules/queries based on different query languages. Syntax highlighting and
 * autocompletion for SPARQL is currently supported.
 */
public class QueryEditorStandalonePanel extends Panel implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(QueryEditorStandalonePanel.class);
    private static final List<String> FORMATS = Arrays.asList(new String[] { "SPARQL", "OWLDL", "SWRL", "ITPMQL" });
    private static final List<String> OUTPUTS = Arrays.asList(new String[] { "Table", "Diagram", "Kpi", "Graph",
        "GraphV", "GraphSeq", "GraphStairs", "Notification", "No" });

    private static final String CSS_INFO =
        "Use flexBox-[default|primary|warning|success|info|danger], e.g. flexBox-success.";
    private static final String KEYWORDS_INFO = HtmlUtil.br("") + "Keywords: " + HtmlUtil.br("") + HtmlUtil.b("id:")
        + " A unique identifier of the node." + HtmlUtil.br("") + HtmlUtil.b("title:")
        + " The title of the node to be rendered." + HtmlUtil.br("") + HtmlUtil.b("parentId:")
        + "The id of the parent node in the graph or empty if the node is a root node." + HtmlUtil.br("")
        + HtmlUtil.b("text:") + " Optional node text." + HtmlUtil.br("") + HtmlUtil.b("icon:")
        + " An optional icon for the text. Use any Bootstrap Glyphicon. Example: icon-user for glyphicon-user"
        + HtmlUtil.br("") + HtmlUtil.b("status:") + " Any CSS class to format the color and style of the node. "
        + CSS_INFO;

    private static final String CSS_ANNOTATOR_INFO = HtmlUtil.br("") + HtmlUtil.br("Html/Bootstrap Plugin: ")
        + HtmlUtil.br("") + HtmlUtil.b("icon:task{}: ")
        + "Will be replaced by a Bootstrap Glyphicon named \"glyphicon-task\"." + HtmlUtil.br("")
        + HtmlUtil.b("icon:task{color:blue, class:flexbox}: ")
        + "Create a blue task icon with css class \"flexbox\"." + HtmlUtil.br("")
        + HtmlUtil.b("icon:user{color:blue, text:Click on it to open a popup!}: ")
        + "Blue user-icon with popup functionality." + HtmlUtil.br("") + HtmlUtil.b("html:small{Small text}: ")
        + "Creates a HTML tag of type small." + HtmlUtil.br("") + HtmlUtil.b("itpm:Requirement{Link Label}")
        + "A link to the concept view." + HtmlUtil.br("")
        + HtmlUtil.b("itpm:Requirement{rec:instance001, Link Label}") + "A link to an instance.";

    private static final List<String> OUTPUT_DESCRIPTIONS =
        Arrays.asList(new String[] {
            "The result is formated as a table and linked in the PM Data menu." + CSS_ANNOTATOR_INFO,
            "Diagrams are rendered as bar-charts in the dashboard.",
            String.format(
                    "Key performance indicators are single values rendered in the dashboard. Usage: row1=value, row2=optionalCssClass (%s), row3=optionalCssIcon (%s).",
                    CSS_INFO, "e.g. icon-star for the Bootstrap glyphicon glyphicon-star"),
            "This format renders nested hierarchies (breakdown structure). " + KEYWORDS_INFO,
            "Same as Graph, but the nodes will be arranged in V-shape (e.g. to represent the phases of a V-SDLC process). "
                + "Not completely implemented yet." + KEYWORDS_INFO,
            "Same as Graph, but the nodes will be arranged sequentially (e.g. to represent a project steps or processes)"
                + KEYWORDS_INFO,
            "Same as Graph, but the nodes will be arranged in steps (e.g. to represent a waterfall SDLC) - not implemented yet."
                + KEYWORDS_INFO,
            "If the criteria is fulfilled then the information is used to send a notification to some project member (not implemented yet).",
            "The query is marked as irrelevant. It will be kept persistent, but not rendered anywhere (not implemented yet)." });

    private final RuleModel ruleModel;
    private final boolean isNew;
    private DropDownChoice<String> format;
    private DropDownChoice<String> output;
    private TextField<String> name;
    private TextField<Integer> order;
    private TextArea<String> definition;
    private TextArea<String> description;
    private CheckBox dashboard;

    private WebMarkupContainer previewBlock = null;
    private DefaultDataTable<Map<String, String>, String> defaultDataTable = null;

    private String knowledgeAreaName;
    private final RuleService ruleService;
    private ISubmitCallback callbackObj;

    public QueryEditorStandalonePanel(String id, FeedbackPanel feedbackPanel, PMRule rule, RuleService ruleService) {
        super(id);
        this.isNew = false;
        this.ruleModel = new RuleModel(rule);
        this.setOutputMarkupId(true);
        this.knowledgeAreaName = rule.getBelongsTo().getName();
        this.ruleService = ruleService;
        this.createRuleForm(id, feedbackPanel);
    }

    public QueryEditorStandalonePanel(String id, String knowledgeAreaName, FeedbackPanel feedbackPanel,
            RuleService ruleService) {
        super(id);
        this.isNew = true;
        this.ruleModel = new RuleModel();
        this.setOutputMarkupId(true);
        this.knowledgeAreaName = knowledgeAreaName;
        this.ruleService = ruleService;
        this.createRuleForm(id, feedbackPanel);
    }

    public void setCallbackObj(ISubmitCallback callbackObj) {
        this.callbackObj = callbackObj;
    }

    public Form<RuleModel> createRuleForm(String formName, final FeedbackPanel feedbackPanel) {
        if (isNew) {
            ruleModel.setDashboard(true);
        }
        format = new DropDownChoice<String>("format", FORMATS);
        output = new DropDownChoice<String>("output", OUTPUTS);
        name = new TextField<String>("name", new PropertyModel<String>(ruleModel, "name"));
        order = new TextField<Integer>("order", new PropertyModel<Integer>(ruleModel, "order"));
        dashboard = new CheckBox("dashboard", new PropertyModel<Boolean>(ruleModel, "dashboard"));
        definition = new TextArea<String>("definition", new PropertyModel<String>(ruleModel, "definition"));
        description = new TextArea<String>("description", new PropertyModel<String>(ruleModel, "description"));
        final SemanticMetaEditorPanel semEditor = new SemanticMetaEditorPanel("semEditor");

        ValueMap ruleMap = new ValueMap();
        ruleMap.add("definition", ruleModel.getDefinition());
        ruleMap.add("description", ruleModel.getDescription());
        ruleMap.add("name", ruleModel.getName());
        ruleMap.add("order", String.valueOf(ruleModel.getOrder()));
        ruleMap.add("dashboard", String.valueOf(ruleModel.isDashboard()));
        ruleMap.add("format", ruleModel.getFormat());
        ruleMap.add("output", ruleModel.getOutput());
        CompoundPropertyModel<RuleModel> formModel = new CompoundPropertyModel<RuleModel>(this.ruleModel);
        Form<RuleModel> form = new Form<RuleModel>("rule_form", formModel);

        final Model<String> popupDescr = Model.of(createPopupMarkup(ruleModel.getOutput()));
        final Label bsPopover = new Label("output-popup", popupDescr);
        bsPopover.setOutputMarkupId(true);
        bsPopover.setEscapeModelStrings(false);
        form.add(bsPopover);
        output.add(new OnChangeAjaxBehavior() {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                // some code
                int ndx = Integer.parseInt(output.getValue());
                String updatedDescr = createPopupMarkup(ndx);
                popupDescr.setObject(updatedDescr);
                target.add(bsPopover);
            }
        });

        AjaxButton submitButton = new AjaxButton("ajaxSubmit", form) {
            private static final long serialVersionUID = -414950996359021471L;

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                previewBlock.setVisible(false);
                RuleModel editRule = ruleModel;
                RuleModel val = (RuleModel) form.getModelObject();
                PMRule rule = editRule.getRule();
                DAOFactory factory = new ITPMDAOFactory(false);
                String formatStr = format.getConvertedInput();
                String outputStr = output.getConvertedInput();
                val.setKnowledgeArea(knowledgeAreaName);
                val.setFormat(formatStr);
                val.setOutput(outputStr);
                PMRule itpmRule = editRule.getRule();
                KnowledgeArea knowledgeArea;
                Connection con = QueryEditorPage.con;
                try {
                    knowledgeArea = (KnowledgeArea) factory.createBean(knowledgeAreaName, con);
                    knowledgeArea.setName(knowledgeAreaName);
                    itpmRule.setBelongsTo(knowledgeArea);
                } catch (MDDException e) {
                    logger.error(e.getMessage());
                }

                try {
                    itpmRule.setFormat((QueryFormat) factory.createBean(formatStr, con));
                } catch (MDDException e) {
                    logger.error(e.getMessage());
                }

                try {
                    itpmRule.setOutput((OutputFormat) factory.createBean(outputStr, con));
                } catch (MDDException e) {
                    logger.error(e.getMessage());
                }

                String cleanedRule = cleanHtml(rule.getRuleDefinition());
                itpmRule.setRuleDefinition(cleanedRule);
                if (isNew) {
                    Random randomGenerator = new Random();
                    int rand = randomGenerator.nextInt(10000);
                    rule.setId(rand);
                }
                logger.info("ID: " + rule.getId());

                try {
                    if (isNew) {
                        ruleService.insertRule(itpmRule);
                        success(String.format("Rule \"%s\" added to knowledge base.", rule.getName()));
                        val.initPMRule();
                        handleAfterDbUpdate(true, target);
                    } else {
                        ruleService.updateRule(itpmRule);
                        success(String.format("Rule \"%s\" has been updated.", rule.getName()));
                        handleAfterDbUpdate(false, target);
                    }
                } catch (MDDException e) {
                    QueryEditorStandalonePanel.logger.error(e.getMessage());
                    error(e.getMessage());
                    if (feedbackPanel != null) {
                        target.add(feedbackPanel);
                    }
                }
            }

        };

        AjaxButton previewButton = new AjaxButton("ajaxPreview", form) {
            private static final long serialVersionUID = 1L;
            private boolean firstTime = true;
            private Label previewMsgLabel;
            private Model<String> messageModel = Model.of("");

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                RuleModel editRule = ruleModel;
                logger.info(editRule.getName());
                if (firstTime) {
                    previewMsgLabel = new Label("success-msg", messageModel);
                    previewBlock.add(previewMsgLabel);
                }
                firstTime = false;
                try {
                    String cleanedRule = cleanHtml(definition.getValue());
                    QueryInterface qI =
                        new ConnectionUtil().getQueryInterface(format.getConvertedInput(), cleanedRule);
                    List<List<String>> result = qI.query(cleanedRule);
                    result = Util.parseAnnotations(result);
                    List<String> titles = qI.extractNames(cleanedRule);
                    previewBlock.setVisible(true);
                    previewBlock.remove(defaultDataTable);
                    messageModel.setObject(String.format(
                            "Nice! The rule is valid and %d records were found (see the preview snippet below).",
                            result.size()));
                    previewMsgLabel.add(new AttributeModifier("class", "alert alert-info"));
                    refreshTable(result, titles, "preview-table", 5);
                    previewBlock.add(defaultDataTable);
                    target.add(previewBlock);
                    target.add(defaultDataTable);
                    this.getFeedbackMessages().clear();
                } catch (MDDException e) {
                    QueryEditorStandalonePanel.logger.error(e.getMessage());
                    error(e.getMessage());
                    if (feedbackPanel != null) {
                        target.add(feedbackPanel);
                    }
                    // show error msg twice (once on top and once before submit field)
                    String sparqlDelim = "Complete SPARQL: ";
                    String shortErrorMessage =
                        e.getMessage().contains(sparqlDelim) ? e.getMessage().split(sparqlDelim)[0].trim() : e
                                .getMessage();
                    messageModel.setObject(shortErrorMessage);
                    previewBlock.setVisible(true);
                    previewBlock.remove(defaultDataTable);
                    previewBlock.add(defaultDataTable);
                    previewMsgLabel.add(new AttributeModifier("class", "alert alert-danger"));
                    target.add(previewBlock);
                }

            }
        };

        form.add(submitButton);
        form.add(previewButton);
        form.add(definition);
        form.add(format);
        form.add(output);
        form.add(description);
        semEditor.setVisible(false);
        form.add(semEditor);
        final WebMarkupContainer nameBlock = new WebMarkupContainer("name-block");
        form.add(nameBlock);
        nameBlock.add(name);

        final WebMarkupContainer orderBlock = new WebMarkupContainer("order-block");
        form.add(orderBlock);
        orderBlock.add(order);
        form.add(dashboard);

        previewBlock = new WebMarkupContainer("preview-block");
        previewBlock.setOutputMarkupId(true);
        previewBlock.setOutputMarkupPlaceholderTag(true);
        List<List<String>> emptyList = new ArrayList<List<String>>();
        previewBlock.add(this.refreshTable(emptyList, null, "preview-table", 3));
        this.defaultDataTable.setOutputMarkupId(true);
        this.defaultDataTable.setOutputMarkupPlaceholderTag(true);
        form.add(previewBlock);
        previewBlock.setVisible(false);
        if (isNew) {
            this.add(form);
            nameBlock.setVisible(true);
        } else {
            this.add(form);
            formModel.setObject(this.ruleModel);
        }

        final AbstractDefaultAjaxBehavior behave = new AbstractDefaultAjaxBehavior() {
            private static final long serialVersionUID = 1L;

            public void renderHead(Component component, IHeaderResponse response) {
                String script = "var jsQueryURL ='" + this.getCallbackUrl() + "';";
                response.render(new JavaScriptContentHeaderItem(script, "", ""));

            }

            @Override
            protected void respond(final AjaxRequestTarget target) {
                String jsonResponse = null;
                try {
                    jsonResponse =
                        new QueryEditorResponse().checkAutomatable(RequestCycle.get().getRequest()
                                .getRequestParameters());
                    TextRequestHandler textRequestHandler =
                        new TextRequestHandler("application/json", "UTF-8", jsonResponse);
                    RequestCycle.get().scheduleRequestHandlerAfterCurrent(textRequestHandler);
                } catch (MDDException e) {
                    logger.error(e.getMessage());

                }
            }
        };
        form.add(behave);
        return form;
    }

    /**
     * Override this method on demand.
     * 
     * @param isNew
     * @param target
     */
    public void handleAfterDbUpdate(boolean isNew, AjaxRequestTarget target) {
        if (callbackObj != null) {
            callbackObj.submit(isNew, target);
        } else {
            if (isNew) {
                target.add(this.getPage());
            } else {
                target.add(this.getPage());
            }
        }
        // target.add(QueryEditorForm.this.rulePageRef.getInfoFeedback());
    }

    private String cleanHtml(String html) {
        final String PREVENT_LINE_BREAK = "PP-PREVENT-LINE-BREAK-PP";
        html = html == null ? "" : html;
        html = html.replaceAll("\n", PREVENT_LINE_BREAK);
        String cleaned = StringEscapeUtils.unescapeHtml(html).replaceAll("[^\\x20-\\x7e]", "");
        cleaned = cleaned.replaceAll(PREVENT_LINE_BREAK, "\n");
        return cleaned;
    }

    private String createPopupMarkup(String outputFormat) {
        int i = 0;
        for (String f : OUTPUTS) {
            if (f.equals(ruleModel.getOutput())) {
                return createPopupMarkup(i);
            }
            i++;
        }
        return OUTPUTS.get(0); // on default the first is used
    }

    private String createPopupMarkup(int ndx) {
        return IconUtil.createBSPopover(String.format("Information about the %s format", ruleModel.getOutput()), "",
                "tag", true, OUTPUT_DESCRIPTIONS.get(ndx));
    }

    private DefaultDataTable<Map<String, String>, String> refreshTable(final List<List<String>> dataS,
        List<String> titles, String tableWicketId, int maxRecords) {
        final List<List<String>> datas;
        if (maxRecords > 0 && dataS.size() > maxRecords) {
            datas = dataS.subList(0, maxRecords);
        } else {
            datas = dataS;
        }

        if (titles == null) {
            titles = new ArrayList<String>();
            if (datas.size() > 0) {
                for (int i = 0; i < datas.get(0).size(); i++) {
                    titles.add("Col " + (i + 1));
                }
            }
        }
        final List<Map<String, String>> data = Util.listToMap(titles, datas, true);

        // Using a DefaultDataTable
        ISortableDataProvider<Map<String, String>, String> dataProvider = new GenericSortableDataProvider(data);
        List<InteractiveTableColumn> columns = new ArrayList<InteractiveTableColumn>();
        for (String key : titles) {
            InteractiveTableColumn col = new InteractiveTableColumn(Model.of(key), key);
            columns.add(col);
        }
        if (columns.isEmpty()) {
            InteractiveTableColumn col = new InteractiveTableColumn(Model.of(""), "-");
            columns.add(col);
        }
        defaultDataTable =
            new DefaultDataTable<Map<String, String>, String>(tableWicketId, columns, dataProvider, maxRecords);
        defaultDataTable.setOutputMarkupPlaceholderTag(true);
        defaultDataTable.setOutputMarkupId(true);
        return defaultDataTable;
    }
}
