package org.cdlflex.itpm.web.semanticEditor;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AbstractDefaultAjaxBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptContentHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.cdlflex.itpm.web.standalone.BasePage;

/**
 * The Semantic Meta Editor Page renders a single Semantic Meta Editor, which runs in its isolated sandbox. Additionally
 * a second editor can be toggled to ease the comparison between different models. The triggering is performed via JS
 * events for better performance.
 */
public class SemanticMetaEditorPage extends BasePage {
    private static final long serialVersionUID = 2273116859121005462L;
    private boolean comparisonEditorOn;

    public SemanticMetaEditorPage() {
        super();
        comparisonEditorOn = false;
        final Form<SemanticMetaEditorPage> form = new Form<SemanticMetaEditorPage>("mainSemForm");
        final Panel panel = new SemanticMetaEditorPanel("semEditor1");

        final Panel panel2 = new SemanticMetaEditorPanel("semEditor2");
        form.add(panel);

        final WebMarkupContainer comparisonEditor = new WebMarkupContainer("comparisonEditor");
        setComparisonEditorVisibility(comparisonEditor, comparisonEditorOn);

        form.add(comparisonEditor);
        comparisonEditor.add(panel2);
        comparisonEditor.add(AttributeModifier.append("class", "comparison-editor-unique"));
        add(form);

        final AbstractDefaultAjaxBehavior behave = new AbstractDefaultAjaxBehavior() {
            private static final long serialVersionUID = 2923087393268148392L;

            public void renderHead(Component component, IHeaderResponse response) {
                String script = "var panelComparisonEditorURL ='" + this.getCallbackUrl() + "';";
                response.render(new JavaScriptContentHeaderItem(script, "", ""));
            }

            @Override
            protected void respond(final AjaxRequestTarget target) {
                comparisonEditorOn = !comparisonEditorOn; // toggle
            }
        };
        add(behave);
    }

    private void setComparisonEditorVisibility(WebMarkupContainer comparisonEditor, boolean comparisonEditorOn) {
        if (comparisonEditorOn) {
            comparisonEditor.add(AttributeModifier.replace("style", "display:block"));
        } else {
            comparisonEditor.add(AttributeModifier.append("style", "display:none"));
        }
    }
}
