package org.cdlflex.itpm.web.metamodeltools;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cdlflex.itpm.generated.mddconnector.MDDFunctions;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAO;
import org.cdlflex.mdd.sembase.semaccess.MDDBeanInterface;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides methods to modify/remove records and linked records via the available umlTUowl metamodel information.
 */
public class MetaModelDataUpdate {
    private static final Logger LOGGER = LoggerFactory.getLogger(MetaModelDataUpdate.class);
    private static final MetaModelBrowser M_BROWSER = new MetaModelBrowser();
    private static final MDDFunctions MDD_FUNCTIONS = new MDDFunctions();
    private static final ConnectionUtil CU = new ConnectionUtil();

    /**
     * Same as {@link #removeRecord(String, MetaClass, MDDBeanInterface)} but iterates over several records within the
     * same transaction.
     * 
     * @param modelName
     * @param metaClass
     * @param recordIds
     * @throws MDDException
     */
    public void removeRecords(String modelName, MetaClass metaClass, List<String> recordIds) throws MDDException {
        Connection con = CU.getOpenConnection(modelName);
        try {
            con.begin();
            for (String recordId : recordIds) {
                removeRecordInternal(con, modelName, metaClass, recordId);
            }
            con.commit();
        } catch (MDDException e) {
            LOGGER.error(e.getMessage());
            try {
                con.rollback();
            } catch (Exception e1) {
                LOGGER.warn(e1.getMessage());
            }
        }
    }

    public void removeRecord(String modelName, MetaClass metaClass, String recordId) throws MDDException {
        Connection con = CU.getOpenConnection(modelName);
        try {
            con.begin();
            removeRecordInternal(con, modelName, metaClass, recordId);
            con.commit();
        } catch (MDDException e) {
            LOGGER.error(e.getMessage());
            try {
                con.rollback();
            } catch (Exception e1) {
                LOGGER.warn(e1.getMessage());
            }
        }
    }

    private void removeRecordInternal(Connection con, String modelName, MetaClass metaClass, String recordId)
        throws MDDException {
        Object bean = CU.getFactory().create(metaClass.getName().replaceAll("\\s", ""), con).readRecord(recordId);
        if (bean == null) {
            throw new MDDException(String.format("No such %s with id=%s exists.", metaClass.getOriginalName(),
                    recordId));
        }
        removeAllNestedBeansCompositions(con, metaClass, (MDDBeanInterface) bean);
    }

    /**
     * Remove a record and remove all of its linked sub-entries (compositions) recursively if they are no
     * self-references.
     * 
     * @param modelName the name of the model, e.g. itpm or config
     * @param bean
     */
    public void removeRecord(String modelName, MetaClass metaClass, MDDBeanInterface bean) {
        Connection con = CU.getOpenConnection(modelName);
        try {
            con.begin();
            removeAllNestedBeansCompositions(con, metaClass, bean);
            con.commit();
        } catch (MDDException e) {
            LOGGER.error(e.getMessage());
            try {
                con.rollback();
            } catch (Exception e1) {
                LOGGER.warn(e1.getMessage());
            }
        } finally {

        }
    }

    /**
     * Iterate through all compositions (nested) and remove all linked objects. Compositions are ignored if they are
     * detected as to be self-references (i.e., they link to themselves).
     * 
     * @param con a connection which is already open and for which a transaction has already been begun. The connection
     *        must be commited or closed after the method-call again (manually!).
     * @param metaClass
     * @param bean
     * @return
     * @throws MDDException
     */
    private List<MDDBeanInterface> removeAllNestedBeansCompositions(Connection con, MetaClass metaClass,
        MDDBeanInterface bean) throws MDDException {
        List<MDDBeanInterface> list = new ArrayList<MDDBeanInterface>();
        Map<MetaAssociation, MetaClass> associations =
            M_BROWSER.getMDDSimpleAccess().getAllSuperAssociations(metaClass);
        Iterator<Map.Entry<MetaAssociation, MetaClass>> it = associations.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<MetaAssociation, MetaClass> entry = it.next();
            MetaAssociation a = entry.getKey();
            if (a.isComposition()) {
                LOGGER.debug(String.format("%s is a composition so the linked objects will be removed.", a.getName()));
                if (M_BROWSER.getMDDSimpleAccess().getAllSubClasses(a.getTo().getMetaClass()).contains(metaClass)) {
                    LOGGER.info(String
                            .format("Skipped removing %s because it is a self-reference. Remove the entries manually if necessary.",
                                    a.getName()));
                } else {
                    Set<?> linkedObjects = MDD_FUNCTIONS.getBeanAssociationDynamically(bean, a.getName());
                    for (Object l : linkedObjects) {
                        if (l != null) {
                            removeAllNestedBeansCompositions(con, entry.getValue(), (MDDBeanInterface) l);
                        }
                    }
                }
            }
        }
        DAO<Object> dao = CU.getFactory().create(metaClass.getName().replaceAll("\\s", ""), con);
        LOGGER.info(String.format("Remove %s of %s.", bean.toString(), metaClass.getName()));
        dao.remove(bean);
        return list;
    }
}
