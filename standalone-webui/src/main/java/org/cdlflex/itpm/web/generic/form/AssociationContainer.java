package org.cdlflex.itpm.web.generic.form;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.vaynberg.wicket.select2.Select2MultiChoice;

/**
 * Container which keeps for each association then number of possible choices and the actually selected choices
 * (interface to {@link Select2MultiChoice}).
 */
public class AssociationContainer implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<OptionEntry> selected = new ArrayList<OptionEntry>();
    private final List<OptionEntry> available;
    private GenericFormPanel optSubForm;
    private Select2MultiChoice<OptionEntry> select2;

    public AssociationContainer(List<OptionEntry> available) {
        this.available = available;
    }

    public void setSelected(List<OptionEntry> selected) {
        this.selected = selected;
    }

    public List<OptionEntry> getSelected() {
        return selected;
    }

    public List<OptionEntry> getAllChoiches() {
        return available;
    }

    public void setSubForm(GenericFormPanel subForm) {
        this.optSubForm = subForm;
    }

    public GenericFormPanel getSubForm() {
        return optSubForm;
    }

    public void setSelect2(Select2MultiChoice<OptionEntry> select2) {
        this.select2 = select2;
    }

    public Select2MultiChoice<OptionEntry> getSelect2() {
        return select2;
    }
}
