package org.cdlflex.itpm.web.synchronizer;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.PropertyModel;
import org.cdlflex.itpm.generated.configurationloader.ConfigurationLoader;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.mdd.sembase.MDDException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Button to reload the app (demo) data without flushing the Ontologies (flushing the ontologies may require a restart
 * of the Web app, at least for Jena TDB).
 */
public class ReloadButton extends AjaxLink<Void> {
    private static final long serialVersionUID = -5562712640504628574L;
    private static final Logger LOGGER = LoggerFactory.getLogger(ReloadButton.class);
    private SyncButton reloadButtonModel;
    private final String reloadText = "Reload Demo Data";

    public ReloadButton(String id) {
        super(id);
        reloadButtonModel = new SyncButton();
        reloadButtonModel.setButton(reloadText, "floppy-open");
        Label reloadButtonText =
            new Label("reload-data-label", new PropertyModel<String>(reloadButtonModel, "value"));
        reloadButtonText.setEscapeModelStrings(false);
        this.add(reloadButtonText);
    }

    @Override
    public void onClick(AjaxRequestTarget target) {
        final SyncButton reloadButtonModel = new SyncButton();
        final String reloadText = "Reload Data";
        reloadButtonModel.setButton(reloadText, "floppy-open");
        try {
            ConfigurationLoader loader =
                new ConfigurationLoader(new ConnectionUtil().getConfigLoader().isTestEnvironment(),
                        new ConnectionUtil().getOpenITPMConnection(), new ConnectionUtil().getOpenConfigConnection());
            loader.loadAppData(); // reloads only instances without flushing/reseting the ontology.
        } catch (MDDException e) {
            LOGGER.error(e.getMessage());
            error(e.getMessage());
        }
        reloadButtonModel.setButton(reloadText, "floppy-open");
        target.add(this);
    }
}
