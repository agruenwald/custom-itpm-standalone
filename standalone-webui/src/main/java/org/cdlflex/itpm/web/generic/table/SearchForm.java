package org.cdlflex.itpm.web.generic.table;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.util.value.ValueMap;

/** Search Form (right-hand side on top) for generic table page **/
public final class SearchForm extends Form<ValueMap> {
    private static final long serialVersionUID = 1L;
    private IDataContainer tablePanel;
    private String searchValue;

    public SearchForm(IDataContainer tablePanel, final String id) {
        super(id, new CompoundPropertyModel<ValueMap>(new ValueMap()));
        setMarkupId("searchForm");
        TextField<String> searchField = new TextField<String>("search");
        searchField.setType(String.class);
        add(searchField);
        this.tablePanel = tablePanel;
        searchValue = "";
    }

    public String getValue() {
        return searchValue;

    }

    public void updateTablePanel(IDataContainer tablePanel) {
        this.tablePanel = tablePanel;
    }

    @Override
    public final void onSubmit() {
        String modelName = this.tablePanel.getModelName();
        ValueMap values = getModelObject();
        searchValue = values.getString("search");
        searchValue = searchValue == null ? "" : searchValue;
        tablePanel.readGenericData(modelName, tablePanel.getMetaClassName(), searchValue);
    }
}
