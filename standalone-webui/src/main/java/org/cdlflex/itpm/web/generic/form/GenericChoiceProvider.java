package org.cdlflex.itpm.web.generic.form;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.vaynberg.wicket.select2.Response;
import com.vaynberg.wicket.select2.TextChoiceProvider;

/**
 * Generic choice provider for generic forms. A generic choice provider consists of value-text pairs in the same way as
 * classical HTML select menus.
 */
public class GenericChoiceProvider extends TextChoiceProvider<OptionEntry> {
    private static final long serialVersionUID = -173237330790490860L;
    private List<OptionEntry> entities;

    public GenericChoiceProvider(List<OptionEntry> entities) {
        this.entities = entities;
    }

    @Override
    public void query(String term, int page, Response<OptionEntry> response) {
        List<OptionEntry> matching = new ArrayList<OptionEntry>();
        term = term.trim();
        for (OptionEntry e : entities) {
            if (e.getValue().toLowerCase().contains(term.toLowerCase())) {
                if (!matching.contains(e)) {
                    matching.add(e);
                }
            }
        }
        response.addAll(matching);
        response.setHasMore(response.size() == 10);

    }

    @Override
    public Collection<OptionEntry> toChoices(Collection<String> ids) {
        Collection<OptionEntry> result = new ArrayList<OptionEntry>();
        for (String id : ids) {
            for (OptionEntry e : entities) {
                if (e.getId().equals(id)) {
                    result.add(e);
                    break;
                }
            }
        }
        return result;
    }

    @Override
    protected String getDisplayText(OptionEntry choice) {
        return choice.getValue();
    }

    @Override
    protected Object getId(OptionEntry choice) {
        return choice.getId();
    }
}
