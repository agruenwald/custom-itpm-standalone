package org.cdlflex.itpm.web.generic.chart;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import org.cdlflex.itpm.generated.model.PMRule;

/**
 * Wrapps the data for a Key Performance Indicator unit which can be displayed on the dashboard.
 */
public class KpiModel implements AbstractDashboardComponent, Serializable {
    private static final long serialVersionUID = 1L;
    private static final DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
    private static final DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();

    private final String title;
    private final String value;
    private String statusCssClass;
    private String icon;
    private PMRule rule;

    public KpiModel(String title, String value, PMRule rule) {
        this.title = title;
        this.value = value;
        this.rule = rule;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String plot() {
        try {
            long d = Long.parseLong(value);
            symbols.setGroupingSeparator(' ');
            return formatter.format(d);
        } catch (NumberFormatException e) {
            return value;
        }
    }

    @Override
    public PMRule getRule() {
        return rule;
    }

    public String getStatusCssClass() {
        return statusCssClass;
    }

    public void setStatusCssClass(String statusCssClass) {
        this.statusCssClass = statusCssClass;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
