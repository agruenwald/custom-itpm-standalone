package org.cdlflex.itpm.web.metamodeltools;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.WordUtils;
import org.cdlflex.itpm.generated.mddconnector.MDDFunctions;
import org.cdlflex.itpm.generated.mddconnector.MDDSimpleAccess;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.itpm.web.utils.IconUtil;
import org.cdlflex.mdd.modelrepo.AdvancedModelStoreImpl;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.MDDBeanInterface;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAttribute;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Interface for the (optimized) output of basic data based on the generically generated classes and the underlying
 * meta-models.
 */
public class MetaModelBrowser implements Serializable {
    private static final long serialVersionUID = 1061273805877516525L;
    private static final boolean SHOW_ASSOCIATIONS = true;
    private static final Logger LOGGER = LoggerFactory.getLogger(MetaModelBrowser.class);
    private static final MDDFunctions MDD_FUNCTIONS = new MDDFunctions();
    private static final MDDSimpleAccess MDD_SIMPLE_ACCESS = new MDDSimpleAccess(SHOW_ASSOCIATIONS);
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private List<String> titles;
    private static final String ORIGIN = "Origin";
    private static final List<String> MDD_EXTRA = Arrays.asList(new String[] { ORIGIN }); // "Created at",
                                                                                          // "last Change",
                                                                                          // "versionInfo", "origin"});
    private static Map<String, MetaModel> cachedMetaModels = new HashMap<String, MetaModel>();

    /**
     * Get a meta model based on its modelname (cached).
     * 
     * @param modelName
     * @return
     * @throws MDDException
     */
    public MetaModel getMetaModelClasses(String modelName) throws MDDException {
        titles = new ArrayList<String>();
        if (cachedMetaModels.containsKey(modelName)) {
            return cachedMetaModels.get(modelName);
        } else {
            AdvancedModelStoreImpl modelStoreImpl =
                new AdvancedModelStoreImpl(new ConnectionUtil().getConfigLoader());
            MetaModel mm = modelStoreImpl.findMetaModel(modelName);
            try {
                mm = modelStoreImpl.retrieveMetaModelMerged(mm); // merge classes with same name etc. - important!
            } catch (NullPointerException e) {
                throw new MDDException(String.format("Unknown model name %s.", modelName));
            }
            cachedMetaModels.put(modelName, mm);
            return mm;
        }
    }

    /**
     * Convert the result retrieved via the MDD interfaces into table format.
     * 
     * @param modelName itpm, config - must not be empty.
     * @param metaClassName the class name looking for
     * @param fullTextSearch empty or some value
     * @return a list of rows and maps.
     */
    public List<Map<String, String>> getTableData(String modelName, String metaClassName, String fullTextSearch) {
        MetaClass metaClass;
        try {
            metaClass = new MetaModelBrowser().getMetaModelClasses(modelName).findClassByName(metaClassName);
            if (metaClass == null) {
                LOGGER.error("Could not find metaClass {}.", metaClassName);
            } else {
                return this.getTableData(modelName, metaClass, fullTextSearch);

            }
        } catch (MDDException e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * Convert the result retrieved via the MDD interfaces into table format.
     * 
     * @param c
     * @param fullTextSearch
     * @return
     */
    private List<Map<String, String>> getTableData(String modelName, MetaClass c, String fullTextSearch) {
        List<Map<String, String>> list = new ArrayList<Map<String, String>>();

        try {
            List<Object> results =
                new ConnectionUtil().getFactory()
                        .create(c.getName().replaceAll(" ", ""), new ConnectionUtil().getOpenConnection(modelName))
                        .readFulltext(fullTextSearch);
            for (int i = 0; i < results.size(); i++) {
                MDDBeanInterface r = (MDDBeanInterface) results.get(i);
                Map<String, String> map = new LinkedHashMap<String, String>();
                for (MetaAttribute a : MDD_SIMPLE_ACCESS.getAllSuperAttributes(c)) {
                    String value = MDD_FUNCTIONS.getBeanAttributeDynamically(r, a.getOriginalName());
                    map.put(WordUtils.capitalize(a.getOriginalName()), value);
                }

                Iterator<Entry<MetaAssociation, MetaClass>> iterator =
                    MDD_SIMPLE_ACCESS.getAllSuperAssociations(c).entrySet().iterator();
                while (iterator.hasNext()) {
                    Entry<MetaAssociation, MetaClass> associationEntry = iterator.next();
                    Set<Object> linkedObjects = null;
                    try {
                        linkedObjects =
                            MDD_FUNCTIONS.getBeanAssociationDynamically(r, associationEntry.getKey().getName());
                        // two cases: either set is empty (sets) or first element is null (associations with cardinality
                        // one)
                        if (linkedObjects.size() == 0
                            || (linkedObjects.size() == 1 && linkedObjects.iterator().next() == null)) {
                            map.put(breaker(associationEntry.getKey().getName()), "");
                        } else if (associationEntry.getKey().getTo().getMetaClass().isEnumClass()) {
                            map.put(breaker(associationEntry.getKey().getName()), linkedObjects.iterator().next()
                                    .getClass().getSimpleName());
                        } else {
                            map.put(breaker(associationEntry.getKey().getName()),
                                    String.format("(%d)", linkedObjects.size(), linkedObjects.toString()));
                        }
                    } catch (MDDException e) {
                        LOGGER.error(e.getMessage());
                    }

                }

                String recordInfoData =
                    String.format(
                            bold("Created") + " %s</br> " + bold("Last Change") + " %s</br> "
                                + bold("Record version") + " %s</br>" + bold("Origin") + " %s",
                            SIMPLE_DATE_FORMAT.format(r.getCreatedAt()),
                            SIMPLE_DATE_FORMAT.format(r.getLastChange()),
                            String.valueOf(r.getVersionInfo()),
                            r.getOriginLink().isEmpty() ? r.getOrigin() : String.format(
                                    " <a href=\"%s\" target=\"_blank\">%s</a>", r.getOriginLink(), r.getOrigin()));

                String icon = IconUtil.createBSPopover("Record information", "recordInfo", "", true, recordInfoData);
                map.put(ORIGIN, icon);
                list.add(map);
            }
        } catch (MDDException e) {
            LOGGER.error(e.getMessage());
        }
        titles = filterTitles(c, list);
        return list;
    }

    private String bold(String string) {
        return String.format("<span style=\"font-weight:bold\">%s:</span>", string);
    }

    /**
     * remove all columns that are entirely empty in all records.
     * 
     * @param fullData
     * @return
     */
    private List<String> filterTitles(MetaClass c, List<Map<String, String>> fullData) {
        List<String> titles = extractTitles(c);
        List<String> filteredTitles = new ArrayList<String>();
        for (String title : titles) {
            boolean someContent = false;
            for (Map<String, String> record : fullData) {
                String content = record.get(title);
                if (!content.isEmpty()) {
                    someContent = true;
                    break;
                }
            }

            if (someContent) {
                filteredTitles.add(title);
            }
        }
        return filteredTitles;
    }

    private List<String> extractTitles(MetaClass c) {
        List<String> titles = new ArrayList<String>();
        for (MetaAttribute a : MDD_SIMPLE_ACCESS.getAllSuperAttributes(c)) {
            titles.add(WordUtils.capitalize(a.getOriginalName()));
        }
        for (MetaAssociation a : MDD_SIMPLE_ACCESS.getAllSuperAssociations(c).keySet()) {
            titles.add(breaker(a.getName()));
        }
        titles.addAll(MDD_EXTRA);
        return titles;
    }

    /**
     * Converts "relatesToComponent" into "Relates To Component"
     * 
     * @param name any metaattribute or metaclass name or label
     * @return a human-friendly variant of the label
     */
    public static String labelFormatter(String name) {
        name = breaker(name);
        name = WordUtils.capitalize(name);
        return name;
    }

    /**
     * Converts "sayHello" into "say Hello". Is used to reduce column width in html tables and to make text more
     * human-readable.
     * 
     * @param str
     * @return
     */
    private static String breaker(String str) {
        String[] r = str.split("(?=\\p{Lu}\\p{Ll})");
        if (r.length <= 0) {
            return str;
        }
        String sNew = "";
        for (String p : r) {
            sNew = sNew.isEmpty() ? WordUtils.capitalize(p) : sNew + " " + p;
        }
        return sNew;
    }

    public List<String> getTitles(MetaClass c) {
        return titles == null ? this.extractTitles(c) : titles;
    }

    /**
     * Clear the cache after updates of the metamodel have been performed (e.g. in the Semantic Web Editor).
     */
    public static void invalidateCache() {
        cachedMetaModels.clear();
    }

    public MDDSimpleAccess getMDDSimpleAccess() {
        return MDD_SIMPLE_ACCESS;
    }
}
