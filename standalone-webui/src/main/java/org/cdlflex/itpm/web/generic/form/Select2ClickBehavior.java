package org.cdlflex.itpm.web.generic.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AbstractDefaultAjaxBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.request.IRequestParameters;
import org.apache.wicket.request.cycle.RequestCycle;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaynberg.wicket.select2.ApplicationSettings;
import com.vaynberg.wicket.select2.JQuery;

/**
 * Handles clicks on selected items (Select2).
 */
public class Select2ClickBehavior extends AbstractDefaultAjaxBehavior {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(Select2ClickBehavior.class);
    private final MetaAssociation metaAssociation;
    private OptionEntry optionEntry;

    /**
     * The constructor is used to pass references for reconstructing the selected objects based on the identified
     * (selected) id.
     * 
     * @param metaAssociation
     */
    public Select2ClickBehavior(MetaAssociation metaAssociation) {
        this.metaAssociation = metaAssociation;
        this.optionEntry = null;
    }

    @Override
    public void renderHead(Component component, IHeaderResponse response) {
        final ApplicationSettings settings = ApplicationSettings.get();
        if (settings.isIncludeJqueryUI()) {
            response.render(JavaScriptHeaderItem.forReference(settings.getJqueryUIReference()));
        }

        String script =
            "$('#%1$s').select2('container').find('ul')"
                + ".on('click', function(e) { \n"
                + "console.log('Clicked on ' + $(this));"
                + "var items=$(this).closest('.controls').find('input:hidden').val(); \n"
                + "var htmlItems=$(this).find('.select2-search-choice'); \n"
                + "var pos = 0; var i=0; \n"
                + "htmlItems.each(function(){ "
                + "if ($(this).hasClass('select2-search-choice-focus')) { \n"
                + "pos = i; \n"
                + "} \n"
                + "i++; \n"
                + "}); \n"
                + "var wicketBackendURL = '"
                + this.getCallbackUrl()
                + "';"
                + "Wicket.Ajax.get({ u: wicketBackendURL + '&action=clickedOnItem&itemsSeparatedByComma='+items + '&selectedItems='+pos});"
                + "});";

        script += "\n\n";
        script +=
            "$('body').on('focus', function(e) { \n" + "   if($('.select2-search-choice-focus').length > 0) {\n"
                + "   	console.log('focous OUT SERACH CHOICE!!!!'); \n" + "	} \n" + "   var wicketBackendURL = '"
                + this.getCallbackUrl() + "';" + "   Wicket.Ajax.get({ u: wicketBackendURL + '&action=blur'}); \n"
                + "});";
        /*
         * script += "$('#%1$s').select2('container')" + //"$(':not(\".genericform-edit-concept\")')" +
         * ".on('focusout', function(e) { \n" + "   console.log('target:', e.target);\n" +
         * "   console.log('delay over!!!'); \n" + "   var wicketBackendURL = '" + this.getCallbackUrl() + "';" +
         * "   Wicket.Ajax.get({ u: wicketBackendURL + '&action=blur'}); \n" + "});";
         */
        response.render(OnDomReadyHeaderItem.forScript(JQuery.execute(script, component.getMarkupId())));
    }

    @Override
    protected void respond(final AjaxRequestTarget target) {
        IRequestParameters params = RequestCycle.get().getRequest().getRequestParameters();
        if (params.getParameterValue("action").toString("").equals("clickedOnItem")) {
            LOGGER.info("ClickedOnItem called");
            String concatenatedItems = params.getParameterValue("itemsSeparatedByComma").toString("");
            int selectedItemIndex = Integer.parseInt(params.getParameterValue("selectedItems").toString("-1"));
            String[] itemIds = concatenatedItems.split(",");
            List<String> optionIds = new ArrayList<String>();
            if (concatenatedItems.trim().length() > 0) {
                for (String itemId : itemIds) {
                    optionIds.add(itemId.trim());
                }
                selectedItemsOnClick(target, optionIds);
            }
            if (optionIds.size() > 0 && selectedItemIndex > -1) {
                this.selectedItemsClick(target, metaAssociation, optionIds.get(selectedItemIndex));
            } else {
                this.blur(target);
            }
        } else if (params.getParameterValue("action").toString("").equals("blur")) {
            LOGGER.info("Blur called");
            this.blur(target);
        } else {
            LOGGER.error(String.format("Unknown AJAX request."));
        }
    }

    /**
     * Override this method to catch the blur event.
     * 
     * @param target
     */
    public void blur(AjaxRequestTarget target) {

    }

    /**
     * Override this method to get the currently focused item.
     * 
     * @param target
     * @param metaAssociation the meta association for which the selection has been taken.
     * @param selectedItemid the item the user focused. The method is called on click.
     */
    public void selectedItemsClick(AjaxRequestTarget target, MetaAssociation metaAssociation, String selectedItemId) {

    }

    /**
     * Override this method to get the selected item ids whenever the user clicks on a selected item
     * 
     * @param optionIds the selected ids.
     */
    public void selectedItemsOnClick(AjaxRequestTarget target, List<String> optionIds) {

    }

    public MetaAssociation getMetaAssociation() {
        return metaAssociation;
    }

    public OptionEntry getOptionEntry() {
        return optionEntry;
    }

    public void setOptionEntry(OptionEntry oE) {
        this.optionEntry = oE;
    }
}
