package org.cdlflex.itpm.web.standalone;

import java.util.List;

import org.apache.wicket.Application;
import org.apache.wicket.markup.html.IPackageResourceGuard;
import org.apache.wicket.markup.html.SecurePackageResourceGuard;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.JavaScriptResourceReference;
import org.apache.wicket.settings.IExceptionSettings;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.apache.wicket.util.time.Duration;
import org.cdlflex.itpm.web.store.TriggerLoadConfigData;
import org.cdlflex.itpm.web.utils.BaseConfigUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.vaynberg.wicket.select2.ApplicationSettings;

/**
 * Application object for your web application. If you want to run this application without deploying, run the
 * Start.java module in the test directory.
 * 
 */
public class WicketApplication extends WebApplication {
    private static final Logger logger = LoggerFactory.getLogger(WicketApplication.class);

    private static final AhyBrandProviderMockup AHY_BRAND_MOCKUP = new AhyBrandProviderMockup();
    protected static List<CssResourceReference> CSS_BUNDLES;
    protected static List<JavaScriptResourceReference> JS_BUNDLES;
    private static boolean initDone = false;

    /**
     * @see org.apache.wicket.Application#getHomePage()
     */
    @Override
    public Class<HomePage> getHomePage() {
        return HomePage.class;
    }

    /**
     * @see org.apache.wicket.Application#init()
     */
    @Override
    public void init() {
        super.init();
        new BaseConfigUtil().setDefaultBaseConfigPath();
        getMarkupSettings().setDefaultBeforeDisabledLink("");
        getMarkupSettings().setDefaultAfterDisabledLink("");
        Application.get().getResourceSettings().setDefaultCacheDuration(Duration.NONE); // for OFC diagrams
        getApplicationSettings().setPageExpiredErrorPage(HomePage.class);

        ApplicationContext applicationContext =
            WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
        getComponentInstantiationListeners().add(new SpringComponentInjector(this, applicationContext));

        ApplicationSettings.get().setIncludeJqueryUI(false);
        ApplicationSettings.get().setIncludeJavascript(false);
        IPackageResourceGuard packageResourceGuard = getResourceSettings().getPackageResourceGuard();
        if (packageResourceGuard instanceof SecurePackageResourceGuard) {
            SecurePackageResourceGuard guard = (SecurePackageResourceGuard) packageResourceGuard;
            guard.addPattern("+*.ttf");
            guard.addPattern("+*.eot");
            guard.addPattern("+*.svg");
            guard.addPattern("+*.png");
            guard.addPattern("+*.woff");
            guard.addPattern("+*.gif");
        }

        CSS_BUNDLES = AHY_BRAND_MOCKUP.getCssReferences();
        JS_BUNDLES = AHY_BRAND_MOCKUP.getJavaScriptReferences();
        for (CssResourceReference bundle : CSS_BUNDLES) {
            getResourceBundles().addCssBundle(BasePage.class, bundle.getName());
        }
        for (JavaScriptResourceReference bundle : JS_BUNDLES) {
            getResourceBundles().addJavaScriptBundle(BasePage.class, bundle.getName());
        }
        getApplicationSettings().setInternalErrorPage(ErrorHandlingPage.class);
        getExceptionSettings().setUnexpectedExceptionDisplay(IExceptionSettings.SHOW_INTERNAL_ERROR_PAGE);
        /*
         * WebApplicationContextUtils .getRequiredWebApplicationContext(this.getServletContext())
         * .getAutowireCapableBeanFactory() .autowireBean(this);
         */

        // only reset if not possible otherwise (fallback -first installation)
        if (initDone) {
            logger.warn("Skipped initialization a second time.");
            return; // for spring
        }

        // Very important that only executed once. A second time would result in an invalid
        // data model!!! (because loading is terminated during the process)
        // The method is triggered twice because of Spring.
        try {
            TriggerLoadConfigData.loadAppDataFlushOntologisIfEmpty();
            logger.info("Welcome! Server started up.");
            initDone = true;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
}
