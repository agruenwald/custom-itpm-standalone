package org.cdlflex.itpm.web.standalone;

import org.apache.wicket.markup.html.panel.Panel;

/**
 * This class provides the navigation structure for the standalone version. It is not used if integrated into the
 * CDLFlex Bus environment because the navigation menu is created based on the available services in that case.
 * 
 */
public class NavMenu extends Panel {
    private static final long serialVersionUID = 1L;

    public NavMenu(final String componentName) {
        super(componentName);
    }
}
