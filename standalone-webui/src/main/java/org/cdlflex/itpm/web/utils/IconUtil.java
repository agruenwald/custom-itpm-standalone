package org.cdlflex.itpm.web.utils;

/**
 * Creates Bootstrap HTML markup for the representation of (clickable) icons. Offers a methodic way to create icons and
 * render them based on a specific state, such as: "An icon is on", "An icon is off",
 * "An icon shows that the result is good/bad.", ...
 */
public final class IconUtil {
    public static final String BS_ICON_PLACEHOLDER =
        "<span class=\"%s\" %s><i title=\"%s\" class=\"glyphicon glyphicon-%s\" %s style=\"color:%s\"></i></span>";
    private static final String BS_ICON_PLACEHOLDER_CLICKABLE =
        "<span class=\"%s clickable-icon\"  %s><i title=\"%s\" " 
      + "class=\"glyphicon glyphicon-%s %s\" style=\"color:%s\"></i></span>";
    public static final String ID_PLACEHOLDER = "<span class=\"%s\" style=\"display:none\">%s</span>";
    private static final String CSS_GOOD = "icon-status-good";
    private static final String CSS_BAD = "icon-status-bad";
    private static final String CSS_ON = "icon-status-on";
    private static final String CSS_OFF = "icon-status-off";

    private IconUtil() {
        
    }
    
    /**
     * Create a bootstrap icon.
     * @param title the title.
     * @param className the class name.
     * @param glyphicon the icon (glyhpicon, compare {@link http://getbootstrap.com/}.
     * @param isClickable if true then the icon is clickable (link).
     * @return a HTML bootstrap icon.
     */
    public static String createBSIcon(String title, String className, String glyphicon, boolean isClickable) {
        return createBSIcon(title, className, glyphicon, false, false, false, false, isClickable);
    }

    public static String createBSIcon(String title, String className, String glyphicon, boolean isClickable, String id) {
        return createBSIcon(title, className, glyphicon, false, false, false, false, isClickable, id);
    }

    public static String createBSIcon(String title, String className, String glyphicon, boolean isClickable,
        String id, String color) {
        return createBSIcon(title, className, glyphicon, false, false, false, false, isClickable, id, color);
    }

    public static String createBSIconGoodBad(String title, String className, String glyphicon, boolean isGood,
        boolean isClickable) {
        return createBSIcon(title, className, glyphicon, isGood, !isGood, false, false, isClickable);
    }

    public static String createBSIconOnOff(String title, String className, String glyphicon, boolean isOn,
        boolean isClickable) {
        return createBSIcon(title, className, glyphicon, false, false, isOn, !isOn, isClickable);
    }

    public static String addHiddenId(String className, String id) {
        return String.format(ID_PLACEHOLDER, className, id);
    }

    private static String createBSIcon(String title, String className, String glyphicon, boolean isGood,
        boolean isBad, boolean isOn, boolean isOff, boolean isClickable) {
        return createBSIcon(title, className, glyphicon, isGood, isBad, isOn, isOff, isClickable, "");
    }

    private static String createBSIcon(String title, String className, String glyphicon, boolean isGood,
        boolean isBad, boolean isOn, boolean isOff, boolean isClickable, String elemId) {
        return createBSIcon(title, className, glyphicon, isGood, isBad, isOn, isOff, isClickable, elemId, "");
    }

    private static String createBSIcon(String title, String className, String glyphicon, boolean isGood,
        boolean isBad, boolean isOn, boolean isOff, boolean isClickable, String elemId, String color) {
        String iconStatus = "";
        if (isGood) {
            iconStatus = CSS_GOOD;
        } else if (isBad) {
            iconStatus = CSS_BAD;
        } else if (isOn) {
            iconStatus = CSS_ON;
        } else if (isOff) {
            iconStatus = CSS_OFF;
        }
        String icon = "";
        String idAttr = elemId.isEmpty() ? "" : String.format(" id=\"%s\" ", elemId);
        if (isClickable) {
            icon =
                String.format(BS_ICON_PLACEHOLDER_CLICKABLE, className, idAttr, title, glyphicon, iconStatus, color);
        } else {
            icon = String.format(BS_ICON_PLACEHOLDER, className, idAttr, title, glyphicon, iconStatus, color);
        }
        return icon;
    }

    /**
     * Create an icon with popover functionality. Force to create the required JavaScript next to the element. This will
     * not work for Ajax reloads though becaues the JS will not be executed.
     * 
     * @param title
     * @param className
     * @param glyphicon
     * @param isClickable
     * @param content
     * @return
     */
    public static String createBSPopoverForceJs(String title, String className, String glyphicon,
        boolean isClickable, String content) {
        String id = org.cdlflex.mdd.sembase.Util.generateId("", "").trim();
        String icon = IconUtil.createBSIcon(title, "recordInfo", "tag", true, id);
        icon +=
            String.format("<script>$('#%s').popover({title: '%s', content:'%s', html:true});</script>", id, title,
                    content);
        return icon;
    }

    /**
     * Same as {@link #createBSPopoverForceJs(String, String, String, boolean, String)} but the JavaScript required will
     * not be created at the element. Instead the JS will be handled via agruenwald.js. All elements with the CSS class
     * "bs-popover" will be threaded as popover elements.
     * 
     * @param title
     * @param className
     * @param glyphicon
     * @param isClickable
     * @param content
     * @return
     */
    public static String createBSPopover(String title, String className, String glyphicon, boolean isClickable,
        String content) {
        return createBSPopover(title, className, glyphicon, isClickable, content, "");
    }

    public static String createBSPopover(String title, String className, String glyphicon, boolean isClickable,
        String content, String color) {
        String id = org.cdlflex.mdd.sembase.Util.generateId("", "").trim();
        String icon =
            IconUtil.createBSIcon(title, "recordInfo bs-popover", glyphicon.isEmpty() ? "tag" : glyphicon, true, id,
                    color);
        String popoverContent =
            String.format("<span id=\"%s\" class=\"bs-popover-content\" style=\"display:none\">"
                + "<span>%s</span><span>%s</span></span>", id + "-content", title, content);
        // icon += String.format("<script>$('#%s').popover({title: '%s', content:'%s', html:true});</script>", id,
        // title, content);
        icon += popoverContent;
        return icon;
    }
}
