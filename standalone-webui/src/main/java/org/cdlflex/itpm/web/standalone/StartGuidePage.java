package org.cdlflex.itpm.web.standalone;

import org.cdlflex.mdd.sembase.MDDException;

/**
 * The page provides an entry point to the demo and is a mix of marketing and introduction.
 */
public class StartGuidePage extends BasePage {
    private static final long serialVersionUID = 1L;

    public StartGuidePage() throws MDDException {
        super();
    }
}
