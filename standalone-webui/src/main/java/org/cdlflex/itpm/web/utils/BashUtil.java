package org.cdlflex.itpm.web.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Some scripts require execution from command line, such as the code generation. Those scripts can be called via the
 * Bash/Unix interface and are configured in $SEMBASE_HOME/sembase/bin.
 */
public class BashUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(BashUtil.class);
    private static final String CODEGEN_REFRESH_SCRIPT = "/bin/codegen";
    public static final String FLUSH_DATASTORE = "/bin/flush-data";

    /**
     * simple access to execute commands on the command line (mainly unix).
     * @param command the command, or a series of commands, e.g. mvn clean install; or ls -lta;
     * @return a String which equals the command line output
     * @throws IOException is thrown if access fails.
     * @throws InterruptedException is thrown if command takes too long or is interrupted.
     */
    public static String bashOutput(String command) throws IOException, InterruptedException {
        String[] cmds = { "/bin/sh", "-c", command };
        Process p = Runtime.getRuntime().exec(cmds);
        BufferedReader read = new BufferedReader(new InputStreamReader(p.getInputStream()));
        p.waitFor();
        String res = "";
        while (read.ready()) {
            res += (read.readLine());
        }
        return res;
    }

    /**
     * Execute a specific available unix script including the passing of parameters.
     * 
     * @param scriptName the name of the script which is located in $SEMBASE_HOME;
     * @return a String which equals the command line output
     * @throws IOException is thrown if access fails.
     * @throws InterruptedException is thrown if command takes too long or is interrupted.
     * @throws MDDException is thrown if execution of script failed (e.g., because of model
     * processing logic). 
     */
    public static String execScript(ConfigLoader configLoader, String scriptName, String... parameters)
        throws IOException, InterruptedException, MDDException {
        String skript = configLoader.getHomeDir() + scriptName; // either in karaf/bin or in the home-dir of the user
        for (String parameter : parameters) {
            skript += " " + parameter;
        }
        LOGGER.info(String.format("Call OS script %s.", skript));
        // String[] cmds = {skript};
        Process p = Runtime.getRuntime().exec(skript, parameters);
        BufferedReader read = new BufferedReader(new InputStreamReader(p.getInputStream()));
        p.waitFor();
        String res = "";
        while (read.ready()) {
            res += (read.readLine());
        }
        return res;
    }

    /**
     * Trigger the shell-script to rebuild relevant sources after the code generation has been performed.
     * 
     * @param configLoader the configuration loader.
     * @return the message (typically a maven build result output string)
     * @throws IOException is thrown if access fails.
     * @throws InterruptedException is thrown if command takes too long or is interrupted.
     * @throws MDDException is thrown if execution of script failed (e.g., because of model
     */
    public static String execCodeGenScript(ConfigLoader configLoader) throws IOException, InterruptedException,
        MDDException {
        return bashOutput(configLoader.getHomeDir() + CODEGEN_REFRESH_SCRIPT);
    }
}
