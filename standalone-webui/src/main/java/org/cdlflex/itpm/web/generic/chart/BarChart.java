package org.cdlflex.itpm.web.generic.chart;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.cdlflex.itpm.generated.model.PMRule;
import org.cdlflex.itpm.web.utils.Util;

/**
 * Default implementation to render very simple bar charts based on OpenFlash Charts.
 */
public class BarChart implements AbstractDashboardComponent, Serializable {
    private static final long serialVersionUID = 1L;
    protected static final int ALPHA_BARS = 100;
    private List<String> x;
    private List<Double> y;
    private String title;
    private final PMRule rule;
    private boolean showTitle;

    private static final String[] COLOR_SCHEME_1DIM = new String[] {"#0075B0", // blue
    // "#5cb85c" //green
    };

    public static final String COLOR_SCHEME[] = new String[]{
        "#0075B0", // blue
        "#5cb85c", // green
        /* soft for green */
        "#CA343A", // "#cc6666", //red
        "#cc9966", // orange
        "#99cc66", // light green
        "#666699", // purple
    };

    public BarChart(String title, PMRule rule, boolean showTitle) {
        this.title = title;
        this.x = new LinkedList<String>();
        this.y = new LinkedList<Double>();
        this.rule = rule;
        this.showTitle = showTitle;
    }

    public List<String> getX() {
        return x;
    }

    public void setX(List<String> x) {
        this.x = x;
    }

    public List<Double> getY() {
        return y;
    }

    public void setY(List<Double> y) {
        this.y = y;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String plot() {
        double min = Collections.min(this.getY()) + 1;
        double max = Collections.max(this.getY()) + 1;

        int minRange = max != 0 ? (int) Math.ceil(max / 25.0) : 0;
        int maxRange = max != 0 ? (int) Math.ceil(max / 25.0) : 0;
        min = Math.max(0, min - minRange);
        if (max <= 100) {
            min = 0.0;
        }
        max = max + maxRange;
        int rand = (int) (Math.random() * ((COLOR_SCHEME_1DIM.length)));
        String barColor = COLOR_SCHEME_1DIM[rand];
        String res =
            getChartMarkup(getTitle(), min, max)
                + String.format(
                        String.format("&bar=%s", ALPHA_BARS) + ",%s,%s,10&" + "&values=%s&" + "&x_labels=%s&",
                        barColor, "",// label for bars (e.g. "hours per week")
                        Util.asString(this.y, ','), Util.asString(this.x, ','), min, max);
        return res;
    }

    protected String getChartMarkup(String title, double yMin, double yMax) {
        // in case that there is not title some spaces are added to avoid that the most upper scale lavel is not cut.
        String contensed =
            String.format(showTitle ? String.format("&title=%s,{font-size: 20px; color: #333333}&", title) : String
                    .format("&title=%s,{font-size: 5px; color: #333333}&", "  ")
                + "&x_label_style=15,#666666,2&"
                + "&x_axis_steps=1&"
                + "&y_legend=,15,#666666&"
                + "&y_label_style=15,#666666&"
                + "&bg_colour=#ffffff&"
                + "&x_axis_colour=#808080&"
                + "&x_grid_colour=#808080&"
                + "&y_axis_colour=#808080&"
                + "&y_grid_colour=#808080&"
                + "&y_ticks=5,10,2&"
                + String.format("&y_min=%s&", yMin) + String.format("&y_max=%s&", yMax));
        return contensed;
    }

    @Override
    public PMRule getRule() {
        return rule;
    }
}
