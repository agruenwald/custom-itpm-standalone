package org.cdlflex.itpm.web.queryEditor;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.cdlflex.itpm.generated.model.PMRule;

/**
 * The central form which is used to edit queries. A form is shown within the nested structure of knowledge areas,
 * including a query editor with syntax highlighting, autocompletion and a preview table.
 */
public class QueryEditorForm extends QueryEditorStandalonePanel {
    private static final long serialVersionUID = 1L;
    private QueryEditorPage rulePageRef;

    /**
     * Form for updating a rule.
     * 
     * @param id
     * @param knowledgeAreaName
     * @param feedbackPanel
     * @param rule
     * @param ruleService
     * @param rulePageRef
     */
    public QueryEditorForm(String id, String knowledgeAreaName, FeedbackPanel feedbackPanel, PMRule rule,
            RuleService ruleService, QueryEditorPage rulePageRef) {
        super(id, feedbackPanel, rule, ruleService);
        this.rulePageRef = rulePageRef;
    }

    /**
     * Creation of a new rule/query.
     * 
     * @param id
     * @param knowledgeAreaName
     * @param feedbackPanel
     * @param ruleService
     * @param rulePageRef
     */
    public QueryEditorForm(String id, String knowledgeAreaName, FeedbackPanel feedbackPanel, RuleService ruleService,
            QueryEditorPage rulePageRef) {
        super(id, knowledgeAreaName, feedbackPanel, ruleService);
        this.rulePageRef = rulePageRef;
    }

    public void handleAfterDbUpdate(boolean isNew, AjaxRequestTarget target) {
        if (isNew) {
            target.add(QueryEditorForm.this.rulePageRef);
        } else {
            target.add(QueryEditorForm.this.rulePageRef.getTabsContainer());
        }
        target.add(QueryEditorForm.this.rulePageRef.getInfoFeedback());
    }
}
