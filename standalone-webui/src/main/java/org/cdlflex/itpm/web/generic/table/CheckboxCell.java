package org.cdlflex.itpm.web.generic.table;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

/**
 * Element which is added to generic tables to enable the deletion of multiple records via checkboxes.
 */
public class CheckboxCell extends Panel {
    private static final long serialVersionUID = 1L;

    /**
     * Create a new checkbox element which can be added to the generated table. The element holds information about the
     * record id so that it can be processed by JavaScript UX functions and {@link DeleteButtonPanel}.
     * 
     * @param id the id
     * @param metaClassName the meta class name which specifies the type of the record
     * @param recId the record id
     */
    public CheckboxCell(String id, String metaClassName, final String recId) {
        super(id);
        final AjaxCheckBox cb = new AjaxCheckBox("selCheckbox", Model.of(Boolean.TRUE)) {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                // placeholder for future implementation
            }

        };
        add(cb);
        HiddenField<String> idHf = new HiddenField<String>("deleteId", Model.of(recId));
        add(idHf);
        cb.setModelValue(new String[] { recId });
    }
}
