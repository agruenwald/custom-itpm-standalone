package org.cdlflex.itpm.web.generic.chart;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.cdlflex.itpm.web.generic.table.QueryTablePanel;
import org.cdlflex.itpm.web.queryEditor.QueryEditorIntegratedPanel;
import org.cdlflex.itpm.web.queryEditor.QueryEditorStandalonePanel;
import org.cdlflex.itpm.web.queryEditor.RuleService;
import org.cdlflex.itpm.web.reusable.ISubmitCallback;

/**
 * Panel for the visualization of (generic) tables in the Dashboard.
 */
public class DashboardTablePanel extends Panel {
    private static final long serialVersionUID = 1L;
    private static final int MAX_ROWS_PER_PAGE = 6;
    private final RuleService ruleService;
    private final FeedbackPanel notificationPanel;
    private AbstractDashboardComponent dashboardComponent;
    private boolean showAll;
    private static final String SHOW_LESS = "Show less";
    private static final String SHOW_ALL = "Show all";

    public DashboardTablePanel(String id, AbstractDashboardComponent dashboardComponent, RuleService ruleService,
            FeedbackPanel notificationPanel) {
        super(id);
        this.ruleService = ruleService;
        this.notificationPanel = notificationPanel;
        this.dashboardComponent = dashboardComponent;
        showAll = false;
        plot();
    }

    public String plot() {
        int maxSize = showAll ? 999 : MAX_ROWS_PER_PAGE;
        Label header = new Label("header", dashboardComponent.getTitle());
        add(header);
        final Form<?> form = new Form<String>("form");
        add(form);

        final QueryTablePanel queryTablePanel = new QueryTablePanel("table-container", null, maxSize);
        queryTablePanel.renderPanel(null, dashboardComponent.getRule());
        form.add(queryTablePanel);
        int records = queryTablePanel.getRecords();

        final Model<String> ajaxText = Model.of(SHOW_ALL);
        final AjaxLink<String> showAll = new AjaxLink<String>("show-all", ajaxText) {
            private boolean showAll = true;
            private static final long serialVersionUID = 1L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                if (showAll) {
                    this.setModelObject(SHOW_LESS);
                    ajaxText.setObject(SHOW_LESS);
                    queryTablePanel.setMaxSize(999);
                } else {
                    this.setModelObject(SHOW_ALL);
                    ajaxText.setObject(SHOW_ALL);
                    queryTablePanel.setMaxSize(MAX_ROWS_PER_PAGE);
                }
                queryTablePanel.refreshGenericData();
                showAll = !showAll;
                target.add(this);
                // target.add(queryTablePanel);
                target.add(queryTablePanel.getTable());
                // target.add(form);
            }
        };
        showAll.add(new Label("show-all-label", ajaxText));
        form.add(showAll);
        showAll.setVisible(records > MAX_ROWS_PER_PAGE);

        // integrated query editor.
        final QueryEditorStandalonePanel queryEditor =
            new QueryEditorStandalonePanel("query-editor", notificationPanel, dashboardComponent.getRule(),
                    ruleService);
        ISubmitCallback onEditBehavior = new ISubmitCallback() {
            private static final long serialVersionUID = 1L;

            @Override
            public void submit(boolean isNew, AjaxRequestTarget target) {
                showAll.setVisible(false);
                target.add(showAll);
            }
        };
        final QueryEditorIntegratedPanel integratedQueryEditor =
            new QueryEditorIntegratedPanel("query-editor-integrated", queryTablePanel, queryEditor, null,
                    onEditBehavior);
        form.add(integratedQueryEditor);
        return null;
    }
}
