package org.cdlflex.itpm.web.generic.table;

import java.util.List;
import java.util.Map;

/**
 * Abstract interface to exchange generic table data.
 * 
 */
public interface IDataContainer extends IDataRefreshContainer {
    /**
     * Get the data for a generic table.
     * 
     * @param the modelName e.g. itpm, config or empty if the rule-visualization mode is on
     * @param the metaClassName (e.g. DataSourceConfig) or empty if the rule-visualization mode is on
     * @param fullText the text to search for.
     * @return a list of records >= 0.
     */
    public List<Map<String, String>> readGenericData(String modelName, String metaClassName, String fulltext);

    public String getModelName();

    /**
     * get the name of the current meta class if available (during the rule-visualizatino mode not available).
     * 
     * @return
     */
    public String getMetaClassName();

    /**
     * If implemented shows the overview perspective for table/form views.
     */
    public void showOverview();
}
