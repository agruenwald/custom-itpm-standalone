package org.cdlflex.itpm.web.synchronizer;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.PropertyModel;
import org.cdlflex.mdd.sembase.MDDException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A model for buttons with an icon (bootstrap) and two states (e.g. synch on/off, etc.) to toggle between the detailed
 * and the standard view in the synchronization table.
 */
public class ToggleMsgButton extends AjaxLink<Void> {
    private static final long serialVersionUID = 6202145422209204779L;
    private static final Logger LOGGER = LoggerFactory.getLogger(ToggleMsgButton.class);
    private final SynchronizerPage page;
    private SyncButton model;
    private static final String SHOW_DETAILS = "Show Details";
    private static final String HIDE_DETAILS = "Hide Details";

    public ToggleMsgButton(String id, SynchronizerPage syncPage) {
        super(id);
        model = new SyncButton();
        this.page = syncPage;
        model.setButton(getStatusBasedText(), getStatusBasedIcon());
        Label reloadButtonText = new Label("toggle-msg-label", new PropertyModel<String>(model, "value"));
        reloadButtonText.setEscapeModelStrings(false);
        this.add(reloadButtonText);
    }

    @Override
    public void onClick(AjaxRequestTarget target) {
        page.setDetail(!page.isDetail());
        try {
            page.refreshTable(page.getTable());
            model.setButton(getStatusBasedText(), getStatusBasedIcon());
            target.add(page.getForm());
            target.add(this);
        } catch (MDDException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private String getStatusBasedText() {
        String text = page.isDetail() ? HIDE_DETAILS : SHOW_DETAILS;
        return text;
    }

    private String getStatusBasedIcon() {
        return "expand";
    }
}
