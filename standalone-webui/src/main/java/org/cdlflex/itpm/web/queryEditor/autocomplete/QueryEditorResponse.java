package org.cdlflex.itpm.web.queryEditor.autocomplete;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.wicket.request.IRequestParameters;
import org.cdlflex.itpm.web.utils.Util;
import org.cdlflex.mdd.sembase.MDDException;

public class QueryEditorResponse {
    private static final Logger logger = Logger.getLogger(QueryEditorResponse.class);

    private static final String REQUEST = "req";
    private static final String RULE_EDITOR = "rule_editor";
    private static final String FUNCTION = "f";

    private static final String GET_ONTOLOGY = "get_ontology";
    private static final String SUGGEST_COMPLETION = "suggest";

    public String checkAutomatable(IRequestParameters params) throws MDDException {
        logger.info(String.format("Called %s.", QueryEditorResponse.class));
        String json = "";
        String request = params.getParameterValue(REQUEST).toString("").trim();
        if (request.equalsIgnoreCase(RULE_EDITOR)) {
            String format = params.getParameterValue("format").toString("").trim();
            // String content = params.getParameterValue("content").toString("").trim();
            // String position = params.getParameterValue("cursor_position").toString("").trim();
            String function = params.getParameterValue(FUNCTION).toString("").trim();
            String ontologyPrefix = "";
            if (function.equalsIgnoreCase(GET_ONTOLOGY)) {
                ConceptService s = new ConceptService(format, ontologyPrefix);
                Map<String, List<String>> concepts = s.retrieveConceptsAllModels();
                json += this.wrapListWithJson(concepts.get(ConceptService.OWL_OBJECT_PROPERTY), "objectProperties");
                json += ",";
                json += this.wrapListWithJson(concepts.get(ConceptService.OWL_CLASS), "classes");
                json += ",";
                json += this.wrapListWithJson(concepts.get(ConceptService.OWL_DATA_PROPERTY), "dataProperties");
                json = getJsonRoot(json);
            } else if (function.equalsIgnoreCase(SUGGEST_COMPLETION)) {
                // future work
                logger.debug("other suggestions for autocompletion...");
            } else {
                logger.error("Bad request.");
                // resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
            }
        } else {
            logger.error("Bad request.");
            // resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
        return json;
    }

    private String wrapListWithJson(List<String> list, String name) {
        String strList = Util.asString(list, "\"", "\"", ',');
        String res = String.format("\"%s\": [%s]", name, strList);
        return res;
    }

    protected String getJsonRoot(String jsonBody) {
        String res = String.format("{%s}", jsonBody);
        return res;
    }
}
