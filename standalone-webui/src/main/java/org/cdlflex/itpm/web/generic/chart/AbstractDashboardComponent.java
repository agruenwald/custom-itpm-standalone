package org.cdlflex.itpm.web.generic.chart;

import java.io.Serializable;

import org.cdlflex.itpm.generated.model.PMRule;

/**
 * Abstract interface to display charts/single diagrams of any type.
 */
public interface AbstractDashboardComponent extends Serializable {
    /**
     * Get the title of a diagram.
     * @return the title.
     */
    String getTitle();

    /**
     * Plot the diagram.
     * @return the library-internal representation of the diagram (string).
     */
    String plot();

    /**
     * Optional: the rule from which the component origins.
     * @return the rule / query.
     */
    PMRule getRule();
}
