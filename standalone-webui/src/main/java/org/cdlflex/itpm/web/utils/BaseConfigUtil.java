package org.cdlflex.itpm.web.utils;

import java.io.File;
import java.io.IOException;

import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Get the root path in which the application (jar, Eclipse etc.) is executed.
 */
public class BaseConfigUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseConfigUtil.class);
    private static final String APPLICATION_NAME = "standalone-webui";
    private static final String SEMBASE_HOME_JAVA_PROPERTY = "sembase.home";

    /**
     * Read the default Java system variable "sembase.home" or set it to an default value if not available (the default
     * value will be the path in which the executable resides).
     * 
     * @return the value to which the system variable has been set.
     */
    public String setDefaultBaseConfigPath() {
        String sembaseHome = SEMBASE_HOME_JAVA_PROPERTY;
        String sembaseHomeVal = System.getProperty(sembaseHome);
        if (sembaseHomeVal == null) {
            sembaseHomeVal = new BaseConfigUtil().getExecutableRoot();
            LOGGER.warn(String.format("\"%s\" not set. Assign default value %s. "
                + "\nPlease ensure that the set path is correct in your environment.", sembaseHome, sembaseHomeVal,
                    sembaseHome));
        }
        try {
            sembaseHomeVal = ConfigLoader.getUrl(sembaseHomeVal).getFile();
            System.setProperty(sembaseHome, sembaseHomeVal);
        } catch (IOException e) {
            LOGGER.error("Cannot assign {}: {}.", SEMBASE_HOME_JAVA_PROPERTY, e.getMessage());
        }
        LOGGER.info(String.format("System property \"%s\" is set to \"%s\".", sembaseHome, sembaseHomeVal));
        return sembaseHomeVal;
    }

    /**
     * Get the root path in which the application (jar, Eclipse etc.) is executed. The determination of the path needs
     * some flexiblity. E.g. the path for Eclipse in which the current location resides is
     * /Users/andreas_gruenwald/git/custom-itpm-standalone/standalone-webui/target/test-classes/. The accurate path
     * would however be Users/andreas_gruenwald/git/custom-itpm-standalone/standalone-webui/. Hence there may be some
     * iteration over the file path necessary which is encapsulated within this method.
     * 
     * @return the absolute path to the executable application.
     */
    public String getExecutableRoot() {
        File exPath = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getFile());
        if (!exPath.exists()) {
            throw new RuntimeException(String.format("Execution path %s does not exist.", exPath));
        }
        String relativeName = exPath.getName();
        int maxUp = 30;
        int i = 0;
        boolean found = true;
        while (exPath.isDirectory() && !relativeName.equals(APPLICATION_NAME) || (i > maxUp)) {
            LOGGER.info(String.format("Try to find application name %s in path %s...", APPLICATION_NAME, exPath));
            exPath = exPath.getParentFile();
            if (exPath == null) {
                // root reached
                found = false;
                break;
            }
            relativeName = exPath.getName();
            i++;
        }
        if (!found) {
            exPath = new File(".");
            LOGGER.info("Determined path via new File(.): {} vs {}", exPath.getAbsolutePath(),
                    exPath.getAbsoluteFile());

        }
        try {
            return ConfigLoader.getUrl(exPath.getAbsolutePath()).getFile();
        } catch (IOException e) {
            LOGGER.error("Could not position at root directory: {}", e.getMessage());
            return exPath.getAbsolutePath();
        }
    }
}
