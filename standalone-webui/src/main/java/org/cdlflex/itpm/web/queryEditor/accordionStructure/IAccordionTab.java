package org.cdlflex.itpm.web.queryEditor.accordionStructure;

import org.apache.wicket.extensions.markup.html.tabs.ITab;

/**
 * Abstract header of an accordion tab which contains information about its activity, the number of sub-entities, the
 * knowledge area it belongs, etc.
 */
public interface IAccordionTab extends ITab {
    public boolean isNewActive();

    public void setNewActive(boolean newActive);

    public int getCount();

    public String getKnowledgeArea();
}
