package org.cdlflex.itpm.web.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cdlflex.itpm.web.metamodeltools.MetaModelBrowser;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Filter shortcuts from text defined via the query editor and inject icons, css classes and tags. Supported patterns: -
 * itpm:Requirement{Label text} : results in a link to the requirement view - itpm:Requirement{Label text, rec: 255} :
 * link to a specific requirement instance - icon:task{class:cssClassname, color: blue, "you did it"} : Creates a popup
 * icon with a specific classname, color and alternative text. - icon:user{}: Creates a user-icon (glyphicon from
 * Bootstrap) - html:small{"I am some small text} - creates html <small>...</small> - html:image{src:"../user.gif",
 * alt:"Alternative test} : embeds an image (use it carefully!)
 */
public class CssAnnotator {
    private static final Logger LOGGER = LoggerFactory.getLogger(CssAnnotator.class);

    final StringBuffer sb = new StringBuffer();
    static final Pattern TWO_TAGS_AND_BRACES = Pattern.compile("([a-z]{2,10})\\:([a-zA-Z-]{1,45})\\s?[{](.*?)[}]");
    static final Pattern TWO_TAGS_NO_BRACES = Pattern.compile("([a-z]{2,10})\\:([a-zA-Z-]{1,45})");

    private static final String BOOTSTRAP_ICON_TAG = "icon";
    private static final String HTML_TAG = "html";

    // (.*?)\\}
    public static String parseAnnotations(String text) {
        text = replaceIcons(text);
        return text;
    }

    protected static String replaceIcons(String text) {
        List<CssAnnotatorPattern> result = new ArrayList<CssAnnotatorPattern>();
        LOGGER.debug("Text: {}", text);
        final Matcher matcher = TWO_TAGS_AND_BRACES.matcher(text);
        while (matcher.find()) {
            if (matcher.groupCount() == 3) {
                CssAnnotatorPattern cap = new CssAnnotatorPattern();
                cap.setMatchedPattern(matcher.group());
                cap.setDomain(matcher.group(1).trim());
                cap.setTag(matcher.group(2).trim());
                extractBetweenBraces(matcher.group(3), cap);
                result.add(cap);
            }
        }
        text = replaceAnnotations(result, text);
        /*
         * final Matcher matcher2 = TWO_TAGS_NO_BRACES.matcher(text); while(matcher2.find()){ if (matcher2.groupCount()
         * == 2) { CssAnnotatorPattern cap = new CssAnnotatorPattern(); cap.setMatchedPattern(matcher2.group());
         * cap.setDomain(matcher2.group(1).trim()); cap.setTag(matcher2.group(2).trim());
         * extractBetweenBraces(cap.getMatchedPattern(), cap); result.add(cap); } } text = replaceAnnotations(result,
         * text);
         */
        return text;
    }

    private static String replaceAnnotations(List<CssAnnotatorPattern> caps, String text) {
        for (CssAnnotatorPattern cap : caps) {
            text = text.replace(cap.getMatchedPattern(), CssAnnotator.annotationToHtml(cap));
        }
        return text;
    }

    private static void extractBetweenBraces(String group, CssAnnotatorPattern cap) {
        // split by "," but only outside quotes
        String[] pairs = group.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
        if (pairs.length == 0) {
            pairs = new String[] { group };
        }
        for (String p : pairs) {
            String[] keyValuePairs = p.split("\\:(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
            if (keyValuePairs.length > 1) {
                cap.getAttributes().put(keyValuePairs[0].replace("\"", "").trim(),
                        keyValuePairs[1].replace("\"", "").trim());
            } else {
                cap.getAttributes().put("", p.trim().replace("\"", ""));
            }
        }
    }

    /**
     * 
     * @param cap
     * @return html text or annotation if nothing matched (important for css attributes).
     */
    protected static String annotationToHtml(CssAnnotatorPattern cap) {
        String html = "";
        boolean matched = false;
        if (cap.getDomain().equals(BOOTSTRAP_ICON_TAG)) {
            matched = true;
            String altText = "";
            if (cap.getAttributes().containsKey("alt")) {
                altText = cap.getAttributes().get("alt");
            } else if (cap.getAttributes().containsKey("text")) {
                altText = cap.getAttributes().get("text");
            }
            String title = getAttr(cap, "title", "");
            if (!altText.isEmpty()) {
                html =
                    IconUtil.createBSPopover(title, getAttr(cap, "class", ""), cap.getTag(), true, altText,
                            getAttr(cap, "color", ""));
            } else {
                html =
                    IconUtil.createBSIcon(title, getAttr(cap, "class", ""), cap.getTag(), false, "",
                            getAttr(cap, "color", ""));
            }
        } else if (cap.getDomain().equals(HTML_TAG)) {
            matched = true;
            Iterator<Map.Entry<String, String>> it = cap.getAttributes().entrySet().iterator();
            StringBuffer buf = new StringBuffer();
            while (it.hasNext()) {
                Map.Entry<String, String> attr = it.next();
                if (!attr.getKey().isEmpty()) {
                    buf.append(" ").append(attr.getKey()).append("=\"").append(attr.getValue()).append("\"");
                }
            }
            html = String.format("<%s%s>%s</%s>", cap.getTag(), buf, getAttr(cap, "", ""), cap.getTag());
        } else {
            String identifiedNamespace = "";
            try {
                MetaModel mm = new MetaModelBrowser().getMetaModelClasses(cap.getDomain());
                identifiedNamespace = mm != null ? mm.getNamespacePrefix() : "";
            } catch (MDDException e) {
                LOGGER.debug(e.getMessage());
            }

            if (!identifiedNamespace.isEmpty()) {
                // any model namespace, e.g. itpm, config, etc.
                String WICKET_LINK_PATTERN =
                    "wicket/bookmarkable/org.cdlflex.itpm.web.generic.table.GenericTablePage";
                String WICKET_LINK_PATTERN_RECORD =
                    "wicket/bookmarkable/org.cdlflex.itpm.web.generic.table.GenericTablePage?metaClass=%s&recId=%s&view=%s";
                if (getAttr(cap, "rec", "").isEmpty()) {
                    html =
                        HtmlUtil.a(WICKET_LINK_PATTERN + "?view=" + identifiedNamespace + "&id=" + cap.getTag() + ""
                            + "&recId=" + getAttr(cap, "rec", ""), getAttr(cap, "", cap.getTag()));
                } else {
                    html =
                        HtmlUtil.a(String.format(WICKET_LINK_PATTERN_RECORD, cap.getTag(), getAttr(cap, "rec", ""),
                                identifiedNamespace, getAttr(cap, "rec", "")), getAttr(cap, "", cap.getTag()));
                }
                matched = true;
            }
        }
        if (matched) {
            return html;
        } else {
            return cap.getMatchedPattern();
        }
    }

    private static String getAttr(CssAnnotatorPattern cap, String key, String defaultValue) {
        if (cap.getAttributes().containsKey(key)) {
            return cap.getAttributes().get(key);
        } else {
            return defaultValue;
        }
    }

}
