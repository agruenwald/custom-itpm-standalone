package org.cdlflex.itpm.web.utils;

/**
 * Creates simple HTML patterns for the generic table approach. With Wicket it would require the creation of dozens of
 * panels which is less compact and more complicated.
 */
public final class HtmlUtil {
    private HtmlUtil() {
        
    }
    
    /**
     * Create a link.
     * @param link the text.
     * @return the HTML link.
     */
    public static String a(String link) {
        return a(link, link);
    }

    /**
     * Create a HTML link with specific text.
     * @param link the link.
     * @param html the label text / html.
     * @return the HTML link.
     */
    public static String a(String link, String html) {
        return String.format("<a href=\"%s\">%s</a>", link, html);
    }

    /**
     * Create a HTML link with specific text and target.
     * @param link the link.
     * @param html the label text / HTML.
     * @param target the target, e.g. "_blank".
     * @return the HTML link.
     */
    public static String a(String link, String html, String target) {
        return String.format("<a href=\"%s\" target=\"%s\">%s</a>", link, target, html);
    }

    /**
     * Add a line break.
     * @param html the html.
     * @return the html with a line break at the end.
     */
    public static String br(String html) {
        return String.format("%s<br />", html);
    }

    /**
     * Format html bold.
     * @param html the html / text.
     * @return html formated as bold HTML.
     */
    public static String b(String html) {
        return String.format("<b>%s</b>", html);
    }

    /**
     * Encapsulate html within span element.
     * @param html the html / text.
     * @return HTML element.
     */
    public static String span(String html) {
        return span(html, "");
    }

    /**
     * 
     * Encapsulate html within span element.
     * @param html the html / text.
     * @param cssClass css class of the span element.
     * @return HTML element.
     */
    public static String span(String html, String cssClass) {
        return String.format("<span class=\"%s\">%s</span>", cssClass, html);
    }

    /**
     * Encapsulate html within small HTML element.
     * @param html the html / text.
     * @return the HTML element.
     */
    public static String small(String html) {
        return small(html, "");
    }

    /**
     * Encapsulate html within small HTML element.
     * @param html the html / text element.
     * @param cssClass css class of the small element. 
     * @return the HTML element.
     */
    public static String small(String html, String cssClass) {
        return String.format("<small class=\"%s\">%s</small>", cssClass, html);
    }

    /**
     * Create a link out of an URL.
     * @param url an address, possibly without protocol; e.g. www.google.com.
     * @return a browser compatible URL, e.g. http://www.google.com.
     */
    public static String link(String url) {
        return link(url, "http");
    }

    /**
     * Create a link.
     * @param url the URL, possibly incomplete; e.g. www.google.com
     * @param protocol the protocol, e.g. http
     * @return a browser compatible URL, e.g. http://www.google.com.
     */
    public static String link(String url, String protocol) {
        if (!url.contains("://")) {
            return protocol + "://" + url;
        } else {
            return url;
        }
    }

    /**
     * Convert spaces into non-breakable HTML spaces.
     * @param html the text.
     * @return the text with HTML spaces.
     */
    public static String noBreak(String html) {
        return html.replaceAll(" ", "&nbsp");
    }

}
