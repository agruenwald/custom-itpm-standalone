package org.cdlflex.itpm.web.generic.table;

/**
 * Abstract interface to refresh other components.
 * 
 */
public interface IDataRefreshContainer {
    public void refreshGenericData();
}
