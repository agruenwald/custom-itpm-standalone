package org.cdlflex.itpm.web.store;

import org.cdlflex.itpm.generated.ext.ITPMDAOFactory;
import org.cdlflex.itpm.generated.model.SPARQL;
import org.cdlflex.mdd.modelrepo.AdvancedModelStoreImpl;
import org.cdlflex.mdd.modelrepo.ModelStore;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.sembase.jena.semquery.QueryFactoryJena;
import org.cdlflex.mdd.sembase.semquery.QueryInterface;
import org.cdlflex.mdd.sembase.semquery.QueryInterfaceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ConnectionUtil {
    private static final String DEFAULT_PROJECT = "";
    private static final String ITPM = "itpm";
    private static final String CONFIG = "config";
    private static ITPMDAOFactory factory;
    private static Connection itpmCon;
    private static Connection configCon;
    private static final Logger logger = LoggerFactory.getLogger(ConnectionUtil.class);
    private ConfigLoader configLoader = new ConfigLoader(false);

    public ITPMDAOFactory getFactory() {
        if (factory == null) {
            factory = new ITPMDAOFactory(false);
        }
        return factory;
    }

    public static void setFactory(ITPMDAOFactory f) {
        factory = f;
    }

    public void refreshConnections() {
        try {
            itpmCon.close();
        } catch (Exception e) {

        }
        try {
            configCon.close();
        } catch (Exception e) {

        }

        configCon = this.getFreshConfigConnection();
        itpmCon = this.getFreshITPMConnection();
    }

    public Connection getOpenITPMConnection() {
        factory = this.getFactory();
        if (itpmCon == null) {
            try {
                itpmCon = factory.createConnection(DEFAULT_PROJECT, ITPM);
                itpmCon.connect();
            } catch (MDDException e) {
                logger.error(e.getMessage());
                itpmCon = null;
            }
        }
        if (itpmCon == null) {
            logger.error("Sever problem: ITPM connection could not be established.");
            return itpmCon;
        }
        try {
            itpmCon.connect(); // re-connect in any case
        } catch (MDDException e) {

        }
        return itpmCon;
    }

    /**
     * get a query interface and parse the model type based on the prefixes contained within the (core) rule.
     * 
     * @param format the format of the query/rule, e.g. SPARQL.class.instance.getId()
     * @param cleanedRule the rule without prefixing and without HTML characters, e.g. SELECT ?x ?y for SPARQL (note
     *        that the prefixing is not included).
     * @return an appropriate query interface including the correct connection. The core ITPM model is used in case that
     *         there has no other model been found.
     * @throws MDDException
     */
    public QueryInterface getQueryInterface(String format, String cleanedRule) throws MDDException {
        Connection con = null;
        cleanedRule = stripQuoteText(cleanedRule);
        if (this.countOccurences(cleanedRule, ITPM + ":") > 0) {
            con = getOpenITPMConnection();
        } else if (this.countOccurences(cleanedRule, CONFIG + ":") > 0) {
            con = getOpenConfigConnection();
        } else {
            ModelStore modelStore = new AdvancedModelStoreImpl(configLoader);
            List<String> modelNames = modelStore.findAllMetaModelNames();
            String prefix = ITPM; // default
            for (String modelName : modelNames) {
                int count = this.countOccurences(cleanedRule, modelNames + ":");
                if (count > 0) {
                    prefix = modelName;
                }
            }
            con = factory.createConnection(DEFAULT_PROJECT, prefix);
            con.connect();
        }
        QueryInterface qI = new ConnectionUtil().getQueryInterface(format, con);
        return qI;
    }

    /**
     * Get a SPARQL query interface which is the default.
     * 
     * @return a query interface including connection to the store.
     * @throws MDDException
     */
    public QueryInterface getDefaultQueryInterface() throws MDDException {
        try {
            return getQueryInterface(new SPARQL().getId(), this.getOpenITPMConnection());
        } catch (MDDException e) {
            return getQueryInterface(new SPARQL().getId(), this.getOpenConfigConnection());
        }
    }

    /**
     * Remove everything between quotes, e.g. this is a 'test' or this is another "test" will be transformed into this
     * is a or this is another.
     * 
     * @param query
     */
    private String stripQuoteText(String query) {
        query = query.replaceAll("\".*?\"", "");
        query = query.replaceAll("\'.*?\'", "");
        return query;
    }

    private int countOccurences(String string, String findStr) {
        int lastIndex = 0;
        int count = 0;
        while (lastIndex != -1) {
            lastIndex = string.indexOf(findStr, lastIndex);
            if (lastIndex != -1) {
                count++;
                lastIndex += findStr.length();
            }
        }
        return count;
    }

    public QueryInterface getQueryInterface(String format, Connection con) throws MDDException {
        QueryInterfaceFactory queryFactory = new QueryFactoryJena(con);
        QueryInterface qI = queryFactory.getQueryInterface(format);
        if (qI == null) {
            throw new MDDException(
                    String.format(
                            "The format %s is not supported by the underlying technology stack (%s)."
                                + " If you are eager to use the query format then you need to replace or extend the underlying technology. "
                                + " For instance, if you are using Jena then OWLDL is not supported. You need to switch to the OWLAPI in "
                                + " the module \"custom-itpm-generated.\"", format, queryFactory.getClass()
                                    .getSimpleName()));
        }
        return qI;
    }

    public Connection getOpenConfigConnection() {
        factory = this.getFactory();
        if (configCon == null) {
            try {
                configCon = factory.createConnection(DEFAULT_PROJECT, CONFIG);
                configCon.connect();
            } catch (MDDException e) {
                logger.error(e.getMessage());
                configCon = null;
            }
        }
        if (configCon == null) {
            logger.error("Severe problem: ITPM connection could not be established.");
        }
        try {
            configCon.connect(); // re-connect in any case
        } catch (MDDException e) {

        }
        return configCon;
    }

    public ConfigLoader getConfigLoader() {
        return this.configLoader;
    }

    public Connection getFreshConfigConnection() {
        return this.getFreshConnection(CONFIG);
    }

    public Connection getFreshITPMConnection() {
        return this.getFreshConnection(ITPM);
    }

    public Connection getFreshConnection(String namespacePrefix) {
        factory = this.getFactory();
        Connection con = null;
        try {
            con = factory.createConnection(DEFAULT_PROJECT, namespacePrefix);
            con.connect();
        } catch (MDDException e) {
            logger.error(e.getMessage());
            con = null;
        }

        if (con == null) {
            logger.error("Severe problem: " + namespacePrefix + " connection could not be established.");
        }
        try {
            con.connect(); // re-connect in any case
        } catch (MDDException e) {

        }
        return con;
    }

    public Connection getOpenConnection(String name) {
        if (name.equals(ITPM)) {
            return getOpenITPMConnection();
        } else if (name.equals(CONFIG)) {
            return getOpenConfigConnection();
        } else {
            throw new RuntimeException(String.format("Unknown model name %s. Could not establish connection.", name));
        }
    }

    /**
     * Try to determine if the underlying data model contains data.
     * 
     * @return true if there is no data within the models (in Jena e.g. these typically causes exception during the
     *         access later on).
     */
    public boolean isNoData() {
        ConnectionUtil cu = new ConnectionUtil();
        Connection configCon = cu.getFreshConfigConnection();
        try {
            try {
                configCon.begin();
            } catch (MDDException e) {
                logger.warn(e.getMessage());
                return true;
            } finally {
                configCon.close();
            }
            cu.getFreshConfigConnection();
        } catch (MDDException e) {
            logger.warn(e.getMessage());
        }
        return false;
    }
}
