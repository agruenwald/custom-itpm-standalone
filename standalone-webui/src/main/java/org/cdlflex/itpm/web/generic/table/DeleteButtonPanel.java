package org.cdlflex.itpm.web.generic.table;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.util.value.ValueMap;
import org.cdlflex.itpm.web.metamodeltools.MetaModelDataUpdate;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This form is added at the end of a table. It inserts a button for deletion of multiple table entries. The button is
 * JS-supported and is activated only if records are selected. Furthermore, a click on the button does not result in an
 * immediate submit, but a modal window for confirmation pops up before.
 */
public class DeleteButtonPanel extends Panel {
    private static final Logger logger = LoggerFactory.getLogger(DeleteButtonPanel.class);
    private static final long serialVersionUID = 1L;
    private static final MetaModelDataUpdate METAMODEL_UPDATER = new MetaModelDataUpdate();

    /**
     * Create a new panel with a deletion button.
     * 
     * @param id the id of the panel
     * @param modelName the name of the model which the metaClass is part of.
     * @param metaClass the meta class of the umlTUowl model.
     * @param genericTablePanel
     */
    public DeleteButtonPanel(String id, String modelName, MetaClass metaClass, GenericTablePanel genericTablePanel) {
        super(id);
        add(new DeleteForm("deleteForm", modelName, metaClass, genericTablePanel));

    }

    private final class DeleteForm extends Form<ValueMap> {
        private static final long serialVersionUID = 1L;
        private final HiddenField<String> hf;
        private final String modelName;
        private final MetaClass metaClass;
        private final GenericTablePanel genericTablePanel;

        public DeleteForm(final String id, String modelName, MetaClass metaClass, GenericTablePanel genericTablePanel) {
            super(id, new CompoundPropertyModel<ValueMap>(new ValueMap()));
            setMarkupId("deleteForm");
            hf = new HiddenField<String>("serIds");
            hf.setMarkupId("serializedIds");
            add(hf);
            this.modelName = modelName;
            this.metaClass = metaClass;
            this.genericTablePanel = genericTablePanel;
        }

        @Override
        public final void onSubmit() {
            String serializedIds = hf.getValue().trim();
            logger.trace(String.format("Called onSubmit() with serIds=%s.", serializedIds));
            String[] ids = hf.getValue().split("[,]");
            List<String> errorList = new LinkedList<String>();
            if (ids.length <= 0 || ids[0].isEmpty()) {
                logger.error(String.format("Invalid serializedIds (%s). Deletion failed.", serializedIds));
                error("Deletion failed. Contact the administrator.");
            } else {
                try {
                    METAMODEL_UPDATER.removeRecords(modelName, metaClass, Arrays.asList(ids));
                } catch (MDDException e) {
                    errorList.add(e.getMessage());
                }

                if (!errorList.isEmpty()) {
                    for (String e : errorList) {
                        error(e);
                    }
                } else {
                    info(String.format("%d %s%s %s been deleted.", ids.length, metaClass.getOriginalName(),
                            ids.length > 1 ? "s" : "", ids.length > 1 ? "have" : "has"));
                    // data must be refreshed explicitly
                    genericTablePanel.refreshGenericData();
                }
            }
        }
    }
}
