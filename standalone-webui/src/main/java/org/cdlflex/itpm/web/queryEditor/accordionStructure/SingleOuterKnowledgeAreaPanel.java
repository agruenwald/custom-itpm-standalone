package org.cdlflex.itpm.web.queryEditor.accordionStructure;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.cdlflex.itpm.generated.model.PMRule;
import org.cdlflex.itpm.web.queryEditor.QueryEditorForm;
import org.cdlflex.itpm.web.queryEditor.QueryEditorPage;
import org.cdlflex.itpm.web.reusable.NotificationPanelBootstrap;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.mdd.sembase.MDDException;

/**
 * Represents a single knowledge area panel. Each knowledge area panel consists of a sub-accordion in which the queries
 * are listed. One tab of the sub-accordion enables users to add new queries.
 */
public class SingleOuterKnowledgeAreaPanel extends Panel {
    private static final Logger logger = Logger.getLogger(SingleOuterKnowledgeAreaPanel.class);

    private static final long serialVersionUID = 1L;
    public WebMarkupContainer createRule;
    private final IAccordionTab iAccordion;
    private final QueryEditorPage rulePageRef;
    protected final ListView<PMRule> listView;
    protected final String knowledgeAreaName;

    public SingleOuterKnowledgeAreaPanel(String panelId, String area, IAccordionTab iAccordion,
            final List<PMRule> rules, QueryEditorPage rulePage, String knowledgeAreaName) {
        super(panelId);
        this.rulePageRef = rulePage;
        this.iAccordion = iAccordion;
        this.knowledgeAreaName = knowledgeAreaName;
        this.setOutputMarkupId(true);

        createRule = new WebMarkupContainer("create_new") {
            private static final long serialVersionUID = 1L;

            @Override
            public void onConfigure() {
                boolean visibility = SingleOuterKnowledgeAreaPanel.this.iAccordion.isNewActive();
                setVisible(visibility);

            }
        };
        add(createRule);

        FeedbackPanel feedbackPanel = new NotificationPanelBootstrap("feedback_new");
        feedbackPanel.setOutputMarkupId(true);
        feedbackPanel.setFilter(new FilterError());
        add(feedbackPanel);

        add(new QueryEditorForm("form_add_rule", getKnowledgeAreaName(), feedbackPanel, rulePage.getRuleService(),
                this.rulePageRef));

        // get the list of items to display from provider (database, etc)
        // in the form of a LoadableDetachableModel
        IModel<List<PMRule>> detachedRules = new LoadableDetachableModel<List<PMRule>>() {
            private static final long serialVersionUID = 1L;

            protected List<PMRule> load() {
                List<PMRule> pmRules;
                try {
                    pmRules =
                        new ConnectionUtil().getFactory()
                                .create(PMRule.class, new ConnectionUtil().getOpenITPMConnection()).readFulltext("");
                } catch (MDDException e) {
                    logger.error(e.getMessage());
                    return new ArrayList<PMRule>();
                }
                // SingleOuterKnowledgeAreaPanel.this.rulePageRef.getRuleService().readRulesComplete();
                List<PMRule> rules =
                    SingleOuterKnowledgeAreaPanel.this.rulePageRef.getRuleService().filterRulesByKnowledgeArea(
                            pmRules, SingleOuterKnowledgeAreaPanel.this.knowledgeAreaName);
                logger.info(String.format("Loaded %s and replaced size of knowledge area to %d.",
                        SingleOuterKnowledgeAreaPanel.this.knowledgeAreaName, rules.size()));
                SingleOuterKnowledgeAreaPanel.this.rulePageRef.getKnowledgeAreas().put(
                        SingleOuterKnowledgeAreaPanel.this.knowledgeAreaName, rules.size());
                return rules;
            }
        };

        listView = new ListView<PMRule>("rules", detachedRules) {
            private static final long serialVersionUID = 1L;

            protected void populateItem(final ListItem<PMRule> itemModel) {
                PMRule item = itemModel.getModelObject();
                itemModel.add(new InnerQueryPanel("rule", item, SingleOuterKnowledgeAreaPanel.this.rulePageRef,
                        SingleOuterKnowledgeAreaPanel.this));
            }
        };
        listView.setOutputMarkupId(true);
        add(listView);
    }

    public String getKnowledgeAreaName() {
        return this.knowledgeAreaName;
    }
}
