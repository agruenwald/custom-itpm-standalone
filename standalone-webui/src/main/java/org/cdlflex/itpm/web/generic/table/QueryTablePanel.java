package org.cdlflex.itpm.web.generic.table;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.ISortableDataProvider;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.cdlflex.itpm.generated.model.PMRule;
import org.cdlflex.itpm.generated.model.Table;
import org.cdlflex.itpm.web.generic.flexGraph.FlexGraphPanel;
import org.cdlflex.itpm.web.reusable.GenericSortableDataProvider;
import org.cdlflex.itpm.web.reusable.InteractiveTableColumn;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.itpm.web.utils.Util;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semquery.QueryInterface;

/**
 * Panel to visualize generic data tables based on user-defined queries/rules.
 */
public class QueryTablePanel extends Panel implements IDataContainer {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(QueryTablePanel.class);
    private static final int ROWS_PER_PAGE_DEFAULT = 20;
    private String titleName;
    private List<String> titles = new LinkedList<String>();
    protected final List<Map<String, String>> data;
    @SuppressWarnings("rawtypes")
    private DefaultDataTable table;
    private final Form<?> form;
    private PMRule selectedRule;
    private FlexGraphPanel flexGraph;
    private Model<String> titleModel;
    private final PageParameters parameters;
    private int tableRowsPerPage;

    public QueryTablePanel(String panelId, SearchForm searchForm, final PageParameters parameters, PMRule selectedRule) {
        this(panelId, parameters, ROWS_PER_PAGE_DEFAULT);
        this.renderPanel(searchForm, selectedRule);
    }

    /**
     * Create object but do not render it (and the search form component) yet.
     * 
     * @param panelId
     * @param parameters
     * @param selectedRule
     */
    public QueryTablePanel(String panelId, final PageParameters parameters) {
        this(panelId, parameters, ROWS_PER_PAGE_DEFAULT);
    }

    @SuppressWarnings("rawtypes")
    public QueryTablePanel(String panelId, final PageParameters parameters, int rowsPerPage) {
        super(panelId);
        tableRowsPerPage = rowsPerPage;
        form = new Form("generic-table-form");
        form.setOutputMarkupId(true);
        add(form);
        this.parameters = parameters;
        this.data = new ArrayList<Map<String, String>>();
    }

    public void setMaxSize(int maxRows) {
        this.tableRowsPerPage = maxRows;
        table.setItemsPerPage(maxRows);
    }

    public void renderPanel(SearchForm searchForm, PMRule selectedRule) {
        this.selectedRule = selectedRule;
        readGenericData("", "", searchForm == null ? "" : searchForm.getValue());
        boolean isOverView = parameters == null || parameters.get("recId").toString("").isEmpty();
        // getDataFromRule(selectedRule);
        table = createTable(data);
        form.add(table);
        table.setVisible(isOverView);

        // this.readGenericData("");
        // table = createTable(data);
        flexGraph =
            new FlexGraphPanel("flexgraph", selectedRule == null ? new Table() : selectedRule.getOutput(), data);
        form.add(flexGraph);
        flexGraph.setVisible(selectedRule != null);
        // set visibility: show all types as table (even diagrams because they are shown at the home-page as diagrams
        // except graphs.
        if (selectedRule != null) {
            if (flexGraph.getJSLayout(selectedRule.getOutput()).isEmpty()) {
                flexGraph.setVisible(false);
            } else {
                table.setVisible(false);
            }
        }
        Label fake = new Label("generic-table");
        add(fake);
        fake.setVisible(false);
        titleModel = Model.of(titleName);
    }

    /**
     * read data and automatically detect whether a rule is used to read from or the metamodel is queried. If rule-based
     * reading is detected then the retrieved rules are filtered afterwards (fulltextsearch). For the metamodel search
     * the fulltext search is already implemented natively.
     * 
     * @param modelName itpm, config, ...
     * @param fulltextSearch for fulltext search
     * @return
     */
    public List<Map<String, String>> readGenericData(String fulltextSearch) {
        data.clear();
        List<Map<String, String>> result = this.getData(selectedRule);
        List<Map<String, String>> resultFiltered = new ArrayList<Map<String, String>>();
        for (Map<String, String> row : result) {
            boolean match = false;
            for (String value : row.values()) {
                if (fulltextSearch.isEmpty() || value.toLowerCase().contains(fulltextSearch.toLowerCase())) {
                    match = true;
                    break;
                }
            }
            if (match) {
                resultFiltered.add(row);
            }
        }
        resultFiltered = Util.parseAnnotationsMap(resultFiltered);
        data.addAll(resultFiltered);
        return data;
    }

    public DefaultDataTable<Map<String, String>, String> createTable(final List<Map<String, String>> data) {
        ISortableDataProvider<Map<String, String>, String> dataProvider = new GenericSortableDataProvider(data);
        List<InteractiveTableColumn> columns = new ArrayList<InteractiveTableColumn>();
        for (String key : titles) {
            columns.add(new InteractiveTableColumn(Model.of(key), key));
        }
        if (columns.isEmpty()) {
            columns.add(new InteractiveTableColumn(Model.of("-"), "-"));
        }
        DefaultDataTable<Map<String, String>, String> defaultDataTable =
            new DefaultDataTable<Map<String, String>, String>("dataTableGenericPanel", columns, dataProvider,
                    tableRowsPerPage);
        defaultDataTable.setOutputMarkupId(true);
        return defaultDataTable;
    }

    public List<List<String>> getDataFromRule(PMRule rule) {
        String query = rule.getRuleDefinition();
        List<List<String>> result = new LinkedList<List<String>>();
        try {
            QueryInterface queryInterface = new ConnectionUtil().getQueryInterface(rule.getFormat().getId(), query);
            result = queryInterface.query(query);
            titles = queryInterface.extractNames(query);
        } catch (MDDException e) {
            logger.error(e.getMessage());
            return result;
        } catch (Exception eOther) {
            try {
                new ConnectionUtil().getOpenITPMConnection().rollback();
            } catch (Exception e) {

            }
        }
        return result;
    }

    private List<Map<String, String>> getData(PMRule selectedRule) {
        List<Map<String, String>> data = new LinkedList<Map<String, String>>();
        if (selectedRule != null) {
            List<List<String>> datas = getDataFromRule(selectedRule);
            titleName = selectedRule.getName();
            data = Util.listToMap(titles, datas, true);
        }
        return data;
    }

    public String getModelName() {
        return "";
    }

    public String getMetaClassName() {
        return "";
    }

    /**
     * The refresh behavior is only implemented for rules. The implementation for the metamodel classes is located in
     * {@link GenericTablePanel}.
     */
    @Override
    public void refreshGenericData() {
        readGenericData("");
        titleModel.setObject(titleName);
        if (flexGraph != null && flexGraph.isVisible()) {
            flexGraph.renderFlexGraph(selectedRule.getOutput(), data);
        }
    }

    @Override
    public List<Map<String, String>> readGenericData(String modelName, String metaClassName, String fulltext) {
        return readGenericData(fulltext);
    }

    @Override
    public void showOverview() {
        // nothing to do here
        table.setVisible(true);
    }

    public String getTitle() {
        return titleModel.getObject();
    }

    public int getRecords() {
        return this.data.size();
    }

    public DefaultDataTable<?, ?> getTable() {
        return table;
    }
}
