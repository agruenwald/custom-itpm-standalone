package org.cdlflex.itpm.web.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A utility to ease the visualization of generic data.
 */
public final class Util {
    private static final Logger LOGGER = LoggerFactory.getLogger(Util.class);
    private static final int SIXTY = 60;
    private static final int TWENTY_FOUR = 24;
    private static final int THOUSAND = 1000;
    private static final double DEFAULT_DOUBLE_VAL = 0.1;
    private static final double TWO = 2.0;
    
    private Util() {
        
    }
    
    /**
     * Get a value != null. Instead of null an empty string is returned.
     * @param value the value, possibly null.
     * @return a value != null.
     */
    public static String getValue(String value) {
        if (value == null) {
            return "";
        } else {
            return value;
        }
    }

    /**
     * Get the median of a list of values.
     * @param m a list of integers.
     * @return the median.
     */
    public static int median(List<Integer> m) {
        int middle = m.size() / 2;
        if (middle == 0) {
            return 0;
        } else if (m.size() % 2 == 1) {
            return (int) Math.floor(m.get(middle));
        } else {
            return (int) ((m.get(middle - 1) + m.get(middle)) / TWO);
        }
    }

    /**
     * Convert a list into a string.
     * @param list a list.
     * @param delimiter the delimiter.
     * @param <E> the data type of the entries.
     * @return a string, containing all list entries separated by the delimiter.
     */
    public static <E> String asString(List<E> list, char delimiter) {
        return Util.asString(list, "", "", delimiter);
    }

    /**
     * Convert a list into a string.
     * @param list the list.
     * @param before the first character.
     * @param after the trailing character.
     * @param delimiter a delimiter.
     * @param <E> the data type of the entries.
     * @return a string, containing all list entries separated by the delimiter,
     * with leading and trailing strings.
     */
    //CHECKSTYLE:OFF 4 parameters are ok.
    public static <E> String asString(List<E> list, String before, String after, char delimiter) {
        String res = "";
        for (int i = 0; i < list.size(); i++) {
            res += i == 0 ? "" : delimiter;
            res += before + String.valueOf(list.get(i)) + after;
        }
        return res;
    }
    //CHECKSTYLE:ON

    /**
     * Extract a specific column from a list of records.
     * @param list a list of records.
     * @param col the column number >= 0.
     * @return a list with the extracted column values.
     */
    public static List<String> colToList(List<List<String>> list, int col) {
        List<String> colList = new LinkedList<String>();
        for (List<String> e : list) {
            if (!(col >= e.size())) {
                colList.add(e.get(col));
            } else {
                colList.add("");
            }
        }
        return colList;
    }

    /**
     * Extract a set of columns from a set of records.
     * @param list a list of records. 
     * @param cols a list of columns.
     * @return a list with the column values extracted.
     */
    public static List<Double> numColToList(List<List<String>> list, List<Integer> cols) {
        List<Double> colList = new LinkedList<Double>();
        for (List<String> e : list) {
            for (int col : cols) {
                double val = DEFAULT_DOUBLE_VAL;
                try {
                    val = Double.parseDouble(e.get(col));
                //CHECKSTYLE:OFF
                } catch (Exception ex) {
                }
                //CHECKSTYLE:ON
                colList.add(val);
            }
        }
        return colList;
    }

    /**
     * Extract a specific column from a list of records (double values).
     * @param list a list of records.
     * @param col the column number >= 0.
     * @return a list with the extracted column values.
     */
    public static List<Double> numColToList(List<List<String>> list, int col) {
        List<Integer> colList = new ArrayList<Integer>();
        colList.add(col);
        return numColToList(list, colList);
    }

    /**
     * Converts a list of titles and a set of data rows into a list, where each key represents an attribute value of an
     * entry.
     * 
     * @param titles the column titles <= the data row length
     * @param data a list of entries. each entry is another list which represents the values of a record.
     * @param parseAnnotations if true then annotations according to {@link CssAnnotator} will be replaced (included in
     *        the cycle to reduce performance overhead).
     * @return a list of records. Each consists of maps.
     */
    public static List<Map<String, String>> listToMap(List<String> titles, List<List<String>> data,
        boolean parseAnnotations) {
        // convert list into map to show it.
        List<Map<String, String>> mapData = new ArrayList<Map<String, String>>();
        for (List<String> d : data) {
            Map<String, String> map = new HashMap<String, String>();
            for (int i = 0; i < d.size(); i++) {
                String cellContent = d.get(i);
                cellContent = CssAnnotator.parseAnnotations(cellContent);
                if (i < titles.size()) {
                    map.put(titles.get(i), d.get(i));
                } else {
                    LOGGER.error("No title any more: " + cellContent);
                    titles.add("Unnamed column " + (i + 1));
                    map.put(titles.get(titles.size() - 1), cellContent);
                }
            }
            mapData.add(map);
        }
        return mapData;
    }

    /**
     * Replace all special annotations.
     * @param data a set of records.
     * @return the records with replaced special annotations.
     */
    public static List<List<String>> parseAnnotations(List<List<String>> data) {
        for (int i = 0; i < data.size(); i++) {
            for (int j = 0; j < data.get(i).size(); j++) {
                String newContent = CssAnnotator.parseAnnotations(data.get(i).get(j));
                data.get(i).set(j, newContent);
            }
        }
        return data;
    }
    
    /**
     * Replace all special annotations.
     * @param data a set of records (each records is represented by a map).
     * @return the records with replaced special annotations.
     */
    public static List<Map<String, String>> parseAnnotationsMap(List<Map<String, String>> data) {
        for (int i = 0; i < data.size(); i++) {
            for (int j = 0; j < data.get(i).size(); j++) {
                for (String key : data.get(i).keySet()) {
                    String newContent = CssAnnotator.parseAnnotations(data.get(i).get(key));
                    data.get(i).put(key, newContent);
                }
            }
        }
        return data;
    }

    /**
     * Distance between a date and now.
     * 
     * @param histDate the historical date
     * @return a string with the distance information such as "< 1h" or "1 year ago"
     */
    public static String getDateDistanceInfo(Date histDate) {
        Date now = new Date();

        String res = "";
        long dist = (now.getTime() - histDate.getTime()) / THOUSAND;
        String prefix = "<";
        String postfix = ""; // "ago";

        if (dist < 0) {
            LOGGER.error(String.format("Hist date > now (%s > %s).", histDate.toString(), now.toString()));
            dist = dist * (-1);
            postfix = "in future";
        }

        if (dist < SIXTY * SIXTY) {
            // 1 hour
            res = prefix + dist / SIXTY + "m " + postfix;
        } else if (dist < SIXTY * SIXTY * TWENTY_FOUR) {
            res = prefix + dist / SIXTY / SIXTY + "h " + postfix;
        } else {
            res = prefix + dist / SIXTY / SIXTY / TWENTY_FOUR + "d " + postfix;
        }
        return res;
    }
}
