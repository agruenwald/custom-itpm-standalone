package org.cdlflex.itpm.web.store;

import org.cdlflex.itpm.generated.configurationloader.ConfigurationLoader;
import org.cdlflex.itpm.web.backend.SynchronizerJob;
import org.cdlflex.mdd.modelrepo.Installer;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Trigger the load of demo data in the standalone version of the app.
 */
public class TriggerLoadConfigData {
    private static final Logger logger = LoggerFactory.getLogger(TriggerLoadConfigData.class);

    /**
     * Flush the old ontology and reload all ontologies and the app demo data.
     * 
     * @return an empty string or a string with the error msg if the loading failed.
     */
    public static String loadAppDataFlushOntologies() throws MDDException {
        String eMsg = "";
        Connection itpmCon = null;
        Connection configCon = null;
        try {
            SynchronizerJob.stop();
            itpmCon = new ConnectionUtil().getFreshITPMConnection();
            configCon = new ConnectionUtil().getFreshConfigConnection();
            ConfigurationLoader loader =
                new ConfigurationLoader(new ConnectionUtil().getConfigLoader().isTestEnvironment(), itpmCon,
                        configCon);
            Installer.clean(new ConnectionUtil().getConfigLoader());
            Installer.installInternalModels(new ConnectionUtil().getConfigLoader());
            itpmCon.connectOnDemand();
            configCon.connectOnDemand();
            itpmCon.resetOntology();
            configCon.resetOntology();
            loader.loadAppData();
            new ConnectionUtil().refreshConnections();
            SynchronizerJob.restart();
        } catch (MDDException e) {
            logger.error(e.getMessage());
            eMsg = e.getMessage();
            throw e;
        } catch (Exception e) {
            eMsg = e.getMessage();
            try {
                configCon.rollback();
            } catch (MDDException e2) {
            }
            try {
                itpmCon.commit();
            } catch (MDDException e2) {
            }
            throw new MDDException(e.getMessage());
        }
        return eMsg;
    }

    /**
     * Initialize the ontologies and the app data only if it the ontology is still empty.
     * 
     * @return the error msg or an empty string on success.
     * @throws MDDException
     */
    public static String loadAppDataFlushOntologisIfEmpty() throws MDDException {
        if (new ConnectionUtil().isNoData()) {
            return loadAppDataFlushOntologies();
        } else {
            return "";
        }
    }

    /**
     * is called when the server is started from Start.java. The loading may be disabled because it might be preferred
     * to load the data manually once and then reuse the data to improved the speed of the startup.
     * 
     * @throws MDDException
     */
    public static void loadConfigDataManualStart() throws MDDException {
        // loadConfigData();
    }

    /**
     * This method can be called manually to load default data.
     * 
     * @param softLoad
     * @throws MDDException
     */
    public static void loadConfigData(boolean softLoad) throws MDDException {
        logger.info("Load configuration data...");
        ConfigLoader configLoader = new ConfigLoader(false);
        // Reset required if underlying data model has changed (e.g. generation).
        // Models for live-environment are stored in backup-dir, and then they
        // are reloaded from there (to loose no data).
        // In the test environment they are directly copied from the backup-dir.
        Installer.reset(configLoader);
        Installer.installModelsFromBackupDir(configLoader);

        Connection itpmCon = null;
        Connection configCon = null;
        try {
            itpmCon = new ConnectionUtil().getOpenITPMConnection();
            configCon = new ConnectionUtil().getOpenConfigConnection();
            itpmCon.resetOntology();
            configCon.resetOntology();
            ConfigurationLoader loader = new ConfigurationLoader(false, itpmCon, configCon);
            loader.resetOntologyOnlyFirstTime(true);
            loader.loadAppData();
        } catch (MDDException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                itpmCon.close();
            } catch (Exception e) {
            }
            try {
                configCon.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * Trigger the loading of data manually from the outside.
     * 
     * @param args
     * @throws MDDException
     */
    public static void main(String[] args) throws MDDException {
        TriggerLoadConfigData.loadConfigData(false);
    }
}
