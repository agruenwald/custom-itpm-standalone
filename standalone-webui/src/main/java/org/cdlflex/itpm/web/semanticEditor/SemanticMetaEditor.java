package org.cdlflex.itpm.web.semanticEditor;

import org.apache.wicket.markup.html.WebPage;
import org.cdlflex.base.api.WebAppModule;

public class SemanticMetaEditor implements WebAppModule {
    @Override
    public String getName() {
        return "Semantic Meta Editor";
    }

    @Override
    public Class<? extends WebPage> getPageClass() {
        return SemanticMetaEditorPage.class;
    }

    @Override
    public String[] getAuthorities() {
        return new String[0];
    }
}
