package org.cdlflex.itpm.web.synchronizer;

import java.io.Serializable;

/**
 * A model for buttons with an icon (bootstrap) and two states (e.g. synch on/off, etc.)
 */
public class SyncButton implements Serializable {
    private static final long serialVersionUID = 6202145422209204779L;

    private String value;

    public SyncButton() {
        this.value = "";
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setButton(String text, String icon) {
        this.value = String.format("<i class=\"glyphicon glyphicon-%s\"></i>&nbsp;%s", icon, text);
    }
}
