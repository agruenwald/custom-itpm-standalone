package org.cdlflex.itpm.web.generic.form;

import java.io.Serializable;

import org.cdlflex.mdd.sembase.semaccess.MDDBeanInterface;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;

/**
 * Represents a single entry within a select2 field which can contain different contents in the form builder. First, it
 * may contain a list of MDDBeanInterface instances which are linked via association to a root bean. Second it may
 * contain only textual values of any type which do not relate to the MDD bean approach. The latter is usable for
 * providing lists of enum characters and other choice fields.
 * 
 */
public class OptionEntry implements Serializable {
    private static final long serialVersionUID = 4293613266511381956L;
    private String id;
    private String value;
    private MDDBeanInterface optionalBean;
    private MetaClass optionalMetaClass;

    public OptionEntry(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public MDDBeanInterface getOptionalBean() {
        return optionalBean;
    }

    public void setOptionalBean(MDDBeanInterface optionalBean) {
        this.optionalBean = optionalBean;
    }

    public MetaClass getOptionalMetaClass() {
        return optionalMetaClass;
    }

    public void setOptionalMetaClass(MetaClass optionalMetaClass) {
        this.optionalMetaClass = optionalMetaClass;
    }

    @Override
    public boolean equals(Object o) {
        if (this.id.equals(((OptionEntry) o).id)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return String.valueOf(this.getId()).hashCode();
    }

    @Override
    public String toString() {
        return this.id;
    }
}
