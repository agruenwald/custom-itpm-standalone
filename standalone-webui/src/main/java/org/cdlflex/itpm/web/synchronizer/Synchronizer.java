package org.cdlflex.itpm.web.synchronizer;

import org.apache.wicket.markup.html.WebPage;
import org.cdlflex.base.api.WebAppModule;

public class Synchronizer implements WebAppModule {
    @Override
    public String getName() {
        return "Synchronizer";
    }

    @Override
    public Class<? extends WebPage> getPageClass() {
        return SynchronizerPage.class;
    }

    @Override
    public String[] getAuthorities() {
        return new String[0];
    }
}
