package org.cdlflex.itpm.web.reusable;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.model.Model;

/**
 * Remove single CSS classes from a Wicket component. naive approach; breaks with e.g. "foo foo-bar" & "foo"
 */
public class CssClassRemover extends AttributeModifier {
    private static final long serialVersionUID = 1L;

    public CssClassRemover(String cssClass) {
        super("class", new Model<String>(cssClass));
    }

    @Override
    protected String newValue(String currentValue, String valueToRemove) {
        return currentValue.replaceAll(valueToRemove, "");
    }
}
