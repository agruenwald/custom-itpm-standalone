package org.cdlflex.itpm.web.backend;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.codec.binary.Base64;
import org.cdlflex.itpm.synchronizer.SynchronizerService;
import org.cdlflex.itpm.synchronizer.SynchronizerServiceImpl;
import org.cdlflex.itpm.synchronizer.standalone.ServiceFactory;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.itpm.web.store.TriggerLoadConfigData;
import org.cdlflex.itpm.web.utils.BaseConfigUtil;
import org.cdlflex.mdd.sembase.MDDException;
import org.openengsb.core.api.Domain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The job is executed as a background thread in Wicket and is executed periodically. Additionally the class provides
 * access to the synchronization service to start and stop it manually.
 */
public class SynchronizerJob implements ServletContextListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(SynchronizerJob.class);
    private static final String SYNC_SETTINGS = "settings-synchronizer";
    private static final String ACTIVE_ON_DEFAULT = "active-on-default";
    private static final Integer STARTUP_DELAY_MINUTES = 1;
    private static boolean isRunning;
    private static final Integer HOURS_MULTIPLICATOR = 601000;

    private static volatile SynchronizerService synchronizerService;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        LOGGER.info("=========> Initialize synchronizer in background listener...");
        LOGGER.info("Test critical depencency (common codec location; lib version must be greater 1.5): {} ",
                Base64.class.getProtectionDomain().getCodeSource().getLocation());
        isRunning = false;
        new BaseConfigUtil().setDefaultBaseConfigPath();
        try {
            TriggerLoadConfigData.loadAppDataFlushOntologisIfEmpty();
            synchronizerService = new SynchronizerServiceImpl(new ConnectionUtil().getFactory());
            Map<String, List<Domain>> connectors =
                new ServiceFactory(new ConnectionUtil().getFactory(), new ConnectionUtil().getFreshITPMConnection(),
                        new ConnectionUtil().getConfigLoader()).createDefaultServices();
            synchronizerService.setServiceMap(connectors);
            // enable to activate the service
            restart(true, false);
        } catch (MDDException e) {
            LOGGER.error(String.format("==========> Failed to start synchronization service: %s", e.getMessage()));
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        stop();
    }

    /**
     * Restart the synchronizer immediately without any delay.
     */
    public static void restart() {
        restart(false, false);
    }

    /**
     * Restart the synchronizer based on the config settings.
     * 
     * @param wait if true then a waiting period is introduced to avoid clashes
     * @param force if true then a start will be forced no matter what the state within the configuration file is.
     *        during the startup of the Web app.
     */
    public static void restart(boolean wait, boolean force) {
        boolean isActiveOnDefault = force;
        if (!force) {
            try {
                String active =
                    new ConnectionUtil().getConfigLoader().findPropertySmart(SYNC_SETTINGS, ACTIVE_ON_DEFAULT);
                isActiveOnDefault = active.isEmpty() ? isActiveOnDefault : Boolean.parseBoolean(active);
            } catch (MDDException e) {
                LOGGER.error(e.getMessage());
            }
        }
        if (isActiveOnDefault) {
            if (wait) {
                LOGGER.info(
                        "======> Synchronizer will be started in {} minute(s) (to avoid clashes during startup).",
                        STARTUP_DELAY_MINUTES);
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(STARTUP_DELAY_MINUTES * HOURS_MULTIPLICATOR);
                            synchronizerService.start();
                            isRunning = true;
                            LOGGER.info("=========> Started synchronizer...");
                        } catch (InterruptedException e) {
                            LOGGER.info("=========> Woke up.", e.getMessage());
                        }
                    }
                }.start();
            } else {
                synchronizerService.start();
                isRunning = true;
                LOGGER.info("=========> Started synchronizer...");
            }
            LOGGER.info("Test path of ooxml: "
                + org.openxmlformats.schemas.spreadsheetml.x2006.main.CTWorksheet.class.getProtectionDomain()
                        .getCodeSource().getLocation());
        } else {
            LOGGER.info("=========> Synchronizer is currently deactivated.");
        }
    }

    /**
     * Stop the synchronizer job properly. Always use this method and not the synchronizer stop method when accessing
     * the service in the Web UI!
     */
    public static void stop() {
        try {
            LOGGER.info("=========> Shutting down synchronizer...");
            synchronizerService.stop();
            isRunning = false;
        //CHECKSTYLE:OFF
        } catch (Exception e) {
            LOGGER.warn(String.format("Could not shut down synchronizer. Maybe it was already down (%s).",
                    e.getMessage()));
        }
        //CHECKSTYLE:ON
    }

    /**
     * Get the running state.
     * @return if true then the background process is running.
     */
    public static boolean isRunning() {
        return isRunning;
    }

}
