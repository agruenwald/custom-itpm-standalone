package org.cdlflex.itpm.web.queryEditor.accordionStructure;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.cdlflex.itpm.generated.model.PMRule;
import org.cdlflex.itpm.generated.model.SPARQL;

/**
 * Wicket model which carries information about rules/query for the query editor.
 */
public class RuleModel implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(RuleModel.class);

    private PMRule pmRule;
    private String knowledgeArea;
    private String output;
    private String format;

    public RuleModel() {
        PMRule rule = new PMRule();
        rule.setFormat(new SPARQL());
        pmRule = rule;
        this.knowledgeArea = "";
        this.output = "";
        this.format = "";
    }

    public RuleModel(PMRule rule) {
        pmRule = rule;
        this.knowledgeArea = rule.getBelongsTo() == null ? "" : rule.getBelongsTo().getName();
        this.output = rule.getOutput().getId();
        this.format = rule.getFormat().getId();

    }

    public PMRule getRule() {
        return this.pmRule;
    }

    public void initPMRule() {
        pmRule.setId(0);
        pmRule.setName("");
        pmRule.setDescription("");
        pmRule.setRuleDefinition("");
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getKnowledgeArea() {
        return knowledgeArea;
    }

    public void setKnowledgeArea(String knowledgeArea) {
        this.knowledgeArea = knowledgeArea;
    }

    public String getName() {
        return pmRule.getName();
    }

    public void setName(String name) {
        this.pmRule.setName(name);
    }

    public String getDescription() {
        return this.pmRule.getDescription();
    }

    public void setDescription(String description) {
        this.pmRule.setDescription(description);
    }

    public String getId() {
        return String.valueOf(this.pmRule.getId());
    }

    public void setId(String id) {
        try {
            this.pmRule.setId(Integer.parseInt(id));
        } catch (Exception e) {
            logger.error("Could not parse id in RuleModel (id=" + id + ").");
        }
    }

    public String getDefinition() {
        return this.pmRule.getRuleDefinition();
    }

    public void setDefinition(String definition) {
        this.pmRule.setRuleDefinition(definition);
    }

    public void setOrder(int order) {
        this.pmRule.setOrder(order);
    }

    public int getOrder() {
        return this.pmRule.getOrder();
    }

    public void setDashboard(boolean db) {
        this.pmRule.setDashboard(db);
    }

    public boolean isDashboard() {
        return pmRule.getDashboard();
    }
}
