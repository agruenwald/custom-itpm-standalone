package org.cdlflex.itpm.web.standalone;

import java.util.LinkedList;
import java.util.List;

import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

/**
 * To follow the structure of the CDLFlex Bus environment and to ease integration the CSS and Javascript bundles are
 * loaded explicitly during startup. Use this class to add any CSS or JS source which is used within the application.
 * 
 */
public class AhyBrandProviderMockup {
    private CssResourceReference getStyleSheet(String path) {
        return new CssResourceReference(WicketApplication.class, path);
    }

    public JavaScriptResourceReference getJavaScript(String path) {
        return new JavaScriptResourceReference(WicketApplication.class, path);
    }

    public List<CssResourceReference> getCssReferences() {
        List<CssResourceReference> list = new LinkedList<CssResourceReference>();
        list.add(getStyleSheet("css/bootstrap.css")); // replace with .min to improve performance
        list.add(getStyleSheet("css/context.bootstrap.css"));
        list.add(getStyleSheet("css/itpm-style.css"));
        list.add(getStyleSheet("css/flexbox.css"));
        list.add(getStyleSheet("css/show-hint.css"));
        list.add(getStyleSheet("css/codemirror.css"));
        list.add(getStyleSheet("css/ambiance-adapted.css"));
        list.add(getStyleSheet("css/show-hint.css"));
        list.add(getStyleSheet("css/uml/graph.css"));
        list.add(getStyleSheet("css/select2/select2.css"));
        list.add(getStyleSheet("css/select2/select2-bootstrap.css"));
        list.add(getStyleSheet("css/activity-stream.css"));
        return list;
    }

    public List<JavaScriptResourceReference> getJavaScriptReferences() {
        /* base page - cdlflex framework */
        List<JavaScriptResourceReference> list = new LinkedList<JavaScriptResourceReference>();
        list.add(getJavaScript("js/jquery.min.js"));
        list.add(getJavaScript("js/jquery_extend.js")); // for drag and drop functionality
        list.add(getJavaScript("js/jquery-ui.js"));
        list.add(getJavaScript("js/bootstrap.min.js"));
        list.add(getJavaScript("js/bootbox.min.js"));
        list.add(getJavaScript("js/swfobject.js"));
        list.add(getJavaScript("js/context.js"));
        list.add(getJavaScript("js/codemirror.js"));
        list.add(getJavaScript("js/show-hint.js"));
        list.add(getJavaScript("js/matchbrackets.js"));
        list.add(getJavaScript("js/ql-api.js"));
        list.add(getJavaScript("js/sparql.js"));
        list.add(getJavaScript("js/agruenwald.js"));
        list.add(getJavaScript("js/generic/flexTable.js"));

        /* metamodel editor */
        list.add(getJavaScript("js/metamodel/MetaElement.js"));
        list.add(getJavaScript("js/metamodel/MetaComment.js"));
        list.add(getJavaScript("js/metamodel/MetaClass.js"));
        list.add(getJavaScript("js/metamodel/MetaAttribute.js"));
        list.add(getJavaScript("js/metamodel/MetaClassPoint.js"));
        list.add(getJavaScript("js/metamodel/MetaAssociation.js"));
        list.add(getJavaScript("js/metamodel/MetaPackage.js"));
        list.add(getJavaScript("js/metamodel/MetaModel.js"));
        list.add(getJavaScript("js/metamodel/MetaModelUtil.js"));

        list.add(getJavaScript("js/util/utils.js"));
        list.add(getJavaScript("js/uml/shapes.js"));
        list.add(getJavaScript("js/uml/labelHandler.js"));
        list.add(getJavaScript("js/uml/uml.js"));
        list.add(getJavaScript("js/uml/graph.js"));
        list.add(getJavaScript("js/uml/comments.js"));
        list.add(getJavaScript("js/uml/umlfactory.js"));
        list.add(getJavaScript("js/graphmap/undo.js"));
        list.add(getJavaScript("js/uml/mathprocessor.js"));
        list.add(getJavaScript("js/uml/menu.js"));

        /* flexbox graphs (on top of metamodel editor JS API) */
        list.add(getJavaScript("js/flexbox/flexbox.js"));
        list.add(getJavaScript("js/flexbox/flexbox_factory.js"));
        list.add(getJavaScript("js/flexbox/tree_layout.js"));
        list.add(getJavaScript("js/flexbox/other_layouts.js"));

        /* Other scripts */
        list.add(getJavaScript("js/ajax-chosen.js"));
        list.add(getJavaScript("js/bootstrap-datepicker.js"));
        list.add(getJavaScript("js/chosen.jquery.min.js"));
        list.add(getJavaScript("js/select2/select2.js"));

        list.add(getJavaScript("js/chart.min.js"));
        list.add(getJavaScript("js/activity-stream.js"));

        return list;
    }
}
