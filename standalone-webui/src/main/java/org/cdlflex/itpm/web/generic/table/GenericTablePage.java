package org.cdlflex.itpm.web.generic.table;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.ISortableDataProvider;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.cdlflex.itpm.generated.model.PMRule;
import org.cdlflex.itpm.web.queryEditor.QueryEditorIntegratedPanel;
import org.cdlflex.itpm.web.queryEditor.QueryEditorStandalonePanel;
import org.cdlflex.itpm.web.queryEditor.RuleService;
import org.cdlflex.itpm.web.reusable.GenericSortableDataProvider;
import org.cdlflex.itpm.web.reusable.InteractiveTableColumn;
import org.cdlflex.itpm.web.reusable.NotificationPanelBootstrap;
import org.cdlflex.itpm.web.standalone.BasePage;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.openengsb.core.api.security.annotation.SecurityAttribute;
import org.ops4j.pax.wicket.api.PaxWicketMountPoint;

/**
 * The generic table page visualizes all kinds of generic data. This can be - generic metamodel data defined via the
 * Semantic Web Editor - query results (manually defined queries) Tables as well as graphs are supported for manually
 * defined queries. A navigation menu with search functionality is provided on the right-hand side. The queries can be
 * edited on-the-fly.
 */
@PaxWicketMountPoint(mountPoint = "rules")
@SecurityAttribute(key = "org.openengsb.ui.component", value = "USER_ADMIN")
public class GenericTablePage extends BasePage {
    private static final long serialVersionUID = 1L;
    private static final String DEFAULT_TITLE = "Generic Table";
    private PMRule selectedRule;
    private NotificationPanelBootstrap notificationPanel;
    protected final List<Map<String, String>> data;
    private final GenericTableNavPanel navigation;
    @SpringBean
    private RuleService ruleService;

    public GenericTablePage() {
        this(new PageParameters());
    }

    @SuppressWarnings({ "rawtypes" })
    public GenericTablePage(final PageParameters parameters) {
        this.data = new ArrayList<Map<String, String>>();
        this.selectedRule = null;
        notificationPanel = new NotificationPanelBootstrap("infoFeedback");
        add(notificationPanel);

        GenericTablePanel genericTablePanel = null;
        boolean renderNoDataTable = false;
        final DefaultDataTable table;
        Label description;
        if (parameters.get("view").toString("").length() > 0) {
            navigation =
                new GenericTableNavPanel("table-navpanel", parameters, GenericTablePage.class, this,
                        genericTablePanel, true);
            this.add(navigation);
            MetaClass metaClass =
                navigation.renderMetaModelMenu(parameters.get("view").toString(""),
                        new ConnectionUtil().getOpenConnection(parameters.get("view").toString("")),
                        !Boolean.parseBoolean(parameters.get("expandedmenu").toString("false")));
            if (metaClass == null) {
                // no classes available
                Label noData = new Label("generic-table");
                add(noData);
                noData.setVisible(false);
                renderNoDataTable = true;
                add(new Label("headertitle", DEFAULT_TITLE));
            } else {
                genericTablePanel =
                    new GenericTablePanel("generic-table", parameters.get("view").toString(""), metaClass,
                            navigation.getSearchForm(), parameters, !parameters.get("recId").toString("").isEmpty());
                navigation.getSearchForm().updateTablePanel(genericTablePanel);
                add(genericTablePanel);
                add(new Label("headertitle", genericTablePanel.getTitle()));
            }
            List<Map<String, String>> emptyTableData = new ArrayList<Map<String, String>>();
            table = createEmptyTable(emptyTableData);
            Label placeHolderLabel = new Label("flexgraph");
            placeHolderLabel.setVisible(false);
            add(placeHolderLabel);
            description = new Label("description");
            add(description);
            description.setVisible(false);
            Label fakeQueryEditor = new Label("queryEditor", "");
            fakeQueryEditor.setVisible(false);
            add(fakeQueryEditor);

            add(table);
            if (!renderNoDataTable) {
                table.setVisible(false);
            }
        } else {
            renderNoDataTable = true;
            QueryTablePanel queryTable = new QueryTablePanel("generic-table", parameters);
            navigation =
                new GenericTableNavPanel("table-navpanel", parameters, GenericTablePage.class, this, queryTable, true);
            this.add(navigation);
            selectedRule = navigation.renderRuleMenu();
            queryTable.renderPanel(navigation.getSearchForm(), selectedRule);

            add(queryTable);
            add(new Label("headertitle", queryTable.getTitle()));
            description = new Label("description", selectedRule == null ? "" : selectedRule.getDescription());
            add(description);
            description.setVisible(selectedRule != null);

            // integrated query editor.
            QueryEditorStandalonePanel queryEditor =
                new QueryEditorStandalonePanel("query-editor", notificationPanel, selectedRule, ruleService);
            QueryEditorIntegratedPanel integratedQueryEditor =
                new QueryEditorIntegratedPanel("queryEditor", description, queryEditor, queryTable);
            add(integratedQueryEditor);
        }
    }

    public DefaultDataTable<Map<String, String>, String> createEmptyTable(final List<Map<String, String>> data) {
        ISortableDataProvider<Map<String, String>, String> dataProvider = new GenericSortableDataProvider(data);
        List<InteractiveTableColumn> columns = new ArrayList<InteractiveTableColumn>();
        columns.add(new InteractiveTableColumn(Model.of("-"), "-"));
        DefaultDataTable<Map<String, String>, String> defaultDataTable =
            new DefaultDataTable<Map<String, String>, String>("dataTable", columns, dataProvider, 20);
        defaultDataTable.setOutputMarkupId(true);
        return defaultDataTable;
    }
}
