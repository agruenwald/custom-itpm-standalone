package org.cdlflex.itpm.web.generic.chart;

import java.util.Comparator;

import org.cdlflex.itpm.generated.model.PMRule;

/**
 * Sort rules based on their order number.
 */
public class DashboardComponentSorter implements Comparator<PMRule> {

    @Override
    public int compare(PMRule rule1, PMRule rule2) {
        return rule1.getOrder() - rule2.getOrder();
    }

}
