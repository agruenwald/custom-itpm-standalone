package org.cdlflex.itpm.web.queryEditor.accordionStructure;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.IFormModelUpdateListener;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.cdlflex.itpm.generated.model.PMRule;
import org.cdlflex.itpm.web.queryEditor.QueryEditorForm;
import org.cdlflex.itpm.web.queryEditor.QueryEditorPage;
import org.cdlflex.itpm.web.reusable.NotificationPanelBootstrap;
import org.cdlflex.mdd.sembase.MDDException;

/**
 * Represents the inner accordion (second level) which contains a header with the query/rule name and encapsulates the
 * modification form.
 */
public class InnerQueryPanel extends Panel implements IFormModelUpdateListener {
    private static final long serialVersionUID = 1L;
    private FeedbackPanel feedbackPanel;

    public InnerQueryPanel(String panelId, final PMRule rule, final QueryEditorPage rulePageRef,
            SingleOuterKnowledgeAreaPanel accordionPartPanel) {
        super(panelId);
        add(new Label("rule_name", rule.getName()));

        feedbackPanel = new NotificationPanelBootstrap("feedback_edit");
        this.initFeedbackPanel(feedbackPanel);

        this.setOutputMarkupId(true);
        feedbackPanel.setFilter(new FilterError());

        AjaxLink<?> removeLink = new AjaxLink<Void>("remove") {
            private static final long serialVersionUID = 1L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                try {
                    rulePageRef.getRuleService().removeRule(rule);
                    success(String.format("Removed rule \"%s\".", rule.getName()));
                    target.add(rulePageRef.getInfoFeedback());
                    target.add(rulePageRef);
                    target.add(rulePageRef.getTabsContainer());
                } catch (MDDException e) {
                    error(e.getMessage());
                    target.add(feedbackPanel);
                }
            }
        };
        add(removeLink);
        QueryEditorForm form =
            new QueryEditorForm("rule_form", accordionPartPanel.getKnowledgeAreaName(), feedbackPanel, rule,
                    rulePageRef.getRuleService(), rulePageRef);
        add(form);

    }

    private void initFeedbackPanel(FeedbackPanel feedbackPanel) {
        feedbackPanel.setOutputMarkupId(true);
        this.feedbackPanel = feedbackPanel;
        add(this.feedbackPanel);
    }

    @Override
    public void updateModel() {
        // nothing to do here
    }
}
