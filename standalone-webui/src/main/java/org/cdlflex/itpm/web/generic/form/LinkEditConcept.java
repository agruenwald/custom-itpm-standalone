package org.cdlflex.itpm.web.generic.form;

import java.util.Map;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.cdlflex.itpm.web.metamodeltools.MetaModelBrowser;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAOUtil;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaynberg.wicket.select2.Select2MultiChoice;

/**
 * Represents the icon/link which appears to edit records within a subform of the root form.
 */
public class LinkEditConcept extends AjaxLink<String> {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(LinkEditConcept.class);
    private final Map<String, AssociationContainer> containers;
    private final GenericFormPanel subForm;
    private final String modelName;
    private final Select2MultiChoice<OptionEntry> select2Elem;
    private final FormBuilderMetaClass formBuilder;

    public LinkEditConcept(String panelId, String modelName, Map<String, AssociationContainer> containers,
            GenericFormPanel subForm, Select2ClickBehavior tmpSelect2ClickBehavior,
            Select2MultiChoice<OptionEntry> select2Elem, FormBuilderMetaClass formBuilder) {
        super(panelId);
        this.setOutputMarkupId(true);
        this.setOutputMarkupPlaceholderTag(true);
        this.setVisible(false);
        this.containers = containers;
        this.subForm = subForm;
        this.modelName = modelName;
        this.select2Elem = select2Elem;
        this.formBuilder = formBuilder;
    }

    @Override
    public void onClick(AjaxRequestTarget target) {
        MetaAssociation association = formBuilder.getEditSelectClickBehavior().getMetaAssociation();
        OptionEntry oE = formBuilder.getEditSelectClickBehavior().getOptionEntry();
        String extractedMetaClassName = DAOUtil.deserializeMetaClassFromBean(oE.getId());
        String extractedId = DAOUtil.deserializeIdFromBean(oE.getId());
        MetaClass extractedMc = null;
        try {
            extractedMc =
                new MetaModelBrowser().getMetaModelClasses(modelName).findClassByName(extractedMetaClassName);
            FormBuilderMetaClass subFormBuilder = containers.get(association.getName()).getSubForm().getFormBuilder();
            containers.get(association.getName()).getSubForm().updateButton(true, true);
            subFormBuilder.getRepeatingView().removeAll();
            subFormBuilder.buildForm(subFormBuilder.getRepeatingView(), modelName, extractedMc, association,
                    extractedId);
            target.add(subFormBuilder.getRepeatingView().getParent());
            subForm.setVisible(!subForm.isVisible());
            select2Elem.setVisible(!subForm.isVisible());
            target.add(subForm.getParent());
            if (!subForm.isVisible()) {
                this.setVisible(false);
            }
        } catch (MDDException e) {
            logger.error(e.getMessage());
            return;
        }
    }
};
