package org.cdlflex.itpm.web.generic.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.WordUtils;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.config.generated.model.SimpleDataField;
import org.cdlflex.itpm.generated.mddconnector.MDDFunctions;
import org.cdlflex.itpm.web.generic.table.GenericTablePage;
import org.cdlflex.itpm.web.metamodeltools.MetaModelBrowser;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAO;
import org.cdlflex.mdd.sembase.semaccess.DAOUtil;
import org.cdlflex.mdd.sembase.semaccess.MDDBeanInterface;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAttribute;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A bridge between forms and the MDD approach to modify nested entities via the Web-UI.
 */
public class MDDFormBridge {
    private static final Logger logger = LoggerFactory.getLogger(MDDFormBridge.class);
    /** can be used to simulate updates (for testing) **/
    private static final boolean FAKE_UPDATES = false;
    private static final String FAKE_LOG_PREFIX = FAKE_UPDATES ? "***** FAKED ACTION: " : "";
    private static final String ITPM_DASHBOARD_ORIGIN = "ITPM Web Dashboard";
    private static final MDDFunctions MDD_FUNCTIONS = new MDDFunctions();
    private static final MetaModelBrowser MDD_BROWSER = new MetaModelBrowser();
    private static final ConnectionUtil CU = new ConnectionUtil();
    private MetaClass metaClassTypeOfBean;
    private FormBuilderMetaClass rootFormBuilder;
    private String modelName;
    private List<AttributeContainer> attributeContainers;
    private Map<String, AssociationContainer> associationContainers;

    public MDDFormBridge(String modelName, MetaClass metaClassTypeOfBean, FormBuilderMetaClass rootFormBuilder,
            List<AttributeContainer> attributeContainers, Map<String, AssociationContainer> associationContainers) {
        this.metaClassTypeOfBean = metaClassTypeOfBean;
        this.rootFormBuilder = rootFormBuilder;
        this.modelName = modelName;
        this.attributeContainers = attributeContainers;
        this.associationContainers = associationContainers;
    }

    public FormBuilderMetaClass getRootFormBuilder() {
        return rootFormBuilder;
    }

    /**
     * Deserialize the form input into a Java bean which can be processed by the generated MDD DAOs and the
     * MMDBeanConnector (takes care about creating and linking associations, etc.) The data is first marshalled into
     * adequate data source configuration format and then automatically processed by the MDD connector.
     * 
     * @param button
     */
    public void processOnSubmit(Button button, AjaxRequestTarget target, Form<?> form, boolean isSubForm,
        String selectedItemId, MetaAssociation subBeanAssociation) {
        DataSourceConfig configBridge;
        try {
            configBridge =
                new ConnectionUtil().getFactory().createBean(DataSourceConfig.class,
                        new ConnectionUtil().getOpenConfigConnection());
            configBridge.setOriginArea(ITPM_DASHBOARD_ORIGIN);

            // process attributes
            List<String> values = new ArrayList<String>();
            int ndx = 1;
            for (AttributeContainer attrContainer : attributeContainers) {
                FormComponent<?> tf = attrContainer.getAttributeValue();
                SimpleDataField simpleField = new SimpleDataField();
                simpleField.setConceptAttributeName(attributeContainers.get(ndx - 1).getAttributeName());
                simpleField.setPos(ndx);
                configBridge.getHas().add(simpleField);
                values.add(tf.getValue());
                if (!isSubForm) {
                    updateRootBeanAttribute(metaClassTypeOfBean, getRootFormBuilder().getRootBean(),
                            attrContainer.getAttributeName(), tf.getValue());
                }
                ndx++;
            }

            if (!isSubForm) {
                MDDBeanInterface rootBean = getRootFormBuilder().getRootBean();
                Iterator<Entry<String, AssociationContainer>> it = associationContainers.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<String, AssociationContainer> entry = it.next();
                    String assocName = entry.getKey();
                    AssociationContainer container = entry.getValue();
                    logger.info(String.format("Assoc %s: %s/%s selected.", assocName, container.getSelected().size(),
                            container.getAllChoiches().size()));

                    MetaAssociation association =
                        getAssociationByName(
                                MDD_BROWSER.getMDDSimpleAccess().getAllSuperAssociations(metaClassTypeOfBean),
                                assocName);
                    if (association == null) {
                        throw new MDDException(String.format("Failed to find association %s at bean %s.", assocName,
                                metaClassTypeOfBean));
                    }
                    // determine the removed entries
                    List<MDDBeanInterface> removedLinks = this.getRemovedLinks(rootBean, association, container);
                    // remove the links from the root bean instance
                    this.removeLinksFromBean(rootBean, association, removedLinks);
                    for (MDDBeanInterface bean : removedLinks) {
                        logger.info("====> Removed {}", bean.getDisplayFriendlyName());
                        if (association.isComposition()) {
                            // physically remove bean
                            DAO<MDDBeanInterface> dao =
                                CU.getFactory().create(DAOUtil.deserializeMetaClassFromBean(bean.toString()),
                                        CU.getOpenConnection(modelName));
                            MDDBeanInterface prevBean = dao.readRecordViaBean(bean);
                            if (prevBean == null) {
                                throw new MDDException(String.format(
                                        "Removed composition bean \"%s\" does not exist. Has it been"
                                            + " removed by another user meanwhile?", bean.getDisplayFriendlyName()));
                            } else {
                                if (!FAKE_UPDATES) {
                                    dao.remove(bean);
                                }
                            }
                            logger.info(String.format(
                                    "%sPhysically removed referenced bean \"%s\" because it is a compositino.",
                                    FAKE_LOG_PREFIX, bean.getDisplayFriendlyName()));
                        }
                    }

                    if (association.getTo().getMetaClass().isEnumClass()) {
                        for (OptionEntry oE : container.getSelected()) {
                            MDDBeanInterface bean = oE.getOptionalBean();
                            bean = CU.getFactory().createBean(oE.getValue(), CU.getOpenConnection(modelName));
                            addBeanToParent(this.getRootFormBuilder().getRootBean(), bean, association);
                        }
                    } else {
                        for (OptionEntry oE : container.getSelected()) {
                            MDDBeanInterface bean = oE.getOptionalBean();
                            DAO<MDDBeanInterface> dao =
                                CU.getFactory().create(DAOUtil.deserializeMetaClassFromBean(bean.toString()),
                                        CU.getOpenConnection(modelName));
                            MDDBeanInterface prevBean = dao.readRecordViaBean(bean);
                            if (prevBean == null) {
                                if (!FAKE_UPDATES) {
                                    dao.insert(bean);
                                }
                                logger.info(String.format("%sInserted reference bean %s.", FAKE_LOG_PREFIX,
                                        bean.getDisplayFriendlyName()));
                            } else {
                                boolean hasChanged = dao.merge(bean, prevBean, false);
                                if (hasChanged) {
                                    if (!FAKE_UPDATES) {
                                        bean.setLastChange(new Date());
                                        dao.update(bean);
                                    }
                                    logger.info(String.format("%sUpdated reference bean %s.", FAKE_LOG_PREFIX,
                                            bean.getDisplayFriendlyName()));
                                }
                            }
                            // those entries which already existed but haven't been created newly.
                            addBeanToParent(this.getRootFormBuilder().getRootBean(), bean, association);
                        }
                    }
                }
            }

            MetaClass metaClass = this.metaClassTypeOfBean;
            try {
                if (isSubForm) {
                    processSubform(button, target, form, selectedItemId, subBeanAssociation);
                } else {
                    DAO<MDDBeanInterface> dao =
                        CU.getFactory().create(DAOUtil.deserializeMetaClassFromBean(metaClassTypeOfBean.toString()),
                                CU.getOpenConnection(modelName));
                    Map<String, String> errors =
                        dao.validate(rootFormBuilder.getRootBean(), selectedItemId.isEmpty());
                    addErrorNotifications(errors, button);
                    if (errors.isEmpty()) {
                        String action = "";
                        if (selectedItemId.isEmpty()) {
                            if (!FAKE_UPDATES) {
                                dao.insert(rootFormBuilder.getRootBean());
                                action = "added";
                            }
                            logger.info(String.format("%sInserted root bean \"%s\".", FAKE_LOG_PREFIX,
                                    rootFormBuilder.getRootBean().getDisplayFriendlyName()));
                        } else {
                            if (!FAKE_UPDATES) {
                                rootFormBuilder.getRootBean().setLastChange(new Date());
                                dao.update(rootFormBuilder.getRootBean());
                                action = "updated";
                            }
                            logger.info(String.format("%sUpdated root bean \"%s\".", FAKE_LOG_PREFIX, rootFormBuilder
                                    .getRootBean().getDisplayFriendlyName()));
                        }
                        /*
                         * MDDBeanInterface bi = callMetaConnectorUpstream(metaClass, modelName, configBridge, values);
                         */
                        button.info(String.format("Successfully %s %s \"%s\".", action, WordUtils
                                .uncapitalize(metaClass.getOriginalName()), rootFormBuilder.getRootBean()
                                .getDisplayFriendlyName()));
                        form.clearInput();
                        boolean returnHard = false;
                        if (returnHard) {
                            RequestCycle.get().setResponsePage(GenericTablePage.class,
                                    new PageParameters().add("view", modelName));
                        } else {
                            // refresh table and then show overview
                            rootFormBuilder.getRootDataContainer()
                                    .readGenericData(modelName, metaClass.getName(), "");
                            rootFormBuilder.getRootDataContainer().showOverview();
                            target.add(form.getPage());
                        }
                    }
                }

            } catch (MDDException e) {
                button.error(String.format("Failed to insert/update %s: %s",
                        WordUtils.uncapitalize(metaClass.getOriginalName()), e.getMessage()));
            }
        } catch (MDDException e1) {
            button.error(e1.getMessage());
            if (isSubForm) {
                return;
            }
        }
        target.add(form);
    }

    private MetaAssociation getAssociationByName(Map<MetaAssociation, MetaClass> allSuperAssociations,
        String assocName) {
        for (MetaAssociation a : allSuperAssociations.keySet()) {
            if (a.getName().equals(assocName)) {
                return a;
            }
        }
        return null;
    }

    private void processSubform(Button button, AjaxRequestTarget target, Form<?> form, String selectedItemId,
        MetaAssociation subBeanAssociation) throws MDDException {
        final MDDFunctions MDD_FUNCTIONS = new MDDFunctions();
        final boolean isUpdate = !selectedItemId.isEmpty();
        MDDBeanInterface addedObject = null;
        DAO<Object> dao =
            new ConnectionUtil().getFactory().create(metaClassTypeOfBean.getName().replaceAll("\\s", ""),
                    new ConnectionUtil().getOpenConnection(modelName));
        try {
            if (!isUpdate) {
                addedObject =
                    new ConnectionUtil().getFactory().createBean(metaClassTypeOfBean.getName().replaceAll("\\s", ""),
                            new ConnectionUtil().getOpenConnection(modelName));
                addedObject.setOrigin("ITPM Web Dashboard");
            } else {
                addedObject = (MDDBeanInterface) dao.readRecord(selectedItemId);
                if (addedObject == null) {
                    throw new MDDException(String.format(
                            "Entry with id %s not found. Unexpected behavior. Contact the admin.", selectedItemId));
                }
            }
        } catch (MDDException e1) {
            button.error(e1.getMessage());
            return;
        }
        for (int i = 0; i < attributeContainers.size(); i++) {
            FormComponent<?> tf = attributeContainers.get(i).getAttributeValue();
            String type =
                this.getAttributeByName(metaClassTypeOfBean, attributeContainers.get(i).getAttributeName())
                        .getRange();
            MDD_FUNCTIONS.setBeanAttributeDynamically(addedObject, attributeContainers.get(i).getAttributeName(),
                    tf.getValue(), type);
        }

        dao.assignGeneratedId(addedObject);
        Map<String, String> errors = dao.validate(addedObject, false);
        if (!errors.isEmpty()) {
            addErrorNotifications(errors, button);
        } else {
            // currently no nested associations over more than 2 levels supported
            addBeanToParent(getRootFormBuilder().getRootBean(), addedObject, subBeanAssociation);
            OptionEntry addedOptionEntry =
                new OptionEntry(addedObject.toString(), addedObject.getDisplayFriendlyName());
            addedOptionEntry.setOptionalBean(addedObject);
            addedOptionEntry.setOptionalMetaClass(metaClassTypeOfBean);
            // without removing the previous entry the UI field will not be updated.
            AssociationContainer assocContainer =
                this.getRootFormBuilder().getAssociationContainerByName(subBeanAssociation.getName());
            assocContainer.getAllChoiches().remove(addedOptionEntry); // if available
            assocContainer.getSelected().remove(addedOptionEntry); // if available
            assocContainer.getAllChoiches().add(addedOptionEntry);
            assocContainer.getSelected().add(addedOptionEntry);
            form.getParent().setVisible(false);
            assocContainer.getSelect2().setVisible(true);
            target.add(form.getParent().getParent());
        }
    }

    private void addBeanToParent(MDDBeanInterface rootBean, MDDBeanInterface addedObject,
        MetaAssociation subBeanAssociation) throws MDDException {
        if (!subBeanAssociation.getTo().getMetaClass().isEnumClass()) {
            MDD_FUNCTIONS.getBeanAssociationDynamically(getRootFormBuilder().getRootBean(),
                    subBeanAssociation.getName()).remove(addedObject);
        }
        MDD_FUNCTIONS.setBeanAssociationDynamically(getRootFormBuilder().getRootBean(), addedObject,
                subBeanAssociation);
    }

    /**
     * Get those subform entries which have been removed by the user. The determination algorithm is: Those entries
     * which are still linked in the root bean but which do not appear in the select2 selection any more have been
     * removed by the user.
     * 
     * @param rootBean the root bean
     * @param association the specific association
     * @param container the association container
     * @return
     * @throws MDDException
     */
    private List<MDDBeanInterface> getRemovedLinks(MDDBeanInterface rootBean, MetaAssociation association,
        AssociationContainer container) throws MDDException {
        List<MDDBeanInterface> removedBeans = new ArrayList<MDDBeanInterface>();
        Set<?> linkedObjects = null;
        try {
            linkedObjects = MDD_FUNCTIONS.getBeanAssociationDynamically(rootBean, association.getName());
        } catch (NullPointerException e) {
            logger.info(String.format("Failed to access association %s from bean %s.", association.getName(),
                    rootBean.getDisplayFriendlyName()));
            throw e;
        }
        linkedObjects.remove(null);
        for (Object o : linkedObjects) {
            MDDBeanInterface linkedBean = (MDDBeanInterface) o;
            boolean beanSelected = false;
            for (OptionEntry sel : container.getSelected()) {
                // ignorecase again is for enums
                if (association.getTo().getMetaClass().isEnumClass()) {
                    if (sel.getId().equalsIgnoreCase(linkedBean.toString())) {
                        beanSelected = true;
                        break;
                    }
                } else if (sel.getOptionalBean().toString().equalsIgnoreCase(linkedBean.toString())) {
                    beanSelected = true;
                    break;
                }
            }
            if (!beanSelected) {
                removedBeans.add(linkedBean);
            } else {
                // add it to the record (?)
            }
        }
        return removedBeans;
    }

    /**
     * Added or updated sub-beans must be submitted and hence the changes can be added immediately to the root bean. For
     * removed beans this is not the case hence they must be removed from the root bean during the submission of the
     * root form.
     * 
     * @param rootBean
     * @param association
     * @param removedLinks the removed links of the specific association
     * @throws MDDException
     */
    private void removeLinksFromBean(MDDBeanInterface rootBean, MetaAssociation association,
        List<MDDBeanInterface> removedLinks) throws MDDException {
        for (MDDBeanInterface bean : removedLinks) {
            if (association.isMaximumRangeGreaterThan(1)) {
                MDD_FUNCTIONS.getBeanAssociationDynamically(rootBean, association.getName()).remove(bean);
            } else {
                MDD_FUNCTIONS.setBeanAssociationDynamically(rootBean, null, association);
            }
        }
    }

    /**
     * Update the attribute field value in the attribute
     * 
     * @param rootBean
     * @param attributeName
     * @param attributeValue
     * @throws MDDException
     */
    private void updateRootBeanAttribute(MetaClass metaClass, MDDBeanInterface rootBean, String attributeName,
        String attributeValue) throws MDDException {
        try {
            MetaAttribute attribute = this.getAttributeByName(metaClass, attributeName);
            if (attribute == null) {
                throw new MDDException(String.format("Could not find attribute \"%s\" for metaClass %s.",
                        attributeName, metaClass.getOriginalName()));
            }
            MDD_FUNCTIONS.setBeanAttributeDynamically(rootBean, attributeName, attributeValue, attribute.getRange());
        } catch (IllegalArgumentException e) {
            throw new MDDException(String.format("Invalid value for \"%s\": %s (%s).", attributeName, attributeValue,
                    e.getMessage()));
        }
    }

    private MetaAttribute getAttributeByName(MetaClass metaClass, String name) {
        for (MetaAttribute a : new MetaModelBrowser().getMDDSimpleAccess().getAllSuperAttributes(metaClass)) {
            if (a.getOriginalName().equals(name)) {
                return a;
            }
        }
        return null;
    }

    public void addErrorNotifications(Map<String, String> errors, Button button) {
        if (!errors.isEmpty()) {
            for (String msg : errors.values()) {
                String[] words = msg.split(" ");
                String msgNew = "";
                for (String w : words) {
                    msgNew = msgNew.isEmpty() ? WordUtils.capitalize(w) : msgNew + " " + w;
                }
                button.error(msgNew);
            }
        }
    }

    public static MDDBeanInterface createBean(String modelName, MetaClass c) throws MDDException {
        return createBean(modelName, c, "");
    }

    public static MDDBeanInterface createBean(String modelName, MetaClass c, String selectedId) throws MDDException {
        String cName = c.getName().replaceAll("\\s", "");
        MDDBeanInterface beanInstance =
            new ConnectionUtil().getFactory().createBean(cName, new ConnectionUtil().getOpenConnection(modelName));
        if (selectedId.length() > 0) {
            DAO<?> dao =
                new ConnectionUtil().getFactory().create(cName, new ConnectionUtil().getOpenConnection(modelName));
            beanInstance = (MDDBeanInterface) dao.readRecord(selectedId);
        }
        return beanInstance;
    }
}
