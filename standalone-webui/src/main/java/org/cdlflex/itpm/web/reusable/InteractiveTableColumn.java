package org.cdlflex.itpm.web.reusable;

import java.util.Map;

import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

/**
 * Provides the possibility to add text to a table column, and (action) icons as well.
 */
public class InteractiveTableColumn extends AbstractColumn<Map<String, String>, String> implements
        IColumn<Map<String, String>, String> {
    private final String key;

    public InteractiveTableColumn(IModel<String> model, String key) {
        super(model, key);
        this.key = key;
    }

    private static final long serialVersionUID = 1L;

    public IModel<String> getDataModel(IModel<Map<String, String>> rowModel) {
        PropertyModel<String> propertyModel = new PropertyModel<String>(rowModel, key);
        return propertyModel;
    }

    @Override
    public void populateItem(Item<ICellPopulator<Map<String, String>>> item, String componentId,
        IModel<Map<String, String>> rowModel) {
        String x = this.getDataModel(rowModel).getObject();
        if (x != null && x.contains("<") && x.contains(">")) {
            Label icon = new Label(componentId, x);
            icon.setEscapeModelStrings(false);
            item.add(icon);
        } else {
            item.add(new Label(componentId, x));
        }

    }
}
