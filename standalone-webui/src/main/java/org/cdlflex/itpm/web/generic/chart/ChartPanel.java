package org.cdlflex.itpm.web.generic.chart;

import org.apache.wicket.IResourceListener;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptContentHeaderItem;
import org.apache.wicket.markup.html.IHeaderContributor;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.protocol.http.RequestUtils;
import org.apache.wicket.request.handler.resource.ResourceRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.JavaScriptResourceReference;
import org.apache.wicket.request.resource.ResourceReference;
import org.cdlflex.itpm.web.standalone.HomePage;

/**
 * Implementation of the OpenFlash SWF Chart component (Wicket Ajax interface).
 */
public class ChartPanel extends Panel implements IHeaderContributor, IResourceListener {
    private static final ResourceReference OFC = new 
            JavaScriptResourceReference(HomePage.class, "open-flash-chart.swf");
    private AbstractDashboardComponent chart;
    private final Form<String> form;

    public ChartPanel(String id, IModel<?> model, AbstractDashboardComponent chart) {
        super(id, model);
        setOutputMarkupId(false);
        this.setOutputMarkupPlaceholderTag(true);
        this.chart = chart;
        add(new Label("header", chart.getRule().getName()));
        form = new Form<String>("form");
        form.setOutputMarkupId(true);
        add(form);
    }

    /**
     * Get the form element.
     * @return the form.
     */
    public Form<?> getForm() {
        return form;
    }

    @Override
    protected boolean getStatelessHint() {
        return false;
    }

    @Override
    public final void onResourceRequested() {
        String jsonData = getData();
        ByteArrayResource jsonResource = new ByteArrayResource("text/plain", jsonData.getBytes());
        ResourceRequestHandler requestHandler = new ResourceRequestHandler(jsonResource, null);
        requestHandler.respond(getRequestCycle());
    }
    
    /**
     * Get the diagram as a string.
     * @return the diagram as a string.
     */
    public String getData() {
        String res = "";
        if (chart.getTitle().equalsIgnoreCase("DEMO CHART FEATURES")) {
            res =
                "&title=Flash+Chart+Demo,{font-size:26px;}& " + "&x_axis_steps=1&" + "&y_ticks=5,10,5&"
                    + "&line=1,0xB0E050,Jan,12&" + "&line_2=2,0xA0D040,Feb,12&" + "&line_dot_3=3,0x99CC33,Mar,12,5&"
                    + "&values=5,4,6,5,3,4,7,6&" + "&values_2=8,10,9,12,11,12,11,12&"
                    + "&values_3=15,18,18,17,13,16,19,17&"
                    + "&x_labels=week+1,week+2,week+3,week+4,week+5,week+6,week+7,week+8&" + "&y_min=0&"
                    + "&y_max=20&" + "&tool_tip=Visitors%3A+%23val%23&";
        } else {
            return chart.plot();
        }
        return res;
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        this.urlFor(OFC, this.getPage().getPageParameters());
        String dataUrl = urlFor(IResourceListener.INTERFACE, this.getPage().getPageParameters()).toString();
        dataUrl = RequestUtils.toAbsolutePath("", dataUrl);
        String script = "";
        script += "charts.push('" + dataUrl + "');";
        response.render(new JavaScriptContentHeaderItem(script, "", ""));
    }

    private static final long serialVersionUID = 1L;

}
