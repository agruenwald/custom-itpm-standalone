package org.cdlflex.itpm.web.queryEditor.accordionStructure;

import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.feedback.IFeedbackMessageFilter;

public class FilterInfo implements IFeedbackMessageFilter {
    private static final long serialVersionUID = 1L;

    public boolean accept(FeedbackMessage message) {
        return message.getLevel() < FeedbackMessage.WARNING ? true : false;
    }
}
