package org.cdlflex.itpm.web.queryEditor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.cdlflex.itpm.generated.ext.ITPMDAOFactory;
import org.cdlflex.itpm.generated.model.PMRule;
import org.cdlflex.itpm.generated.model.SPARQL;
import org.cdlflex.itpm.web.queryEditor.accordionStructure.AccordionTab;
import org.cdlflex.itpm.web.queryEditor.accordionStructure.FilterInfo;
import org.cdlflex.itpm.web.queryEditor.accordionStructure.IAccordionTab;
import org.cdlflex.itpm.web.queryEditor.accordionStructure.SingleOuterKnowledgeAreaPanel;
import org.cdlflex.itpm.web.reusable.NotificationPanelBootstrap;
import org.cdlflex.itpm.web.standalone.BasePage;
import org.cdlflex.itpm.web.store.ConnectionUtil;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.openengsb.core.api.security.annotation.SecurityAttribute;
import org.ops4j.pax.wicket.api.PaxWicketMountPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Central page for adding, editing and deleting queries and rules in the ITPM project. Each query belongs to a given
 * knowledge area (typically PMBoK knowledge areas such as "Communication Management" or "Human Resource Management".
 * However these knowledge areas can be configured and extended (the page will adapt dynamically). Each query consists
 * of the query definition itself, some description, a format (e.g. SPARQL) and an output type (e.g. the output shall be
 * rendered as a table, as a graph, as a diagram, etc.) As the query editor supports autocompletion features it reuses
 * the data which has been modeled with the Semantic Meta editor.
 */
@PaxWicketMountPoint(mountPoint = "rules")
@SecurityAttribute(key = "org.openengsb.ui.component", value = "USER_ADMIN")
public class QueryEditorPage extends BasePage {
    private static final long serialVersionUID = 2875734486012725351L;
    private static final Logger logger = LoggerFactory.getLogger(QueryEditorPage.class);

    // @PaxWicketBean(name = "ruleService") for EngSb
    @SpringBean
    private RuleService ruleService;

    public static Connection con;
    public static ITPMDAOFactory factory;
    static {
        factory = new ConnectionUtil().getFactory();
        con = new ConnectionUtil().getOpenITPMConnection();
    }
    private static final String SPARQL_AREAS =
        "SELECT ?area (count(?rule) as ?number) WHERE { "
            + "?area  rdfs:subClassOf itpm:KnowledgeArea.  OPTIONAL { ?areaIndividual a ?area. ?areaIndividual ^itpm:belongsTo ?rule }  } "
            + "GROUP BY ?area ORDER BY ?area";

    private List<PMRule> pmRules;
    private Map<String, Integer> knowledgeAreas;
    private WebMarkupContainer tabsContainer;
    private FeedbackPanel infoFeedback;

    public QueryEditorPage() {
        super();
        initContent();
    }

    public QueryEditorPage(PageParameters parameters) {
        super(parameters);
    }

    private void initContent() {
        knowledgeAreas = new HashMap<String, Integer>();
        List<IAccordionTab> iknowl = this.populateRulePage();
        QueryEditorPanel<IAccordionTab> tabs = new QueryEditorPanel<IAccordionTab>("tabs", iknowl);
        this.tabsContainer = tabs.getTabsContainer();
        this.tabsContainer.setOutputMarkupId(true);
        tabs.setOutputMarkupId(true);
        add(tabs);
        infoFeedback = new NotificationPanelBootstrap("infoFeedback");
        infoFeedback.setOutputMarkupId(true);
        this.setOutputMarkupId(true);
        infoFeedback.setFilter(new FilterInfo());
        add(infoFeedback);
    }

    protected List<IAccordionTab> populateRulePage() {
        List<List<String>> knowledgeAreas = new ArrayList<List<String>>();
        try {
            knowledgeAreas =
                new ConnectionUtil().getQueryInterface(new SPARQL().getId(),
                        new ConnectionUtil().getOpenITPMConnection()).query(SPARQL_AREAS);
            for (List<String> row : knowledgeAreas) {
                this.knowledgeAreas.put(row.get(0), Integer.parseInt(row.get(1)));
            }
        } catch (MDDException e) {
            logger.error(e.getMessage());
        }
        try {
            pmRules = new ArrayList<PMRule>(); // init
            pmRules =
                new ConnectionUtil().getFactory().create(PMRule.class, new ConnectionUtil().getOpenITPMConnection())
                        .readFulltext("");
        } catch (MDDException e1) {
            logger.error(e1.getMessage());
            error(e1.getMessage());
        }
        // this.ruleService.readRulesComplete();
        List<IAccordionTab> iknowl = new ArrayList<IAccordionTab>();
        for (final List<String> area : knowledgeAreas) {
            final String a = area.get(0);
            iknowl.add(new AccordionTab(new Model<String>(area.get(0)), a) {
                private static final long serialVersionUID = 1L;

                public Panel getPanel(String panelId) {
                    List<PMRule> rules =
                        QueryEditorPage.this.ruleService.filterRulesByKnowledgeArea(QueryEditorPage.this.pmRules,
                                this.getKnowledgeArea());
                    return new SingleOuterKnowledgeAreaPanel(panelId, a, this, rules, QueryEditorPage.this, this
                            .getKnowledgeArea());
                }

                @Override
                public int getCount() {
                    try {
                        return QueryEditorPage.this.knowledgeAreas.get(this.getKnowledgeArea());
                    } catch (Exception e) {
                        return 0;
                    }
                }

            });
        }
        return iknowl;
    }

    public RuleService getRuleService() {
        return this.ruleService;
    }

    public FeedbackPanel getInfoFeedback() {
        return this.infoFeedback;
    }

    public Map<String, Integer> getKnowledgeAreas() {
        return this.knowledgeAreas;
    }

    public List<PMRule> getPmRules() {
        return pmRules;
    }

    public WebMarkupContainer getTabsContainer() {
        return tabsContainer;
    }
}
