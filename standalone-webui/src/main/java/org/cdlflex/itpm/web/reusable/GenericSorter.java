package org.cdlflex.itpm.web.reusable;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Map;

import org.apache.commons.lang.WordUtils;
import org.jsoup.Jsoup;

/**
 * Comparator for generic tables. Sorts by attribute name ascending or descending. Considers HTML content. HTML tags are
 * stripped except there is only HTML (e.g. an icon).
 */
public class GenericSorter implements Comparator<Map<String, String>>, Serializable {
    private static final long serialVersionUID = 1L;

    private boolean desc;
    private String cname;

    public GenericSorter() {
        desc = false;
        cname = "";
    }

    public void setColumnName(String cname) {
        this.cname = cname;
    }

    public void setDescending(boolean desc) {
        this.desc = desc;
    }

    @Override
    public int compare(Map<String, String> rec1, Map<String, String> rec2) {
        if (rec1.size() <= 0) {
            return 0;
        }
        String name = cname;
        // default: order by first column
        if (name.isEmpty()) {
            name = rec1.keySet().iterator().next();
        } else if (!rec1.containsKey(name)) {
            name = WordUtils.capitalize(name);
        }

        String text1 = Jsoup.parse(rec1.get(name)).text();
        String text2 = Jsoup.parse(rec2.get(name)).text();
        text1 = text1.isEmpty() ? rec1.get(name) : text1;
        text2 = text2.isEmpty() ? rec2.get(name) : text2;
        return text1.compareTo(text2) * (desc ? -1 : 1);
    }
}
