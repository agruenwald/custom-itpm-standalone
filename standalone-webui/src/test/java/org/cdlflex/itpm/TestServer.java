package org.cdlflex.itpm;

import org.apache.wicket.util.time.Duration;
import org.cdlflex.itpm.web.store.TriggerLoadConfigData;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.bio.SocketConnector;
import org.eclipse.jetty.webapp.WebAppContext;

/**
 * Test server, which can be started either within an IDE, or via maven / command line.
 */
public final class TestServer {
    private static final int PORT_NUMBER = 8080;
    
    private TestServer() {
        
    }
    
    /**
     * Start the test server.
     * @param args optional arguments (currently none).
     * @throws Exception is thrown if the server does not start correctly.
     */
    public static void main(String[] args) throws Exception {
        int timeout = (int) Duration.ONE_HOUR.getMilliseconds();
        Server server = new Server();
        SocketConnector connector = new SocketConnector();
        // Set some timeout options to make debugging easier.
        connector.setMaxIdleTime(timeout);
        connector.setSoLingerTime(-1);
        connector.setPort(PORT_NUMBER);
        server.addConnector(connector);
        server.setAttribute("org.mortbay.jetty.Request.maxFormContentSize", -1); // added - because form exceeded size
                                                                                 // for UML editor
        server.setAttribute("org.eclipse.jetty.server.Request.maxFormContentSize", -1);
        // new BaseConfigUtil().setDefaultBaseConfigPath();
        
        WebAppContext bb = new WebAppContext();
        TriggerLoadConfigData.loadConfigDataManualStart(); // drop and reload data (depending on setting either manually
                                                           // or every time)
        bb.setServer(server);
        bb.setContextPath("/");
        // bb.setWar("src/main/webapp");
        bb.setWar("src/main/webapp");
        bb.setMaxFormContentSize(-1); // please work!

        server.setHandler(bb);

        try {
            System.out.println(">>> STARTING EMBEDDED JETTY SERVER, PRESS ANY KEY TO STOP");
            server.start();
            System.in.read();
            System.out.println(">>> STOPPING EMBEDDED JETTY SERVER");
            server.stop();
            server.join();
        //CHECKSTYLE:OFF
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        //CHECKSTYLE:ON
    }
}
