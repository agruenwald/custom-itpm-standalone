package org.cdlflex.itpm;

import org.apache.wicket.util.tester.WicketTester;
import org.cdlflex.base.WicketApplication;
import org.cdlflex.itpm.web.queryEditor.QueryEditorPage;
import org.cdlflex.itpm.web.semanticEditor.SemanticMetaEditorPage;
import org.cdlflex.itpm.web.standalone.HomePage;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 * Simply tests if the pages are correctly rendered/loaded.
 */
public class PageRenderTest {
    private WicketTester tester;
    @Autowired
    private WicketApplication app;
    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Test the programmatic rendering of a page.
     */
    @Test
    @Ignore
    public void testRulePageRendering() {
        // WicketTester tester = new WicketTester();
        tester.startPage(QueryEditorPage.class);
        tester.assertRenderedPage(QueryEditorPage.class);
    }

    /**
     * Test the programmatic rendering of the home page.
     */
    @Test
    @Ignore
    public void testHomePageRendering() {
        // WicketTester tester = new WicketTester();
        tester.startPage(HomePage.class);
        tester.assertRenderedPage(HomePage.class);
    }

    /**
     * Test the programmatic rendering of the Semantic Moel editor.
     */
    @Test
    @Ignore
    public void testSemanticWebEditorPageRendering() {
        // WicketTester tester = new WicketTester();
        tester.startPage(SemanticMetaEditorPage.class);
        tester.assertRenderedPage(SemanticMetaEditorPage.class);
    }

}
