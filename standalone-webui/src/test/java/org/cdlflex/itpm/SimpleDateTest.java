package org.cdlflex.itpm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang.StringEscapeUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple tests for the execution of queries and data extraction.
 */
public class SimpleDateTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleDateTest.class);

    /**
     * Test date conversion.
     * @throws ParseException is thrown in case of failure.
     */
    @Test
    public void testDate() throws ParseException {
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS");
        String strDate = "2013-11-07 09:59:43 098";
        Date d = sdf.parse(strDate);
        LOGGER.info(d.toString());
        // result: Mon Dec 31 09:59:43 CET 2012
    }

    /**
     * Test if special characters are escaped correctly.
     */
    @Test
    public void testSpecialChars() {
        String html = " (COALESCE(SUM(?workingHours), &#039;0.0&#039;) AS ?notAvId";
        String clean = StringEscapeUtils.unescapeHtml(html).replaceAll("[^\\x20-\\x7e]", "");
        assertTrue(!clean.contains("#039"));
    }

    /**
     * Test the extraction of a namespace from a SPARQL query.
     */
    @Test
    public void testNamespaceExtraction() {
        String example =
            "SELECT ?id ?n ?oa ?stype ?url " + "WHERE { "
                + "    ?x config:hasDataSourceConfigName ?n. :rdf :rdf :owl :owl :owl"
                + "            ?x config:hasDataSourceConfigId ?id. "
                + "            ?x config:hasDataSourceConfigOriginArea ?oa. "
                + "            ?x config:hasDataSourceConfigUrl ?url. "
                + "            ?x config:hasDataSourceConfigSourceType ?stype.";

        String findStr = "config:";
        int lastIndex = 0;
        int count = 0;
        while (lastIndex != -1) {
            lastIndex = example.indexOf(findStr, lastIndex);
            if (lastIndex != -1) {
                count++;
                lastIndex += findStr.length();
            }
        }
        assertEquals(5, count);

    }

}
