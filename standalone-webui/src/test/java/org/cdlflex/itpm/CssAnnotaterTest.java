package org.cdlflex.itpm;

import static org.junit.Assert.*;

import org.cdlflex.itpm.web.utils.CssAnnotator;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Filter shortcuts from text defined via the query editor and inject icons, css classes and tags.
 */
public class CssAnnotaterTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(CssAnnotaterTest.class);

    /**
     * Test the rendering of a bootstrap icon.
     */
    @Test
    public void testIcon() {
        String text =
            "This is some text with a cool glyphicon icon:task{color: blue, alt:\"I am the popup text\"} in " 
            + "the middle.";
        String result = CssAnnotator.parseAnnotations(text);
        LOGGER.info(result);
        assertTrue(result.contains("<i"));
        assertTrue(result.contains("color"));
        assertTrue(result.contains("I am the popup text"));
        assertTrue(!result.contains("{"));
        assertTrue(!result.contains("}"));
    }

    /**
     * Test HTML support via special annotation.
     */
    @Test
    public void testHtml() {
        String text = "This is some small html text: html:small{I am small text}. Nice huh?";
        String result = CssAnnotator.parseAnnotations(text);
        LOGGER.info(result);
        assertTrue(result.contains("<small>"));
        assertTrue(!result.contains("{"));
        assertTrue(!result.contains("}"));
        assertTrue(result.contains(":"));
        result = result.replace(":", "");
        assertFalse(result.contains(":"));
    }

    /**
     * Test the support of links.
     */
    @Test
    public void testLinkToEntity() {
        String text = "This should produce a link to an entity itpm:Requirement{} of the type requirement.";
        String result = CssAnnotator.parseAnnotations(text);
        LOGGER.info(result);
        assertTrue(!result.contains("{"));
        assertTrue(!result.contains("}"));
        assertTrue(!result.contains(":"));
        assertTrue(result.contains("<a"));
    }

    /**
     * Test splitting outside quotes (regex).
     */
    @Test
    public void testQuoteSplit() {
        String text =
            "This is some text, text with some \"ahem, quotes\". It should however, only split outside the quotes.";
        String[] tokens = text.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
        assertEquals(3, tokens.length);
    }

    /**
     * Test special annotation for the rendering of icons and warnings.
     */
    @Test
    public void testFirstRealQuery1() {
        String query = "icon:user{class:flexBox-warning, color:red}";
        String result = CssAnnotator.parseAnnotations(query);
        LOGGER.info(result);
        assertTrue(result.contains("style=\"color:red\""));
    }

    /**
     * Test special annotation for the rendering of popups.
     */
    @Test
    public void testColorAndIconOnPopup() {
        String query = "icon:user{class:flexBox-warning, color:blue, text:A hell of a popup}";
        String result = CssAnnotator.parseAnnotations(query);
        LOGGER.info(result);
        assertTrue(result.contains("style=\"color:blue\""));
    }

    /**
     * Test the rendering of a HTML link with specific label.
     */
    @Test
    public void testExtractLinkText() {
        String query = "config:Requirement{rec:001, details}";
        String result = CssAnnotator.parseAnnotations(query);
        LOGGER.info(result);
        assertTrue(result.contains("<a"));
        assertTrue(result.contains("details"));
        assertTrue(result.contains("Requirement"));
    }

    /**
     * Test the removal of special characters / quotes.
     */
    @Test
    public void testStripAllQuotes() {
        String query = "Some query with \"itpm:blabla\" but config:dada should be found.";
        query = query.replaceAll("\".*?\"", "");
        query = query.replaceAll("\'.*?\'", "");
        assertTrue(query.contains("config:dada"));
        assertFalse(query.contains("itpm:blabla"));
    }

    /**
     * Negative test: no links in case of ":" or ".".
     */
    @Test
    public void negTestSwModule() {
        String query = "org.cdlflex.mdd:org.cdlflex.mdd.sembase.owlapi";
        String result = CssAnnotator.parseAnnotations(query);
        LOGGER.info(result);
        assertTrue(!result.contains("<a"));
    }

    /**
     * Negative test 2: no links in case of ":" or ".".
     */
    @Test
    public void negTestSwModule2() {
        String query = "org.cdlflex.itpm:org.cdlflex.mdd.sembase.owlapi";
        String result = CssAnnotator.parseAnnotations(query);
        LOGGER.info(result);
        assertTrue(!result.contains("<a"));
    }

    /**
     * Correct support of the minus symbol in icon annotation.
     */
    @Test
    public void testMinusSign() {
        String query = "icon:ok-sign{}";
        String result = CssAnnotator.parseAnnotations(query);
        LOGGER.info(result);
        assertTrue(!result.contains("<a"));
    }

    /**
     * Test special annotation to link to an external URL.
     */
    @Test
    public void testUrl() {
        String query = "html:a{target:_blank, href:\"http://jenkins.com\", this is link text}";
        String result = CssAnnotator.parseAnnotations(query);
        LOGGER.info(result);
        assertTrue(result.contains("<a"));
    }
}
