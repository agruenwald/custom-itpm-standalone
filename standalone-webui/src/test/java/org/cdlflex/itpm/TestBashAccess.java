package org.cdlflex.itpm;

import static org.junit.Assert.*;

import java.io.IOException;

import org.cdlflex.itpm.web.utils.BashUtil;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.junit.Ignore;
import org.junit.Test;
/**
 * Test bash access (e.g., to start the execution of maven via command line).
 */
public class TestBashAccess {

    /**
     * Test a simple UNIX command.
     * @throws IOException is thrown if the command execution fails.
     * @throws InterruptedException is thrown in case of timeouts, etc.
     */
    @Test
    public void testSimpleIO() throws IOException, InterruptedException {
        String terminalOutput = BashUtil.bashOutput("ls -lta *");
        assertTrue(terminalOutput.length() > 0);
    }

    /**
     * Test maven access.
     * @throws IOException is thrown if the command execution fails.
     * @throws InterruptedException is thrown in case of timeouts, etc.
     */
    @Test
    public void testMvn() throws IOException, InterruptedException {
        String terminalOutput = BashUtil.bashOutput("mvn -v");
        assertTrue(terminalOutput.length() > 0);
    }

    /**
     * Test two primitive maven commands.
     * @throws IOException is thrown if the command execution fails.
     * @throws InterruptedException is thrown in case of timeouts, etc.
     */
    @Test
    public void testMvnTwice() throws IOException, InterruptedException {
        String terminalOutput = BashUtil.bashOutput("mvn -v; mvn -v;");
        assertTrue(terminalOutput.length() > 0);
    }

    /**
     * Execute code generation via command line.
     * @throws IOException is thrown if the command execution fails.
     * @throws InterruptedException is thrown in case of timeouts, etc.
     * @throws MDDException is thrown if the code generation script cannot be executed.
     */
    @Test
    @Ignore
    public void testMvnCodeGenPlugin() throws IOException, InterruptedException, MDDException {
        String terminalOutput = BashUtil.execCodeGenScript(new ConfigLoader(false)); // take script from
                                                                                     // live-environment
        assertTrue(terminalOutput.length() > 0);
    }

}
