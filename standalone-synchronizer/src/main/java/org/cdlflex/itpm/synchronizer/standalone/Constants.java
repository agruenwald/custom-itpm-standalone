package org.cdlflex.itpm.synchronizer.standalone;

/**
 * Most important configuration constants for the test and the synchronization.
 */
public final class Constants {
    private Constants() {
        
    }
    
    public static final boolean ENABLE_MANUAL_TESTING = true; // set to true if relevant
    public static final String ITPM = "itpm";
    public static final String CONFIG = "config";
    public static final String DEFAULT_PROJECT = "";
}
