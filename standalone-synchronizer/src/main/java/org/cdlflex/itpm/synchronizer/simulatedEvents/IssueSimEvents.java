package org.cdlflex.itpm.synchronizer.simulatedEvents;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.SerializationUtils;
import org.cdlflex.conceptdomain.model.ConceptException;
import org.cdlflex.itpm.generated.model.BacklogItemIssueType;
import org.cdlflex.itpm.generated.model.EpicIssueType;
import org.cdlflex.itpm.generated.model.Feature;
import org.cdlflex.itpm.generated.model.Issue;
import org.cdlflex.itpm.generated.model.Product;
import org.cdlflex.itpm.generated.model.Requirement;
import org.cdlflex.itpm.generated.model.Sprint;
import org.cdlflex.itpm.generated.model.UserStoryIssueType;
import org.cdlflex.itpm.generated.model.WorkPackage;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAO;
import org.cdlflex.mdd.sembase.semaccess.DAOFactory;
import org.openengsb.domain.issue.AddIssueEvent;
import org.openengsb.domain.issue.IssueDomainEvents;
import org.openengsb.domain.issue.RemoveIssueEvent;
import org.openengsb.domain.issue.UpdateIssueEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dummy event implementation for unit tests.
 * 
 */
//CHECKSTYLE:OFF Ignore Abstraction coupling 9 (instead of max. 7) and FanOut Complexity.
public class IssueSimEvents implements IssueDomainEvents {
//CHECKSTYLE:ON
    private static final Logger LOGGER = LoggerFactory.getLogger(IssueSimEvents.class);
    private static final int WEEKS_AHEAD = 3;
    private static final int ONE_WEEK = 1000 * 60 * 60 * 24 * 7;

    private void msg(String msg, String metaClass, String instance) {
        LOGGER.info(String.format("+++Fired %s (%s, instance %s).", msg, metaClass, instance));
    }

    @Override
    public void raiseIssueAddEvent(AddIssueEvent e) throws ConceptException {
        msg("+++raiseIssueAddEvent", "Issue", e.getIssue().toString());
        this.processIssue(e.getIssue());
    }

    @Override
    public void raiseIssueUpdateEvent(UpdateIssueEvent e) throws ConceptException {
        msg("+++raiseIssueUpdateEvent", "Issue", e.getIssue().toString());
        this.processIssue(e.getIssue());
    }

    @Override
    public void raiseIssueRemoveEvent(RemoveIssueEvent e) throws ConceptException {
        msg("+++raiseIssueRemoveEvent", "Issue", e.getIssue().toString());
        org.cdlflex.mdd.sembase.Connection itpmCon = null;
        try {
            org.cdlflex.mdd.sembase.semaccess.DAOFactory factory =
                new org.cdlflex.itpm.generated.ext.ITPMDAOFactory(false);
            itpmCon = factory.createConnection("", "itpm");
            itpmCon.connect();

            DAO<Issue> issueDAO = factory.create(Issue.class, itpmCon);
            Map<String, String> params = new HashMap<String, String>();
            params.put("name", e.getIssue().getName());
            Issue issue = e.getIssue();
            List<Issue> issues = issueDAO.readRecordByAttributeValues(params);
            if (issues.size() > 1) {
                LOGGER.error(String.format(
                        "Could not match issue %s with name (origin=%s). Please consider to rename the issue.",
                        issue.getName(), issue.getOrigin()));
            } else if (issues.size() == 0) {
                LOGGER.error(String.format(
                        "Could not find issue %s with name (origin=%s). Probably it has been deleted before.",
                        issue.getName(), issue.getOrigin()));
            } else {
                issueDAO.remove(issues.get(0));
                LOGGER.info(String.format("Removed issue %s in central sembase (origin of event: %s).", issues.get(0)
                        .getName(), e.getOrigin()));
            }

        } catch (MDDException em) {
            throw new ConceptException(em.getMessage());
        } finally {
            try {
                itpmCon.close();
            } catch (MDDException e1) {
                LOGGER.error(e1.getMessage());
            }
        }

    }

    @Override
    public void raiseIssueUpdateTransitionEvent(UpdateIssueEvent e) throws ConceptException {
        msg("+++raiseIssueUpdateTransitionEvent", "Issue", e.getIssue().toString());
    }

    @Override
    public void raiseIssueUpdateVotesEvent(UpdateIssueEvent e) throws ConceptException {
        msg("+++raiseIssueUpdateVotesEvent", "Issue", e.getIssue().toString());
    }

    private void processIssue(Issue issue) throws ConceptException {
        try {
            org.cdlflex.mdd.sembase.semaccess.DAOFactory factory =
                new org.cdlflex.itpm.generated.ext.ITPMDAOFactory(false);
            org.cdlflex.mdd.sembase.Connection itpmCon = factory.createConnection("", "itpm");
            itpmCon.connect();

            // Features are considered to be equivalent to requirements.
            if (issue.getIssueType().getId().equals(new Feature().getId())) {
                updateRequirementOnDemand(factory, itpmCon, issue);
            } /*
               * else if (issue.getIssueType().getId().equals(new Task().getId())) {
               * 
               * }
               */

            if (issue.getIssueType().getId().equals(new EpicIssueType().getId())
                || (issue.getIssueType().getId().equals(new BacklogItemIssueType().getId()) || (issue.getIssueType()
                        .getId().equals(new UserStoryIssueType())))) {
                addAgileWorkpackageOnDemand(factory, itpmCon, issue);
            }
        } catch (MDDException em) {
            throw new ConceptException(em.getMessage());
        }
    }

    private void updateRequirementOnDemand(DAOFactory factory, Connection itpmCon, Issue issue) throws MDDException {
        DAO<Requirement> requirementDAO = factory.create(Requirement.class, itpmCon);
        Map<String, String> attrs = new HashMap<String, String>();
        attrs.put("name", issue.getName());
        List<Requirement> reqs = requirementDAO.readRecordByAttributeValues(attrs);
        if (reqs.size() > 1) {
            LOGGER.warn(String.format("Ambiguous issue name %s could not be mapped onto a single requirement. "
                + "No synchronization between issues and requirements took place. Please rename the issue and/or the"
                + " requirements if you want to fix the problem.", issue.getName()));
        } else if (reqs.size() == 1) {
            Requirement req = reqs.get(0);
            // age comparison won't work since the local tools always assign
            // change stamps automatically and hence a loop will result.
            // e.g. Requirement => central Issue => local Issue => EVENT => Requirement
            Requirement reqC = (Requirement) SerializationUtils.clone(reqs.get(0));
            req.setDescription(issue.getDescription());
            req.setPriority(issue.getPriority());
            // req=new Bean; reqC = oldBean, false=do not actually merge just inform if updated
            if (requirementDAO.merge(req, reqC, false)) {
                // if (isOlder(req.getLastChange(), issue.getLastChange())) {
                req.setLastChange(new Date());
                requirementDAO.update(req);
            } else {
                LOGGER.info(String.format(
                        "Did not update requirement \"%s\" from issue \"%s\" because no changes have been detected.",
                        req.getId(), issue.getId()));
            }
        } /* else {
            at the moment new features do not result in new requirements.
            Please add the code on demand.
        } */
    }

    private void addAgileWorkpackageOnDemand(DAOFactory factory, Connection itpmCon, Issue issue) throws MDDException {
        // if already existing take the existing issue and overwrite only the available fields
        DAO<WorkPackage> wpDAO = factory.create("WorkPackage", itpmCon);
        WorkPackage wp = fillWorkPackage(factory, itpmCon, issue);
        WorkPackage prevWp = readPrevWp(factory, itpmCon, issue);
        
        Sprint sprint = fillSprint(factory, itpmCon, prevWp);
        
        wp.setIsBacklogItemOfSprint(sprint);
        Map<String, String> params = new HashMap<String, String>();
        params.put("name", wp.getName());
        if (wpDAO.readRecordByAttributeValues(params) == null) {
            LOGGER.info(String.format(
                    "Inserted WP %s because it was not yet existing (existing packages are not updated).",
                    wp.getName()));
            wpDAO.insert(wp);
        }
        try {
            if (prevWp == null) {
                wpDAO.insert(wp);
                LOGGER.debug("Added wp to the central system.");
            } else {
                wpDAO.update(wp);
                LOGGER.debug("Updated wp in the central system.");
            }
        //CHECKSTYLE:OFF
        } catch (Exception ex) {
            LOGGER.warn("Could not insert/update issue: " + ex.getMessage());
        //CHECKSTYLE:ON
        } finally {
            try {
                itpmCon.close();
            } catch (MDDException ex) {
                LOGGER.trace(ex.getMessage());
            }
        }
    }
    
    private Sprint fillSprint(DAOFactory factory, Connection itpmCon, WorkPackage wp) throws MDDException {
        Sprint sprint = new Sprint();
        sprint.setName("ITPM default sprint 1");
        sprint.setId("itpm-sprint1");
        sprint.getHasSprintBacklogItemComposition().add(wp);
        DAO<Sprint> sprintDAO = factory.create(Sprint.class, itpmCon);
        Sprint prevSprint = sprintDAO.readRecord(sprint.getId());
        if (prevSprint.getId() == null) {
            sprintDAO.insert(sprint);
        } else {
            sprintDAO.merge(sprint, prevSprint);
            sprintDAO.update(sprint);
        }
        return sprint;
    }

    private WorkPackage readPrevWp(DAOFactory factory, Connection itpmCon, Issue issue) throws MDDException {
        DAO<WorkPackage> wpDAO = factory.create("WorkPackage", itpmCon);
        WorkPackage prevWp = (WorkPackage) wpDAO.readRecord(issue.getId());
        return prevWp;
    }
    
    private WorkPackage fillWorkPackage(DAOFactory factory, Connection itpmCon, Issue issue) throws MDDException {
        WorkPackage wp = null;
        WorkPackage prevWp = readPrevWp(factory, itpmCon, issue);
        if (prevWp == null) {
            wp = (WorkPackage) factory.createBean("WorkPackage", itpmCon);
            wp.setId(issue.getId());
        } else {
            wp = prevWp;
        }

        wp.setName(issue.getName());
        wp.setDescription(issue.getDescription());
        wp.setPriority(issue.getPriority());
        wp.setOrigin(issue.getOrigin()); // continue...
        wp.setEffort(5);
        wp.setDueDate(new Date(new Date().getTime() + ONE_WEEK * WEEKS_AHEAD)); 
        if (issue.getRelatesToComponent() != null) {
            DAO<Product> artifactDAO = factory.create(Product.class, itpmCon);
            if (artifactDAO.readRecord(issue.getRelatesToComponent().getId()) == null) {
                artifactDAO.insert((Product) issue.getRelatesToComponent());
            }
            wp.getOutputsArtifact().add((Product) issue.getRelatesToComponent());
        }
        return wp;
    }
}
