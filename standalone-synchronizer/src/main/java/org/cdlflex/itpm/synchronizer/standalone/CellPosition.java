package org.cdlflex.itpm.synchronizer.standalone;

/**
 * Store the position of a cell.
 */
public class CellPosition {
    private int row;
    private int col;

    /**
     * Set the cell position via the constructor.
     * 
     * @param r the row number.
     * @param c the column number.
     */
    public CellPosition(int r, int c) {
        this.row = r;
        this.col = c;
    }

    /**
     * Get the row number.
     * 
     * @return the row
     */
    public int getRow() {
        return row;
    }

    /**
     * Set the row number.
     * 
     * @param row the row to set
     */
    public void setRow(int row) {
        this.row = row;
    }

    /**
     * get the column number.
     * 
     * @return the col
     */
    public int getCol() {
        return col;
    }

    /**
     * Set the column number.
     * 
     * @param col the col to set
     */
    public void setCol(int col) {
        this.col = col;
    }

}
