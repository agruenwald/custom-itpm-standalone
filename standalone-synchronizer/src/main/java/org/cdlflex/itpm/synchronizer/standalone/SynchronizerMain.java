package org.cdlflex.itpm.synchronizer.standalone;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.itpm.generated.configurationloader.ConfigurationLoader;
import org.cdlflex.itpm.generated.ext.ITPMDAOFactory;
import org.cdlflex.itpm.synchronizer.SynchronizerService;
import org.cdlflex.itpm.synchronizer.SynchronizerServiceImpl;
import org.cdlflex.mdd.modelrepo.Installer;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.sembase.semaccess.DAO;
import org.cdlflex.mdd.sembase.semaccess.DAOFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The test class provides basic integration tests for the synchronizer. The main method can be used to start a
 * lightweight-version of the EngSb integration. Events are not actually fired to the bus but instead a dummy event
 * interface is injected which triggers empty event-methods.
 */
public class SynchronizerMain {
    private static final Logger LOGGER = LoggerFactory.getLogger(SynchronizerMain.class);
    private static DAOFactory daoFactory;
    private static Connection configCon;
    private static Connection itpmCon;
    private static ConfigLoader configLoader = new ConfigLoader(true);
    private static ConfigurationLoader testDataLoader;
    private static ServiceFactory serviceFactory;
    private static final long SLEEP_TIME = 5000;

    /**
     * Initialize the connections and the ontologies.
     * @param changeConfig if true then the configuration is refreshed.
     * @param reinstall if true then the model repository etc. is reinstalled.
     * @throws MDDException is thrown if the synchronization fails.
     */
    public static void init(boolean changeConfig, boolean reinstall) throws MDDException {
        if (reinstall) {
            Installer.reset(configLoader); // reset required if underlying data model has changed (e.g. generation)
            Installer.installModelsFromBackupDir(configLoader);
        }

        daoFactory = new ITPMDAOFactory(configLoader.isTestEnvironment());
        itpmCon = daoFactory.createConnection(Constants.DEFAULT_PROJECT, Constants.ITPM);
        configCon = daoFactory.createConnection(Constants.DEFAULT_PROJECT, Constants.CONFIG);
        itpmCon.connect();
        configCon.connect();
        serviceFactory = new ServiceFactory(daoFactory, itpmCon, configLoader);
        testDataLoader = new ConfigurationLoader(true, itpmCon, configCon);
        if (reinstall) {
            testDataLoader.resetOntologies();
            testDataLoader.loadAppDataProfile4SyncRequirementsBetweenExcelAndGoogle();
        }
        if (changeConfig) {
            changeConfig(daoFactory);
        }
    }

    /**
     * Close the connections.
     */
    public static void tearD() {
        try {
            itpmCon.close();
        } catch (MDDException e) {
            LOGGER.error(e.getMessage());
        }
        try {
            configCon.close();
        } catch (MDDException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private static void changeConfig(DAOFactory pDaoFactory) throws MDDException { // throws exception reveals
                                                                                  // class-not-found in maven
        testDataLoader.removeAllOfType(DataSourceConfig.class, configCon);
        testDataLoader.loadRequirementsSyncData();
        // finally update location of file
        
        configCon.begin();
        DAO<DataSourceConfig> dsDAO = pDaoFactory.create(DataSourceConfig.class, configCon);
        Map<String, String> searchMap = new HashMap<String, String>();
        searchMap.put("name", "RequirementsMSExcel");
        List<DataSourceConfig> sources = dsDAO.readRecordByAttributeValues(searchMap);
        if (sources.size() != 1) {
            throw new MDDException(String.format("Found %d sources for %s.", sources.size(),
                    "RequirementsMSExcel"));
        } else {
            URL excelReqs = ClassLoader.getSystemResource(sources.get(0).getUrl());
            sources.get(0).setUrl(excelReqs.getFile());
            dsDAO.update(sources.get(0));
        }
        configCon.commit();
        
    }

    /**
     * Run the synchronization service in a loop.
     * @param changeConfig if true then the configuration is refreshed.
     * @param reinstall if true then the model repository etc. is reinstalled.
     * @throws Exception is thrown if the synchronization fails.
     */
    public void runExampleSyncLoop(boolean changeConfig, boolean reinstall) throws Exception {
        init(changeConfig, reinstall);
        SynchronizerService syncService = new SynchronizerServiceImpl(daoFactory, null);
        syncService.setSyncCycles(-1); // poll
        syncService.setServiceMap(serviceFactory.createDefaultServices());
        syncService.start();
        while (syncService.isAlive()) {
            // poll;
            try {
                LOGGER.trace(String.format("Sleep for %s ms.", SLEEP_TIME));
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException e) {
                syncService.stop();
            }
        }
    }

    /**
     * Execute the synchronizer as a standalone-version.
     * 
     * @param args not in use.
     * @throws Exception is thrown if the synchronization fails.
     */
    public static void main(String[] args) throws Exception {
        SynchronizerMain main = new SynchronizerMain();
        System.out.println("******************************************");
        System.out.println("******* Start Standalone Synchronizer ******");
        System.out.println("******************************************");
        // crash test for Redmine-dependency (aether in pax may overload correct codec version > 1.3)
        LOGGER.debug("Base64 enc is " + Base64.class.getProtectionDomain().getCodeSource().getLocation());
        System.out.println(org.openxmlformats.schemas.spreadsheetml.x2006.main.CTWorksheet.class
                .getProtectionDomain().getCodeSource().getLocation());
        SynchronizerMain.configLoader = new ConfigLoader(false);
        main.runExampleSyncLoop(false, false);
    }

}
