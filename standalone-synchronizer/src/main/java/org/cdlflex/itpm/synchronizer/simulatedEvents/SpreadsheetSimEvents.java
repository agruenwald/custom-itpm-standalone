package org.cdlflex.itpm.synchronizer.simulatedEvents;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cdlflex.domain.spreadsheet.SpreadsheetDomainEvents;
import org.cdlflex.domain.spreadsheet.event.DeleteRecordEvent;
import org.cdlflex.domain.spreadsheet.event.InsertRecordEvent;
import org.cdlflex.domain.spreadsheet.event.UpdateRecordEvent;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetException;
import org.cdlflex.itpm.generated.ext.ITPMDAOFactory;
import org.cdlflex.itpm.generated.model.Collaborator;
import org.cdlflex.itpm.generated.model.Feature;
import org.cdlflex.itpm.generated.model.Issue;
import org.cdlflex.itpm.generated.model.Requirement;
import org.cdlflex.itpm.generated.model.Stakeholder;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAO;
import org.cdlflex.mdd.sembase.semaccess.DAOFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Java implementation as a substitute for the Drools-rules implementation. The class takes care of any event fired by
 * the spreadsheet tool connectors. Requirement-events are caught and new requirements are converted into issues of the
 * type "Feature".
 * 
 */
//CHECKSTYLE:OFF abstraction coupling is 8 instead of 7. fan-out complexity is 21 instead of 20.
public class SpreadsheetSimEvents implements SpreadsheetDomainEvents {
//CHECKSTYLE:ON
    private static final Logger LOGGER = LoggerFactory.getLogger(SpreadsheetSimEvents.class);
    /**
     * All issues which have been created afterwards from now() - ISSUE_DELETION_LIMIT_MINUTES will be removed if a
     * requirement with an equal name is removed.
     */
    private static final int ISSUE_DELETION_LIMIT_MINUTES = 30;
    private static final int ONE_MINUTE = 1000 * 60;

    private void msg(String msg, String metaClass, String instance) {
        LOGGER.info(String.format("+++Fired %s (%s, instance %s).", msg, metaClass, instance));
    }

    @Override
    public void raiseInsertEvent(InsertRecordEvent e) throws SpreadsheetException {
        msg("raiseInsertEvent", e.getMetaClass(), e.getBean().toString());
        if (e.getMetaClass().equals("Requirement")) {
            Requirement req = (Requirement) e.getBean();
            try {
                insertOrUpdateIssue(req, e.getOrigin(), "issue_area");
            } catch (MDDException e1) {
                throw new SpreadsheetException(e1.getMessage());
            }
        }
    }

    @Override
    public void raiseUpdateEvent(UpdateRecordEvent e) throws SpreadsheetException {
        msg("raiseUpdateEvent", e.getMetaClass(), e.getBean().toString());
        if (e.getMetaClass().equals("Requirement")) {
            Requirement req = (Requirement) e.getBean();
            try {
                insertOrUpdateIssue(req, e.getOrigin(), "issue_area");
            } catch (MDDException e1) {
                throw new SpreadsheetException(e1.getMessage());
            }
        }
    }

    @Override
    public void raiseDeleteEvent(DeleteRecordEvent e) throws SpreadsheetException {
        msg("raiseDeleteEvent", e.getMetaClass(), e.getBean().toString());
        try {
            if (e.getMetaClass().equals("Requirement")) {
                Requirement req = (Requirement) e.getBean();
                DAOFactory factory = new org.cdlflex.itpm.generated.ext.ITPMDAOFactory(false);
                Connection itpmCon = factory.createConnection("", "itpm");
                itpmCon.connectOnDemand();
                DAO<Issue> issueDAO = factory.create("Issue", itpmCon);
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", req.getName());
                List<Issue> issues = issueDAO.readRecordByAttributeValues(params);
                for (Issue issue : issues) {
                    if (issue.getCreatedAt().after(
                            new Date(new Date().getTime() - ONE_MINUTE * ISSUE_DELETION_LIMIT_MINUTES))) {
                        issueDAO.remove(issue);
                        LOGGER.info(String.format(
                                "Removed issue %s because the requirement with the same name also was "
                                    + "removed and the creation date was not more than %d minutes ago.",
                                issue.getName(), ISSUE_DELETION_LIMIT_MINUTES));
                    }
                }
            }
        } catch (MDDException emdd) {
            throw new SpreadsheetException(emdd.getMessage());
        }
    }

    /**
     * Equals requirement.function (drools rule).
     * 
     * @param req the requirement.
     * @param origin the origin of the requirement.
     * @param targetOrigin the target origin (e.g. if a requirement is converted into an issue).
     */
    private void insertOrUpdateIssue(Requirement req, String sourceOrigin, String targetOrigin) throws MDDException {
        DAOFactory factory = new ITPMDAOFactory(false);
        org.cdlflex.mdd.sembase.Connection itpmCon = factory.createConnection("", "itpm");
        itpmCon.connectOnDemand();
        // if already existing take the existing issue and overwrite only the available fields
        DAO<Issue> issueDAO = factory.create("Issue", itpmCon);
        Issue issue = null;
        Issue prevIssue = (Issue) issueDAO.readRecord(req.getId());
        if (prevIssue == null) {
            issue = (Issue) factory.createBean("Issue", itpmCon);
            issue.setId(req.getId());
        } else {
            issue = prevIssue;
        }
        fillIssue(issue, req, targetOrigin);
        // report (additonal data from the Semantic Web store is not added here
        Collaborator reporter = new Collaborator(); 
        if (req.getDefine().size() > 0) { 
            DAO<Collaborator> colDAO = factory.create("Collaborator", itpmCon);
            Stakeholder sh1 = (Stakeholder) req.getDefine().iterator().next();
            reporter.setEmail(sh1.getName().toLowerCase() + "@cdlflex.org");
            reporter.setName(sh1.getName().toLowerCase());      
            Collaborator rep = readReporter(colDAO, reporter, sourceOrigin);
            if (rep != null) {
                reporter = rep;
            }
        } else {
            throw new MDDException("At least one stakeholder must exist (requirement " + req.getId() + ").");
        }
        issue.setIssueReportedBy(reporter);
        try {
            updateIssueOnDemand(prevIssue, issue, issueDAO);
        } finally {
            try {
                itpmCon.close();
            } catch (MDDException e) {
                LOGGER.error(e.getMessage());
            }
        }
    }

    private Collaborator readReporter(DAO<Collaborator> colDAO, Collaborator reporter, 
        String sourceOrigin) throws MDDException {
        List<Collaborator> existingCols = colDAO.readFulltext(reporter.getName());
        if (existingCols.size() <= 0) {
            reporter.setOrigin(sourceOrigin);
            LOGGER.debug("Inserted collaborator");
            colDAO.insert(reporter);
            return null;
        } else {
            return (Collaborator) existingCols.get(0);
        }
    }

    private void fillIssue(Issue issue, Requirement req, String targetOrigin) {
        issue.setName(req.getName());
        issue.setDescription("");
        issue.setPriority(req.getPriority());
        issue.setDescription(req.getDescription()); // "This issue has been created based on a requirement");
        issue.setIssueType(new Feature());
        issue.setDueDate(new Date());
        issue.setOrigin(targetOrigin);  
    }

    private void updateIssueOnDemand(Issue prevIssue, Issue issue, DAO<Issue> issueDAO) {
        try {
            if (prevIssue == null) {
                issueDAO.insert(issue);
                LOGGER.debug("Added issue to the central system.");
            } else {
                // important to change last update date otherwise the issue may be older than
                // the local issues and instead of downstreaming the new issue one of the existing
                // local issues may be upstreamed.
                issue.setLastChange(new Date());
                issueDAO.update(issue);
                LOGGER.debug("Updated issue in the central system.");
            }
        //CHECKSTYLE:OFF
        } catch (Exception e) {
        //CHECKSTYLE:ON
            LOGGER.warn("Could not insert/update issue: " + e.getMessage());
        } 
    }
}
