package org.cdlflex.itpm.synchronizer.standalone;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.connector.spreadsheet.excel.internal.MSSpreadsheetService;
import org.cdlflex.connector.spreadsheet.google.internal.GoogleSpreadsheetService;
import org.cdlflex.itpm.synchronizer.simulatedEvents.IssueSimEvents;
import org.cdlflex.itpm.synchronizer.simulatedEvents.SpreadsheetSimEvents;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.sembase.semaccess.DAOFactory;
import org.openengsb.core.api.Domain;

/**
 * Create a map of services which can be used to test/simulate the synchronizer. The live-synchronizer is executed in
 * the EngSb environment and the service map is injected by blueprint (opposed to this solution in which the services
 * are created programatically).
 */
//CHECKSTYLE:OFF Data Abstraction Coupling (11 instead of 7) ignored.
public class ServiceFactory {
//CHECKSTYLE:ON
    private DAOFactory daoFactory;
    private Connection itpmCon;
    private ConfigLoader configLoader;
    private static Map<String, List<Domain>> serviceMap;

    public ServiceFactory(DAOFactory pDaoFactory, Connection pItpmCon, ConfigLoader pConfigLoader) {
        this.daoFactory = pDaoFactory;
        this.itpmCon = pItpmCon;
        this.configLoader = pConfigLoader;
    }

    /**
     * Find the service via domain and connector name.
     * @param domain the name of the domain, e.g. issue
     * @param connector the name of the connnectionr, e.g. redmine
     * @return an abstracted service interface.
     * @throws StreamException is thrown if the access to a service fails.
     */
    public static Domain findService(String domain, String connector) throws StreamException {
        if (!serviceMap.containsKey(domain)) {
            throw new StreamException(String.format("Could not find domain with name=%s.", domain));
        }
        List<Domain> domainServices = serviceMap.get(domain);
        for (Domain d : domainServices) {
            if (d.getInstanceId().contains(connector)) {
                return d;
            }
        }
        throw new StreamException(String.format("Could not find connector with name=%s.", connector));
    }

    /**
     * Create all default services.
     * @return a list of services, which can be accessed via their domain name.
     */
    public Map<String, List<Domain>> createDefaultServices() {
        serviceMap = new LinkedHashMap<String, List<Domain>>();
        List<Domain> spreadsheetServices = new ArrayList<Domain>();
        spreadsheetServices.add(new MSSpreadsheetService("excel+spreadsheet", new SpreadsheetSimEvents(), daoFactory,
                itpmCon, configLoader));
        spreadsheetServices.add(new GoogleSpreadsheetService("google+spreadsheet", new SpreadsheetSimEvents(),
                daoFactory, itpmCon, configLoader));
        serviceMap.put("spreadsheet", spreadsheetServices);
        List<Domain> issueServices = new ArrayList<Domain>();
        org.openengsb.connector.jira.ServiceInstanceFactory jiraFactory =
            new org.openengsb.connector.jira.ServiceInstanceFactory();
        jiraFactory.setDaoFactory(daoFactory);
        jiraFactory.setItpmCon(itpmCon);
        jiraFactory.setEvents(new IssueSimEvents());
        issueServices.add(jiraFactory.createNewInstance("issue+jira"));
        org.openengsb.connector.redmine.ServiceInstanceFactory redmineFactory =
            new org.openengsb.connector.redmine.ServiceInstanceFactory();
        redmineFactory.setEvents(new IssueSimEvents());
        issueServices.add(redmineFactory.createNewInstance("issue+redmine"));
        serviceMap.put("issue", issueServices);
        
        List<Domain> qmServices = new ArrayList<Domain>();
        org.cdlflex.connector.jenkins.ServiceInstanceFactory jenkinsFactory =
            new org.cdlflex.connector.jenkins.ServiceInstanceFactory();
        jenkinsFactory.setDaoFactory(daoFactory);
        jenkinsFactory.setItpmCon(itpmCon);
        qmServices.add(jenkinsFactory.createNewInstance("qm+jenkins"));
        serviceMap.put("qm", qmServices);

        List<Domain> scrumServices = new ArrayList<Domain>();
        org.cdlflex.connector.scrumwise.ServiceInstanceFactory scrumwiseFactory =
            new org.cdlflex.connector.scrumwise.ServiceInstanceFactory();
        scrumwiseFactory.setDaoFactory(daoFactory);
        scrumwiseFactory.setItpmCon(itpmCon);
        scrumServices.add(scrumwiseFactory.createNewInstance("scrum+scrumwise"));
        serviceMap.put("scrum", scrumServices);

        return serviceMap;
    }
}
