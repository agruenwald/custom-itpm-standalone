package org.cdlflex.itpm.synchronizer.itests.utils;

import java.util.List;

import org.cdlflex.conceptdomain.concept.sync.BasicSyncDomain;
import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAOFactory;
import org.cdlflex.mdd.sembase.semaccess.MDDBeanInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utils which are handy for the execution of integration tests.
 */
public final class TestHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestHelper.class);

    private TestHelper() {

    }

    /**
     * Trim Ids (remove special chars).
     * 
     * @param str a typical domain shortcut of the OpenEngSb, e.g., spreadsheet+excel
     * @return the cleaned string, eg., spreadsheetexcel.
     */
    public static String trimDomainId(String str) {
        str = str.replaceAll("\\+", "");
        str = str.replaceAll("-", "");
        return str;
    }

    /**
     * Attach a specific data source configuration to a connector service.
     * 
     * @param service the connector instance.
     * @param activeConfigs all available configurations (active).
     * @return an appropriate data source configuration for the service.
     * @throws StreamException is thrown if no active configuration corresponds to the service.
     */
    public static DataSourceConfig attachConfigToService(BasicSyncDomain<?> service,
        List<DataSourceConfig> activeConfigs) throws StreamException {
        for (DataSourceConfig ac : activeConfigs) {
            if (trimDomainId(service.getInstanceId()).contains(trimDomainId(ac.getConnectorType()))) {
                service.setDataSourceConfig(ac);
                return ac;
            }
        }
        throw new StreamException(String.format("Could not match service %s with any active config.",
                service.getInstanceId()));
    }

    /**
     * Get the number of records for a specific class (shortcut).
     * 
     * @param factory the DAOFactory.
     * @param opencon the related connection.
     * @param conceptClass Requirement.class, Issue.class etc.
     * @return size >= 0
     * @throws MDDException is thrown if the access to the data store fails.
     */
    public static int sizeChecker(DAOFactory factory, Connection opencon,
        Class<? extends MDDBeanInterface> conceptClass) throws MDDException {
        return factory.create(conceptClass, opencon).readFulltext("").size();
    }

    /**
     * Automatically check if the number of selected active configs equals the expected number.
     * 
     * @param activeConfigs all active configs found.
     * @param wantedServices the wanted number of services (a string with terms to match configs - used only for
     *        information purpose)
     * @param expectedNumber the expected number of configs to be found
     * @throws MDDException is thrown if active configs != expectedNumber
     */
    public static void checkActivatedConfigs(List<DataSourceConfig> activeConfigs, List<String> wantedServices,
        int expectedNumber) throws MDDException {
        if (activeConfigs.size() != expectedNumber) {
            throw new MDDException(String.format("Activated services != %d. (%s wanted)", expectedNumber,
                    wantedServices));
        }
        LOGGER.info(String.format("Activated configs: %d.", activeConfigs.size()));
    }
}
