package org.cdlflex.itpm.synchronizer.itests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.conceptdomain.model.ConceptException;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetException;
import org.cdlflex.itpm.generated.model.Bug;
import org.cdlflex.itpm.generated.model.Collaborator;
import org.cdlflex.itpm.generated.model.Issue;
import org.cdlflex.itpm.generated.model.Project;
import org.cdlflex.itpm.generated.model.Requirement;
import org.cdlflex.itpm.generated.model.Stakeholder;
import org.cdlflex.itpm.synchronizer.SynchronizerService;
import org.cdlflex.itpm.synchronizer.itests.reusable.IssueComp;
import org.cdlflex.itpm.synchronizer.itests.setup.AbstractITestPattern;
import org.cdlflex.itpm.synchronizer.itests.setup.Config;
import org.cdlflex.itpm.synchronizer.itests.setup.TestSetupSceleton;
import org.cdlflex.itpm.synchronizer.itests.utils.TestHelper;
import org.cdlflex.itpm.synchronizer.standalone.ServiceFactory;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAO;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openengsb.domain.issue.IssueDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An issue which is removed centrally shall be removed in all tool domains which downstream that issue.
 */
public class Test11aRemoveIssueCentrally extends TestSetupSceleton implements AbstractITestPattern {
    private static final Logger LOGGER = LoggerFactory.getLogger(Test04RequirementSyncPushIssuesToRedmineToo.class);
    private static final String DEFAULT_PROJECT = "itpm";
    private IssueDomain jiraService;
    private IssueDomain redmineService;
    private List<DataSourceConfig> activeConfigs;

    @Before
    @Override
    public void modifyConfigForTest() throws IllegalAccessError, MDDException, StreamException, ConceptException {
        boolean ignoreTests = isIgnoreTests();
        org.junit.Assume.assumeTrue(!ignoreTests);
        List<String> wantedServices = Arrays.asList(new String[] { Config.JIRA, Config.REDMINE });
        activeConfigs = deactivateAllConfigsBut(wantedServices);
        TestHelper.checkActivatedConfigs(activeConfigs, wantedServices, 2);
        LOGGER.info(String.format("Activated configs: %d.", activeConfigs.size()));
        modifyServiceContentForTest();
    }

    @Override
    public void modifyServiceContentForTest() throws MDDException, StreamException, ConceptException {
        // find domain services and attach appropriate configurations for execution
        jiraService = (IssueDomain) ServiceFactory.findService("issue", "jira");
        redmineService = (IssueDomain) ServiceFactory.findService("issue", "redmine");
        TestHelper.attachConfigToService(jiraService, activeConfigs);
        TestHelper.attachConfigToService(redmineService, activeConfigs);

        // remove everything from Jira
        List<Project> jiraProjects = jiraService.getProjects("");
        for (Project jp : jiraProjects) {
            jiraService.removeProject(jp);
        }
        // remove everything from Redmine
        List<Project> redmineProjects = redmineService.getProjects("");
        for (Project jp : redmineProjects) {
            redmineService.removeProject(jp);
        }
    }

    @Override
    public void preTest() throws MDDException, SpreadsheetException, StreamException, ConceptException {
        // check central store
        assertEquals(0, sizeChecker(Issue.class));
        assertEquals(0, sizeChecker(Requirement.class));
        assertEquals(0, sizeChecker(Stakeholder.class));

        // check Jira
        if (null != jiraService.getProject(DEFAULT_PROJECT)) {
            throw new StreamException(String.format("Default project %s not cleaned in Jira!", DEFAULT_PROJECT));
        }

        // check Redmine
        if (null != redmineService.getProject(DEFAULT_PROJECT)) {
            throw new StreamException(String.format("Default project %s not cleaned in Redmine!", DEFAULT_PROJECT));
        }
    }

    @Test
    public void iTest() throws Exception {
        final int definedCycles = 3;
        preTest();
        SynchronizerService synchronizer = getSyncService();
        synchronizer.setSyncCycles(definedCycles);
        synchronizer.start();
        getSyncListener().listen(synchronizer);
        getSyncListener().assertIfError();
        assertTrue(String.format("Cycle number was not %d but %d.", synchronizer.getSyncCylces(), getSyncListener()
                .getCycleNo()), getSyncListener().getCycleNo() >= definedCycles);
    }

    @Override
    @Ignore
    public boolean recurringTestComparison() throws StreamException, ConceptException, MDDException {
        int cycleNo = getSyncListener().getCycleNo();
        String testIssueName = "Jira Issue";
        try {
            if (cycleNo == 1) {
                // add new issue to Jira
                Issue issue1 = new Issue();
                issue1.setName(testIssueName);
                issue1.setDescription("This issue has been created in Jira during the test "
                    + this.getClass().getSimpleName());
                issue1.setPriority(3);
                issue1.setIssueType(new Bug());
                Collaborator agr = getDaoFactory().create(Collaborator.class, getItpmCon()).readRecord("agr");
                Collaborator dwi = getDaoFactory().create(Collaborator.class, getItpmCon()).readRecord("dwi");
                issue1.setIssueReportedBy(agr);
                issue1.setIssueAssignedTo(dwi);
                jiraService.addIssue(DEFAULT_PROJECT, issue1);
            } else if (cycleNo == 2) {
                // compressed pre-test (existence of issues)
                IssueComp.checkNumberOfIssues(jiraService, DEFAULT_PROJECT, 1);
                IssueComp.checkNumberOfIssues(redmineService, DEFAULT_PROJECT, 1);
                IssueComp.issueComparisonJiraTest(activeConfigs, getDaoFactory(), getItpmCon(), DEFAULT_PROJECT,
                        jiraService, redmineService);
                // check if issue exists in central sembase
                DAO<Issue> centralIssueDAO = getDaoFactory().create(Issue.class, getItpmCon());
                Issue issue = centralIssueDAO.readFulltext(testIssueName).get(0);
                assertTrue(issue != null);
                assertTrue(null != issue.getIssueReportedBy());
                assertTrue(null != issue.getIssueAssignedTo());

                DAO<DataSourceConfig> cdao = getDaoFactory().create(DataSourceConfig.class, getConfigCon());
                for (DataSourceConfig c : activeConfigs) {
                    c.setUpstream(false);
                    c.setDownstream(true);
                    cdao.update(c);
                }
                for (Issue issueR : centralIssueDAO.readFulltext("")) {
                    centralIssueDAO.remove(issueR);
                }
                assertEquals(0, centralIssueDAO.readFulltext("").size());
                // Test does not work because the updated configs are not immediately
                // recognized by the synchronizer (synchronizer caches some data)
            } else if (cycleNo == 3) {
                IssueComp.checkNumberOfIssues(jiraService, DEFAULT_PROJECT, 0);
                IssueComp.checkNumberOfIssues(redmineService, DEFAULT_PROJECT, 0);
                Map<String, String> searchMap = new HashMap<String, String>();
                searchMap.put("name", testIssueName);
                Issue centralIssue =
                    getDaoFactory().create(Issue.class, getItpmCon()).readRecordByAttributeValues(searchMap).get(0);
                assertTrue(centralIssue == null);
            }
        } catch (AssertionError e) {
            e.printStackTrace();
            fail(e.getMessage());
            return false;
        }
        return true;
    }
}
