package org.cdlflex.itpm.synchronizer.itests.setup;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.itpm.generated.configurationloader.ConfigurationLoader;
import org.cdlflex.itpm.generated.ext.ITPMDAOFactory;
import org.cdlflex.itpm.synchronizer.ISyncCallBackObject;
import org.cdlflex.itpm.synchronizer.SynchronizerService;
import org.cdlflex.itpm.synchronizer.SynchronizerServiceImpl;
import org.cdlflex.itpm.synchronizer.itests.reusable.SyncListener;
import org.cdlflex.itpm.synchronizer.itests.utils.TestHelper;
import org.cdlflex.itpm.synchronizer.standalone.Constants;
import org.cdlflex.itpm.synchronizer.standalone.ServiceFactory;
import org.cdlflex.mdd.modelrepo.Installer;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.sembase.semaccess.DAO;
import org.cdlflex.mdd.sembase.semaccess.DAOFactory;
import org.cdlflex.mdd.sembase.semaccess.MDDBeanInterface;
import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Sceleton with setup methods with setup to the basic data and service environment including access. (for a structured
 * way of performing integration tests for the Synchronization of tool domains).
 */
public abstract class TestSetupSceleton implements ISyncCallBackObject {
    private static final Boolean IS_TEST_ENV = true; // change to true -> false is for debugging with WEB UI
    private static final Logger LOGGER = LoggerFactory.getLogger(TestSetupSceleton.class);
    private static DAOFactory daoFactory;
    private static Connection configCon;
    private static Connection itpmCon;
    private static ConfigurationLoader testDataLoader;
    private static ServiceFactory serviceFactory;

    private static final ConfigLoader CONFIG_LOADER = new ConfigLoader(IS_TEST_ENV);
    private static final String ITEST_TESTDATA_BACKUP_SIMPLE = "pm-data/backup";
    private static final String ITEST_OPERATING_DIR_SIMPLE = "pm-data/";
    private static File ITEST_TESTDATA_BACKUP;
    private static File ITEST_OPERATING_DIR;
    private SynchronizerService syncService = null;
    private static final SyncListener syncListener = new SyncListener();

    public static final String IGNORE_ITESTS_SYNCHRONIZER = "ignore-itests-synchronizer";


    /**
     * Check if tests are disabled.
     * @return true if tests should be ignored.
     * @throws MDDException is thrown if reading the test ignoring configuration property file
     * fails.
     */
    public static boolean isIgnoreTests() throws MDDException {
        return Boolean.parseBoolean(CONFIG_LOADER.findPropertySmart("test-config",
               IGNORE_ITESTS_SYNCHRONIZER));
    }
    
    /**
     * Central way to deactivate the tests via property settings, either in the resources/ path of the current module or
     * in the $SEMBASE/env/test-config.properties. Reason: Test environment and licences may not always be available.
     * All tests will be ignored if the parameter ignore-tests-thedomain-theconnector has been set to true in the
     * test-config.properties file. In all other cases the tests will be executed.
     * 
     * @throws MDDException is only thrown in very severe cases which are not expected to happen or when the base
     *         configuration is missing (the test-config file itself doesn't need to exist).
     */
    @Before
    public void beforeMethod() throws MDDException {
        boolean ignoreTests = isIgnoreTests();
        org.junit.Assume.assumeTrue(!ignoreTests);
    }

    public static SyncListener getSyncListener() {
        return syncListener;
    }

    @Before
    public void setup() throws MDDException, IOException, IllegalAccessError {
        if (isIgnoreTests()) {
            return;
        }
        String homeDir = CONFIG_LOADER.getHomeDir();
        ITEST_TESTDATA_BACKUP = new File(String.format("%s/%s", homeDir, ITEST_TESTDATA_BACKUP_SIMPLE));
        ITEST_OPERATING_DIR = new File(String.format("%s/%s", homeDir, ITEST_OPERATING_DIR_SIMPLE));
        if (!ITEST_TESTDATA_BACKUP.exists()) {
            throw new MDDException(String.format("PM backup dirrectory is missing: %s.", ITEST_TESTDATA_BACKUP));
        } else if (!ITEST_TESTDATA_BACKUP.exists()) {
            throw new MDDException(String.format("PM backup dir is not a directory: %s.", ITEST_OPERATING_DIR));
        }

        for (File f : ITEST_TESTDATA_BACKUP.listFiles()) {
            FileUtils.copyFileToDirectory(f, ITEST_OPERATING_DIR);
        }
        reinstallModels();
        setupSemanticStorePlusConfig();
        syncService = prepareSynchronizer(this);
    }

    public SynchronizerService getSyncService() {
        return syncService;
    }

    private static void reinstallModels() throws MDDException {
        Installer.reset(CONFIG_LOADER); // reset required if underlying data model has changed (e.g. generation)
        Installer.installModelsFromBackupDir(CONFIG_LOADER);
    }

    private static void setupSemanticStorePlusConfig() throws MDDException {
        daoFactory = new ITPMDAOFactory(CONFIG_LOADER.isTestEnvironment());
        itpmCon = daoFactory.createConnection(Constants.DEFAULT_PROJECT, Constants.ITPM);
        configCon = daoFactory.createConnection(Constants.DEFAULT_PROJECT, Constants.CONFIG);
        itpmCon.connect();
        configCon.connect();
        serviceFactory = new ServiceFactory(daoFactory, itpmCon, CONFIG_LOADER);
        testDataLoader = new ConfigurationLoader(true, itpmCon, configCon);
        testDataLoader.resetOntologies();
        testDataLoader.loadAppData();
        itpmCon.connectOnDemand();
        configCon.connectOnDemand();
    }

    private SynchronizerService prepareSynchronizer(ISyncCallBackObject callbackObj) throws MDDException {
        SynchronizerService syncService = new SynchronizerServiceImpl(daoFactory, null, callbackObj);
        syncService.setSyncCycles(-1); // poll
        syncService.setServiceMap(serviceFactory.createDefaultServices());
        return syncService;
    }

    public static void deactivateAllConfigs() throws MDDException {
        deactivateAllConfigsBut(new ArrayList<String>());
    }

    /**
     * Deactivate all configurations except those that match one of the patterns (name, originArea, connectorType are
     * compared)
     * 
     * @param pattern any String case insensitive
     * @return the active configs
     * @throws MDDException
     */
    public static List<DataSourceConfig> deactivateAllConfigsBut(List<String> pattern) throws MDDException {
        List<DataSourceConfig> activeConfigs = new ArrayList<DataSourceConfig>();
        DAO<DataSourceConfig> dao = daoFactory.create(DataSourceConfig.class, configCon);
        List<DataSourceConfig> configs = dao.readFulltext("");
        configCon.begin();
        for (DataSourceConfig conf : configs) {
            boolean ignore = false;
            for (String p : pattern) {
                if (conf.getOriginArea().toLowerCase().contains(p.toLowerCase())
                    || conf.getConnectorType().toLowerCase().contains(p.toLowerCase())
                    || conf.getName().toLowerCase().equals(p.toLowerCase())) {
                    ignore = true;
                    break;
                }
            }
            if (ignore) {
                conf.setDeactivated(false);
                dao.update(conf);
                activeConfigs.add(conf);
            } else {
                conf.setDeactivated(true);
            }
        }
        configCon.commit();
        return activeConfigs;
    }

    @After
    public void tearDown() throws IOException {
        try {
            if (isIgnoreTests()) {
                return;
            }
        } catch (MDDException e1) {
           throw new IOException(e1);
        }
        try {
            itpmCon.close();
        } catch (Exception e) {
        }
        try {
            configCon.close();
        } catch (Exception e) {
        }
    }

    public static DAOFactory getDaoFactory() {
        return daoFactory;
    }

    public static Connection getConfigCon() {
        return configCon;
    }

    public static Connection getItpmCon() {
        return itpmCon;
    }

    public static ConfigurationLoader getTestDataLoader() {
        return testDataLoader;
    }

    public static ServiceFactory getServiceFactory() {
        return serviceFactory;
    }

    /**
     * Get the number of records for a specific class (shortcut)
     * 
     * @param conceptClass Requirement.class, Issue.class etc.
     * @return size >= 0
     * @throws MDDException
     */
    public static int sizeChecker(Class<? extends MDDBeanInterface> conceptClass) throws MDDException {
        return TestHelper.sizeChecker(getDaoFactory(), getItpmCon(), conceptClass);
    }

    /**
     * Override this method with the unit test that should be executed after each loop
     * 
     * @throws Exception any exception and/or assertion needs to be caught
     * @return
     */
    public abstract boolean recurringTestComparison() throws Exception;

    @Override
    public boolean callSyncCycleCompleted(int cycleNo) {
        try {
            LOGGER.info("===============> Sync cycle (before #recurringTestComparison() {}/{})", cycleNo,
                    getSyncService().getSyncCylces());
            getSyncListener().setCycleNo(cycleNo);
            return recurringTestComparison();
        } catch (AssertionError e) {
            LOGGER.error(String.format("Failed in cycle %s: %s", cycleNo, e.getMessage()));
            e.printStackTrace();
            getSyncListener().setStatus(false, e.getMessage());
            return false;
        } catch (Exception e) {
            getSyncListener().setStatus(false, e.getMessage());
            return false;
        }
    }
}
