package org.cdlflex.itpm.synchronizer.itests.utils;

import java.util.Iterator;
import java.util.List;

import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.domain.spreadsheet.SpreadsheetDomain;
import org.cdlflex.domain.spreadsheet.model.Spreadsheet;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetRow;

public class ReqUtils {
    /**
     * Change the description text of a requirement with id=70 in the requirements excel sheet
     * 
     * @param excelService
     * @param activeConfigs
     * @param changedDescrText
     * @throws StreamException
     */
    public static void changeReqDescr(SpreadsheetDomain excelService, List<DataSourceConfig> activeConfigs,
        String reqId, String changedText) throws StreamException {
        // apply change directly in MS Spreadsheet to get events fired.
        DataSourceConfig reqConfig = TestHelper.attachConfigToService(excelService, activeConfigs);
        Spreadsheet reqSheet = excelService.retrieveSpreadsheet(reqConfig);
        SpreadsheetRow req70Row = null;
        Iterator<SpreadsheetRow> it = reqSheet.iterator();
        int rowNumber = reqConfig.getStartRow();
        while (it.hasNext()) {
            SpreadsheetRow r = it.next();
            if (r.getCell(1).getValue().equals(reqId)) {
                req70Row = r;
                break;
            }
            rowNumber++;
        }
        if (req70Row == null) {
            throw new StreamException(String.format("Value of Excel row 7, pos 1 is not %s!", reqId));
        } else {
            req70Row.getCell(3).setValue(changedText);
        }
        reqSheet.insertOrUpdate(req70Row, rowNumber);
        excelService.persistSpreadsheet(reqSheet);
    }

    public static void changeReqPrio(SpreadsheetDomain excelService, List<DataSourceConfig> activeConfigs,
        String reqId, int prioChanged) throws StreamException {
        // apply change directly in MS Spreadsheet to get events fired.
        DataSourceConfig reqConfig = TestHelper.attachConfigToService(excelService, activeConfigs);
        Spreadsheet reqSheet = excelService.retrieveSpreadsheet(reqConfig);
        SpreadsheetRow req30Row = null;
        Iterator<SpreadsheetRow> it = reqSheet.iterator();
        int rowNumber = reqConfig.getStartRow();
        while (it.hasNext()) {
            SpreadsheetRow r = it.next();
            if (r.getCell(1).getValue().equals(reqId)) {
                req30Row = r;
                break;
            }
            rowNumber++;
        }
        if (req30Row == null) {
            throw new StreamException(String.format("Value of Excel row 7, pos 1 is not %s!", reqId));
        } else {
            req30Row.getCell(7).setValue(String.valueOf(prioChanged));
        }
        reqSheet.insertOrUpdate(req30Row, rowNumber);
        excelService.persistSpreadsheet(reqSheet);
    }
}
