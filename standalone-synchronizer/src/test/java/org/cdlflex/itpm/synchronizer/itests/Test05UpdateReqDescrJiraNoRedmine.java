package org.cdlflex.itpm.synchronizer.itests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.conceptdomain.model.ConceptException;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.domain.spreadsheet.SpreadsheetDomain;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetException;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetRow;
import org.cdlflex.itpm.generated.model.Issue;
import org.cdlflex.itpm.generated.model.Project;
import org.cdlflex.itpm.generated.model.Requirement;
import org.cdlflex.itpm.generated.model.Stakeholder;
import org.cdlflex.itpm.synchronizer.SynchronizerService;
import org.cdlflex.itpm.synchronizer.itests.reusable.IssueComp;
import org.cdlflex.itpm.synchronizer.itests.reusable.RequirementComp;
import org.cdlflex.itpm.synchronizer.itests.setup.AbstractITestPattern;
import org.cdlflex.itpm.synchronizer.itests.setup.TestSetupSceleton;
import org.cdlflex.itpm.synchronizer.itests.utils.ReqUtils;
import org.cdlflex.itpm.synchronizer.itests.utils.TestHelper;
import org.cdlflex.itpm.synchronizer.standalone.ServiceFactory;
import org.cdlflex.mdd.sembase.MDDException;
import org.junit.Before;
import org.junit.Test;
import org.openengsb.domain.issue.IssueDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * In the first step the issues are created based on requirements. In the second step the description of a requirement
 * is updated. This should result in updated issues.
 */
public class Test05UpdateReqDescrJiraNoRedmine extends TestSetupSceleton implements AbstractITestPattern {
    private static final Logger LOGGER = LoggerFactory.getLogger(Test04RequirementSyncPushIssuesToRedmineToo.class);
    private static final String DEFAULT_PROJECT = "itpm";
    private SpreadsheetDomain excelService;
    private SpreadsheetDomain googleService;
    private IssueDomain jiraService;
    private List<DataSourceConfig> activeConfigs;

    @Before
    @Override
    public void modifyConfigForTest() throws IllegalAccessError, MDDException, StreamException, ConceptException {
        boolean ignoreTests = isIgnoreTests();
        org.junit.Assume.assumeTrue(!ignoreTests);
        List<String> wantedServices = Arrays.asList(new String[] { "Requirements", "Jira" });
        activeConfigs = deactivateAllConfigsBut(wantedServices);
        TestHelper.checkActivatedConfigs(activeConfigs, wantedServices, 3);
        LOGGER.info(String.format("Activated configs: %d.", activeConfigs.size()));
        modifyServiceContentForTest();
    }

    @Override
    public void modifyServiceContentForTest() throws MDDException, StreamException, ConceptException {
        // find domain services and attach appropriate configurations for execution
        excelService = (SpreadsheetDomain) ServiceFactory.findService("spreadsheet", "excel");
        googleService = (SpreadsheetDomain) ServiceFactory.findService("spreadsheet", "google");
        jiraService = (IssueDomain) ServiceFactory.findService("issue", "jira");
        TestHelper.attachConfigToService(excelService, activeConfigs);
        TestHelper.attachConfigToService(googleService, activeConfigs);
        TestHelper.attachConfigToService(jiraService, activeConfigs);

        // remove everything from Google
        Iterator<SpreadsheetRow> rows =
            googleService.retrieveSpreadsheet(TestHelper.attachConfigToService(googleService, activeConfigs))
                    .iterator();
        while (rows.hasNext()) {
            rows.next();
            rows.remove();
        }
        // remove everything from Jira
        List<Project> jiraProjects = jiraService.getProjects("");
        for (Project jp : jiraProjects) {
            jiraService.removeProject(jp);
        }
    }

    @Override
    public void preTest() throws MDDException, SpreadsheetException, StreamException, ConceptException {
        // check central store
        assertEquals(0, sizeChecker(Issue.class));
        assertEquals(0, sizeChecker(Requirement.class));
        assertEquals(0, sizeChecker(Stakeholder.class));

        // check Jira
        if (null != jiraService.getProject(DEFAULT_PROJECT)) {
            throw new StreamException(String.format("Default project %s not cleaned in Jira!", DEFAULT_PROJECT));
        }
    }

    @Test
    public void iTest() throws Exception {
        preTest();
        SynchronizerService synchronizer = getSyncService();
        synchronizer.setSyncCycles(5);
        synchronizer.start();
        getSyncListener().listen(synchronizer, 15); // wait a little longer to ease debugging
        getSyncListener().assertIfError();
        assertTrue(String.format("Cycle number was not 5 but %d.", getSyncListener().getCycleNo()), getSyncListener()
                .getCycleNo() >= 5);
    }

    @Override
    public boolean recurringTestComparison() throws StreamException, ConceptException, MDDException {
        int cycleNo = getSyncListener().getCycleNo();
        try {
            String changedDescrText = "XXXX Updated description text XXXX";
            if (cycleNo == 1) {
                assertEquals(7, jiraService.getIssues(DEFAULT_PROJECT, "").size());
                RequirementComp.defaultReqSpreadsheets(activeConfigs, getDaoFactory(), getItpmCon(), excelService,
                        googleService);
                RequirementComp.reqAfterSyncSemStore(activeConfigs, getDaoFactory(), getItpmCon());
                IssueComp.issueComparisonDefaultReqs(activeConfigs, getDaoFactory(), getItpmCon(), DEFAULT_PROJECT,
                        jiraService);

                ReqUtils.changeReqDescr(excelService, activeConfigs, "70", changedDescrText);
                /*
                 * //apply change directly in MS Spreadsheet to get events fired. DataSourceConfig reqConfig =
                 * TestHelper.attachConfigToService(excelService, activeConfigs); Spreadsheet reqSheet =
                 * excelService.retrieveSpreadsheet(reqConfig); SpreadsheetRow req70Row = null; Iterator<SpreadsheetRow>
                 * it = reqSheet.iterator(); int rowNumber = reqConfig.getStartRow() + 0; while (it.hasNext()) {
                 * SpreadsheetRow r = it.next(); if (r.getCell(1).getValue().equals("70")) { req70Row = r; break; }
                 * rowNumber++; } if (req70Row == null) { throw new
                 * StreamException(String.format("Value of Excel row 7, pos 1 is not 70!")); } else {
                 * req70Row.getCell(3).setValue(changedDescrText); } reqSheet.insertOrUpdate(req70Row, rowNumber);
                 * excelService.persistSpreadsheet(reqSheet);
                 */

            } else if (cycleNo == 3) {
                // at least here everything should be downstreamed
                RequirementComp.checkReq70DescrUpdate(activeConfigs, changedDescrText, excelService, googleService);
            } else if (cycleNo == 5) {
                RequirementComp.checkReq70DescrUpdate(activeConfigs, changedDescrText, excelService, googleService);
                IssueComp.issueComparisonDefaultReqs(activeConfigs, getDaoFactory(), getItpmCon(), DEFAULT_PROJECT,
                        jiraService);
                IssueComp.checkIssueDescrUpdate(activeConfigs, DEFAULT_PROJECT, "Show PM Data in Dashboard",
                        changedDescrText, jiraService);
            } else if (cycleNo == 7) {
                RequirementComp.checkReq70DescrUpdate(activeConfigs, changedDescrText, excelService, googleService);
                IssueComp.issueComparisonDefaultReqs(activeConfigs, getDaoFactory(), getItpmCon(), DEFAULT_PROJECT,
                        jiraService);
            }
        } catch (AssertionError e) {
            e.printStackTrace();
            fail(e.getMessage());
            return false;
        }
        return true;
    }
}
