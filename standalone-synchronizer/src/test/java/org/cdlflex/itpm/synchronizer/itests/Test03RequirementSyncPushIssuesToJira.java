package org.cdlflex.itpm.synchronizer.itests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.conceptdomain.model.ConceptException;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.domain.spreadsheet.SpreadsheetDomain;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetException;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetRow;
import org.cdlflex.itpm.generated.model.Issue;
import org.cdlflex.itpm.generated.model.Project;
import org.cdlflex.itpm.generated.model.Requirement;
import org.cdlflex.itpm.generated.model.Stakeholder;
import org.cdlflex.itpm.synchronizer.SynchronizerService;
import org.cdlflex.itpm.synchronizer.itests.reusable.IssueComp;
import org.cdlflex.itpm.synchronizer.itests.reusable.RequirementComp;
import org.cdlflex.itpm.synchronizer.itests.setup.AbstractITestPattern;
import org.cdlflex.itpm.synchronizer.itests.setup.TestSetupSceleton;
import org.cdlflex.itpm.synchronizer.itests.utils.TestHelper;
import org.cdlflex.itpm.synchronizer.standalone.ServiceFactory;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAO;
import org.junit.Before;
import org.junit.Test;
import org.openengsb.domain.issue.IssueDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Integration test for the services MSExcel, Google Spreadsheets, Jira Issues: Jira issues (features) are created from
 * requirements based on the workflow configuration.
 */
public class Test03RequirementSyncPushIssuesToJira extends TestSetupSceleton implements AbstractITestPattern {
    private static final Logger LOGGER = LoggerFactory.getLogger(Test03RequirementSyncPushIssuesToJira.class);
    private static final String DEFAULT_PROJECT = "itpm";
    private SpreadsheetDomain excelService;
    private SpreadsheetDomain googleService;
    private IssueDomain jiraService;
    private List<DataSourceConfig> activeConfigs;

    @Before
    @Override
    public void modifyConfigForTest() throws IllegalAccessError, MDDException, StreamException, ConceptException {
        boolean ignoreTests = isIgnoreTests();
        org.junit.Assume.assumeTrue(!ignoreTests);
        List<String> wantedServices = Arrays.asList(new String[] { "Requirements", "Jira" });
        activeConfigs = deactivateAllConfigsBut(wantedServices);
        TestHelper.checkActivatedConfigs(activeConfigs, wantedServices, 3);
        LOGGER.info(String.format("Activated configs: %d.", activeConfigs.size()));
        modifyServiceContentForTest();
    }

    @Override
    public void modifyServiceContentForTest() throws MDDException, StreamException, ConceptException {
        // find domain services and attach appropriate configurations for execution
        excelService = (SpreadsheetDomain) ServiceFactory.findService("spreadsheet", "excel");
        googleService = (SpreadsheetDomain) ServiceFactory.findService("spreadsheet", "google");
        jiraService = (IssueDomain) ServiceFactory.findService("issue", "jira");
        TestHelper.attachConfigToService(excelService, activeConfigs);
        TestHelper.attachConfigToService(googleService, activeConfigs);
        TestHelper.attachConfigToService(jiraService, activeConfigs);

        // remove everything from Google
        Iterator<SpreadsheetRow> rows =
            googleService.retrieveSpreadsheet(TestHelper.attachConfigToService(googleService, activeConfigs))
                    .iterator();
        while (rows.hasNext()) {
            rows.next();
            rows.remove();
        }
        // remove everything from Jira
        List<Project> jiraProjects = jiraService.getProjects("");
        for (Project jp : jiraProjects) {
            jiraService.removeProject(jp);
        }
    }

    @Override
    public void preTest() throws MDDException, SpreadsheetException, StreamException, ConceptException {
        // check central store
        assertEquals(0, sizeChecker(Issue.class));
        assertEquals(0, sizeChecker(Requirement.class));
        assertEquals(0, sizeChecker(Stakeholder.class));
        // check Google Spreadsheets
        Iterator<SpreadsheetRow> it =
            googleService.retrieveSpreadsheet((TestHelper.attachConfigToService(googleService, activeConfigs)))
                    .iterator();
        assertEquals(false, it.hasNext());
        // check Jira
        if (null != jiraService.getProject(DEFAULT_PROJECT)) {
            throw new StreamException(String.format("Default project %s not cleaned in Jira!", DEFAULT_PROJECT));
        }

        DAO<DataSourceConfig> dao = getDaoFactory().create(DataSourceConfig.class, getConfigCon());
        getConfigCon().connectOnDemand();
        List<DataSourceConfig> configs = dao.readFulltext("");
        assertTrue(configs.size() > 8);
    }

    @Test
    public void iTest() throws Exception {
        preTest();
        SynchronizerService synchronizer = getSyncService();
        synchronizer.setSyncCycles(3);
        synchronizer.start();
        getSyncListener().listen(synchronizer, 10);
        getSyncListener().assertIfError();
        int jiraIssueSize = jiraService.getIssues(DEFAULT_PROJECT, "").size();
        assertEquals(7, jiraIssueSize);
        RequirementComp.defaultReqSpreadsheets(activeConfigs, getDaoFactory(), getItpmCon(), excelService,
                googleService);
        RequirementComp.reqAfterSyncSemStore(activeConfigs, getDaoFactory(), getItpmCon());
        IssueComp.issueComparisonDefaultReqs(activeConfigs, getDaoFactory(), getItpmCon(), DEFAULT_PROJECT,
                jiraService);
    }

    @Override
    public boolean recurringTestComparison() throws StreamException, ConceptException {
        // not in use - override default action
        return true;
    }

    @Override
    public boolean callSyncCycleCompleted(int cycleNo) {
        // not in use - override default action
        return true;
    }
}
