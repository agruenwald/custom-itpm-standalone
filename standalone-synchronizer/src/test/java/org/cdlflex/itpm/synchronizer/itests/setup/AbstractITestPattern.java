package org.cdlflex.itpm.synchronizer.itests.setup;

import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.conceptdomain.model.ConceptException;
import org.cdlflex.mdd.sembase.MDDException;

/**
 * Abstract test pattern which is implemented by any itest to structure testing.
 */
public interface AbstractITestPattern {
    /**
     * Within this method the existing data sources (loaded by the configuration loader in the test environment) are
     * modified (e.g. activated, deactivated) to prepare the environment for the test.
     * 
     * @throws Exception
     */
    public void modifyConfigForTest() throws Exception;

    /**
     * Within this method the content (e.g. of the services) can be modified on demand (e.g. cleaned) to prepare the
     * environment for the test. The central repository is - following the standard setup procedure empty during this
     * phase.
     * 
     * @throws Exception
     */
    public void modifyServiceContentForTest() throws Exception;

    /**
     * Can be used to check if the environment has been setup correctly. It should be called as the first step of a new
     * test.
     * 
     * @throws Exception
     */
    public void preTest() throws Exception;

    /**
     * Implementation of the integration test.
     */
    public void iTest() throws Exception;

    /**
     * Can be optionally implemented to encapsulate a number of data comparisons into a method which can be reused. For
     * instance if a synchronization takes place several times this step could be repeated over and over after each
     * single synchronization step.
     * 
     * @return false if a test failed.
     * @throws StreamException
     */
    public boolean recurringTestComparison() throws StreamException, ConceptException, MDDException;
}
