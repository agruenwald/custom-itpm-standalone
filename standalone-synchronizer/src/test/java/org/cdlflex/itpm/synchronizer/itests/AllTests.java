package org.cdlflex.itpm.synchronizer.itests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
// Test001AssertionBubbling.class,
// Test002WriteExcelFile.class,
// failure: (in configLoader): Object property [has, "/Users/andreas_gruenwald...excel.xslx^^http://www.. of datasoure
// config is null (with the DAO)
// works -> fails when tests before are started? Test01RequirementSyncExcelStartup.class,
// assertion: "no way"
// works -> fails when tests before are started? Test02RequirementSyncWithGoogleSpread.class,
// Test03RequirementSyncPushIssuesToJira.class,
// Test04RequirementSyncPushIssuesToRedmineToo.class,
// Test05UpdateReqDescrJiraNoRedmine.class,
// Test06SyncUpdateReqDescrJiraRedmine.class,
// Test07SyncUpdateReqPrioJiraRedmine.class,
// java.lang.AssertionError: Could not update issue "XXXX Updated description (in Jira) XXXX" for project itpm.
// Test08SyncUpdateIssueDescr.class,
// expected 7 but was 0
// Test09UpdateCentralIssuePrio.class,

// expected 7 but was 0
// Test10AddIssueInJira.class,
// epected 7 but was zero
// Test10aUpdateCentralIssueDescr.class,
// is ignored becasue of several circumstances Test11RemoveIssueInJira.class,
// Test12AddIssuesWithComponents.class,
// Test13AddIssuesAndAssignThem.class
})
public class AllTests {

}
