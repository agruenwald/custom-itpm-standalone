package org.cdlflex.itpm.synchronizer.itests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.conceptdomain.model.ConceptException;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetException;
import org.cdlflex.itpm.generated.model.Bug;
import org.cdlflex.itpm.generated.model.Collaborator;
import org.cdlflex.itpm.generated.model.Issue;
import org.cdlflex.itpm.generated.model.Product;
import org.cdlflex.itpm.generated.model.Project;
import org.cdlflex.itpm.generated.model.Requirement;
import org.cdlflex.itpm.generated.model.Stakeholder;
import org.cdlflex.itpm.synchronizer.SynchronizerService;
import org.cdlflex.itpm.synchronizer.itests.reusable.IssueComp;
import org.cdlflex.itpm.synchronizer.itests.setup.AbstractITestPattern;
import org.cdlflex.itpm.synchronizer.itests.setup.Config;
import org.cdlflex.itpm.synchronizer.itests.setup.TestSetupSceleton;
import org.cdlflex.itpm.synchronizer.itests.utils.TestHelper;
import org.cdlflex.itpm.synchronizer.standalone.ServiceFactory;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAO;
import org.junit.Before;
import org.junit.Test;
import org.openengsb.domain.issue.IssueDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test the usage of components together with issues (Jira and central repo).
 */
public class Test12AddIssuesWithComponents extends TestSetupSceleton implements AbstractITestPattern {
    private static final Logger LOGGER = LoggerFactory.getLogger(Test04RequirementSyncPushIssuesToRedmineToo.class);
    private static final String DEFAULT_PROJECT = "itpm";
    private IssueDomain jiraService;
    private IssueDomain redmineService;
    private List<DataSourceConfig> activeConfigs;

    @Before
    @Override
    public void modifyConfigForTest() throws IllegalAccessError, MDDException, StreamException, ConceptException {
        boolean ignoreTests = isIgnoreTests();
        org.junit.Assume.assumeTrue(!ignoreTests);
        List<String> wantedServices = Arrays.asList(new String[] { Config.JIRA, Config.REDMINE });
        activeConfigs = deactivateAllConfigsBut(wantedServices);
        TestHelper.checkActivatedConfigs(activeConfigs, wantedServices, 2);
        LOGGER.info(String.format("Activated configs: %d.", activeConfigs.size()));
        modifyServiceContentForTest();
    }

    @Override
    public void modifyServiceContentForTest() throws MDDException, StreamException, ConceptException {
        // find domain services and attach appropriate configurations for execution
        jiraService = (IssueDomain) ServiceFactory.findService("issue", "jira");
        redmineService = (IssueDomain) ServiceFactory.findService("issue", "redmine");
        TestHelper.attachConfigToService(jiraService, activeConfigs);
        TestHelper.attachConfigToService(redmineService, activeConfigs);

        // remove everything from Jira
        List<Project> jiraProjects = jiraService.getProjects("");
        for (Project jp : jiraProjects) {
            jiraService.removeProject(jp);
        }
        // remove everything from Redmine
        List<Project> redmineProjects = redmineService.getProjects("");
        for (Project jp : redmineProjects) {
            redmineService.removeProject(jp);
        }
    }

    @Override
    public void preTest() throws MDDException, SpreadsheetException, StreamException, ConceptException {
        // check central store
        assertEquals(0, sizeChecker(Issue.class));
        assertEquals(0, sizeChecker(Requirement.class));
        assertEquals(0, sizeChecker(Stakeholder.class));

        // check Jira
        if (null != jiraService.getProject(DEFAULT_PROJECT)) {
            throw new StreamException(String.format("Default project %s not cleaned in Jira!", DEFAULT_PROJECT));
        }

        // check Redmine
        if (null != redmineService.getProject(DEFAULT_PROJECT)) {
            throw new StreamException(String.format("Default project %s not cleaned in Redmine!", DEFAULT_PROJECT));
        }
    }

    @Test
    public void iTest() throws Exception {
        final int definedCycles = 2;
        preTest();
        SynchronizerService synchronizer = getSyncService();
        synchronizer.setSyncCycles(definedCycles);
        synchronizer.start();
        getSyncListener().listen(synchronizer);
        getSyncListener().assertIfError();
        assertTrue(String.format("Cycle number was not %d but %d.", synchronizer.getSyncCylces(), getSyncListener()
                .getCycleNo()), getSyncListener().getCycleNo() >= definedCycles);
    }

    @Override
    public boolean recurringTestComparison() throws StreamException, ConceptException, MDDException {
        int cycleNo = getSyncListener().getCycleNo();
        List<String> issueNames =
            Arrays.asList(new String[] { "OSGI-Compatibility of umlTUowl", "Improved JSON transformation umlTUowl",
                "Replace JSON completely by OWL storage", "Standalone-Version of umlTUmany", "Publish documentation",
                "Improve UI-forms in custom-itpm", "Activity Stream for custom-itpm",
                "Fix synchronizer overview in custom-itpm", "Remove tables which occur twice in custom-itpm",
                "Build standalone-version for custom-itpm",
                "Build configuration for custom-itpm jetty server (standalone)" });
        try {
            if (cycleNo == 1) {
                DAO<Product> artifactDAO = getDaoFactory().create(Product.class, getItpmCon());

                Product umlTUowl = new Product();
                umlTUowl.setId("umlTUowl");
                umlTUowl.setDescription("umlTUowl Description");
                artifactDAO.insert(umlTUowl);

                Product umlTUmany = new Product();
                umlTUmany.setId("umlTUmany");
                umlTUmany.setDescription("umlTUmany Description");
                artifactDAO.insert(umlTUmany);

                Product customitpm = new Product();
                customitpm.setId("custom-itpm");
                customitpm.setDescription("customitpm description");
                artifactDAO.insert(customitpm);

                List<Product> products = Arrays.asList(new Product[] { umlTUowl, umlTUmany, customitpm });
                for (Product p : products) {
                    jiraService.addArtifact(DEFAULT_PROJECT, p);
                }

                Collaborator agr = getDaoFactory().create(Collaborator.class, getItpmCon()).readRecord("agr");

                for (String issueName : issueNames) {
                    Issue issue1 = new Issue();
                    issue1.setName(issueName);
                    issue1.setDescription("This issue has been created in Jira during the test "
                        + this.getClass().getSimpleName());
                    issue1.setPriority(3);
                    issue1.setIssueType(new Bug());
                    issue1.setIssueReportedBy(agr);
                    if (issueName.contains("umlTUowl")) {
                        issue1.setRelatesToComponent(umlTUowl);
                    } else if (issueName.contains("umlTUmany")) {
                        issue1.setRelatesToComponent(umlTUmany);
                    } else {
                        issue1.setRelatesToComponent(customitpm);
                    }
                    jiraService.addIssue(DEFAULT_PROJECT, issue1);
                }
                List<Issue> jiraIssues = jiraService.getIssues(DEFAULT_PROJECT, "");
                assertEquals(issueNames.size(), jiraIssues.size());
            } else if (cycleNo == 2) {
                // compressed pre-test (existence of issues)
                IssueComp.issueComparisonJiraTest(activeConfigs, getDaoFactory(), getItpmCon(), DEFAULT_PROJECT,
                        jiraService, redmineService);
                List<Issue> centralIssues = getDaoFactory().create(Issue.class, getItpmCon()).readFulltext("");
                assertEquals(issueNames.size(), centralIssues.size());
                // check if products are correctly assigned
                for (Issue iss : centralIssues) {
                    String name = iss.getName();
                    String comp = "";
                    if (name.contains("umlTUowl")) {
                        comp = "umlTUowl";
                    } else if (name.contains("umlTUmany")) {
                        comp = "umlTUmany";
                    } else {
                        comp = "custom-itpm";
                    }
                    assertEquals(comp, iss.getRelatesToComponent().getId());
                }
            }
        } catch (AssertionError e) {
            e.printStackTrace();
            fail(e.getMessage());
            return false;
        }
        return true;
    }
}
