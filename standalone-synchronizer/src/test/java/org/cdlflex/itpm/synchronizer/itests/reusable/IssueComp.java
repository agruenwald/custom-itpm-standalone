package org.cdlflex.itpm.synchronizer.itests.reusable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.conceptdomain.model.ConceptException;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.itpm.generated.model.Issue;
import org.cdlflex.itpm.generated.model.Product;
import org.cdlflex.itpm.generated.model.Requirement;
import org.cdlflex.itpm.generated.model.Stakeholder;
import org.cdlflex.itpm.synchronizer.itests.utils.IssueUtils;
import org.cdlflex.itpm.synchronizer.itests.utils.TestHelper;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAO;
import org.cdlflex.mdd.sembase.semaccess.DAOFactory;
import org.openengsb.domain.issue.IssueDomain;

public class IssueComp {

    /**
     * Comparison of the Issues (default) after sending the requirements to the issue domains without any additional
     * modifications
     * 
     * @param activeConfigs
     * @param factory
     * @param itpmCon
     * @param defaultProjectId itpm or so
     * @param domainServices >= 1 services
     * @throws StreamException
     * @throws ConceptException
     * @throws Exception
     */
    public static void issueComparisonDefaultReqs(List<DataSourceConfig> activeConfigs, DAOFactory factory,
        Connection itpmCon, String defaultProjectId, IssueDomain... issueServices) throws MDDException,
        ConceptException, StreamException {

        // basic tests in the services
        for (IssueDomain ds : issueServices) {
            assertEquals(7, ds.getIssues(defaultProjectId, "").size());
        }

        issuesAfterRecSyncSemStore(activeConfigs, factory, itpmCon);

        String sampleIssueName = "Show PM Data in Dashboard";
        List<Issue> issues =
            IssueUtils.collectSameIssues(issueServices, factory, itpmCon, sampleIssueName, defaultProjectId);
        assertTrue(IssueUtils.compareIssues(issues));
    }

    /**
     * Same as {@link #issueComparisonDefaultReqs(List, DAOFactory, Connection, String, IssueDomain...)} but with two
     * essential differences. 1) No requirements services are used and hence no requirements and no issues which are
     * derived from requirements will exist. 2) Not all issues will be compared but only a single on. For this issue the
     * name must be passed.
     * 
     * @param activeConfigs
     * @param factory
     * @param itpmCon
     * @param defaultProjectId
     * @param sampleIssueName the unique name of an issue.
     * @param issueServices
     * @throws MDDException
     * @throws ConceptException
     * @throws StreamException
     * 
     *         public static void issueComparisonWithoutReqs( List<DataSourceConfig> activeConfigs, DAOFactory factory,
     *         Connection itpmCon, String defaultProjectId, String sampleIssueName, IssueDomain... issueServices) throws
     *         MDDException, ConceptException, StreamException {
     * 
     *         //issuesAfterRecSyncSemStore(activeConfigs, factory, itpmCon); List<Issue> issues =
     *         IssueUtils.collectSameIssues(issueServices, factory, itpmCon, sampleIssueName, defaultProjectId);
     *         assertTrue(IssueUtils.compareIssues(issues));
     * 
     *         }
     */

    /**
     * Compare if issues inserted into Jira have been distributed to the other services and if their content is the same
     * 
     * @param activeConfigs
     * @param factory
     * @param itpmCon
     * @param defaultProjectId
     * @param issueServices the first must be Jira, then redmine etc. should follow
     * @throws MDDException
     * @throws ConceptException
     * @throws StreamException
     */
    public static void issueComparisonJiraTest(List<DataSourceConfig> activeConfigs, DAOFactory factory,
        Connection itpmCon, String defaultProjectId, IssueDomain... issueServices) throws MDDException,
        ConceptException, StreamException {

        List<Issue> jiraIssues = issueServices[0].getIssues(defaultProjectId, "");
        for (Issue issue : jiraIssues) {
            List<Issue> issues =
                IssueUtils.collectSameIssues(issueServices, factory, itpmCon, issue.getName(), defaultProjectId);
            assertTrue(IssueUtils.compareIssues(issues));
        }
    }

    /**
     * Issue Data in semantic store after the synchronization of requirments with active workflow engine.
     * 
     * @param activeConfigs
     * @param daoFactory
     * @param itpmCon
     */
    public static void issuesAfterRecSyncSemStore(List<DataSourceConfig> activeConfigs, DAOFactory factory,
        Connection itpmCon) throws MDDException {

        assertEquals(7, TestHelper.sizeChecker(factory, itpmCon, Requirement.class));
        assertEquals(5, TestHelper.sizeChecker(factory, itpmCon, Stakeholder.class));
        assertTrue(7 <= TestHelper.sizeChecker(factory, itpmCon, Product.class));
        assertEquals(7, TestHelper.sizeChecker(factory, itpmCon, Issue.class));

        DAO<Requirement> reqDAO = factory.create(Requirement.class, itpmCon);
        reqDAO.setReadLinkedObjectAttributesLevel1(true);
        reqDAO.setReadLinkedObjectsLevel1(true);
    }

    public static void checkIssueDescrUpdate(List<DataSourceConfig> activeConfigs, String defaultProjectId,
        String issueName, String descrText, IssueDomain... domainServices) throws ConceptException {

        for (IssueDomain ds : domainServices) {
            Issue issue = ds.getIssueByName(defaultProjectId, issueName);
            assertEquals(descrText, issue.getDescription());
        }
    }

    public static void checkIssuePrioUpdate(List<DataSourceConfig> activeConfigs, String defaultProjectId,
        String issueName, int prioChanged, IssueDomain... domainServices) throws ConceptException {

        for (IssueDomain ds : domainServices) {
            Issue issue = ds.getIssueByName(defaultProjectId, issueName);
            assertEquals(prioChanged, issue.getPriority());
        }
    }

    public static boolean checkNumberOfIssues(IssueDomain domainService, String defaultProject, int numberOfIssues)
        throws ConceptException {
        boolean cleanRecord = domainService.getIssues(defaultProject, "").size() == numberOfIssues;
        assertEquals(numberOfIssues, domainService.getIssues(defaultProject, "").size());
        return cleanRecord;
    }
}
