package org.cdlflex.itpm.synchronizer.itests.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.conceptdomain.model.ConceptException;
import org.cdlflex.itpm.generated.model.Issue;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAOFactory;
import org.openengsb.domain.issue.IssueDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utils to ease Issue domain service access and comparisons
 */
public class IssueUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(IssueUtils.class);

    public static boolean compareIssues(List<Issue> issues) {
        Issue issue1 = issues.get(0);
        boolean cleanRecord = true;
        for (int i = 1; i < issues.size(); i++) {
            Issue issue2 = issues.get(i);
            cleanRecord = compareTwoIssuesBasic(issue1, issue2);
            if (!cleanRecord) {
                break;
            }
        }
        return cleanRecord;
    }

    public static boolean compareTwoIssuesBasic(Issue issue1, Issue issue2) {
        boolean cleanRecord = true;
        cleanRecord = cleanRecord ? issue1.getName().equals(issue2.getName()) : false;
        cleanRecord = cleanRecord ? issue1.getPriority() == issue2.getPriority() : false;
        cleanRecord = cleanRecord ? issue1.getDescription().equals(issue1.getDescription()) : false;
        // cleanRecord = cleanRecord ?
        // issue1.getIssueAssignedTo().getEmail().equals(issue2.getIssueAssignedTo().getEmail()) : false;
        cleanRecord = cleanRecord ? issue1.getIssueReportedBy() != null : false;
        cleanRecord = cleanRecord ? issue2.getIssueReportedBy() != null : false;
        cleanRecord = cleanRecord ? issue1.getIssueType().getId().equals(issue2.getIssueType().getId()) : false;
        if (!cleanRecord || !issue1.getIssueReportedBy().getEmail().equals(issue2.getIssueReportedBy().getEmail())) {
            String line = "-----------------------------------------------------------\n";
            String cmpl = "%.25s		%.25s\n";
            String msg = "\n" + line;
            msg += String.format(cmpl, issue1.getName(), issue2.getName());
            msg += String.format(cmpl, issue1.getPriority(), issue2.getPriority());
            msg += String.format(cmpl, issue1.getDescription(), issue2.getDescription());
            msg += String.format(cmpl, issue1.getPriority(), issue2.getPriority());
            msg += String.format(cmpl, "Collaborator 1", "Collaborator 2");
            msg += String.format(cmpl, issue1.getIssueReportedBy().getId(), issue2.getIssueReportedBy().getId());
            msg += String.format(cmpl, issue1.getIssueReportedBy().getName(), issue2.getIssueReportedBy().getName());
            msg +=
                String.format(cmpl, issue1.getIssueReportedBy().getEmail(), issue2.getIssueReportedBy().getEmail());

            msg += String.format(cmpl, issue1.getIssueType().getId(), issue2.getIssueType().getId());
            msg = msg + line;

            LOGGER.warn(String
                    .format("Unfortuantely the reports cannot be compared because Redmine does not allow any reporter !="
                        + " the logged-in user. The compared values are listed below. Please ensure that this message is irrelevant: %s",
                            msg));
        }
        return cleanRecord;
    }

    /**
     * Collect all issues with the same name/identity from all available domain issue services and from the central
     * repository.
     * 
     * @param issueServices all available issue services
     * @param factory
     * @param itpmCon
     * @param sampleIssueName the name of the sampled issue.
     * @param defaultProjectId e.g. itpm
     * @return a list of issues. The list contains instances of the same issue but read from different locations
     *         (central repo and issue domains).
     * @throws MDDException
     * @throws ConceptException
     * @throws StreamException
     */
    public static List<Issue> collectSameIssues(IssueDomain[] issueServices, DAOFactory factory, Connection itpmCon,
        String sampleIssueName, String defaultProjectId) throws MDDException, ConceptException, StreamException {
        List<Issue> issueCollection = new ArrayList<Issue>();
        Map<String, String> searchMap = new HashMap<String, String>();
        searchMap.put("name", sampleIssueName);
        Issue centralIssue = factory.create(Issue.class, itpmCon).readRecordByAttributeValues(searchMap).get(0);
        if (centralIssue == null) {
            throw new StreamException(
                    String.format("Failed to read issue with name=%s (centrally).", sampleIssueName));
        } else {
            issueCollection.add(centralIssue);
        }

        for (IssueDomain ds : issueServices) {
            Issue iss = ds.getIssueByName(defaultProjectId, sampleIssueName);
            if (iss == null) {
                throw new StreamException(String.format("Could not find issue %s in service %s.", sampleIssueName,
                        ds.getInstanceId()));
            } else {
                issueCollection.add(iss);
            }
        }
        return issueCollection;
    }
}
