package org.cdlflex.itpm.synchronizer.itests.reusable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.domain.spreadsheet.SpreadsheetDomain;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetException;
import org.cdlflex.itpm.generated.model.Issue;
import org.cdlflex.itpm.generated.model.Product;
import org.cdlflex.itpm.generated.model.Requirement;
import org.cdlflex.itpm.generated.model.Stakeholder;
import org.cdlflex.itpm.synchronizer.itests.utils.SpreadsheetUtils;
import org.cdlflex.itpm.synchronizer.itests.utils.TestHelper;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAO;
import org.cdlflex.mdd.sembase.semaccess.DAOFactory;

public class RequirementComp {

    /**
     * Comparison of the default Requirement configuration (after successful synchronization of requirement sheets).
     * 
     * @param activeConfigs
     * @param factory
     * @param itpmCon
     * @param domainServices >= 1 services
     * @throws MDDException
     * @throws SpreadsheetException
     * @throws StreamException
     * @return false if a test failed.
     */
    public static boolean defaultReqSpreadsheets(List<DataSourceConfig> activeConfigs, DAOFactory factory,
        Connection itpmCon, SpreadsheetDomain... domainServices) throws MDDException, SpreadsheetException,
        StreamException {
        // basic tests in the services
        for (SpreadsheetDomain ds : domainServices) {
            assertEquals(7, SpreadsheetUtils.getSpreadsheetRows(ds, activeConfigs).size());
        }

        assertTrue(SpreadsheetUtils.compareRowContent(activeConfigs, domainServices));
        SpreadsheetUtils su = new SpreadsheetUtils(activeConfigs, domainServices);
        boolean cleanRecord = true;

        // negative Test
        cleanRecord = cleanRecord ? !su.checkCellContent(1, 1, "999") : false;
        assertTrue(cleanRecord);

        // title and id of first requirement
        cleanRecord = cleanRecord ? su.checkCellContent(1, 1, "10") : false;
        cleanRecord = cleanRecord ? su.checkCellContent(1, 2, "Translate Models into Ontologies and Code") : false;
        // reference to parent Id
        cleanRecord = cleanRecord ? su.checkCellContent(3, 4, "10") : false;
        // title of last requirement
        cleanRecord = cleanRecord ? su.checkCellContent(7, 2, "Show PM Data in Dashboard") : false;

        // inter-related requirements
        cleanRecord = cleanRecord ? su.checkCellContent(4, 5, "10", true) : false;
        cleanRecord = cleanRecord ? su.checkCellContent(5, 5, "10;40", true) : false;
        cleanRecord = cleanRecord ? su.checkCellContent(6, 5, "10;40", true) : false;
        cleanRecord = cleanRecord ? su.checkCellContent(7, 5, "10;40;70", true) : false;

        // priorities
        cleanRecord = cleanRecord ? su.checkCellContent(1, 7, "2") : false;
        cleanRecord = cleanRecord ? su.checkCellContent(2, 7, "3") : false;
        cleanRecord = cleanRecord ? su.checkCellContent(3, 7, "3") : false;
        cleanRecord = cleanRecord ? su.checkCellContent(4, 7, "4") : false;
        cleanRecord = cleanRecord ? su.checkCellContent(5, 7, "2") : false;
        cleanRecord = cleanRecord ? su.checkCellContent(6, 7, "1") : false;
        cleanRecord = cleanRecord ? su.checkCellContent(7, 7, "1") : false;

        // acceptance criteria
        cleanRecord = cleanRecord ? su.checkCellContent(7, 8, "Demo ok") : false;

        // artifacts
        cleanRecord = cleanRecord ? su.checkCellContent(7, 9, "custom-itpm-standalone") : false;

        // method of verification
        cleanRecord = cleanRecord ? su.checkCellContent(7, 10, "") : false;
        cleanRecord = cleanRecord ? su.checkCellContent(1, 10, "Test models are translated into OWL models.") : false;

        // method of validation
        cleanRecord = cleanRecord ? su.checkCellContent(7, 11, "") : false;
        cleanRecord = cleanRecord ? su.checkCellContent(1, 11, "") : false;

        // Stakeholder
        cleanRecord = cleanRecord ? su.checkCellContent(1, 12, "Andreas;Estefania", true) : false;
        cleanRecord = cleanRecord ? su.checkCellContent(2, 12, "Andreas;Estefania", true) : false;
        cleanRecord = cleanRecord ? su.checkCellContent(3, 12, "Andreas;Estefania", true) : false;
        cleanRecord = cleanRecord ? su.checkCellContent(4, 12, "Andreas;Estefania", true) : false;
        cleanRecord = cleanRecord ? su.checkCellContent(5, 12, "Andreas;Dietmar", true) : false;
        cleanRecord = cleanRecord ? su.checkCellContent(6, 12, "Andreas;Dietmar;Richard", true) : false;
        cleanRecord = cleanRecord ? su.checkCellContent(7, 12, "Andreas;Dietmar;Stefan", true) : false;

        assertTrue(cleanRecord);
        return cleanRecord;
    }

    /**
     * Requirement Data in semantic store after a synchronization process.
     * 
     * @param activeConfigs
     * @param daoFactory
     * @param itpmCon
     */
    public static void reqAfterSyncSemStore(List<DataSourceConfig> activeConfigs, DAOFactory factory,
        Connection itpmCon) throws MDDException {
        assertEquals(7, TestHelper.sizeChecker(factory, itpmCon, Requirement.class));
        assertEquals(5, TestHelper.sizeChecker(factory, itpmCon, Stakeholder.class));
        assertTrue(7 <= TestHelper.sizeChecker(factory, itpmCon, Product.class));
        assertEquals(7, TestHelper.sizeChecker(factory, itpmCon, Issue.class));

        DAO<Requirement> reqDAO = factory.create(Requirement.class, itpmCon);
        reqDAO.setReadLinkedObjectAttributesLevel1(true);
        reqDAO.setReadLinkedObjectsLevel1(true);
        sampleReq70(reqDAO);
    }

    public static void sampleReq70(DAO<Requirement> reqDAO) throws MDDException {
        reqDAO.setReadLinkedObjectAttributesLevel1(true);
        reqDAO.setReadLinkedObjectsLevel1(true);
        Requirement req70 = reqDAO.readRecord("70");
        assertEquals("Show PM Data in Dashboard", req70.getName());
        assertEquals(3, req70.getDependsonParent().size());
        assertEquals(1, req70.getProduceArtifact().size());
        assertEquals("custom-itpm-standalone", req70.getProduceArtifact().iterator().next().getId());
        assertEquals(3, req70.getDefine().size());
        assertEquals(1, req70.getPriority());
    }

    /**
     * Check if the description has been updated with some text
     * 
     * @param activeConfigs
     * @param domainServices
     * @throws SpreadsheetException
     * @throws StreamException
     */
    public static void checkReq70DescrUpdate(List<DataSourceConfig> activeConfigs, String descText,
        SpreadsheetDomain... domainServices) throws SpreadsheetException, StreamException {
        // basic tests in the services
        for (SpreadsheetDomain ds : domainServices) {
            assertEquals(7, SpreadsheetUtils.getSpreadsheetRows(ds, activeConfigs).size());
        }
        assertTrue(SpreadsheetUtils.compareRowContent(activeConfigs, domainServices));
        SpreadsheetUtils su = new SpreadsheetUtils(activeConfigs, domainServices);
        assertTrue(su.checkCellContent(7, 3, descText));
    }

    public static void checkReq30PrioUpdate(List<DataSourceConfig> activeConfigs, int prioNew,
        SpreadsheetDomain... domainServices) throws SpreadsheetException, StreamException {
        // basic tests in the services
        for (SpreadsheetDomain ds : domainServices) {
            assertEquals(7, SpreadsheetUtils.getSpreadsheetRows(ds, activeConfigs).size());
        }
        assertTrue(SpreadsheetUtils.compareRowContent(activeConfigs, domainServices));
        SpreadsheetUtils su = new SpreadsheetUtils(activeConfigs, domainServices);
        assertTrue(su.checkCellContent(3, 7, String.valueOf(prioNew)));
    }
}
