package org.cdlflex.itpm.synchronizer.itests;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.conceptdomain.model.ConceptException;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.domain.spreadsheet.SpreadsheetDomain;
import org.cdlflex.domain.spreadsheet.model.Spreadsheet;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetException;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetRow;
import org.cdlflex.itpm.synchronizer.itests.reusable.RequirementComp;
import org.cdlflex.itpm.synchronizer.itests.setup.AbstractITestPattern;
import org.cdlflex.itpm.synchronizer.itests.setup.TestSetupSceleton;
import org.cdlflex.itpm.synchronizer.itests.utils.TestHelper;
import org.cdlflex.itpm.synchronizer.standalone.ServiceFactory;
import org.cdlflex.mdd.sembase.MDDException;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test if changes are correctly persisted in the MS Excel spreadsheet.
 */
public class Test002WriteExcelFile extends TestSetupSceleton implements AbstractITestPattern {
    private static final Logger LOGGER = LoggerFactory.getLogger(Test002WriteExcelFile.class);
    private SpreadsheetDomain excelService;
    private List<DataSourceConfig> activeConfigs;

    @Before
    @Override
    public void modifyConfigForTest() throws IllegalAccessError, MDDException, StreamException, ConceptException {
        boolean ignoreTests = isIgnoreTests();
        org.junit.Assume.assumeTrue(!ignoreTests);
        List<String> wantedServices = Arrays.asList(new String[] { "RequirementsMSExcel" });
        activeConfigs = deactivateAllConfigsBut(wantedServices);
        TestHelper.checkActivatedConfigs(activeConfigs, wantedServices, 1);
        LOGGER.info(String.format("Activated configs: %d.", activeConfigs.size()));
        modifyServiceContentForTest();
    }

    @Override
    public void modifyServiceContentForTest() throws MDDException, StreamException, ConceptException {
        // find domain services and attach appropriate configurations for execution
        excelService = (SpreadsheetDomain) ServiceFactory.findService("spreadsheet", "excel");
    }

    @Override
    public void preTest() throws MDDException, SpreadsheetException, StreamException, ConceptException {
    }

    @Test
    public void iTest() throws Exception {
        DataSourceConfig reqConfig = TestHelper.attachConfigToService(excelService, activeConfigs);
        LOGGER.info(String.format("MS Excel file at %s.", excelService.getHumanURL()));

        Spreadsheet reqSheet = excelService.retrieveSpreadsheet(reqConfig);
        SpreadsheetRow req70Row = null;
        Iterator<SpreadsheetRow> it = reqSheet.iterator();
        int rowNumber = reqConfig.getStartRow() + 0;
        while (it.hasNext()) {
            SpreadsheetRow r = it.next();
            if (r.getCell(1).getValue().equals("70")) {
                req70Row = r;
                break;
            }
            rowNumber++;
        }
        if (req70Row == null) {
            throw new StreamException(String.format("Value of Excel row 7, pos 1 is not 70!"));
        } else {
            req70Row.getCell(3).setValue("XXXXXXXXXX");
        }
        reqSheet.insertOrUpdate(req70Row, rowNumber);
        excelService.persistSpreadsheet(reqSheet);
        RequirementComp.checkReq70DescrUpdate(activeConfigs, "XXXXXXXXXX", excelService);
    }

    @Override
    public boolean callSyncCycleCompleted(int cycleNo) {
        return true; // not in use
    }

    @Override
    public boolean recurringTestComparison() throws StreamException, ConceptException, MDDException {
        // NOT IN USE
        return true;
    }
}
