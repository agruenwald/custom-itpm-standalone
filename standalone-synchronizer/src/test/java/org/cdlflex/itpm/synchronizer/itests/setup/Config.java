package org.cdlflex.itpm.synchronizer.itests.setup;

public class Config {
    public static final String REQ_SERVICES = "Requirements";
    public static final String REQ_MS_EXCEL = "Requirements MSExcel";
    public static final String REQ_GOOGLE = "...";
    public static final String JIRA = "Jira";
    public static final String REDMINE = "Redmine at Digital Ocean";
    public static final String SCRUMWISE = "Scrumwise";
}
