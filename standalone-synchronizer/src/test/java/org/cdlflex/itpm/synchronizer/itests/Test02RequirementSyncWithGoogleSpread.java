package org.cdlflex.itpm.synchronizer.itests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.conceptdomain.model.ConceptException;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.domain.spreadsheet.SpreadsheetDomain;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetException;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetRow;
import org.cdlflex.itpm.generated.model.Issue;
import org.cdlflex.itpm.generated.model.Requirement;
import org.cdlflex.itpm.generated.model.Stakeholder;
import org.cdlflex.itpm.synchronizer.ISyncCallBackObject;
import org.cdlflex.itpm.synchronizer.SynchronizerService;
import org.cdlflex.itpm.synchronizer.itests.reusable.RequirementComp;
import org.cdlflex.itpm.synchronizer.itests.setup.AbstractITestPattern;
import org.cdlflex.itpm.synchronizer.itests.setup.TestSetupSceleton;
import org.cdlflex.itpm.synchronizer.itests.utils.TestHelper;
import org.cdlflex.itpm.synchronizer.standalone.ServiceFactory;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semaccess.DAO;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Requirements are synched between Excel and Google. (they pre-exist in MS Excel). Prerequisite for the execution of
 * the tests: the unit tests of the connectors are on a stable level.
 */
public class Test02RequirementSyncWithGoogleSpread extends TestSetupSceleton implements AbstractITestPattern,
        ISyncCallBackObject {
    private static final Logger LOGGER = LoggerFactory.getLogger(Test02RequirementSyncWithGoogleSpread.class);
    private SpreadsheetDomain excelService;
    private SpreadsheetDomain googleService;
    private List<DataSourceConfig> activeConfigs;

    @Before
    @Override
    public void modifyConfigForTest() throws IllegalAccessError, MDDException, StreamException, ConceptException {
        boolean ignoreTests = isIgnoreTests();
        org.junit.Assume.assumeTrue(!ignoreTests);
        // Google +
        activeConfigs = deactivateAllConfigsBut(Arrays.asList(new String[] { "Requirements" }));
        if (activeConfigs.size() != 2) {
            throw new MDDException(String.format(
                    "Activated services != 3 (was %d). (Google Spreadsheet, Excel Requirements wanted.)",
                    activeConfigs.size()));
        }
        LOGGER.info(String.format("Activated configs: %d.", activeConfigs.size()));
        modifyServiceContentForTest();
    }

    @Override
    public void modifyServiceContentForTest() throws MDDException, StreamException, ConceptException {
        // find domain services and attach appropriate configurations for execution
        excelService = (SpreadsheetDomain) ServiceFactory.findService("spreadsheet", "excel");
        googleService = (SpreadsheetDomain) ServiceFactory.findService("spreadsheet", "google");
        TestHelper.attachConfigToService(excelService, activeConfigs);
        TestHelper.attachConfigToService(googleService, activeConfigs);

        // remove everything from Google
        Iterator<SpreadsheetRow> rows =
            googleService.retrieveSpreadsheet(TestHelper.attachConfigToService(googleService, activeConfigs))
                    .iterator();
        while (rows.hasNext()) {
            rows.next();
            rows.remove();
        }
        // clear the central store as well.
        getItpmCon().connectOnDemand();
        // ok, that's it let's start the test

    }

    @Override
    public void preTest() throws MDDException, SpreadsheetException, StreamException, ConceptException {
        // check central store
        assertEquals(0, sizeChecker(Issue.class));
        assertEquals(0, sizeChecker(Requirement.class));
        assertEquals(0, sizeChecker(Stakeholder.class));
        // check Google Spreadsheets
        Iterator<SpreadsheetRow> it =
            googleService.retrieveSpreadsheet((TestHelper.attachConfigToService(googleService, activeConfigs)))
                    .iterator();
        assertEquals(false, it.hasNext());
        DAO<DataSourceConfig> dao = getDaoFactory().create(DataSourceConfig.class, getConfigCon());
        getConfigCon().connectOnDemand();
        List<DataSourceConfig> configs = dao.readFulltext("");
        assertTrue(configs.size() > 8);
    }

    @Test
    public void iTest() throws Exception {
        preTest();
        SynchronizerService synchronizer = getSyncService();
        synchronizer.setSyncCycles(3);
        synchronizer.start();
        getSyncListener().listen(synchronizer, 10);
        getSyncListener().assertIfError();
        RequirementComp.defaultReqSpreadsheets(activeConfigs, getDaoFactory(), getItpmCon(), excelService,
                googleService);
        RequirementComp.reqAfterSyncSemStore(activeConfigs, getDaoFactory(), getItpmCon());
    }

    @Override
    public boolean recurringTestComparison() throws StreamException {
        return true;
    }

    @Override
    public boolean callSyncCycleCompleted(int cycleNo) {
        // not in use - override default action
        return true;
    }
}
