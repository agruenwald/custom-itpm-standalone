package org.cdlflex.itpm.synchronizer.itests.reusable;

import static org.junit.Assert.assertTrue;

import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.itpm.synchronizer.SynchronizerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Makes handling the synchronizer in integration tests easy. Listens to the synchronizer as long as it is active and
 * aborts the synchronization on time-out to avoid never-ending integration tests.
 */
public class SyncListener {
    private int timeoutMinutes = 10;
    private static final Logger LOGGER = LoggerFactory.getLogger(SyncListener.class);
    private volatile boolean status;
    private volatile int cycleNo;
    private volatile String msg;
    private SynchronizerService synchronizer;

    public SyncListener() {
        status = true;
        cycleNo = 0;
        msg = "";
    }

    /**
     * Listen to an instance of the synchronizer until the synchronizer is not alive any more.
     * 
     * @param synchronizer
     * @throws StreamException
     */
    public void listen(SynchronizerService synchronizer) throws StreamException {
        listen(synchronizer, timeoutMinutes);
        this.synchronizer = synchronizer;
    }

    public void listen(SynchronizerService synchronizer, final int timeout) throws StreamException {
        timeoutMinutes = timeout;
        int ms = 0;
        // enter first time because sync may not be alive yet.
        while (ms == 0 || synchronizer.isAlive()) {
            try {
                LOGGER.trace("Sleep some time while synchronizer is collecting...");
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                synchronizer.stop();
                break;
            }
            ms += 20000;
            if (ms >= (timeoutMinutes * 1000 * 60)) {
                throw new StreamException(String.format(
                        "Timeout: Synchronizer action exceeded %d minutes (at cycle %d).", timeoutMinutes, cycleNo));
            }
        }
    }

    /**
     * @return false if status is not ok.
     */
    public boolean status() {
        return status;
    }

    /**
     * set the status to false if an exception happened or anything else.
     * 
     * @param status
     * @param msg
     */
    public void setStatus(boolean status, String msg) {
        this.status = status;
        this.msg = msg;
        if (!status) {
            if (synchronizer != null) {
                synchronizer.stop();
            }
        }
    }

    public void setCycleNo(int cycleNo) {
        this.cycleNo = cycleNo;
    }

    public int getCycleNo() {
        return cycleNo;
    }

    public String getMsg() {
        return msg;
    }

    public void assertIfError() {
        assertTrue(msg, status);
    }

}
