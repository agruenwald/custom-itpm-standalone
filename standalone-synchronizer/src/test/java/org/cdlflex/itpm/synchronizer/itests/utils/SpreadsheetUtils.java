package org.cdlflex.itpm.synchronizer.itests.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.domain.spreadsheet.SpreadsheetDomain;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetRow;
import org.cdlflex.itpm.synchronizer.standalone.CellPosition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utils to ease Spreadsheet domain service access and comparisons.
 */
public class SpreadsheetUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(SpreadsheetUtils.class);
    private static final String DEFAULT_DELIMITER = ";";

    private List<DataSourceConfig> activeConfigs;
    private SpreadsheetDomain[] services;

    public SpreadsheetUtils(List<DataSourceConfig> activeConfigs, SpreadsheetDomain... services) {
        this.activeConfigs = activeConfigs;
        this.services = services;
    }

    /**
     * Get a list of spreadsheet rows of a specific service.
     * 
     * @param spreadsheetService the service (spreadsheet)
     * @param activeConfigs a list of data source configurations to be tested against. A matching data source
     *        configuration will automatically be assigned to the spreadsheet service.
     * @return a list of rows.
     * @throws StreamException is thrown if streaming of rows fails.
     */
    public static List<SpreadsheetRow> getSpreadsheetRows(SpreadsheetDomain spreadsheetService,
        List<DataSourceConfig> activeConfigs) throws StreamException {
        Iterator<SpreadsheetRow> iterator =
            spreadsheetService.retrieveSpreadsheet(
                    TestHelper.attachConfigToService(spreadsheetService, activeConfigs)).iterator();
        List<SpreadsheetRow> rows = new ArrayList<SpreadsheetRow>();
        while (iterator.hasNext()) {
            rows.add(iterator.next());
        }
        return rows;
    }

    /**
     * Compare the content of different spreadsheet domain services.
     * 
     * @param activeConfigs the active configurations
     * @param domainservices the services, at least two
     * @throws StreamException is thrown if streaming of rows fails.
     * @return true if comparison was fine.
     */
    public static boolean compareRowContent(List<DataSourceConfig> activeConfigs, SpreadsheetDomain... domainservices)
        throws StreamException {
        boolean hasCleanRecord = true;
        List<List<SpreadsheetRow>> rowList = new ArrayList<List<SpreadsheetRow>>();
        for (SpreadsheetDomain service : domainservices) {
            List<SpreadsheetRow> list = SpreadsheetUtils.getSpreadsheetRows(service, activeConfigs);
            rowList.add(list);
        }

        for (int x = 0; x < rowList.get(0).size(); x++) {
            // iterate over the rows of the first service
            for (int r = 0; r < rowList.get(0).size(); r++) {
                // take care - cells start at index 1!
                for (int c = 1; c <= rowList.get(0).get(r).getCells().size(); c++) {
                    // compare with cells of the other services
                    for (int os = 1; os < rowList.size(); os++) {
                        String val1 = "";
                        String val2 = "";
                        try {
                            val1 = rowList.get(0).get(r).getCell(c).getValue().trim();
                            val2 = rowList.get(os).get(r).getCell(c).getValue().trim();
                            hasCleanRecord = stringComp(val1, val2, new CellPosition(r, c));
                        } catch (IndexOutOfBoundsException e) {
                            LOGGER.error(String.format("Comparison between %s and %s failed: %s. "
                                + "The index was [row %d, col %d], the values were %s and %s "
                                + "(may be the second has not been tried any more).",
                                    domainservices[0].getInstanceId(), domainservices[os].getInstanceId(),
                                    e.getMessage(), r, c, val1, val2));
                            throw e;
                        }
                    }
                }
            }
        }
        return hasCleanRecord;
    }

    /**
     * split a string by a delimiter (DEFAULT_DELIMITER).
     * 
     * @param str the string
     * @return a list of split strings.
     */
    public static List<String> delimSplit(String str) {
        List<String> a = Arrays.asList(str.split(DEFAULT_DELIMITER));
        List<String> b = new ArrayList<String>();
        for (String aa : a) {
            aa.trim().toLowerCase();
            b.add(aa);
        }
        return b;
    }

    private static boolean stringComp(String val1, String val2, CellPosition cellPos) {
        val1 = val1.trim().toLowerCase();
        val2 = val2.trim().toLowerCase();
        if (!val1.equals(val2)) {
            LOGGER.warn(String.format("Missmatch between spreadsheet services: %s != %s. At [row,col=%d,%d]", val1,
                    val2, cellPos.getRow(), cellPos.getCol()));
            return false;
        } else {
            return true;
        }
    }

    private static boolean refComp(String val1, String val2) {
        List<String> cleanedValues1 = delimSplit(val1);
        List<String> cleanedValues2 = delimSplit(val2);
        boolean cleanRecord = false;
        if (cleanedValues1.size() == cleanedValues2.size()) {
            cleanRecord = true;
            boolean hasFound = false;
            for (int i = 0; i < cleanedValues1.size(); i++) {
                String cleanedVal1 = cleanedValues1.get(i);
                if (cleanedValues2.contains(cleanedVal1)) {
                    hasFound = true;
                    break;
                }
            }
            cleanRecord = hasFound;
        }
        return cleanRecord;
    }

    /**
     * Check the cell content.
     * 
     * @param row the row number.
     * @param col the column number.
     * @param value the value.
     * @return true if the value in the cell corresponds with the value the parameter.
     * @throws SpreadsheetException
     * @throws StreamException is thrown if streaming of rows fails.
     */
    public boolean checkCellContent(int row, int col, String value) throws StreamException {
        return checkCellContent(row, col, value, false);
    }

    /**
     * Check if all spreadsheet services have a specific value in a cell.
     * 
     * @param row the row number to be matched >= 1 relative to the configuration.
     * @param col the col number in the spreadsheet starting at 1 relative to the configuration.
     * @param value the string value which will be compared
     * @param refList if true then the value is treated as a reference list with ";" as a separator during comparison
     * @return true if all values are equal.
     * @throws StreamException is thrown if streaming of rows fails.
     */
    public boolean checkCellContent(int row, int col, String value, boolean refList) throws StreamException {
        for (SpreadsheetDomain service : services) {
            List<SpreadsheetRow> list = SpreadsheetUtils.getSpreadsheetRows(service, activeConfigs);
            String valRec = list.get(row - 1).getCell(col).getValue();
            return refList ? refComp(value, valRec) : stringComp(value, valRec, new CellPosition(row, col));
        }

        return false;
    }
}
