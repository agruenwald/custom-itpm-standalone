package org.cdlflex.itpm.synchronizer.itests;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.conceptdomain.model.ConceptException;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetException;
import org.cdlflex.itpm.synchronizer.SynchronizerService;
import org.cdlflex.itpm.synchronizer.itests.setup.AbstractITestPattern;
import org.cdlflex.itpm.synchronizer.itests.setup.TestSetupSceleton;
import org.cdlflex.itpm.synchronizer.itests.utils.TestHelper;
import org.cdlflex.mdd.sembase.MDDException;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test if the error statements lead to a correct test result. Ensure that assertion errors are correctly delivered even
 * when using threads.
 */
public class Test001AssertionBubbling extends TestSetupSceleton implements AbstractITestPattern {
    private static final Logger LOGGER = LoggerFactory.getLogger(Test001AssertionBubbling.class);
    private List<DataSourceConfig> activeConfigs;

    @Before
    @Override
    public void modifyConfigForTest() throws IllegalAccessError, MDDException, StreamException, ConceptException {
        boolean ignoreTests = isIgnoreTests();
        org.junit.Assume.assumeTrue(!ignoreTests);
        List<String> wantedServices = Arrays.asList(new String[] {});
        activeConfigs = deactivateAllConfigsBut(wantedServices);
        TestHelper.checkActivatedConfigs(activeConfigs, wantedServices, 0);
        LOGGER.info(String.format("Activated configs: %d.", activeConfigs.size()));
        modifyServiceContentForTest();
    }

    @Override
    public void modifyServiceContentForTest() throws MDDException, StreamException, ConceptException {
    }

    @Override
    public void preTest() throws MDDException, SpreadsheetException, StreamException, ConceptException {
    }

    @Test
    public void iTest() throws Exception {
        preTest();
        SynchronizerService synchronizer = getSyncService();
        synchronizer.setSyncCycles(2);
        synchronizer.start();
        getSyncListener().listen(synchronizer, 10);
        try {
            getSyncListener().assertIfError();
            assertTrue(false);
        } catch (AssertionError fail) {
            assertTrue(true);
        }
    }

    @Override
    public boolean recurringTestComparison() throws StreamException {
        // trigger exception and check if the test fails
        assertTrue("No way", false);
        return true;
    }
}
