package org.cdlflex.itpm.synchronizer.itests;

import java.util.Arrays;
import java.util.List;

import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.conceptdomain.model.ConceptException;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.domain.spreadsheet.SpreadsheetDomain;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetException;
import org.cdlflex.itpm.synchronizer.itests.reusable.RequirementComp;
import org.cdlflex.itpm.synchronizer.itests.setup.AbstractITestPattern;
import org.cdlflex.itpm.synchronizer.itests.setup.TestSetupSceleton;
import org.cdlflex.itpm.synchronizer.itests.utils.TestHelper;
import org.cdlflex.itpm.synchronizer.standalone.ServiceFactory;
import org.cdlflex.mdd.sembase.MDDException;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This integration test only checks if Excel is correctly filled that is why the test equals: Is the test data in the
 * test environment correctly provided before the synchronization actually takes place?
 */
public class Test01RequirementSyncExcelStartup extends TestSetupSceleton implements AbstractITestPattern {
    private static final Logger LOGGER = LoggerFactory.getLogger(Test01RequirementSyncExcelStartup.class);
    private SpreadsheetDomain excelService;
    private List<DataSourceConfig> activeConfigs;

    @Before
    @Override
    public void modifyConfigForTest() throws IllegalAccessError, MDDException, StreamException, ConceptException {
        boolean ignoreTests = isIgnoreTests();
        org.junit.Assume.assumeTrue(!ignoreTests);
        activeConfigs = deactivateAllConfigsBut(Arrays.asList(new String[] { "RequirementsMSExcel" }));
        if (activeConfigs.size() != 1) {
            throw new MDDException(String.format(
                    "Excel Requirements service not identified correctly (%d active configs found).",
                    activeConfigs.size()));
        }
        LOGGER.info(String.format("Activated configs: %d.", activeConfigs.size()));
        modifyServiceContentForTest();
    }

    @Override
    public void modifyServiceContentForTest() throws MDDException, StreamException, ConceptException {
        // find domain services and attach appropriate configurations for execution
        excelService = (SpreadsheetDomain) ServiceFactory.findService("spreadsheet", "excel");
        TestHelper.attachConfigToService(excelService, activeConfigs);
    }

    @Override
    public void preTest() throws MDDException, SpreadsheetException, StreamException, ConceptException {
        // nothing to do here
    }

    @Override
    @Test
    public void iTest() throws Exception {
        preTest();
        RequirementComp.defaultReqSpreadsheets(activeConfigs, getDaoFactory(), getItpmCon(), excelService);
    }

    @Override
    public boolean recurringTestComparison() throws StreamException {
        return true;
    }

    @Override
    public boolean callSyncCycleCompleted(int cycleNo) {
        // not in use - override default action
        return true;
    }
}
