package org.cdlflex.itpm.synchronizer.itests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.cdlflex.conceptdomain.concept.sync.StreamException;
import org.cdlflex.conceptdomain.model.ConceptException;
import org.cdlflex.config.generated.model.DataSourceConfig;
import org.cdlflex.domain.spreadsheet.SpreadsheetDomain;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetException;
import org.cdlflex.domain.spreadsheet.model.SpreadsheetRow;
import org.cdlflex.itpm.generated.model.Issue;
import org.cdlflex.itpm.generated.model.Project;
import org.cdlflex.itpm.generated.model.Requirement;
import org.cdlflex.itpm.generated.model.Stakeholder;
import org.cdlflex.itpm.synchronizer.SynchronizerService;
import org.cdlflex.itpm.synchronizer.itests.reusable.IssueComp;
import org.cdlflex.itpm.synchronizer.itests.reusable.RequirementComp;
import org.cdlflex.itpm.synchronizer.itests.setup.AbstractITestPattern;
import org.cdlflex.itpm.synchronizer.itests.setup.TestSetupSceleton;
import org.cdlflex.itpm.synchronizer.itests.utils.TestHelper;
import org.cdlflex.itpm.synchronizer.standalone.ServiceFactory;
import org.cdlflex.mdd.sembase.MDDException;
import org.junit.Before;
import org.junit.Test;
import org.openengsb.domain.issue.IssueDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Integration test for the services MSExcel, Google Spreadsheets, Jira Issues, and Redmine. Test with both issue
 * services.
 */
public class Test04RequirementSyncPushIssuesToRedmineToo extends TestSetupSceleton implements AbstractITestPattern {
    private static final Logger LOGGER = LoggerFactory.getLogger(Test04RequirementSyncPushIssuesToRedmineToo.class);
    private static final String DEFAULT_PROJECT = "itpm";
    private SpreadsheetDomain excelService;
    private SpreadsheetDomain googleService;
    private IssueDomain jiraService;
    private IssueDomain redmineService;
    private List<DataSourceConfig> activeConfigs;

    @Before
    @Override
    public void modifyConfigForTest() throws IllegalAccessError, MDDException, StreamException, ConceptException {
        boolean ignoreTests = isIgnoreTests();
        org.junit.Assume.assumeTrue(!ignoreTests);
        List<String> wantedServices =
            Arrays.asList(new String[] { "Requirements", "Jira", "Redmine at Digital Ocean" });
        activeConfigs = deactivateAllConfigsBut(wantedServices);
        TestHelper.checkActivatedConfigs(activeConfigs, wantedServices, 4);
        LOGGER.info(String.format("Activated configs: %d.", activeConfigs.size()));
        modifyServiceContentForTest();
    }

    @Override
    public void modifyServiceContentForTest() throws MDDException, StreamException, ConceptException {
        // find domain services and attach appropriate configurations for execution
        excelService = (SpreadsheetDomain) ServiceFactory.findService("spreadsheet", "excel");
        googleService = (SpreadsheetDomain) ServiceFactory.findService("spreadsheet", "google");
        jiraService = (IssueDomain) ServiceFactory.findService("issue", "jira");
        redmineService = (IssueDomain) ServiceFactory.findService("issue", "redmine");
        TestHelper.attachConfigToService(excelService, activeConfigs);
        TestHelper.attachConfigToService(googleService, activeConfigs);
        TestHelper.attachConfigToService(jiraService, activeConfigs);
        TestHelper.attachConfigToService(redmineService, activeConfigs);

        // remove everything from Google
        Iterator<SpreadsheetRow> rows =
            googleService.retrieveSpreadsheet(TestHelper.attachConfigToService(googleService, activeConfigs))
                    .iterator();
        while (rows.hasNext()) {
            rows.next();
            rows.remove();
        }
        // remove everything from Jira
        List<Project> jiraProjects = jiraService.getProjects("");
        for (Project jp : jiraProjects) {
            jiraService.removeProject(jp);
        }
        // remove everything from Redmine
        List<Project> redmineProjects = redmineService.getProjects("");
        for (Project jp : redmineProjects) {
            redmineService.removeProject(jp);
        }
    }

    @Override
    public void preTest() throws MDDException, SpreadsheetException, StreamException, ConceptException {
        // check central store
        assertEquals(0, sizeChecker(Issue.class));
        assertEquals(0, sizeChecker(Requirement.class));
        assertEquals(0, sizeChecker(Stakeholder.class));

        // check Jira
        if (null != jiraService.getProject(DEFAULT_PROJECT)) {
            throw new StreamException(String.format("Default project %s not cleaned in Jira!", DEFAULT_PROJECT));
        }

        // check Redmine
        if (null != redmineService.getProject(DEFAULT_PROJECT)) {
            throw new StreamException(String.format("Default project %s not cleaned in Redmine!", DEFAULT_PROJECT));
        }
    }

    @Test
    public void iTest() throws Exception {
        preTest();
        SynchronizerService synchronizer = getSyncService();
        synchronizer.setSyncCycles(3);
        synchronizer.start();
        getSyncListener().listen(synchronizer, 10);
        getSyncListener().assertIfError();
    }

    @Override
    public boolean recurringTestComparison() throws StreamException, ConceptException, MDDException {
        try {
            assertEquals(7, jiraService.getIssues(DEFAULT_PROJECT, "").size());
            assertEquals(7, redmineService.getIssues(DEFAULT_PROJECT, "").size());
            // keep requirements tests because there may be a backstream from Jira/Redmine after update
            RequirementComp.defaultReqSpreadsheets(activeConfigs, getDaoFactory(), getItpmCon(), excelService,
                    googleService);
            RequirementComp.reqAfterSyncSemStore(activeConfigs, getDaoFactory(), getItpmCon());
            IssueComp.issueComparisonDefaultReqs(activeConfigs, getDaoFactory(), getItpmCon(), DEFAULT_PROJECT,
                    jiraService, redmineService);
            return true;
        } catch (AssertionError e) {
            fail(e.getMessage());
            return false;
        }
    }
}
