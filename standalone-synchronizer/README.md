# CDL Flex :: Custom ITPM :: Standalone :: Synchronizer (Leightweight without EngSb)
Run the synchronizer as a background process to enable automated synchronization
between ITPM tools. The synchronization can be monitored and controlled by the
WEB UI.

## Execution
mvn exec:java -Dexec.executable="maven" -Dkaraf.home="/Users/andreas_gruenwald/git/custom-itpm/assembly/src/main/resources"

## Further Information:

More documentation can be found at Confluence under [Custom ITPM](http://cdl-ind.ifs.tuwien.ac.at/confluence/display/UC/Custom+ITPM 
"Custom ITPM").